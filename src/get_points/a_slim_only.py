"""
a_slim_only.py
=============================================
This script just runs through a folder full of netcdfs, and takes out the
relevant areas of the tripolar fields that correspond to the study area.
As the x, y coordinates are cryptic in meaning, I had to find which ones were
reasonable to take through graphing them on a lat-lon grid, and conservatively
guessing what the minimum grid that I might need would be:
y=slice(2160, 2600),  x=slice(2160, 3025)

http://xarray.pydata.org/en/stable/examples/multidimensional-coords.html
"""
import os
import xarray as xr
import matplotlib.pyplot as plt
import cmocean.cm as cmo
import cartopy.crs as ccrs
from src.utilities import my_wrappers as mwr
import config as cfg


def _return_ss(coastline='america'):
    """
    :param coastline: 'america' or 'east-america'.
    :return: ss - a lol with the box of indices to take.
    """
    if coastline == 'east-asia':
        ss = [[380, 980],
              [2200, 2700]]
    else:   # otherwise, give north american coastline.
        ss = [[2160, 3025],
              [2180, 2670]]
    return ss


@mwr.timeit
def slim_file(input_dir=cfg.toy_loc,
              file_name='nemo_ba130o_1d_20050801-20050901_grid-T.nc',
              output_dir=cfg.new_toy_loc,
              relabel=False,
              coastline='america'):
    """
    A quick function that does nothing bus slim a netcdf into the
    region that I could plausibly care about on the ORCA12 grid.
    :param input_dir: where is the input file stored?
    :param file_name: what was it called?
    :param output_dir: where should it go?
    :param relabel: relabel indices from i, j to x, y.
    :param coastline: america or east asia.
    :return: void - the file is saved.
    """

    array = xr.open_dataset(input_dir + file_name)  # Open
    if relabel:
        array = array.rename(i='x', j='y')
    # Initial take useful
    print(array)

    # Final version chosen by iterative Panolpy graphs of x and y coordinates.
    # 400 to 880 in x
    # 2150 to 2800 in y

    ss = _return_ss(coastline=coastline)

    print(ss)

    small_slice = array.isel(y=slice(ss[1][0], ss[1][1]), x=slice(ss[0][0], ss[0][1]))

    # original version inexactly chosen from Panolpy graphs of the x, y coordinates
    # small_slice = array.isel(y=slice(2160, 2600),  x=slice(2160, 3025))  # Take useful

    small_slice.to_netcdf(output_dir + file_name)  # Save

    del array

# These tests all ran well:
# slim_file(file_name='nemo_ba130o_1d_20050801-20050901_grid-T.nc') ## 8.94s
# slim_file(file_name='nemo_ba130o_1d_20050801-20050901_grid-U.nc') ## 3.71s
# slim_file(file_name='nemo_ba130o_1d_20050801-20050901_grid-V.nc') ## 3.73s


@mwr.timeit
def slim_bath(input_path=cfg.bath_path,
              output_path=cfg.slim_bath_path,
              coastline='america'):
    """
    As above, but with a slightly different format.
    :param input_path: the full file path for the original version.
    :param output_path: the full file path for the output version.
    :param coastline: 'america' or 'east-asia'.
    """
    ss = _return_ss(coastline=coastline)  # ss - a lol to mark the box to pick.
    array = xr.open_dataset(input_path)   # Open the netcdf (this is the lazy load way).
    small_slice = array.Bathymetry.isel(y=slice(ss[1][0], ss[1][1]),
                                        x=slice(ss[0][0], ss[0][1]))
    small_slice.to_netcdf(output_path)    # Save the netcdf file.
    ax = plt.axes(projection=ccrs.PlateCarree())
    small_slice.plot.pcolormesh(ax=ax, transform=ccrs.PlateCarree(), cmap=cmo.deep,
                                x='nav_lon', y='nav_lat', add_colorbar=True,
                                cbar_kwargs={"orientation": "horizontal",
                                             'label': 'Depth (m)'},
                                vmin=0, vmax=200)

    ax.coastlines()
    # ax.set_ylim([0, 90])
    # small_slice.plot() #  This puts the colormap the wrong way round.
    plt.show() # let's check that this works.


@mwr.timeit
def run_through_folder(input_dir,
                       output_dir,
                       relabel=False,
                       coastline='america'):
    """
    Scan through a folder, skin the files to get the kernel of potentially useful data,
    and save that to another folder.

    :param input_dir: The directory to scan for .nc files.
    :param output_dir: The directory to put the smaller .nc files in.
    :param relabel: if the netcdf inidices are i and j they will need to be relabelled to
                    x and y.
    :param coastline: 'america' or 'east-asia'.

    :return: void - the files were saved.
    """
    for file in os.listdir(input_dir):  # look at all files in repository
        if file.endswith(".nc"):  # choose only .nc files
            slim_file(input_dir=input_dir,
                      file_name=file,
                      output_dir=output_dir,
                      relabel=relabel,
                      coastline=coastline)


# slim_file()
# run_through_folder(cfg.new_toy_loc, cfg.test_loc)  # The local test repositories on my laptop.
# run_through_folder(cfg.gws_original, cfg.gws_small)  # The repositories on the JASMIN HPC
