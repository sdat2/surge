#!/usr/bin/env python3
"""
A set of helpful miscillanious functions for the other scripts to use.

"""
import numpy as np


def sort_list_of_lists(list_of_lists, sorting_index=0, ignore_list=[0]):
    """
    :param list_of_lists: the input
    :param sorting_index:
    :param ignore_list:
    :return:
    """
    ignored_list = []  # this is probably terrible naming practice

    for index in ignore_list:
        ignored_list.append(list_of_lists.pop(index))

    sorted_lol = sorted(list_of_lists, key=lambda x: x[sorting_index])

    for index in range(len(ignore_list)):
        sorted_lol.insert(ignore_list[index], ignored_list[index])

    return sorted_lol


def mean_and_std(a_list):
    """
    Uses the n-1 definition
    :param a_list: a list of elements of the sample of a distribution
    :return: the mean and standard deviation of the elements of the list
    """
    length = len(a_list)
    total = 0
    sq_total = 0
    for ele in a_list:
        total += ele
        sq_total += ele**2
    mean = total/length
    if length != 0:
        std_dev = np.sqrt((1 / (length - 1))*(sq_total - length * mean**2))
    else:
        std_dev = 0
    return mean, std_dev


def profile_stats_by_label(gmm_profile_list):
    """
    Work out the stats of of an average profile of a class.
    i.e the mean, std_dev, skewness at each level.
    :param gmm_profile_list: the standard list of profiles
    (item 0 is the meta-statistics of that gmm run month, not a profile)
    :return: a dictionary containing the relevant information.
    """
    results_d = {}

    return results_d


def unique_elements(list):
    """
    looks for all the unique elements in a list, returns it sorted
    :return:
    """
    uniq_list = []

    for companies in list:
        if companies not in uniq_list:
            uniq_list.append(companies)

    unique_npa = np.array(uniq_list)
    print('%i unique components' % np.shape(unique_npa))

    unique_npa = np.sort(unique_npa)
    return unique_npa


def sort_keys_by_avg_lat(data_d):
    """

    :param data_d:
    :return:
    """
    key_list = []
    for key in data_d["y_values_d"]:
        key_list.append(key)

    mean_lat_npa = np.zeros(len(key_list), dtype='float32')

    for i in range(len(key_list)):
        temp_no = 0
        number_in_key = len(data_d["y_values_d"][key_list[i]])
        for j in range(number_in_key):
            temp_no += data_d["y_values_d"][key_list[i]][j]
        mean_lat_npa[i] = temp_no/number_in_key

    combined_list = []

    for i in range(len(mean_lat_npa)):
        combined_list.append([key_list[i], mean_lat_npa[i]])

    sorted_combined = sort_list_of_lists(combined_list, sorting_index=1)

    print(sorted_combined)

    sorted_key_list = []

    for i in range(len(sorted_combined)):
        sorted_key_list.append(sorted_combined[i][0])

    return sorted_key_list
