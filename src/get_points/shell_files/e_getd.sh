#!/bin/bash

final_loc=/gws/nopw/j04/primavera1/open/sithom-bas/day_versions/
prefix=moose:/crum/mi-ba130/ond.nc.file/
name=nemo_ba130o_1d_20041101-20041201_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1d_20041201-20050101_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1d_20050101-20050201_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1d_20050201-20050301_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1d_20050301-20050401_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
exit
