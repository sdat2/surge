"""
a_mean_skew_kurt.py
===================
https://matplotlib.org/3.1.0/gallery/ticks_and_spines/
ticklabels_rotation.html#sphx-glr-gallery-ticks-and-spines-ticklabels-rotation-py
https://matplotlib.org/examples/api/colorbar_only.html
import scipy.stats as ss
moments_list[i][j, 3] = ss.kurtosis(field_npa_list[i][j, :])
print(ss.kurtosistest(field_npa[i, :]))
"""
import numpy as np
import scipy.stats as ss
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib
import cmocean.cm as cmo
import matplotlib.colors as colors
import src.plotting.my_plotting_style as mps
import src.utilities.my_wrappers as mwr
import config as cfg
import src.plotting.custom_ticker as ctk
import src.utilities.my_io_functions as mif
# import src.utilities.gaussian_regression_functions as grf
import src.plotting.custom_ticker as ctk
mps.mps_defaults()


@mwr.timeit
def get_xarray_grid(name_list, field='zos', coastline='america'):  # zos    # tauuo     # tauvo
    """
    Compute the kurtosis (Fisher or Pearson) of a dataset.
    Kurtosis is the fourth central moment divided by the square of the variance.
    If Fisher’s definition is used, then 3.0 is subtracted from the result to give 0.0
     for a normal distribution
    I think the one where the natural result for a normal distribution is 0 is more intuitive.

    :param name_list: a list of xarray locations to load up
    :param field: which field do you want to process?
    :return:
    """
    xr_ob_list = []
    field_npa_list = []
    moments_list = []

    for i in range(len(name_list)):
        xr_ob_list.append(xr.load_dataset(name_list[i]))
        if coastline == 'vin-china':
            field_npa_list.append(xr_ob_list[i].variables[field].values.T)
            # TODO try to understand why I had to do this, and whether it
            # does anything objectionable such as reversing the coast direction.
        elif coastline == 'america':
            field_npa_list.append(xr_ob_list[i].variables[field].values)
        # print(field_npa_list[i])
        # print(np.shape(field_npa_list))
        no_points = np.shape(field_npa_list[0])[0]
        moments_list.append(np.zeros([no_points, 4]))
        for j in range(no_points):
            moments_list[i][j, 0] = np.mean(field_npa_list[i][j, :])
            moments_list[i][j, 1] = np.std(field_npa_list[i][j, :])
            moments_list[i][j, 2] = ss.skew(field_npa_list[i][j, :])
            moments_list[i][j, 3] = ss.kurtosis(field_npa_list[i][j, :])
        # print(ss.kurtosistest(field_npa[i, :]))
        #  see e.g. F. J. Anscombe, W. J. Glynn,
        # “Distribution of the kurtosis statistic b2 for normal samples”,
        #  Biometrika, vol. 70, pp. 227-234, 1983.
        # print(moments_list[i

    print('Number Points ', no_points)

    make_stats_plots(moments_list, no_points, variable_name=field, coastline=coastline)
    # make_single_place_plots(field_npa_list, field, coastline=coastline)
    # plot_covariance_matrices(field_npa_list, field=field, coastline=coastline)
    #plot_mean_of_points_by_time(field_npa_list, field)


@mwr.timeit
def plot_mean_of_points_by_time(field_npa_list, field):
    new_field_array = np.zeros([np.shape(field_npa_list[0])[0],
                                np.shape(field_npa_list[0])[1]*2],
                               dtype='float32')
    for i, item in enumerate(field_npa_list[0][:, 0]):
        year_length = np.shape(field_npa_list[0])[1]
        # print('year_length ', year_length)
        new_field_array[i, :year_length] = field_npa_list[0][i, :]
        new_field_array[i, year_length:] = field_npa_list[1][i, :]
    average_field_array = np.zeros(np.shape(field_npa_list[0])[1]*2, dtype='float32')
    for i in range(np.shape(average_field_array)[0]):
        average_field_array[i] = np.mean(new_field_array[:, i])
    fig, axs = plt.subplots(1)
    fig.set_size_inches([10, 6.5])
    # year_length = int(np.shape(average_field_array)[0]/2)
    plt.plot(range(year_length*2),
             average_field_array,
             color='#CB4154',
             label='Mean SSH for All Points',
             alpha=0.5)
    plt.xlim(0, year_length*2)
    plt.xticks([0, year_length/2,
                year_length,
                year_length*(3/2)],
               ['Jan 2004', 'July 2004',
                'Jan 2005', 'July 2005'])

    y_label(field)
    plt.legend()
    mps.save_fig_twice(cfg.stats_plot_sans_suffix + "_5")
    plt.clf()

    def plot_w_gp():
        grf.gaussian_process_regression(np.asarray(range(np.shape(average_field_array)[0])),
                                        average_field_array)

        plt.plot(np.asarray(range(np.shape(average_field_array)[0])),
                 average_field_array,
                 color='#CB4154',
                 label='Real Mean $\eta$ Values')

        plt.legend()
        plt.xlabel('Hours Since Start of 2004')
        plt.ylabel('Mean $\eta$ for All Points')

        mps.save_fig_twice('ahhhhh6')

    plot_w_gp()


@mwr.timeit
def plot_covariance_matrices(field_npa_list, field='zos',
                             coastline='america'):
    print('shape ', np.shape(field_npa_list[0]))
    # X = np.array([[0.1, 0.3, 0.4, 0.8, 0.9],
    #               [3.2, 2.4, 2.4, 0.1, 5.5],
    #               [10., 8.2, 4.3, 2.6, 0.9]
    #               ])
    # print('shape X ', np.shape(X))
    array = np.cov(field_npa_list[0])
    tick_list = ctk.return_place_tick_list(coastline=coastline)
    labels = []
    locs = []

    for i in range(len(tick_list)):
        labels.append(tick_list[i][0])
        locs.append(tick_list[i][1])
    fig, axs = plt.subplots(2, 2)

    for i in range(len(field_npa_list)):
        array = np.cov(field_npa_list[i])
        vmin = -0.02
        vmax = 0.02
        im = axs[0, i].imshow(array, aspect='auto',
                              cmap=cmo.balance,
                              norm=colors.Normalize(vmin=vmin,
                                                    vmax=vmax))
        axs[0, i].invert_yaxis()
        axs[0, i].set_xticks(np.asarray(locs))
        axs[0, i].set_xticklabels(np.asarray(labels), fontsize=4)
        axs[0, i].set_yticks(np.asarray(locs))
        axs[0, i].set_yticklabels(np.asarray(labels), fontsize=4)
        plt.yticks(np.asarray(locs),
                   np.asarray(labels))
        array = np.corrcoef(field_npa_list[i])
        vmin = -1.0
        vmax = 1.0
        im = axs[1, i].imshow(array, aspect='auto',
                              cmap=cmo.balance,
                              norm=colors.Normalize(vmin=vmin,
                                                    vmax=vmax))
        axs[1, i].invert_yaxis()
        axs[1, i].set_xticks(np.asarray(locs))
        axs[1, i].set_xticklabels(np.asarray(labels), fontsize=4)
        axs[1, i].set_yticks(np.asarray(locs))
        axs[1, i].set_yticklabels(np.asarray(labels), fontsize=4)

    for i, label in enumerate(('A', 'B', 'C', 'D')):
        if False:  # i == 0 or i == 1:
            font_color = 'white'
        else:
            font_color = 'black'
        axs.ravel()[i].text(0.05, 0.95, label,
                            color=font_color,
                            transform=axs.ravel()[i].transAxes,
                            fontsize=13, fontweight='bold', va='top')
    mps.defined_size(fig, size='dcsq')
    if coastline=='america':
        mps.save_fig_twice(cfg.plots_loc + field +  '_corr_cov')
    elif coastline == "vin-china":
        mps.save_fig_twice('plots/vc_stats/' + field +  '_corr_cov')
    del fig

    def plot_colorbars():
        fig, axs = plt.subplots(2)
        cmap = cmo.balance
        norm = matplotlib.colors.Normalize(vmin=-0.02, vmax=0.02)
        cb1 = matplotlib.colorbar.ColorbarBase(axs[0],
                                               cmap=cmap,
                                               norm=norm,
                                               orientation='vertical')
        cb1.set_label('Covariance')
        cmap = cmo.balance
        norm = matplotlib.colors.Normalize(vmin=-1, vmax=1)
        cb1 = matplotlib.colorbar.ColorbarBase(axs[1],
                                               cmap=cmap,
                                               norm=norm,
                                               orientation='vertical')
        cb1.set_label('Correlation')
        fig.set_size_inches([0.5, 6.5])
        mps.save_fig_twice(cfg.plots_loc + 'corr_cov_cbar')


@mwr.timeit
def make_single_place_plots(field_npa_list, field, coastline='america'):

    print('shape', np.shape(field_npa_list[0]))
    tick_list = ctk.return_place_tick_list(coastline=coastline)
    new_tick_list = []
    for item in tick_list:
        if item[0] != 'X':
            new_tick_list.append(item)
    # print(new_tick_list)
    new_field_array = np.zeros([len(new_tick_list),
                                np.shape(field_npa_list[0])[1]*2],
                               dtype='float32')
    for i, item in enumerate(new_tick_list):
        point_chosen = new_tick_list[i][1]
        # print(point_chosen)
        # print('i', i)
        # print('item', item)
        year_length = np.shape(field_npa_list[0])[1]
        # print('year_length', year_length)
        new_field_array[i, :year_length] = field_npa_list[0][point_chosen, :]
        new_field_array[i, year_length:] = field_npa_list[1][point_chosen, :]
    # print(new_field_array)
    color_list = mps.replacement_color_list(len(new_tick_list))
    fig, axs = plt.subplots(1)
    fig.set_size_inches([10, 6.5])
    for i in range(len(color_list)):
        plt.plot(range(year_length*2), new_field_array[i, :],
                 color=color_list[i], label=new_tick_list[i][0],
                 alpha=0.5)

    plt.xlim(0, year_length*2)
    plt.xticks([0, year_length/2, year_length, year_length*(3/2)],
               ['Jan 2004', 'July 2004', 'Jan 2005', 'July 2005'])
    y_label(field)
    plt.legend()
    # plt.show()
    if coastline == 'america':
        mps.save_fig_twice(cfg.stats_plot_sans_suffix + "_2")
    if coastline == 'vin-china':
        mps.save_fig_twice('plots/vc_stats/' + field + '_individual')


def y_label(field):
    if field == 'zos':
        plt.ylabel(r'Sea Surface Height above Geoid, $\eta$, (m)')
    elif field == 'tauuo':
        plt.ylabel(r'Sea Surface Stress along U, $\tau_u$, (Pa)')
    elif field == 'tauvo':
        plt.ylabel(r'Sea Surface Stress along V, $\tau_v$, (Pa)')


@mwr.timeit
def make_stats_plots(moments_list, no_points,
                     variable_name='zos', coastline='america'):
    """
    :param moments_list:
    :param no_points:
    :param coastline:
    """
    unit = ''
    if variable_name == 'zos':
        unit = '$\;$(m)'
    elif variable_name in ['tauuo', 'tauvo']:
        unit = '$\;$(Pa)'

    y_label_list = ['Mean ' + unit, 'Std Dev '+ unit , 'Skew', 'Kurtosis']
    label_list = ['2004', '2005']
    color_list = mps.replacement_color_list(len(label_list))

    # fig, ax = plt.subplots

    fig, axs = plt.subplots(4, sharex=True)
    for i in range(len(label_list)):
        for j in range(np.shape(moments_list[i])[1]):
            axs[j].plot(range(np.shape(moments_list[i])[0]),
                        moments_list[i][:, j],
                        color=color_list[i],
                        label=label_list[i])
            axs[j].set_ylabel('Value of ' + y_label_list[j])

    for i in range(4):
        pearr = ss.pearsonr(moments_list[0][:, i], moments_list[1][:, i])
        print(i, pearr)
        axs[i].text(no_points*0.88, 0.7*(np.max(moments_list[0][:, i])
                                         - np.min(moments_list[0][:, i]))
                                    + np.min(moments_list[0][:, i]),
                    '$r_p$=' + "{:.3f}".format(pearr[0]),
                    fontsize=12)

    # lets find the difference betweeen the years.
    diff = moments_list[0][:, 0] - moments_list[1][:, 0]
    mean = np.mean(diff)
    std = np.std(diff)
    mif.write_csv_file([[mean, std]], variable_name + coastline+'.csv' )

    # fig = plt.gcf()
    fig = mps.defined_size(fig, size='double_column_square')
    mps.label_subplots(axs)
    # plt.draw()
    ctk.add_ticks(coastline=coastline)

    # plt.draw()
    # ax.set_xticks(locs)
    # ax.set_xticklabels(labels)
    # ax.grid()
    axs[3].set_xlim([0, no_points])
    axs[3].set_xlabel('Point Along Coast')
    plt.tight_layout()
    plt.legend(loc='lower right')

    if coastline == 'america':
        mps.save_fig_twice(cfg.stats_plot_sans_suffix + variable_name)
    elif coastline == 'vin-china':
        mps.save_fig_twice('plots/vc_stats/' + variable_name + '_moments')

    plt.clf()
    del fig

    fig, axs = plt.subplots(4)
    fig = mps.defined_size(fig, size='scl')

    for i in range(np.shape(moments_list[0])[1]):
        for j in range(len(moments_list)):
            axs[i].hist(moments_list[j][:, i],
                        bins=300,
                        alpha=0.5, density=True,
                        color=color_list[j],
                        label=label_list[j])
        axs[i].set_xlabel(y_label_list[i])
        axs[i].set_ylabel("Density")

    axs[3].legend()
    mps.label_subplots(axs, start_from=4)
    plt.tight_layout()

    if coastline == 'america':
        mps.save_fig_twice(cfg.stats_plot_sans_suffix + variable_name + "_1")
        for i in range(len(moments_list)):
            moments_list[i] = moments_list[i].tolist()
        if variable_name == 'zos':
            mif.write_json_object(moments_list, cfg.data_loc + 'moments_list.json')
    elif coastline == "vin-china":
        mps.save_fig_twice('plots/vc_stats/' + variable_name + '_moments_dist')
