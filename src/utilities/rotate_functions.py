import numpy as np


def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    Initially from https://stackoverflow.com/questions/6802577/rotation-of-3d-vector/25709323
    :param axis: the axis about which to rotate. [3 element list]
    :param theta: the angle (in radians) to rotate by
    :return:
    """
    axis = np.asarray(axis)
    axis = axis / np.sqrt(np.dot(axis, axis))
    a = np.cos(theta / 2.0)
    b, c, d = -axis * np.sin(theta / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return np.array([[aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
                     [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
                     [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc]])


def rotate_by_matrix(xs, ys, zs, rot_matrix):
    """
    Rotate the given set of values by the rotation matrix
    :param xs: x values list
    :param ys: y values list
    :param zs: z values list
    :param rot_matrix: the matrix to rotate by
    :return:
    """
    for i in range(len(xs)):
        vector2 = np.dot(rot_matrix, [xs[i], ys[i], zs[i]])
        xs[i] = vector2[0]
        ys[i] = vector2[1]
        zs[i] = vector2[2]

    return xs, ys, zs


def rotation_matrix_test():
    """

    :return: void
    """
    vector = [3, 5, 0]
    axis = [0, 0, 1]
    theta = np.pi
    matrix = rotation_matrix(axis, theta)
    vector2 = np.dot(matrix, vector)

    for i in range(len(vector2)):
        assert int(vector2[i]) == [-3, -5, 0][i]





def rotation_transform(xs, ys, zs,
                       degree_rot_angle=50,
                       degree_rot_angle2=0,
                       degree_rot_angle3=40):
    """

    :param xs: the list of x values
    :param ys: the list of y values
    :param zs: the list of z values
    :param degree_rot_angle: rotate around z axis
    :param degree_rot_angle2: rotate around x axis
    :param degree_rot_angle3: rotate around y axis
    :return:
    """

    assert len(xs) == len(ys)

    rad_rot_angle = degree_rot_angle / 180 * np.pi
    axis = [0, 0, 1]
    rot_matrix = rotation_matrix(axis, rad_rot_angle)

    rad_rot_angle = degree_rot_angle2 / 180 * np.pi
    axis = [1, 0, 0]
    rot_matrix2 = rotation_matrix(axis, rad_rot_angle)
    rot_matrix = np.dot(rot_matrix, rot_matrix2)

    rad_rot_angle = degree_rot_angle3 / 180 * np.pi
    axis = [0, 1, 0]
    rot_matrix3 = rotation_matrix(axis, rad_rot_angle)
    rot_matrix = np.dot(rot_matrix, rot_matrix3)

    xs, ys, zs = rotate_by_matrix(xs, ys, zs, rot_matrix)

    return xs, ys, zs, rot_matrix
