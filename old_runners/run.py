"""run.py - If we run everything from here it should all be simple to re-run :)
Less faffing around with terminal.

# conda install -c conda-forge pyfftw

"""
import src.utilities.my_wrappers as mwr
import config as cfg



def run_fft_time(coastline='america'):
    import src.f_fourier_transform as fft
    if coastline == 'america':
        item_l = [cfg.merged_2004_file_name, cfg.merged_2005_file_name]
    elif coastline == 'vin-china':
        item_l = [cfg.merged_vc_2004_file_name, cfg.merged_vc_2005_file_name]
    fft.get_xarray_grid([cfg.data_loc + item_l[0], cfg.data_loc + item_l[1]],
                        coastline=coastline)

    # for var_name in ["sowindsp", "tauuo", "tauvo", "zos"]:
    #    fft.run_all(cfg.data_loc + cfg.merged_2005_file_name, var_name)


# run_fft_time(coastline='vin-china')

# run_fft_time()


def param_test():
    import src.p_param_test as ppt
    ppt.test_params()


def print_xarray():
    import xarray as xr
    ds = xr.open_dataset(cfg.toy_loc + cfg.toy_T_file_name)
    print(ds)

# print_xarray()
# get_point_lists()


def katrina():
    import src.coastal_metrics.d_detailed_katrina_plot as dkp
    dkp.load_katrina()

# katrina()
