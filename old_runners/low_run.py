import src.utilities.my_wrappers as mwr
import config as cfg

def comp_low():
    import src.low_freq.comp_low_thermal as clt
    clt.coarsening_test()
    clt.coarsening_test(coastline='vin-china')

# comp_low()


def long_test():
    import src.low_freq.long_test as lt
    lt.test_long_load()
    lt.test_long_load(coastline='vin-china')

# long_test()

def lag_test():
    import src.low_freq.phase_vs_lag_test as pvl
    pvl.test()
    pvl.test(coastline='vin-china')


# lag_test()
