"""
j_efficiency_analysis.py
========================

"""
import numpy as np
import scipy.stats as ss
import matplotlib.pyplot as plt
import cmocean.cm as cmo
from sklearn import linear_model, metrics
import src.utilities.my_io_functions as mif
import src.utilities.my_wrappers as mwr
import src.plotting.custom_ticker as ctk
import src.plotting.my_plotting_style as mps
import bathymetry_integrate as bat
import config as cfg
mps.mps_defaults()


@mwr.timeit
def find_lr_angle(coastline='america'):
    save_prefix = mif.save_prefix(coastline=coastline)

    com_results_big = np.asarray(mif.read_json_object(cfg.data_loc
                                 + save_prefix + 'lin_reg_res.json'),
                                 dtype='float32')
    average_angle_array = np.asarray(mif.read_json_object('data/'+ save_prefix
                                     + 'smoothed_angle.json'), dtype='float32')

    # 1st index: point
    # 2nd index: 2004, 2005
    # 3rd index: MLR, Huber
    # 4nd index: intercept, coeff1, coeff2,  score_train, score_test
    no_points = np.shape(com_results_big)[0]
    point_list = range(no_points)

    angle_array = np.zeros([no_points, 2, 2], dtype='float32')
    for i in range(2):
        for j in range(2):
            angle_array[:, i, j] = np.mod(np.arctan2(com_results_big[:, i, j, 1],
                                          com_results_big[:, i, j, 2])
                                          - 3*np.pi/4, np.pi*2)

    def plot_angles():
        label_lol = [['2004 MLR', '2004 Huber'], ['2005 MLR', '2005 Huber']]
        color_lol = [['orange', 'red'], ['green', 'blue']]

        for i in range(2):
            for j in range(2):
                plt.plot(point_list, angle_array[:, i, j], color=color_lol[j][i], label=label_lol[i][j])

        plt.plot(point_list, average_angle_array[:, 15], color='black', label=r'Coast Bearing, $\sigma=15$')

        plt.ylabel('Angle (radians)')
        plt.legend()
        plt.xlim(0, no_points)
        ctk.add_ticks(coastline=coastline)
        mps.save_fig_twice(cfg.plots_loc + save_prefix + 'reg_angle')
        plt.show()
        plt.clf()

    def plot_correlation():

        print(np.shape(average_angle_array))
        print(average_angle_array)
        pearr_results = np.zeros([2, 2, 99], dtype='float32')
        rsquare_results = np.zeros([2, 2, 99], dtype='float32')

        for i in range(2):
            for j in range(2):
                for k in range(99):
                    pearr_results[i, j, k] = ss.pearsonr(angle_array[:, i, j],
                                                         average_angle_array[:, k])[0]
                    rsquare_results[i, j, k] = metrics.r2_score(angle_array[:, i, j],
                                                                average_angle_array[:, k])

        pearr_reshaped = np.zeros([4, 99], dtype='float32')
        rsquare_reshaped = np.zeros([4, 99], dtype='float32')

        pearr_reshaped[0:2, :] = pearr_results[0, :, :]
        pearr_reshaped[2:, :] = pearr_results[1, :, :]
        rsquare_reshaped[0:2, :] = rsquare_results[0, :, :]
        rsquare_reshaped[2:, :] = rsquare_results[1, :, :]

        plt.imshow(pearr_reshaped.T, cmap=cmo.balance, aspect='auto', vmin=-1, vmax=1)
        plt.xticks([0, 1, 2, 3], ['`4 L', '`4 H', '`5 L', '`5 H'])
        plt.gca().invert_yaxis()
        plt.ylabel('Smoothing Length $\sigma$')
        fig = plt.gcf()
        cbar = plt.colorbar()
        cbar.set_label('correlation')
        mps.defined_size(fig, size='scs')
        mps.save_fig_twice(cfg.plots_loc + save_prefix + 'reg_angle_correlate')
        plt.show()
        plt.clf()

        plt.imshow(rsquare_reshaped.T, cmap=cmo.amp, aspect='auto', #  vmin=0, vmax=1
                   )
        plt.xticks([0, 1, 2, 3], ['`4 L', '`4 H', '`5 L', '`5 H'])
        plt.gca().invert_yaxis()
        plt.ylabel('Smoothing Length $\sigma$')
        fig = plt.gcf()
        cbar = plt.colorbar()
        cbar.set_label('r$^2$')
        mps.defined_size(fig, size='scs')
        mps.save_fig_twice(cfg.plots_loc + save_prefix + 'reg_angle_rsquare')
        plt.show()

    plot_angles()
    plot_correlation()

    return angle_array


@mwr.timeit
def find_responsiveness_magnitude(coastline='america'):
    save_prefix = mif.save_prefix(coastline=coastline)


    com_results_big = np.asarray(mif.read_json_object(cfg.data_loc
                                                     + save_prefix
                                                     +'lin_reg_res.json'), dtype='float32')
    print('np.shape(com_results_big)', np.shape(com_results_big))
    # 1st index: point
    # 2nd index: 2004, 2005
    # 3rd index: MLR, Huber
    # 4nd index: intercept, coeff1, coeff2,  score_train, score_test
    no_points = np.shape(com_results_big)[0]
    magnitude_array = np.zeros([no_points, 2, 2], dtype='float32')
    adjusted_mag_array = np.zeros([no_points, 2, 2], dtype='float32')
    point_list = range(no_points)
    for i in range(2):
        for j in range(2):
            magnitude_array[:, i, j] = np.sqrt(np.square(com_results_big[:, i, j, 1])
                                               + np.square(com_results_big[:, i, j, 2]))
            adjusted_mag_array[:, i, j] = magnitude_array[:, i, j] * (com_results_big[:, i, j, 4]
                                                                      + com_results_big[:, i, j, 3]) / 2

    def plot_mag():
        label_lol = [['2004 MLR', '2004 Huber'], ['2005 MLR', '2005 Huber']]
        color_lol = [['orange', 'red'], ['green', 'blue']]

        for i in range(2):
            for j in range(2):
                plt.plot(point_list, magnitude_array[:, i, j], color=color_lol[j][i], label=label_lol[i][j])

        plt.ylabel('Responsiveness Magnitude (m Pa$^{-1}$)')
        plt.legend()
        plt.xlim(0, no_points)
        ctk.add_ticks(coastline=coastline)
        fig = plt.gcf()
        mps.defined_size(fig, size='dcsh')
        mps.save_fig_twice(cfg.plots_loc + save_prefix + 'reg_mag')
        plt.show()
        plt.clf()

    # plot_mag()

    def plot_adj_mag():
        label_lol = [['2004 MLR', '2004 Huber'], ['2005 MLR', '2005 Huber']]
        color_lol = [['orange', 'red'], ['green', 'blue']]

        for i in range(2):
            for j in range(2):
                plt.plot(point_list, adjusted_mag_array[:, i, j],
                         color=color_lol[j][i], label=label_lol[i][j])

        plt.ylabel('Responsiveness (m Pa$^{-1}$)')
        bat.run_isobath_integrate(coastline=coastline)
        plt.legend(loc='upper right')
        plt.xlim(0, no_points)
        ctk.add_ticks(coastline=coastline)
        fig = plt.gcf()
        mps.defined_size(fig, size='dcsh')
        mps.save_fig_twice(cfg.plots_loc + save_prefix +'adj_reg_mag')
        plt.show()
        plt.clf()

    plot_adj_mag()
    print('adjusted_mag_array', np.shape(adjusted_mag_array))
    return adjusted_mag_array


@mwr.timeit
def responsiveness_regress(coastline='america'):
    save_prefix = mif.save_prefix(coastline=coastline)

    # sklearn.linear_model.Ridge
    # https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Ridge.html
    # sklearn.linear_model.Lasso
    # https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Lasso.html
    angle_derivatives = np.asarray(mif.read_json_object(cfg.data_loc
                                   + save_prefix + 'angle_derivatives.json'),
                                   dtype='float32')
    no_points = np.shape(angle_derivatives)[0]
    print('no_points', no_points)
    point_list = range(no_points)
    print('angle_derivatives ', np.shape(angle_derivatives))
    saved_d = mif.read_json_object(cfg.data_loc + save_prefix +'isobath_dist.json')
    depths_array = np.asarray(saved_d["depths_list"], dtype='float32')
    dist_array = np.asarray(saved_d["dist_lol"], dtype='float32')
    print('depths_array ', np.shape(depths_array))
    print('dist_array ', np.shape(dist_array))

    def check_depth_array():
        for i in range(len(depths_array)):
            plt.plot(point_list, dist_array[:, i],
                     label='to depth ' + str(depths_array[i])
                     + 'm, i=' + str(i))
        plt.legend()
        plt.xlim(0, no_points)
        ctk.add_ticks(coastline=coastline)
        plt.show()
        plt.clf()
    # check_depth_array()

    adjusted_mag_array = find_responsiveness_magnitude(coastline=coastline)
    angle_array = find_lr_angle(coastline=coastline)

    label_lol = [['2004 MLR', '2004 Huber'],
                 ['2005 MLR', '2005 Huber']]
    color_lol = [['orange', 'red'],
                 ['green', 'blue']]

    lasso = linear_model.Lasso(alpha=0.1)
    ridge = linear_model.Ridge(alpha=0.1)

    def regress_seperately():
        fig, axs = plt.subplots(2)
        size = 2.5

        for i in range(2):
            for j in range(2):
                lasso.fit(dist_array, adjusted_mag_array[:, i, j])
                score_lasso = lasso.score(dist_array, adjusted_mag_array[:, i, j])
                print(score_lasso)
                coef_lasso = lasso.coef_
                # print(coef_lasso)
                ridge.fit(dist_array, adjusted_mag_array[:, i, j])
                coef_ridge = ridge.coef_
                score_ridge = ridge.score(dist_array, adjusted_mag_array[:, i, j])
                print(score_ridge)
                # print('coef ', np.shape(coef_ridge))
                # print(coef_ridge)

                def plot_coefficients1():
                    axs[0].plot(depths_array, coef_ridge, 'x', color=color_lol[j][i],
                                label='ridge coef $r^2=${:.2f}'.format(score_ridge), markersize=size)
                    axs[0].plot(depths_array, coef_lasso, '+', color=color_lol[j][i],
                                label='lasso coef $r^2=${:.2f}'.format(score_lasso), markersize=size)
                    axs[0].legend()
                    axs[0].set_xlabel('Isobath Depth')
                    axs[0].set_ylabel('Regression Coefficient')

                plot_coefficients1()

                # adjusted_mag_array
                offset = 5
                lasso.fit(angle_derivatives[:, offset:], adjusted_mag_array[:, i, j])
                print(lasso.score(angle_derivatives[:, offset:], adjusted_mag_array[:, i, j]))
                coef_lasso = lasso.coef_
                score_lasso = lasso.score(angle_derivatives[:, offset:], adjusted_mag_array[:, i, j])
                print(score_lasso)
                # print(coef_lasso)
                ridge.fit(angle_derivatives[:, offset:], adjusted_mag_array[:,  i, j])
                score_ridge = ridge.score(angle_derivatives[:, offset:], adjusted_mag_array[:, i, j])
                print(score_ridge)
                coef_ridge = ridge.coef_
                print('coef ', np.shape(coef_ridge))
                # print(coef_ridge)

                def plot_coefficients2():

                    axs[1].plot(np.asarray(range(offset, len(coef_ridge)+offset)),
                                coef_ridge, 'x',
                                label='ridge coef  $r^2=${:.2f}'.format(score_ridge),
                                color=color_lol[j][i],
                                markersize=size)
                    axs[1].plot(np.asarray(range(offset, len(coef_lasso)+offset)),
                                coef_lasso, '+',
                                label='lasso coef $r^2=${:.2f}'.format(score_lasso),
                                color=color_lol[j][i],
                                markersize=size)

                    axs[1].legend()
                    axs[1].set_xlabel('Smoothing Length $\sigma$  (Pt)')
                    axs[1].set_ylabel('Regression Coefficient')

                plot_coefficients2()
        mps.label_subplots(axs)
        mps.defined_size(fig, size='dcsq')
        plt.tight_layout()
        plt.show()
        plt.clf()

    def regress_together():
        offset = 5
        length_angles = len(angle_derivatives[0, offset:])
        length_dist = len(dist_array[0, :])

        combined_regress_train = np.zeros([no_points, len(angle_derivatives[0, offset:])
                                           + len(dist_array[0, :])], dtype='float32')
        combined_regress_train[:, 0:length_angles] = angle_derivatives[:, offset:]
        combined_regress_train[:, length_angles:] = dist_array[:, :]

        print('np.shape(combined_regress_train)', np.shape(combined_regress_train))
        print('np.shape(adjusted_mag_array)', np.shape(adjusted_mag_array))

        fig, axs = plt.subplots(2)
        size = 2.5

        for i in range(2):
            for j in range(2):

                lasso.fit(combined_regress_train, adjusted_mag_array[:, i, j])
                score_lasso = lasso.score(combined_regress_train,
                                          adjusted_mag_array[:, i, j])
                print(score_lasso)
                coef_lasso = lasso.coef_
                # print(coef_lasso)

                ridge.fit(combined_regress_train, adjusted_mag_array[:, i, j])
                coef_ridge = ridge.coef_
                score_ridge = ridge.score(combined_regress_train, adjusted_mag_array[:, i, j])
                print(score_ridge)

                axs[0].plot(depths_array, coef_ridge[length_angles:], 'x', color=color_lol[j][i],
                            label='ridge coef $r^2=${:.2f}'.format(score_ridge), markersize=size)
                axs[0].plot(depths_array, coef_lasso[length_angles:], '+', color=color_lol[j][i],
                            label='lasso coef $r^2=${:.2f}'.format(score_lasso), markersize=size)
                # axs[0].legend()
                axs[0].set_xlabel('Isobath Depth (m)')
                axs[0].set_ylabel('Regression Coefficient')

                axs[1].plot(np.asarray(range(offset, length_angles + offset)),
                            coef_ridge[0:length_angles], 'x',
                            label='ridge coef  $r^2=${:.2f}'.format(score_ridge),
                            color=color_lol[j][i],
                            markersize=size)
                axs[1].plot(np.asarray(range(offset, length_angles + offset)),
                            coef_lasso[0:length_angles], '+',
                            label='lasso coef $r^2=${:.2f}'.format(score_lasso),
                            color=color_lol[j][i],
                            markersize=size)

                axs[1].legend()
                axs[1].set_xlabel('Smoothing Length $\sigma$  (Pt)')
                axs[1].set_ylabel('Regression Coefficient')

        mps.label_subplots(axs)
        mps.defined_size(fig, size='dcsq')
        plt.tight_layout()
        mps.save_fig_twice(cfg.plots_loc + save_prefix + 'ridge_lasso')
        plt.show()
        plt.clf()

    regress_together()
