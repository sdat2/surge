
import src.utilities.my_wrappers as mwr
import config as cfg

def get_point_lists():
    import src.get_points.h_look_for_coast_points as hlc
    hlc.run_look(coastline='america')
    hlc.run_look(coastline='east-asia')
    import src.get_points.f_select_points as fsp
    fsp.print_lon_lat_list_from_xarray(cfg.data_loc + cfg.day_vc_2004_file_name,
                                       cfg.data_loc + 'vc_lon_lat.csv')


def test_lat_lon_getter():
    print('testing')
    import src.get_points.f_select_points as fsp
    fsp.print_lon_lat_list_from_xarray('nemo_ba130o_1h_20040101-20040131_grid-combined.nc',
                                       'data/lon_lat_point.csv')


# test_lat_lon_getter()
# create_new_point_list()

def angle_getter():
    import src.coastal_metrics.t_angle_finder as taf
    taf.get_angles()
    taf.get_angles(cfg.data_loc + 'vc_lon_lat.csv',
                   coastline='vin-china')
    taf.make_angle_plot()
    taf.make_angle_plot(coastline='vin-china')
    # taf.get_angles(coastline='vin-china')


# angle_getter()


def compare_to_choose_sigma():
    import src.currently_unused.c_comp_kurt_angle as cck
    cck.comp()


# compare_to_choose_sigma()

def bath_finder():
    import src.coastal_metrics.z_bathymetry_finder as zbf
    zbf.produce_bathymetry()
    zbf.produce_bathymetry(coastline='vin-china')


bath_finder()


def bath_correlator():
    import src.coastal_metrics.y_correlate_isobath_distances as yci
    yci.run_correlate_isobaths()
    yci.run_correlate_isobaths(coastline='vin-china')


# bath_correlator()
