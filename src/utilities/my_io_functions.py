"""
A file to contain output protocol
TODO create a function given a set of inputs which will output a definite
TODO file name

"""
import os
import re
import json
import codecs
import pickle
import csv
import numpy as np
import config
from src.utilities import my_wrappers as mwr


@mwr.timeit
def read_json_object(file_name):
    """
    :param file_name:
    :return: json_object: unknown json object
    """
    with codecs.open(file_name, 'rb', encoding='utf-8') as handle:
        json_object = json.load(handle)
    return json_object


@mwr.timeit
def write_json_object(json_object, file_name):
    """
    :param json_object:
    :param file_name:
    :return: void
    """
    with codecs.open(file_name, 'w', encoding='utf-8') as handle:
        json.dump(json_object, handle, separators=(',', ':'), sort_keys=True, indent=4)


@mwr.timeit
def read_pickle_object(file_name):
    """

    :param file_name:
    :return:
    """
    return pickle.load(open(file_name, "rb"))


@mwr.timeit
def write_pickle_object(pickle_object, file_name):
    """
    :param pickle_object:
    :param file_name:
    :return: void
    """
    with open(file_name, 'wb') as handle:
        pickle.dump(pickle_object, handle, protocol=pickle.HIGHEST_PROTOCOL)


@mwr.timeit
def write_csv_file(list_of_lists, file_name):
    """
    :param list_of_lists: a list of the rows to be saved
    :param file_name: A valid file name (including path)
    :return: void
    https://www.programiz.com/python-programming/working-csv-files
    """
    with open(file_name, 'w') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(list_of_lists)


@mwr.timeit
def read_csv_file(file_name, ignore_first=False, to_string_list=[], to_int_list=[]):
    """
    :param file_name: A valid file name (including path)
    :return: list of lists
    https://www.programiz.com/python-programming/working-csv-files
    """

    with open(file_name, 'r') as csv_file:
        list_of_lists = []
        csv_reader = csv.reader(csv_file, delimiter=',')
        start = ignore_first

        for row in csv_reader:
            temp_list = []
            if start:
                start = False
            else:
                for i in range(len(row)):
                    if i in to_string_list:
                        temp_list.append(str(row[i]))
                    elif i in to_int_list:
                        temp_list.append(int(row[i]))
                    else:
                        temp_list.append(float(row[i]))

                list_of_lists.append(temp_list)

    return list_of_lists


def tex_escape(text):
    """
    It is better to plot in TeX, but this involves escaping strings.
    from:
        https://stackoverflow.com/questions/16259923/
        how-can-i-escape-latex-special-characters-inside-django-templates
        :param text: a plain text message
        :return: the message escaped to appear correctly in LaTeX
    # removed unicode(key) from re.escape because this seemed an unnecessary,
      and was throwing an error.
    """
    conv = {
            '&': r'\&',
            '%': r'\%',
            '$': r'\$',
            '#': r'\#',
            '_': r'\_',
            '{': r'\{',
            '}': r'\}',
            '~': r'\textasciitilde{}',
            '^': r'\^{}',
            '\\': r'\textbackslash{}',
            '<': r'\textless{}',
            '>': r'\textgreater{}',
            }
    regex = re.compile('|'.join(re.escape(key) for key in sorted(conv.keys(), key=lambda item: - len(item))))
    return regex.sub(lambda match: conv[match.group()], text)


@mwr.timeit
def return_field_array(name_list, field, coastline='america'):
    import xarray as xr
    xr_ob_list = []
    field_npa_list = []
    for i in range(len(name_list)):
        xr_ob_list.append(xr.load_dataset(name_list[i]))
        if coastline == 'america':
            field_npa_list.append(xr_ob_list[i].variables[field].values)
        else:
            field_npa_list.append(xr_ob_list[i].variables[field].values.T)

    new_field_array = np.zeros([np.shape(field_npa_list[0])[0],
                                np.shape(field_npa_list[0])[1] * 2],
                                dtype='float32')

    for i, item in enumerate(field_npa_list[0][:, 0]):
        year_length = np.shape(field_npa_list[0])[1]
        new_field_array[i, :year_length] = field_npa_list[0][i, :]
        new_field_array[i, year_length:] = field_npa_list[1][i, :]

    return new_field_array


def save_prefix(coastline='america'):
    if coastline == 'america':
        save_prefix = ''
    elif coastline == 'vin-china':
        save_prefix = 'vc_'
    return save_prefix
