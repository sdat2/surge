"""
b_standardise_test.py
=====================
A program to test that pandas is really doing the right thing to my data
when I apply a ufunc in the recommended way across the dataset.
"""
import numpy as np
import xarray as xr


def run_basic_tests(name):
    """
    This does seem to prove that everything is fine w.r.t standardisation.
    :param name: the netcdf full fle path
    :return:
    """
    xr_obj = xr.load_dataset(name)
    print(xr_obj.__repr__)
    point_no = xr_obj.dims['point']
    print('number of points', point_no)
    for var_str in ['zos', 'sowindsp', 'tauuo', 'tauvo']:
        for i in range(point_no):
            point = xr_obj.variables[var_str].isel(point=i).values
            mean = np.mean(point)
            std = np.std(point)
            print('point number', i,
                  ' mean ', mean,
                  ' standard dev ', std)
            assert mean < 10**(-6)
            assert std > 1 - 10**(-3)
            assert std < 1 + 10**(-3)




