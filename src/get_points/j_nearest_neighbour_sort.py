"""
j_nearest_neighbour_sort.py
===========================
This script implements a very simple nearest neighbour sort to order the
raw list of US coast line cells, so that it goes in sequence from the south-western most
point in the gulf of mexico, to the most north Easterly point at the US-Canadian border.
The islands and 'overly convoluted' bays (e.g. Chesepeake) have already been removed from
the list. One failure case of this algorithm would be that it could skip over a spit
if the coast point on the other side of the pinch point happens to be closer to the current
coastal point than the one that is physically coastally adjacent to it, and the script ensures
that there can be no back tracking, so these points will be permanently missed from the
record. It takes negligible time ('run_sort'  0.11215 s) for the c. 700 members of the
ORCA12 slimmed US list. (later versions may take longer).
"""
import numpy as np
import src.utilities.my_io_functions as mif
import src.utilities.my_wrappers as mwr
import config as cfg
# from numba import jit


# @jit(cache=True)
def return_ordered_lol(start_index, end_index, point_lol):
    """
    Give a point list of list, get a point list_of_list smaller or equal in length to the first.
    It looks very inefficient in terms of computation and memory, but luckily Moore's law has meant that
    this no longer matters for such a small case.
    :param start_index: The index that you would like to start the nearest neighbour chain from.
    :param end_index: The final index that you would like the algorithm to terminate at.
    :param point_lol:
    :return:
    """
    ordered_lol = [point_lol[start_index]]
    end_pair = point_lol[end_index]
    point_lol.pop(start_index)
    for i in range(len(point_lol)):
        target = np.asarray(ordered_lol[-1])
        point_npa = np.asarray(point_lol)
        next_index = np.abs(np.square(point_npa[:, 1] - target[1])
                            + np.square(point_npa[:, 0] - target[0])).argmin()
        ordered_lol.append(point_lol[next_index])
        point_lol.pop(next_index)
        if point_lol[next_index] == end_pair:
            break
    return ordered_lol


@mwr.timeit
def run_sort():
    point_lol = mif.read_csv_file(cfg.h_slimmed_coast_point_list_loc)
    point_npa = np.asarray(point_lol, dtype='int16')
    # print('point_npa ', point_npa)
    # print('point_npa shape ', np.shape(point_npa))
    # print('first point ', point_npa[0, 0])
    # print('second point ', point_npa[0, 1])

    start_point_y = np.min(point_npa[:, 0])
    # print(start_point_y)
    # print('shape answer 1 ',  np.shape(np.where(point_npa[:, 0] == start_point_y)))
    start_index = int(np.where(point_npa[:, 0] == start_point_y)[0][0])
    end_point_x = np.max(point_npa[:, 1])
    print(end_point_x)
    # print('start index ', start_index)
    # print(np.where(point_npa[:, 1] == end_point_x))
    # print('shape answer 2 ',  np.shape(np.where(point_npa[:, 1] == end_point_x)))
    end_index = int(np.where(point_npa[:, 1] == end_point_x)[0][0])
    print('start point, y, x', point_lol[start_index])
    print('end point, y, x', point_lol[end_index])
    ordered_lol = return_ordered_lol(start_index, end_index, point_npa.tolist())
    mif.write_csv_file(ordered_lol, cfg.h_slimmed_ordered_point_loc)
