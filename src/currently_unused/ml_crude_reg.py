import numpy as np
import pandas as pd
import xarray as xr
from sklearn.linear_model import LinearRegression
from sklearn import linear_model
from sklearn.linear_model import RidgeCV
from sklearn.linear_model import Lasso
from sklearn.ensemble import RandomForestRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVR
from sklearn.ensemble import GradientBoostingRegressor
import src.utilities.my_wrappers as mwr


@mwr.timeit
def regression(xpca, Y_train, reg_type='linear'):
    """
    This is a hyper-naive way of scanning through different regression processes from
    sklearn, and going through them so that we can see whether any of them are particularly
    good at capturing the data ralations.
    :param xpca: the X training values in a pandas data frame
    :param Y_train: the y training values in a pandas data frame
    :param reg_type: which regression algorithm should be used [only accepts the values given below.
    :return: regression object from sklearn, that can be later used on a test set etc.
    """
    assert reg_type in ['linear', 'ridge', 'ridgeCV', 'lasso',
                        'randomForest', 'MLPRegressor', 'SVR', 'GBR']
    if reg_type == 'linear':
        reg = LinearRegression()
        reg.fit(xpca, Y_train)
    elif reg_type == 'ridge':
        reg = linear_model.Ridge(alpha=1, copy_X=True, fit_intercept=True, max_iter=None,
                                 normalize=False, random_state=None, solver='auto', tol=0.001)
    elif reg_type == 'ridgeCV':
        # This is far too many points, python gave up before finishing this.
        alphas = [np.power(10, x/500) for x in range(1, 5000, 1)]
        reg = RidgeCV(alphas=alphas, fit_intercept=True,
                      normalize=False)
    elif reg_type == 'lasso':
        lasso = Lasso(random_state=0, max_iter=10000)
        alphas = np.logspace(-4, -0.5, 30)
        tuned_parameters = [{'alpha': alphas}]
        n_folds = 5
        reg = GridSearchCV(lasso, tuned_parameters, cv=n_folds)
    elif reg_type == 'randomForest':
        reg = RandomForestRegressor(max_depth=30, random_state=0,
                                    n_estimators=100, n_jobs=-1)
    elif reg_type == 'MLPRegressor':
        parameters = {"activation": ("relu",),
                      "alpha": (0.0001, 0.001), }
        # "logistic", "tanh",
        # , 0.01, 0.1, 1
        reg = GridSearchCV(MLPRegressor(), parameters, cv=10)
    elif reg_type == 'SVR':  # takes ages
        parameters = {"kernel": ("rbf", "linear", "poly"), "C": 100, }
        svr_rbf = SVR(kernel='rbf', C=100, gamma=0.1, epsilon=.1)
        svr_lin = SVR(kernel='linear', C=100, gamma='auto')
        svr_poly = SVR(kernel='poly', C=100, gamma='auto',
                       degree=3, epsilon=.1, coef0=1)
        reg = svr_poly
    elif reg_type == 'GBR':
        reg = GradientBoostingRegressor(random_state=1, n_estimators=10)
    reg.fit(xpca, Y_train)
    return reg


@mwr.timeit
def run_regression(name):
    """
    This program naively runs various regression algorithms.
    :param name: the name [full path] of the netcdf file to read in.
    """
    xr_obj_x = xr.load_dataset(name).drop_vars(["nav_lon", "nav_lat", "zos"])
    df_x = xr_obj_x.to_dataframe().dropna(axis='columns')
    xr_obj_y = xr.load_dataset(name).drop_vars(["nav_lon", "nav_lat",
                                                "sowindsp", "tauuo",
                                                "tauvo", "uos", "vos"])
    df_y = xr_obj_y.to_dataframe().dropna(axis='columns')
    print(df_x.describe())
    print(df_y.describe())
    reg_list = ['linear',
                'ridge',  # 'ridgeCV', - ridge CV made Python give up
                'lasso',
                'randomForest',
                'MLPRegressor',
                #  'SVR', so slow.
                'GBR']
    reg_dict = {}
    for reg_type in reg_list:
        reg = regression(df_x, df_y, reg_type=reg_type)
        reg_dict[reg_type] = reg.score(df_x, df_y)
        print(reg_type, reg_dict[reg_type])

