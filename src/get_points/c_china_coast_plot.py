
import numpy as np
import xarray as xr
import cartopy.crs as ccrs
import cartopy
import matplotlib
import matplotlib.pyplot as plt
import cartopy.feature as cfeat
import src.plotting.my_plotting_style as mps
import src.utilities.my_io_functions as mif
import config as cfg
if not cfg.local:
    matplotlib.use('agg')
mps.mps_defaults()


def create_nice_map(extent=[110, 150, 20, 45]):
    """

    :param extent:
    :return:
    """
    fig, ax = plt.subplots(subplot_kw={'projection': ccrs.PlateCarree()})
    # extent = [-100, -40, 10, 40]

    ax.set_extent(extent)

    ax.gridlines(linewidth=0.5,
                 draw_labels=True,
                 #collection_kwargs={'xlabels_top': None, 'ylabels_right': None}
                 )
    ax.coastlines(resolution='50m', linewidth=0.5)  # 50m and 10m  also valid options
    # ax.add_feature(cfeat.LAKES, facecolor='none', edgecolor='black', linewidth=0.5)

    fig = plt.gcf()
    fig = mps.defined_size(fig, size='double_column_square')

    lakes = cfeat.NaturalEarthFeature(category='physical',
                                      name='lakes',
                                      scale='50m',
                                      facecolor='none',
                                      edgecolor='black',
                                      )

    ax.add_feature(lakes, linewidth=0.5)

    ax.add_feature(cfeat.BORDERS, linestyle=':',  # scale='50m',
                   edgecolor='grey', facecolor='none')

    # Create a feature for States/Admin 1 regions at 1:50m from Natural Earth
    states_provinces = cfeat.NaturalEarthFeature(category='cultural',
                                                 name='admin_1_states_provinces_lines',
                                                 scale='50m',
                                                 facecolor='none')

    ax.add_feature(states_provinces, edgecolor='gray', linewidth=0.2)

    plt.tight_layout()
    return ax


def add_c_places():
    import src.plotting.places as plc
    places_d = plc.c_place_d
    for key in places_d:
        # plt.plot(places_d[k['loc_pair'][0], places_d['loc_pair'][1], label=key)
        plt.scatter(places_d[key]['loc_pair'][0], places_d[key]['loc_pair'][1],
                    label=places_d[key]['acron'] + ': ' + key,
                    s=100, alpha=1, linewidth=1.5, marker='+')
        plt.text(places_d[key]['loc_pair'][0] - 1.5,
                 places_d[key]['loc_pair'][1] + 0.2,
                 places_d[key]['acron'],
                 fontsize=6)


def make_place_map():
    create_nice_map()
    add_c_places()
    plt.legend(bbox_to_anchor=(0., 1.02, 0.98, .102),
               loc='lower left',
               ncol=2, mode="expand",
               borderaxespad=0.)
    plt.tight_layout()
    fig = plt.gcf()
    mps.defined_size(fig, size='dcsq')
    mps.save_fig_twice(cfg.plots_loc + 'c_places')


"""
    # 400 to 880 in x
    # 2150 to 2800 in y

    small_slice = array.isel(y=slice(2180, 2670), x=slice(2160, 3025))
"""
