import xarray as xr
import src.utilities.my_wrappers as mwr
import config as cfg


def time_swap(xr_obj):
    x1 = xr_obj.swap_dims({"time_counter": "time_centered"})
    x2 = x1.reset_coords("time_counter", drop=True)
    return x2.drop_dims(["axis_nbounds", "nvertex"])  # return x2.drop(["axis_nbounds", "nvertex"])


@mwr.timeit
def xarray_upgrade_time(input_dir=cfg.toy_loc,
                        file_name=cfg.big_T_file_name,
                        output_dir=cfg.new_toy_loc):
    xr_obj = xr.load_dataset(input_dir + file_name)
    xr_obj = time_swap(xr_obj)
    print(xr_obj.__repr__)
    xr_obj.to_netcdf(path=output_dir+'t-' + file_name)


def run_upgrade_time_test():
    xarray_upgrade_time(file_name=cfg.big_T_file_name)
    xarray_upgrade_time(file_name=cfg.big_U_file_name)
    xarray_upgrade_time(file_name=cfg.big_V_file_name)


# xr_test_b()

"""
load_and_save_cube(cfg.laptop_home + cfg.toy_loc + cfg.alt_T_file_name)
load_and_save_cube(cfg.laptop_home + cfg.toy_loc + cfg.alt_U_file_name)
load_and_save_cube(cfg.laptop_home + cfg.toy_loc + cfg.alt_V_file_name)
"""
