"""
m_merge_year_points.py
======================

"""
import os
import xarray as xr
import src.utilities.my_wrappers as mwr


@mwr.timeit
def run_merge(merge_list, output_name, time_coord='time_centered'):
    """

    :param merge_list:
    :param output_name:
    :return:
    """
    ds_list = []
    for name in merge_list:
        xr_ds = xr.open_dataset(name)
        print(xr_ds.__repr__)
        ds_list.append(xr_ds)
    combined = xr.combine_nested(ds_list, concat_dim=[time_coord])
    print(combined.__repr__)
    combined.to_netcdf(output_name)


@mwr.timeit
def run_through_1950_folder(input_dir, output_dir, final_name):
    """

    :param input_dir: the input folder that the netcdf files come from.
    :param output_dir: the output folder that the netcdf files go to.
    """
    file_list = os.listdir(input_dir)
    doc_list = [input_dir + x for x in file_list if x.endswith('.nc')
                and not x.startswith('~$')]
    print('there are ', len(doc_list), ' files to go through')
    print(doc_list)
    run_merge(doc_list, output_dir+final_name, time_coord='time')
