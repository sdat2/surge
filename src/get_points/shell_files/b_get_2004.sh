#!/bin/bash
final_loc=/gws/nopw/j04/primavera1/open/sithom-bas/new_versions/
prefix=moose:/crum/mi-ba130/onh.nc.file/
name=nemo_ba130o_1h_20040601-20040630_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1h_20040701-20040731_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1h_20040801-20040831_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1h_20040901-20040930_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
exit
