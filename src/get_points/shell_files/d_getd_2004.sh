#!/bin/bash

final_loc=/gws/nopw/j04/primavera1/open/sithom-bas/day_versions/
prefix=moose:/crum/mi-ba130/ond.nc.file/
name=nemo_ba130o_1d_20040601-20040701_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1d_20040701-20040801_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1d_20040801-20040901_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1d_20040901-20041001_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1d_20041001-20041101_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
exit
