
import numpy as np
from numba import jit
from scipy.stats.stats import pearsonr
import matplotlib.pyplot as plt
import cmocean.cm as cmo
import src.utilities.my_io_functions as mif
import src.utilities.my_wrappers as mwr
import src.f_fourier_transform as fft
import src.plotting.custom_ticker as ctk
import src.plotting.my_plotting_style as mps
import config as cfg
mps.mps_defaults()


@mwr.timeit
def get_np_array(coastline='america', field='zos', time='h'):
    if coastline == 'america' and time =='h':
        runl = [cfg.data_loc + cfg.merged_2004_file_name,
                cfg.data_loc + cfg.merged_2005_file_name]
    elif coastline == 'america' and time =='d':
        runl = [cfg.data_loc + cfg.day_2004_file_name,
                cfg.data_loc + cfg.day_2005_file_name]
    elif coastline == 'vin-china' and time == 'h':
        runl = [cfg.data_loc + cfg.merged_vc_2004_file_name,
                cfg.data_loc + cfg.merged_vc_2005_file_name]
    elif coastline == 'vin-china' and time =='d':
        runl = [cfg.data_loc + cfg.day_vc_2004_file_name,
                cfg.data_loc + cfg.day_vc_2005_file_name]
    npa = mif.return_field_array(runl, field,
                                 coastline=coastline)
    print('np.shape(npa)', np.shape(npa))
    return npa


@mwr.timeit
# @jit(cache=True)
def array_coarsener(array, coarsening_length=24):
    old_length = np.shape(array)[1]
    new_length = old_length // coarsening_length
    new_array = np.zeros([np.shape(array)[0], new_length], dtype='float32')

    for i in range(new_length):
        new_array[:, i] = np.mean(array[:, coarsening_length*(i):coarsening_length*(i+1)], axis=1)

    return(new_array)


@mwr.timeit
def array_demean(array):
    demeaned_array = np.zeros(np.shape(array), dtype='float32')
    mean_array = np.zeros(np.shape(array)[0], dtype='float32')

    mean_array[:] = np.mean(array, axis=1)

    for i in range(np.shape(array)[1]):
        demeaned_array[:, i] = array[:, i] - mean_array[:]

    return demeaned_array


def two_array_correlator(array_one, array_two):
    assert(np.shape(array_one) == np.shape(array_two))
    num_points = np.shape(array_one)[0]
    correlation_npa = np.zeros(num_points, dtype='float32')
    for i in range(num_points):
        correlation_npa[i] = pearsonr(array_one[i, :], array_two[i, :])[0]
    return correlation_npa


@mwr.timeit
def coarsening_test(coastline='america'):

    zos_npa = get_np_array(coastline=coastline)
    ssh_day_npa = array_coarsener(zos_npa)
    tos_npa = get_np_array(field='tos', time='d', coastline=coastline)

    demeaned_temp = array_demean(tos_npa)
    demeaned_zos = array_demean(ssh_day_npa)

    low_pass_zos = fft.low_pass_grid(demeaned_zos, threshold_freq=2, time='d')
    low_pass_tos = fft.low_pass_grid(demeaned_temp, threshold_freq=2, time='d')

    correlation_npa = two_array_correlator(demeaned_temp, demeaned_zos)
    correlation_low_zos = two_array_correlator(demeaned_temp, low_pass_zos)
    correlation_low_zos_low_tos = two_array_correlator(low_pass_tos, low_pass_zos)

    save_prefix = mif.save_prefix(coastline=coastline)
    save_dir = cfg.plots_loc + 'temp/' + save_prefix

    def plot_test():
        plt.clf()
        fig, axs = plt.subplots(1, 2, sharey=True)
        im0 = axs[0].pcolormesh(ssh_day_npa.T, cmap=cmo.balance)
        fig.colorbar(im0, ax=axs[0], label='SSH, $\eta$, (m)',
                     orientation="horizontal")  #, location='bottom' )
        ctk.add_ticks_to_ax(axs[0], coastline=coastline)
        im1 = axs[1].pcolormesh(tos_npa.T, cmap=cmo.thermal)
        fig.colorbar(im1, ax=axs[1], label='Temperature ($^{\circ}$C)',
                     orientation="horizontal")  #, location='bottom')
        ctk.add_ticks(coastline=coastline)
        # axs[1].invert_yaxis()
        axs[0].set_ylabel('Days Since Jan 1st 2004')
        mps.defined_size(fig, size='dcm')
        plt.tight_layout()
        mps.save_fig_twice(save_dir + 'ssh_sst_grid')
        # plt.show()

    # plot_test()


    def plot_demeaned_test(array_one, label_one, array_two, label_two, save_name):
        plt.clf()
        fig, axs = plt.subplots(1, 2, sharey=True)
        im0 = axs[0].pcolormesh(array_one.T, cmap=cmo.balance)
        fig.colorbar(im0, ax=axs[0],
                     label=label_one,
                     orientation="horizontal")  #, location='bottom' )
        ctk.add_ticks_to_ax(axs[0], coastline=coastline)
        im1 = axs[1].pcolormesh(array_two.T, cmap=cmo.balance)
        fig.colorbar(im1, ax=axs[1],
                     label=label_two,
                     orientation="horizontal")  #, location='bottom')
        ctk.add_ticks(coastline=coastline)
        # axs[1].invert_yaxis()
        axs[0].set_ylabel('Days Since Jan 1st 2004')
        mps.defined_size(fig, size='dcm')
        plt.tight_layout()
        mps.save_fig_twice(save_dir + save_name)

        #plt.show()

    def plot_two_tests():
        plot_demeaned_test(demeaned_zos, 'SSH from Point Mean, $\Delta\eta$, (m)',
                           demeaned_temp, 'SST from Point Mean ($^{\circ}$C)',
                           'dm_zos_dm_tos' )
        plot_demeaned_test(low_pass_zos, 'Low Pass SSH from PM, $\Delta\eta$, (m)',
                           low_pass_tos, 'Low Pass SST from PM ($^{\circ}$C)',
                           'lp_zos_lp_tos')

    def plot_correlation_line():
        plt.clf()
        points = range(len(correlation_npa))
        plt.plot(points, correlation_npa, label=r'tos vs.\ zos')
        plt.plot(points, correlation_low_zos,
                 label=r'tos vs. zos$_{\mathrm{lp}}$')
        plt.plot(points, correlation_low_zos_low_tos,
                 label=r'tos$_{\mathrm{lp}}$ vs. zos$_{\mathrm{lp}}$')
        ctk.add_ticks(coastline=coastline)
        plt.xlim([0, len(points)])
        plt.ylabel('Correlation Coefficient, $r_p$')
        plt.legend()
        fig = plt.gcf()
        mps.defined_size(fig, size='sc')
        plt.tight_layout()
        mps.save_fig_twice(save_dir + 'correlations')
        #plt.show()

    plot_correlation_line()


def get_test():
    get_np_array()
    get_np_array(field='tos', time='d')
    get_np_array(coastline='vin-china')
    get_np_array(coastline='vin-china',
                 field='tos', time='d')
