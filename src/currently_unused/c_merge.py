"""
c_merge.py
=================================
Merge all the cubes with the same prefix.
"""

from __future__ import unicode_literals
import iris
import dask.multiprocessing
from dask.distributed import Client
from src.utilities import my_wrappers as mwr
import config as cfg
dask.config.set(scheduler='processes')


@mwr.timeit
def merge_to_one(input_file_dir, input_file_prefix,
                 output_file_name, output_file_dir):
    """

    :param input_file_dir:
    :param input_file_prefix:
    :param output_file_name:
    :param output_file_dir:
    :return:
    """
    cubes = iris.load(input_file_dir + input_file_prefix + '*.nc')
    cube = cubes.concatenate()
    with dask.config.set(scheduler='synchronous'):
        iris.save(cube, output_file_dir + output_file_name)


merge_to_one(cfg.toy_loc, cfg.small_toy_prefix, cfg.small_toy_combined, cfg.toy_loc)
