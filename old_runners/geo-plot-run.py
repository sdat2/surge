import src.utilities.my_wrappers as mwr
import config as cfg

def create_nice_map_plot():
    import matplotlib.pyplot as plt
    import src.get_points.g_plot_datapoints as gpd
    import src.plotting.my_plotting_style as mps
    gpd.create_nice_map()
    gpd.add_places()
    plt.legend(bbox_to_anchor=(0., 1.02, 1, .102),
               loc='lower left',
               ncol=2, mode="expand", borderaxespad=0.)
    fig = plt.gcf()
    mps.defined_size(fig, size='dcsq')
    mps.save_fig_twice(cfg.plots_loc + 'standard_loc_test')
    plt.show()

# create_nice_map_plot()

# ../surge/plot/new_orleans_map.pdf
# ../surge/plot/katrina_graph.pdf
