"""
# program for Ex3A by sdat2 drawing on
# http://kmdouglass.github.io/posts/approximating-diffraction-patterns
# -of-rectangular-apertures-with-the-fft.html
# Go to the bottom of this script to add new function calls
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.fftpack import fft
from scipy.fftpack import fftshift, ifftshift
import src.utilities.my_wrappers as mwr


def sinc(x, offset=0):
    """# The sinc function"""
    if x - offset != 0: # Prevent divide-by-zero
        return np.sin(np.pi * x) / (np. pi * x)
    else:
        return 1


sinc = np.vectorize(sinc)


@mwr.timeit
def FFT_Intensity(amplitude=1, slitWidth=2000, wavelength=0.5,
                  propDistance=10*10**6, bins=2048, AppertureWidth=100000,
                  m=8, s=100, sinusoidal=False, plt_theory=True, x_lim=20000,
                  Task=3):
    """
    This function contains all the code for all 3 tasks, including plotting.
    Not the best design, but better than the 3 free form scripts with globals
    and command line prompts I was using previously.
    """

    def phase(x):
        """#### Internal Phase function for Task 2."""
        return m/2*np.sin(2*np.pi*x/s)

    # Create Space
    x = np.linspace(-AppertureWidth/2, AppertureWidth/2, num=bins)
    field = np.zeros(x.size, dtype='complex128')  # Field complex
    newfield = np.zeros(x.size, dtype='complex128')

    if Task == 1:
        field[np.logical_and(x > -slitWidth / 2,
              x <= slitWidth / 2)] = amplitude + 0j
    else:
        for i in range(len(x)):
            if x[i] > - slitWidth / 2 and x[i] <= slitWidth / 2:
                if sinusoidal:
                    field[i] = amplitude*(np.cos(phase(x[i])) + np.sin(phase(x[i]))*1j)
                else:
                    field[i] = amplitude + 0j

                #### Add Phase Factor Required by Task 3 ####
                if Task == 3:
                    newfield[i] = field[i]*np.exp((1j*np.pi*(x[i]**2))/(wavelength*propDistance))

    ######### Useful FFT Variables #########

    dx = x[1] - x[0]  # Spatial sampling period, microns
    fS = 1 / dx       # Spatial sampling frequency, units are inverse microns
    f = (fS / x.size) * np.arange(0, x.size, step=1)  # a frequency type vector

    ###########ACTUAL FFT#####################
    if Task == 1:
        diffractedField = dx * np.fft.fft(np.fft.fftshift(field))
    else:
        diffractedField = dx * np.fft.fft(field)
        # take the FFT of the apperture function
    ########## Deal with corrected version #####
    if Task == 3:
        newdiffractedField = dx * np.fft.fft(newfield)
    # fftshift uses the symmetry of the appeture to get rid of negative part

    ####### Rescaling ###########################
    xPrime = (np.hstack((f[-int((f.size/2)):] - fS, f[0:int(f.size/2)]))
              * wavelength * propDistance)   #
    IntensTheory = (amplitude / (wavelength * propDistance) *
                    (slitWidth * sinc(xPrime * slitWidth / wavelength / propDistance))**2)

    ########### FURTHER FFT #####################
    IntensFFT = (np.fft.fftshift(diffractedField * np.conj(diffractedField))
                 / wavelength / propDistance)
    if Task == 3:
        newIntensFFT = (np.fft.fftshift(newdiffractedField * np.conj(newdiffractedField))
                        / wavelength / propDistance)

    ################### Plotting #################
    if Task == 1:
        plt.plot(xPrime, np.abs(IntensFFT), '.', label='FFT')
        plt.plot(xPrime, IntensTheory, label='Theory')

    if Task == 2:
        plt.plot(xPrime, np.abs(IntensFFT), '.', label='FFT for Sinusoidal Phase Apperture')
        if plt_theory: plt.plot(xPrime, IntensTheory, label='Simple slit of equal width')

    if Task == 3:
        plt.plot(xPrime, np.abs(IntensFFT), label='FFT (Uncorrected)')
        plt.plot(xPrime, np.abs(newIntensFFT), label='FFT (Corrected)')
        if plt_theory:
            plt.plot(xPrime, IntensTheory, label='FT Theory')

    plt.xlim((-x_lim, x_lim))
    plt.xlabel(r'Screen Position, $\mu m$')
    plt.ylabel(r'Intensity on Screen, $V^2 / \mu m$')
    plt.grid(True)
    plt.legend()
    plt.savefig('Task' + str(Task) + '_lam=' + str(wavelength)
                + '_d=' + str(slitWidth) + '_Sinusoidal='
                + str(sinusoidal)+'_D='
                + str(propDistance)+'_microns.pdf')

    plt.clf()


def tests():
    # For definiteness, use λ = 500nm, d=100 microns,
    # D = 1.0m and L =5mm. Overlay on your plot the #
    # theoretical value of the intensity pattern expected

    print('task 1')
    FFT_Intensity(slitWidth=100, x_lim=200, AppertureWidth=100000,
                  propDistance=5*10**3,  wavelength=0.5, Task=1, bins=2048*2**3)

    # Now calculate and plot the Fraunhofer diffraction pattern of a sinusoidal phase grating.
    # This grating is a slit of extent d = 2mm, outside of which the transmission is zero.
    # Within |x| < d/2, the transmission amplitude is 1.0, and the phase of A is
    # φ(x) = (m/2) sin(2πx/s)
    # where s is the spacing of the phase maxima, and can be taken as 100microns for this problem.
    # For this calculation, use m = 8. The Fresnel distance d2/λ is 8 m, so calculate
    # the pattern on a screen at D = 10 m. What do you notice about the resulting pattern?

    print('task 2')
    FFT_Intensity(Task=2, sinusoidal=True, plt_theory=False, m=8, s=100,
                  slitWidth=2000,  propDistance=10*10**6)

    # Now modify your program so that the calculation is accurate even in
    # the near-field by adding a phase correction to the aperture function
    # as defined by Equation 9. Repeat your calculations in the previous#
    # two tasks for D = 5mm for the slit, and D = 0.5m for the phase grating,
    # and plot the results. Do the intensity patterns look sensible?

    print('task 3')
    FFT_Intensity(Task=3,slitWidth=100, x_lim=200, plt_theory=False,
                  AppertureWidth=10000,
                  propDistance=5*10**3,  wavelength=0.5, bins=2048*2**3)
    FFT_Intensity(Task=3, sinusoidal=True, plt_theory=False, m=8, s=100,
                  AppertureWidth=10000,
                  slitWidth=1000,  propDistance=0.5*10**6, x_lim=800)

    # for i in [500*x for x in range(1,40)]:
    #    FFT_Intensity(slitWidth=i, Task=3)

