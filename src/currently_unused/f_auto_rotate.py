"""
f_auto_rotate.py
================
This program was supposed to use IRIS' automatic functionality to rotate the verlocities
and stresses so that they can be U, V type, but unfortunately this has not yet been possible,
ans so has been abandoned because it is of low importance. I did get a better idea of how to use
IRIS syntax however so it was not entirely a wasted exercise.

TypeError: 'Cube' object does not support item assignment

<bound method Cube.coords of <iris 'Cube' of
 ocean surface current along i-axis /(m/s)
  (time_counter: 744; -- : 440; -- : 865)>>
<bound method Cube.coords of <iris 'Cube' of
 surface_downward_x_stress / (N/m2)
  (time_counter: 744; -- : 440; -- : 865)>>
<bound method Cube.coords of <iris 'Cube'
 of ocean surface current along j-axis / (m/s)
  (time_counter: 744; -- : 440; -- : 865)>>
<bound method Cube.coords of <iris 'Cube' of
 surface_downward_y_stress / (N/m2)
  (time_counter: 744; -- : 440; -- : 865)>>

https://stackoverflow.com/questions/7559595/
python-runtimewarning-overflow-encountered-in-long-scalars
So the burden is on you to choose appropriate dtypes so that no operation overflows.

oceTauY # BSOSE
:long_name = "meridional surf. wind stress, >0 increases vVel";
:units = "N/m^2";
:standard_name = "oceTAUY";
:mate = "oceTAUX";
:coordinates = "dxG rAs iter dyC";

float tauvo(time_centered=744, y=440, x=865); # ORCA12
  :_FillValue = 1.0E20f; // float
  :standard_name = "surface_downward_y_stress";  --> "oceTAUY"
  :long_name = "Surface Downward Y Stress";      --> "Meridional Wind Stress, >0 increases V"
  :units = "N/m2";
  :online_operation = "average";
  :interval_operation = "360 s";
  :interval_write = "1 h";
  :cell_methods = "time: mean (interval: 360 s)";
  :cell_measures = "area: area";
  :coordinates = "time_counter nav_lon nav_lat";
  :_ChunkSizes = 107U, 63U, 124U; // uint

float VVEL(time=60, Z=52, YG=588, XC=2160); # BSOSE
  :_FillValue = NaNf; // float
  :units = "m/s";
  :long_name = "Meridional Component of Velocity (m/s)";
  :standard_name = "VVEL";
  :mate = "UVEL";
  :coordinates = "hFacS iter rAs dxG dyC drF";

float vos(time_centered=744, y=440, x=865); # ORCA12
  :_FillValue = 1.0E20f; // float
  :standard_name = "sea_surface_northward_sea_water_velocity";  --> "VVEL"
  :long_name = "ocean surface current along j-axis";            --> "Meridional Velocity, V, (m/s)"
  :units = "m/s";
  :online_operation = "average";
  :interval_operation = "360 s";
  :interval_write = "1 h";
  :cell_methods = "time: mean (interval: 360 s)";
  :cell_measures = "area: area";
  :coordinates = "time_counter nav_lon nav_lat";
  :_ChunkSizes = 107U, 63U, 124U; // uint

float oceTAUX(time=60, YC=588, XG=2160); # BSOSE
  :_FillValue = NaNf; // float
  :units = "N/m^2";
  :long_name = "zonal surface wind stress, >0 increases uVel";
  :standard_name = "oceTAUX";
  :mate = "oceTAUY";
  :coordinates = "rAw dyG dxC iter";

float tauuo(time_centered=744, y=440, x=865);
  :_FillValue = 1.0E20f; // float
  :standard_name = "surface_downward_x_stress"; --> "oceTAUX"
  :long_name = "Surface Downward X Stress";     --> "Zonal Wind Stress, >0 increases U"
  :units = "N/m2";
  :online_operation = "average";
  :interval_operation = "360 s";
  :interval_write = "1 h";
  :cell_methods = "time: mean (interval: 360 s)";
  :cell_measures = "area: area";
  :coordinates = "time_counter nav_lon nav_lat";
  :_ChunkSizes = 107U, 63U, 124U; // uint

float uos(time_centered=744, y=440, x=865);
  :_FillValue = 1.0E20f; // float
  :standard_name = "sea_surface_eastward_sea_water_velocity";  --> "UVEL"
  :long_name = "ocean surface current along i-axis";           --> "Zonal Wind Velocity, U, (m/s)"
  :units = "m/s";
  :online_operation = "average";
  :interval_operation = "360 s";
  :interval_write = "1 h";
  :cell_methods = "time: mean (interval: 360 s)";
  :cell_measures = "area: area";
  :coordinates = "time_counter nav_lon nav_lat";
  :_ChunkSizes = 107U, 63U, 124U; // uint

float UVEL(time=60, Z=52, YC=588, XG=2160); # BSOSE
  :_FillValue = NaNf; // float
  :units = "m/s";
  :long_name = "Zonal Component of Velocity (m/s)";
  :standard_name = "UVEL";
  :mate = "VVEL";
  :coordinates = "hFacW iter rAw dyG dxC drF";

"""
import numpy as np
import iris
import iris.analysis.cartography as iac
import src.utilities.my_wrappers as mwr
import config as cfg


@mwr.timeit
def auto_rotate(input_dir=cfg.toy_loc,
                u_file_name=cfg.big_U_file_name,
                v_file_name=cfg.big_V_file_name,
                output_dir=cfg.rot_test_loc):
    """
        iac.rotate_grid_vectors function acts on a single slice only.
    Realised due to error:
    ValueError: non-broadcastable output operand with shape (440,865)
     doesn't match the broadcast shape (744,440,865)
    :param input_dir:
    :param u_file_name:
    :param v_file_name:
    :param output_dir:
    :return
    TODO This does not work - will have to both look up the CF conventions
    TODO and make new cubes to store the results. I.e not really worth it.
    TODO There are currently higher priorities in this Master's project and
    TODO I might come back later to do this.
    """
    u_cubes = iris.load(input_dir + u_file_name)
    v_cubes = iris.load(input_dir + v_file_name)
    first = True

    # print(u_cubes[0])
    # print(v_cubes[0])
    #print('coords', u_cubes[0].coords())
    if False:
        cube = u_cubes[0]
        print(cube.long_name)
        cube.long_name = "Zonal Wind Velocity, U, (m/s)"
        print(cube.long_name)
        cube.standard_name = "UVEL"
        print(cube.standard_name)
        print(cube.units)

    for i in [0, 1]:
        print(i)
        print(u_cubes[i].summary(True))
        print(v_cubes[i].summary(True))

        for j in range(np.shape(u_cubes[i])[0]):
            u_cubes[i][j, :, :], v_cubes[i][j, :, :] = \
               iac.rotate_grid_vectors(u_cubes[i][j, :, :], v_cubes[i][j, :, :])

    # print(u_cubes)
    # print(v_cubes)

    """

    for cube in u_cubes:
        print(cube.coords)
    for cube in v_cubes:
        print(cube.coords)
    """

    # iac.rotate_grid_vectors()

    iris.save(u_cubes, output_dir + u_file_name)
    iris.save(v_cubes, output_dir + v_file_name)


auto_rotate()
