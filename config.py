"""
config.py - a file to store all of the file paths so that the code
can be made more readable, and data flows more easily altered.
author: Candidate 8205R
"""
# A variable to allow easy toggling between settings needed for remote
# and local work.
local = True

jasmin_home = '/home/users/sithom/'
jasmin_repo = jasmin_home + 'surge/'
jasmin_group_workspace = '/gws/nopw/j04/primavera1/open/sithom-bas/'
jasmin_init_subdir = jasmin_group_workspace + 'new_versions/'
jasmin_small_subdir = jasmin_group_workspace + 'small_versions/'
jasmin_ea_small = jasmin_group_workspace + 'ea_small/'
jasmin_vc_point = jasmin_group_workspace + 'vc_point/'
jasmin_subdir_sep_points = jasmin_group_workspace + 'point_versions/'
jasmin_day_big = jasmin_group_workspace + 'day_versions/'
jasmin_day_small = jasmin_group_workspace + 'day_small/'
jasmin_day_point = jasmin_group_workspace + 'day_point/'
jasmin_ea_day_small = jasmin_group_workspace + 'ea_day_small/'
jasmin_vc_day_point = jasmin_group_workspace + 'vc_day_point/'

ea_small_1950 = jasmin_group_workspace + 'control-1950-ea-small/'
vc_point_1950 = jasmin_group_workspace + 'control-1950-vc-point/'

ea_vas_small_1950 = jasmin_group_workspace + 'control-1950-vas-ea-small/'
ea_uas_small_1950 = jasmin_group_workspace + 'control-1950-uas-ea-small/'

small_1950 = jasmin_group_workspace + 'control-1950-small/'
point_1950 = jasmin_group_workspace + 'control-1950-point/'

vas_small_1950 = jasmin_group_workspace + 'control-1950-vas-small/'
uas_small_1950 = jasmin_group_workspace + 'control-1950-uas-small/'

control_1950_source = '/gws/nopw/j04/primavera5/stream1/PRIMAVERA/HighResMIP/' \
                      'NERC/HadGEM3-GC31-HH/control-1950/r1i1p1f1/PrimOday/zos/gn/v20180927/'

control_1950_vas = '/gws/nopw/j04/primavera5/stream1/PRIMAVERA/HighResMIP/NERC/HadGEM3-GC31-HH/control-1950/r1i1p1f1/Prim1hr/vas/gn/v20180927/'
control_1950_uas = '/gws/nopw/j04/primavera5/stream1/PRIMAVERA/HighResMIP/NERC/HadGEM3-GC31-HH/control-1950/r1i1p1f1/Prim1hr/uas/gn/v20180927'

# '/gws/nopw/j04/primavera5/stream1/PRIMAVERA/HighResMIP/NERC/HadGEM3-GC31-HH/control-1950/r1i1p1f1/Prim1hr/vas/gn/v20180927/vas_Prim1hr_HadGEM3-GC31-HH_control-1950_r1i1p1f1_gn_205010010000-205012302300.nc'

laptop_home = '/Users/simon/surge/'
data_loc = "data/"
toy_loc = data_loc + "toy/"
new_toy_loc = data_loc + "new_toy/"
ea_toy_loc = data_loc + "ea_toy/"
test_loc = data_loc + "test/"
rot_test_loc = data_loc + "rot_test/"

plots_loc = "plots/"

bath_path = data_loc + 'eORCA12_bathymetry_v2.4.nc'  # Bathymetry
slim_bath_path = data_loc + 'eORCA12_bath_v2.4_slimmed.nc'
slim_ea_bath_path = data_loc + 'eORCA12_bath_v2.4_slimmed_east_asia.nc'
toy_U_file_name = "nemo_ba130o_1d_20050801-20050901_grid-U.nc"
toy_V_file_name = "nemo_ba130o_1d_20050801-20050901_grid-V.nc"
toy_T_file_name = "nemo_ba130o_1d_20050801-20050901_grid-T.nc"
toy_ea_T_file_name = "nemo_ba130o_1h_20050801-20050831_grid-T-east-asia.nc"
small_toy_prefix = "small-nemo_ba130o_1d_20050801-20050901_grid-"
small_toy_combined = "small-nemo_ba130o_1d_20050801-20050901_all.nc"

alt_T_file_name = 'nemo_ba130o_1h_20041001-20041031_grid-T.nc'
alt_U_file_name = 'nemo_ba130o_1h_20041001-20041031_grid-U.nc'
alt_V_file_name = 'nemo_ba130o_1h_20041001-20041031_grid-U.nc'
alt_combined = "alt_1h_20041001-20041031_grid.nc"

big_T_file_name = 'nemo_ba130o_1h_20050801-20050831_grid-T.nc'
big_U_file_name = 'nemo_ba130o_1h_20050801-20050831_grid-U.nc'
big_V_file_name = 'nemo_ba130o_1h_20050801-20050831_grid-V.nc'

init_points_full_path = data_loc + 'possible_coastal_locations.csv'
yin_stations_full_path = data_loc + 'yin_stations.csv'

yin_plot_path_sans_suffix = plots_loc + 'yin_stations'

coast_cell_mark1_sans_suffix = plots_loc + 'coast_cells_new_mark1'

h_auto_point_list_loc = data_loc + 'auto_point_new_list.csv'
h_auto_loc_list_loc = data_loc + 'auto_loc_new_list.csv'

us_coast_mark_two_sans_suffix = plots_loc + 'us_coast_cells_new_mark_two'

h_auto_us_point_list_loc = data_loc + 'us_point_new_list.csv'
h_auto_us_loc_list_loc = data_loc + 'us_loc_new_list.csv'
h_ea_auto_point = data_loc + 'ea_point_new_list.csv'
h_ea_auto_loc = data_loc + 'ea_loc_new_list.csv'
h_ea_auto_slim_point = data_loc + 'ea_point_slim_list.csv'
h_vin_chin_list = data_loc + 'vin_chin_point_list.csv'

us_slimmed_coast_mark_three_sans_suffix = plots_loc + 'us_slimmed_coast_mark_four'

h_slimmed_coast_point_list_loc = data_loc + 'slimmed_coast_mark_four_point_list.csv'
h_slimmed_coast_loc_list_loc = data_loc + 'slimmed_coast_mark_four_loc_list.csv'

toy_point_nc = 'combined_aug_hourly_points.nc'

h_nc_points_sans_suffix = plots_loc + 'nc_points_all'

h_slimmed_ordered_point_loc = data_loc + 'new_ordered_slim_points.csv'


prefix_list_2005 = ['nemo_ba130o_1h_20050101-20050131_grid-',
                    'nemo_ba130o_1h_20050201-20050228_grid-',
                    'nemo_ba130o_1h_20050301-20050331_grid-',
                    'nemo_ba130o_1h_20050401-20050430_grid-',
                    'nemo_ba130o_1h_20050501-20050531_grid-',
                    'nemo_ba130o_1h_20050601-20050630_grid-',
                    'nemo_ba130o_1h_20050701-20050731_grid-',
                    'nemo_ba130o_1h_20050801-20050831_grid-',
                    'nemo_ba130o_1h_20050901-20050930_grid-',
                    'nemo_ba130o_1h_20051001-20051031_grid-',
                    'nemo_ba130o_1h_20051101-20051130_grid-',
                    'nemo_ba130o_1h_20051201-20051231_grid-']

prefix_list_2004 = ['nemo_ba130o_1h_20040101-20040131_grid-',
                    'nemo_ba130o_1h_20040201-20040228_grid-',
                    'nemo_ba130o_1h_20040301-20040331_grid-',
                    'nemo_ba130o_1h_20040401-20040430_grid-',
                    'nemo_ba130o_1h_20040501-20040531_grid-',
                    'nemo_ba130o_1h_20040601-20040630_grid-',
                    'nemo_ba130o_1h_20040701-20040731_grid-',
                    'nemo_ba130o_1h_20040801-20040831_grid-',
                    'nemo_ba130o_1h_20040901-20040930_grid-',
                    'nemo_ba130o_1h_20041001-20041031_grid-',
                    'nemo_ba130o_1h_20041101-20041130_grid-',
                    'nemo_ba130o_1h_20041201-20041231_grid-']

day_prefixes_2004 = ['nemo_ba130o_1d_20040101-20040201_grid-',
                     'nemo_ba130o_1d_20040201-20040301_grid-',
                     'nemo_ba130o_1d_20040301-20040401_grid-',
                     'nemo_ba130o_1d_20040401-20040501_grid-',
                     'nemo_ba130o_1d_20040501-20040601_grid-',
                     'nemo_ba130o_1d_20040601-20040701_grid-',
                     'nemo_ba130o_1d_20040701-20040801_grid-',
                     'nemo_ba130o_1d_20040801-20040901_grid-',
                     'nemo_ba130o_1d_20040901-20041001_grid-',
                     'nemo_ba130o_1d_20041001-20041101_grid-',
                     'nemo_ba130o_1d_20041101-20041201_grid-',
                     'nemo_ba130o_1d_20041201-20050101_grid-']

day_prefixes_2005 = ['nemo_ba130o_1d_20050101-20050201_grid-',
                     'nemo_ba130o_1d_20050201-20050301_grid-',
                     'nemo_ba130o_1d_20050301-20050401_grid-',
                     'nemo_ba130o_1d_20050401-20050501_grid-',
                     'nemo_ba130o_1d_20050501-20050601_grid-',
                     'nemo_ba130o_1d_20050601-20050701_grid-',
                     'nemo_ba130o_1d_20050701-20050801_grid-',
                     'nemo_ba130o_1d_20050801-20050901_grid-',
                     'nemo_ba130o_1d_20050901-20051001_grid-',
                     'nemo_ba130o_1d_20051001-20051101_grid-',
                     'nemo_ba130o_1d_20051101-20051201_grid-',
                     'nemo_ba130o_1d_20051201-20060101_grid-']

# nemo_ba130o_1h_20040101-20040131_grid-combined.nc'

combined_suffix_nc = 'combined.nc'
combined_2011_point_name = 'nemo_ba130o_1h_2011_points.nc'

sep_point_dir = 'point_versions/' + 'separated/'
combined_point_dir = 'point_versions/' + 'combined/'

stats_plot_sans_suffix = plots_loc + 'stats_points_plot'


merged_vc_2004_file_name = 'vc_points_2004_nemo.nc'
merged_vc_2005_file_name = 'vc_points_2005_nemo.nc'


merged_2004_file_name = 'points_2004_nemo.nc'
standardised_2004_file_name = 'points_2004_anomalies.nc'

day_2004_file_name = 'day_2004_nemo.nc'
day_2005_file_name = 'day_2005_nemo.nc'

day_vc_2004_file_name = 'day_vc_2004_nemo.nc'
day_vc_2005_file_name = 'day_vc_2005_nemo.nc'

merged_2005_file_name = 'points_2005_nemo.nc'
standardised_2005_file_name = 'points_2005_anomalies.nc'

merged_1950_name = 'points_1950_control.nc'
vc_merged_1950_name = 'vc_points_1950_control.nc'


merged_2011_file_name = 'points_2011_nemo.nc'
standardised_2011_file_name = 'points_2011_anomalies.nc'


f_fourier_trans_test_loc = plots_loc + 'f_fourier_transform/'
