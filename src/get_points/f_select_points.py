"""
f_select_points.py
==================
Select a load of points.

Hypothetically:
The dask uncertainty effect: the scientific results can be changed depending
 on whether or not you turn on an intermediate measurement.

"""
import os
import numpy as np
import xarray as xr
# from memory_profiler import profile
import src.utilities.my_wrappers as mwr
import src.utilities.my_io_functions as mif
import src.get_points.d_upgrade_time as dut
import config as cfg


# @profile
@mwr.timeit
def select_points_xr(xr_ds, coastline='america'):

    # csv_object = mif.read_csv_file(cfg.yin_stations_full_path,
    #                                to_string_list=[2, 3])
    # pair_array = run_index_extraction(csv_object, xr_ds)

    if coastline == 'america':
        pair_array = np.asarray(mif.read_csv_file(cfg.h_slimmed_ordered_point_loc),
                                dtype='int16')
    elif coastline == 'vin-china':
        # (vin is an alternative colloquial name of viet nam)
        # for backing of this claim, go to vinmart :) (great nem)
        pair_array = np.asarray(mif.read_csv_file(cfg.h_vin_chin_list),
                                dtype='int16')

    # print(pair_array)
    # print(pair_array[0, 0], pair_array[0, 1])

    # return _new_version(xr_ds, pair_array)
    return _new_version(xr_ds, pair_array)

@mwr.timeit
def _new_version(xr_ds, pair_array):
    """I hope that this new version should be substantially faster than the old,
       because that I think that it should parralelise the process."""

    return xr_ds.isel(x=xr.DataArray(pair_array[:, 1].tolist(), dims=["point"]),
                      y=xr.DataArray(pair_array[:, 0].tolist(), dims=["point"]))


@mwr.timeit
def _old_version(xr_ds, pair_array):
    """ This version seems very slow."""

    ds_list = []

    for i in range(np.shape(pair_array)[0]):
        ds_list.append(xr_ds.isel(x=pair_array[i, 1],
                                  y=pair_array[i, 0]))
        # For some unknown reason the previous script put these the wrong way round!!
        # This is very counter intuitive, I need to think about this more!!!

    del pair_array

    return xr.concat(ds_list, "point")


#  @profile
@mwr.timeit
def select_month_xr(file_name_list,
                    output_file_loc,
                    coastline='america'):

    ds_list = []

    for name in file_name_list:
        xr_ds = xr.open_dataset(name)
        xr_ds = dut.time_swap(xr_ds)
        print(xr_ds.__repr__)
        ds_list.append(select_points_xr(xr_ds, coastline=coastline))
        del xr_ds
        # Within current statistical error this line 'del xr_ds' does not speed up the process.
        # But the process that didn't have it froze, so I'll keep it in by the precautionary
        # principle. [This freezing might have been caused by a number of other things]

    combined = xr.combine_nested(ds_list,
                                 concat_dim=[None],
                                 compat='override',)

    print(combined.__repr__)
    combined.to_netcdf(output_file_loc)

@mwr.timeit
def select_1950_xr(input_dir, file_name,
                   output_dir, coastline='america'):
    """

    :param input_dir: the input folder that the names come from.
    :param file_name: the netcdf file name (will be left unchanged).
    :param output_dir: the output directory for the points.
    """
    xr_ds = xr.open_dataset(input_dir + file_name)
    point_ds = select_points_xr(xr_ds, coastline=coastline)
    point_ds.to_netcdf(output_dir + file_name)


@mwr.timeit
def run_through_1950_folder(input_dir, output_dir,
                            min_inclusive, max_exclusive,
                            coastline='america'):
    """

    :param input_dir: the input folder that the netcdf files come from.
    :param output_dir: the output folder that the netcdf files go to.
    """
    file_list = os.listdir(input_dir)
    doc_list = [x for x in file_list if x.endswith('.nc')
                and not x.startswith('~$')]
    print('there are ', len(doc_list), ' files to go through')
    print(doc_list)
    for i, doc_label in enumerate(doc_list):
        if i >= min_inclusive and i < max_exclusive:
            print('looking for', input_dir + doc_label)
            select_1950_xr(input_dir, doc_label,
                           output_dir, coastline=coastline)

# import config as cfg
# run_through_1950_folder(cfg.test_loc, cfg.rot_test_loc)

#  @profile
@mwr.timeit
def test_for_single_netcdf(name, output_name, coastline='america'):
    xr_ds = xr.open_dataset(name)
    xr_ds = dut.time_swap(xr_ds)
    print(xr_ds.__repr__)
    xr_ds_new = select_points_xr(xr_ds, coastline=coastline)
    del xr_ds
    print(xr_ds_new)
    xr_ds_new.to_netcdf(output_name)
    del xr_ds_new


"""
fsp.test_for_single_netcdf(cfg.new_toy_loc + 'nemo_ba130o_1d_20050801-20050901_grid-T.nc',
                               cfg.new_toy_loc + 'selected_daily_points_august_05.nc')
"""


def print_lon_lat_list_from_xarray(name, output_name):
    xr_ds = xr.open_dataset(name)
    lat = xr_ds.coords['nav_lat'].values
    # print(lat)
    lon = xr_ds.coords['nav_lon'].values
    del xr_ds
    # print(lon)
    pair_lol = []
    for i in range(len(lat)):
        pair_lol.append([lon[i], lat[i], i])
    mif.write_csv_file(pair_lol, output_name)


"""
# Name                    Version                   Build  Channel
xarray                    0.15.0                     py_0
"""
