"""
A gaussian regression function program heavily tailored to serve o_scatter_plotter.py
These functions are very-much not general.
"""
import numpy as np
import matplotlib.pyplot as plt
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import (RBF, Matern, RationalQuadratic,
                                              ExpSineSquared, DotProduct,
                                              ConstantKernel, WhiteKernel)
from src.utilities import my_io_functions as mif
from src.utilities import my_wrappers as mwr
from src.utilities import sobol_functions as sbl
from src.plotting import my_plotting_style as mps
mps.mps_defaults(quality='high')


@mwr.timeit
def gaussian_process_regression(x_value, y_value, **kwargs):
    """

    :param x_value: The full set of x values
    :param y_value: The full set of y values (target values for the regression)
    Both x_value and y_value lists are
    :param add_label: which variables are being plotted (string)
    :param kwargs: the z_values_d can be passed in here
    :return:
    """

    print('given shapes', np.shape(x_value), np.shape(y_value))
    # This weird section is required to
    rng = np.random.RandomState(0)  # passing seed in?
    x_temp = np.array(x_value)
    x_npa_sample = rng.uniform(0, 5, len(x_value))[:, np.newaxis]
    x_npa_sample[:, 0] = x_temp[:]
    y_npa_sample = np.array(y_value)

    x_npa_sample.reshape(-1, 1)
    y_npa_sample.reshape(-1, 1)

    chosen_list = sbl.sobol_index_generator(seed=35455,
                                            number_of_samples=6000,
                                            initial_list_size=len(x_npa_sample))

    x_npa_sample = sbl.npa_subsampler_gpr_format(x_npa_sample, chosen_list)
    y_npa_sample = sbl.npa_subsampler(y_npa_sample, chosen_list)

    range_x = np.max(x_npa_sample) - np.min(x_npa_sample)
    xmin = np.min(x_npa_sample) - range_x*0.04
    xmax = np.max(x_npa_sample) + range_x*0.04

    print('transformed shapes', np.shape(x_npa_sample), np.shape(y_npa_sample))

    def kernel_working():
        kernel = (1.0 * ExpSineSquared(length_scale=8000, length_scale_bounds=(5000, 10000),
                                       periodicity=8000)
                  + 1.0 * WhiteKernel(noise_level=1e-1,  #  noise_level_bounds=(1e-2, 1e+2)
                                )
                  * ExpSineSquared(length_scale=8000, length_scale_bounds=(5000, 10000),
                                   periodicity=8000)
                  + 1.0 * WhiteKernel(noise_level=1e-1))
        return kernel

    def kernel_attempt_new():
        kernel = (1.0 * ExpSineSquared(length_scale=8000, length_scale_bounds=(5000, 10000),
                                       periodicity=8000)
                  + 1.0 * WhiteKernel(noise_level=1e-1,  #  noise_level_bounds=(1e-2, 1e+2)
                                      )
                  * RBF(length_scale=3000,
                        length_scale_bounds=(2000, 10000),
                        # periodicity=8000
                        )
                  + 1.0 * WhiteKernel(noise_level=1e-1))
        return kernel

    kernel = kernel_working()

    gp_object = GaussianProcessRegressor(kernel=kernel, alpha=0.0).fit(x_npa_sample, y_npa_sample)

    x_pred_npa = np.linspace(xmin, xmax, 10000)
    y_pred_npa, y_cov = gp_object.predict(x_pred_npa[:, np.newaxis], return_cov=True)

    # plot it once normally as default
    gaussian_process_plot(x_pred_npa, y_pred_npa,
                          y_cov, gp_object, kernel)

    return x_pred_npa, y_pred_npa, y_cov


@mwr.timeit
def gaussian_process_plot(x_pred_npa, y_pred_npa,
                          y_cov, gp_object, kernel,
                           **kwargs):
    """

    :param x_pred_npa: the x array created to put the prediction on
    :param y_pred_npa: the y prediction
    :param y_cov: the y covariance maxtrix at each point in y_pred_npa
    :param gp_object: the gaussian object after fitting
    :param kernel: the initial kernel tried
    :param kwargs: z_values_label and z_values can be passed in here
    :return:
    """

    plt.plot(x_pred_npa,
             y_pred_npa,
             '#002147',
             lw=1,
             zorder=9,
             alpha=0.7,
             label='GP Prediction')

    for sig_mult, alpha in [[1, 0.4], [2, 0.2], [3, 0.1], [4, 0.1]]:
        # This is the strength of the shading at each value of sigma
        plt.fill_between(x_pred_npa, y_pred_npa - sig_mult*np.sqrt(np.diag(y_cov)),
                         y_pred_npa + sig_mult*np.sqrt(np.diag(y_cov)),
                         alpha=alpha, color='#a3c1ad', label='%s $\sigma$ envelope' % str(sig_mult))

    # plt.plot(x_pred_npa, 0.5*np.sin(3*x_pred_npa), 'r', lw=3, zorder=9)

    plt.title(mif.tex_escape("Initial: %s\nOptimum: %s\nLog-Marginal-Likelihood: %s"
                             % (kernel, gp_object.kernel_,
                                gp_object.log_marginal_likelihood(gp_object.kernel_.theta))))

    plt.xlim([np.min(x_pred_npa), np.max(x_pred_npa)])


