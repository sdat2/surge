"""
h_look_for_coast_points.py
==========================

"""
from numba import jit
import numpy as np
import xarray as xr
import config as cfg
import src.utilities.my_io_functions as mif
import src.utilities.my_wrappers as mwr


def _test_plot(locs_lol, inverse=True, map=False, save_vc_choice=True):
    import matplotlib.pyplot as plt
    if map:
        import src.get_points.c_china_coast_plot as ccp
        import src.plotting.my_plotting_style as mps
        import cmocean.cm as cmo
        import cartopy.crs as ccrs
        import config as cfg
        ax = ccp.create_nice_map(extent=[104, 126, 16, 42])

        small_slice = xr.open_dataarray(cfg.slim_ea_bath_path)
        small_slice.plot.pcolormesh(ax=ax, transform=ccrs.PlateCarree(),
                                    cmap=cmo.deep, alpha=0.2,
                                    x='nav_lon', y='nav_lat', add_colorbar=True,
                                    cbar_kwargs={"orientation": "horizontal",
                                                 'label': 'Depth (m)',
                                                 "shrink": 0.5,
                                                 "pad": 0.01},
                                    vmin=0, vmax=200)

        ax.coastlines()
        plt.tight_layout()
        fig = plt.gcf()
        ccp.add_c_places()
        plt.legend(bbox_to_anchor=(0., 1.02, 0.98, .102),
                   loc='lower left',
                   ncol=2, mode="expand",
                   borderaxespad=0.)
        mps.defined_size(fig, size='dcsq')

    locs_npa = np.asarray(locs_lol)
    print(np.shape(locs_npa))
    if inverse:
        plt.plot(locs_npa[:, 1], locs_npa[:, 0], 'x-', color='green', markersize=0.3)
    else:
        plt.plot(locs_npa[:, 0], locs_npa[:, 1], 'x-', color='green', markersize=0.3)

    if save_vc_choice:
        mps.save_fig_twice(cfg.plots_loc + 'vin-china-choice')

    # plt.show()


def _compare_lengths(coast_points_lol, shape):
    """
    :param coast_points_lol: a list of pairs.
    :param shape: The original shape of the netcdf slice.
    """
    print('length of coast list ', len(coast_points_lol))
    print('Which means ', len(coast_points_lol) / (shape[0]*shape[1])*100,
          ' percent of the grid is a coast point')  # 0.84% of the map is a coastal point
    # print(coast_points_lol)

def _find_loc_pairs(coast_points_lol, xr_obj):

    nlat_array = xr_obj.coords["nav_lat"].values
    nlon_array = xr_obj.coords["nav_lon"].values

    coast_locs_lol = []

    for i in range(len(coast_points_lol)):
        coast_locs_lol.append([nlon_array[coast_points_lol[i][0],
                                          coast_points_lol[i][1]],
                               nlat_array[coast_points_lol[i][0],
                                          coast_points_lol[i][1]],
                               ])
    return coast_locs_lol

@mwr.timeit
def run_look(coastline='america'):
    """
    :param coastline: either america or east-asia
    """
    assert coastline in ['america', 'east-asia']

    if coastline == 'east-asia':
        xr_obj = xr.open_dataset('data/toy/' + cfg.toy_ea_T_file_name )
    else:
        xr_obj = xr.open_dataset('data/new_toy/' + cfg.toy_T_file_name)
        # open_dataset loads lazily, so copes with being provided with unwise
        # large files.

    # xr_slice = xr_obj.isel(time_counter=0).zos
    xr_slice = xr_obj.isel(time_counter=0).sowindsp
    # print(xr_slice.__repr__)

    array = xr.ufuncs.isnan(xr_slice).values

    print(array)
    shape = np.shape(array)

    @mwr.timeit
    @jit(cache=True)
    def return_coastal_lol(tmp_array, shape):
        coast_points_lol = []  # in tmp_array indices:

        print('shape', shape)  # shape (440, 865)

        for i in range(1, shape[0] - 1):
            for j in range(1, shape[1] - 1):
                if not tmp_array[i, j]:
                    if (tmp_array[i - 1, j] or tmp_array[i, j - 1]
                        or tmp_array[i + 1, j] or tmp_array[i, j + 1]):
                        coast_points_lol.append([i, j])

        return coast_points_lol

    coast_points_lol = return_coastal_lol(array, shape)

    _compare_lengths(coast_points_lol, shape)

    coast_locs_lol = _find_loc_pairs(coast_points_lol, xr_obj)

    if coastline == 'america':
        mif.write_csv_file(coast_points_lol, cfg.h_auto_point_list_loc)
        mif.write_csv_file(coast_locs_lol, cfg.h_auto_loc_list_loc)
        slim_american_coast(coast_locs_lol, coast_points_lol, shape)
    else:
        mif.write_csv_file(coast_points_lol, cfg.h_ea_auto_point)
        mif.write_csv_file(coast_locs_lol, cfg.h_ea_auto_loc)

        # _test_plot(coast_locs_lol)
        # _test_plot(coast_points_lol)
        extract_mainland_ea(coast_points_lol, shape, xr_obj)


@mwr.timeit
def extract_mainland_ea(coast_points_lol, shape, xr_obj):
    """
    :param coast_points_lol: coast points lol.
    :param shape: great shape.
    """
    import src.get_points.j_nearest_neighbour_sort as jnn
    coast_points_lol = jnn.return_ordered_lol(0, 4018, coast_points_lol)
    # _test_plot(coast_points_lol)
    _compare_lengths(coast_points_lol, shape)
    # _test_plot(coast_points_lol)

    def slim_lol(input_lol, start, end, break_start, break_end):
        output_lol = []
        for i in range(len(input_lol)):
            if i >= start and i <= end:
                if i <= break_start or i >= break_end:
                    output_lol.append(input_lol[i])
        return output_lol

    coast_points_lol = slim_lol(coast_points_lol, 45, 963, 551, 566)

    _compare_lengths(coast_points_lol, shape)
    # _test_plot(coast_points_lol)

    mif.write_csv_file(coast_points_lol, cfg.h_vin_chin_list)

    coast_locs_lol = _find_loc_pairs(coast_points_lol, xr_obj)
    _test_plot(coast_locs_lol, inverse=False, map=True)

    # 388
    # 193
    # index 838


@mwr.timeit
def slim_american_coast(coast_locs_lol, coast_points_lol, shape):
    """
    > 24 degrees N
    &  if > - 96 degrees E then > 25 degrees North
    & if > - 79.6 degrees E then > 30 degrees North
    & if > - 72 degrees E then > 39 degrees North

    """

    us_point_lol = []
    us_loc_lol = []

    for i in range(len(coast_points_lol)):
        if coast_locs_lol[i][1] > 24:  # good
            if coast_locs_lol[i][0] > -96:  # bad
                if coast_locs_lol[i][1] > 25:  # good
                    if coast_locs_lol[i][0] > -79.6:  # bad
                        if coast_locs_lol[i][1] > 30:  # good
                            if coast_locs_lol[i][0] > -72:  # bad
                                if coast_locs_lol[i][1] > 39:  # good
                                    us_loc_lol.append([coast_locs_lol[i][0], coast_locs_lol[i][1]])
                                    us_point_lol.append([coast_points_lol[i][0], coast_points_lol[i][1]])
                            else:  # good
                                us_loc_lol.append([coast_locs_lol[i][0], coast_locs_lol[i][1]])
                                us_point_lol.append([coast_points_lol[i][0], coast_points_lol[i][1]])
                    else:  # good
                        us_loc_lol.append([coast_locs_lol[i][0], coast_locs_lol[i][1]])
                        us_point_lol.append([coast_points_lol[i][0], coast_points_lol[i][1]])
            else:  # good
                us_loc_lol.append([coast_locs_lol[i][0], coast_locs_lol[i][1]])
                us_point_lol.append([coast_points_lol[i][0], coast_points_lol[i][1]])

    mif.write_csv_file(us_loc_lol, cfg.h_auto_us_loc_list_loc)
    mif.write_csv_file(us_point_lol, cfg.h_auto_us_point_list_loc)

    print('length of coast list ', len(us_point_lol))
    print('Which means ', len(us_point_lol)/(shape[0]*shape[1])*100,
          ' percent of the grid is a US coast point')  # 0.23% of the map is a US coast point
    # print(coast_points_lol)

    slimmed_point_lol = []
    slimmed_loc_lol = []

    for i in range(len(us_point_lol)):

        lat = us_loc_lol[i][1]
        lon = us_loc_lol[i][0]
        x_point = us_point_lol[i][0]
        y_point = us_point_lol[i][1]
        added = False

        if lat < 34.79 or lat > 36.19:  # good
            if lat < 36.93 or lat > 37.15:  # good
                if lat > 36:  # bad
                    if lon > -76:  # good
                        if lat > 37.1:  # bad
                            if lon > -75.94:
                                if lat > 37.4:  # bad
                                    if lon > -75.85:
                                        if lat > 37.63:  # bad
                                            if lon > -75.65:
                                                if lat > 37.73:  # bad
                                                    if lon > -75.57:
                                                        if lat > 40.99:  # bad
                                                            if lon > -72.11:
                                                                if lon > -70.98:  # bad
                                                                    if lat > 41.48:
                                                                        slimmed_loc_lol.append([lon, lat])
                                                                        slimmed_point_lol.append([x_point, y_point])
                                                                        added = True
                                                                else:
                                                                    slimmed_loc_lol.append([lon, lat])
                                                                    slimmed_point_lol.append([x_point, y_point])
                                                                    added = True
                                                        else:
                                                            slimmed_loc_lol.append([lon, lat])
                                                            slimmed_point_lol.append([x_point, y_point])
                                                            added = True
                                                else:
                                                    slimmed_loc_lol.append([lon, lat])
                                                    slimmed_point_lol.append([x_point, y_point])
                                                    added = True
                                        else:
                                            slimmed_loc_lol.append([lon, lat])
                                            slimmed_point_lol.append([x_point, y_point])
                                            added = True
                                else:
                                    slimmed_loc_lol.append([lon, lat])
                                    slimmed_point_lol.append([x_point, y_point])
                                    added = True
                        else:
                            slimmed_loc_lol.append([lon, lat])
                            slimmed_point_lol.append([x_point, y_point])
                            added = True
                else:
                    slimmed_loc_lol.append([lon, lat])
                    slimmed_point_lol.append([x_point, y_point])
                    added = True

        if lat > 40 and added:
            if lon < -76:  # guarantees no great lakes
                slimmed_point_lol.pop()
                slimmed_loc_lol.pop()
                added = False
            if lat > 40.9:
                if lon < -72.83:  # guarantees no long island.
                    slimmed_point_lol.pop()
                    slimmed_loc_lol.pop()
                    added = False
                elif lat > 45:
                    slimmed_point_lol.pop()
                    slimmed_loc_lol.pop()  # guarantees no canadian points
                    added = False

        if lat > 44.14 and lat < 44.58:
            if lon > -69.06 and lon < -68.06 and added:
                slimmed_loc_lol.pop()
                slimmed_point_lol.pop()
                added = False
        if lon > -66.94 and added:
            slimmed_loc_lol.pop()
            slimmed_point_lol.pop()


    mif.write_csv_file(slimmed_loc_lol, cfg.h_slimmed_coast_loc_list_loc)
    mif.write_csv_file(slimmed_point_lol, cfg.h_slimmed_coast_point_list_loc)

    print('length of coast list ', len(slimmed_point_lol))
    print('Which means ', len(slimmed_point_lol)/(shape[0]*shape[1])*100,
          ' percent of the grid is a US coast excluding the bays initially removed')  # 0.17%

    # print(coast_points_lol)
    # xr_obj.zos.sel(time=)
