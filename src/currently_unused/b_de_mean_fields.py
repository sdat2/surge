"""
b_de_mean_fields
"""
import src.utilities.my_wrappers as mwr
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import xarray as xr
from sklearn.linear_model import LinearRegression


@mwr.timeit
def de_mean_fields(name, out_name, field='zos'):
    """
    Inspired by:
    http://xarray.pydata.org/en/stable/examples/weather-data.html

    :param name:
    :param out_name:
    :param field:
    :return:
    """
    xr_obj = xr.load_dataset(name).drop_vars(["area"])
    df = xr_obj.to_dataframe()
    print(df.describe())

    def plot_means():
        xr_obj.mean(dim="point").to_dataframe().plot()
        plt.show()

    def plot_corr_and_hist():
        df_a = xr_obj.to_dataframe()
        sns.pairplot(df_a.reset_index(), vars=["zos", "vos", "uos", "tauvo", "tauuo"])
        plt.show()

    # plot_corr_and_hist()

    climatology_mean = xr_obj.mean("time_centered")
    climatology_std = xr_obj.std("time_centered")
    stand_anomalies = xr.apply_ufunc(
        lambda x, m, s: (x - m) / s,
        xr_obj,
        climatology_mean,
        climatology_std,
    )
    df = stand_anomalies.to_dataframe()
    print(df.describe())
    print(stand_anomalies.__repr__)
    stand_anomalies.to_netcdf(out_name)

    def plot_std_anomalies_mean():
        stand_anomalies.mean(dim="point").to_dataframe().plot()
        plt.show()

    def basic_regression(dfa):
        dfa = dfa.dropna(axis='columns')
        reg = LinearRegression().fit(df.values[:, 3:], df.values[:, 2])
        print('r squared = ', reg.score(df.values[:, 3:], df.values[:, 2]))
        # r squared =  0.010842902064903681 -- nice and bad :)

    # basic_regression(df)










