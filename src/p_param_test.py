import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from uncertainties import ufloat
from uncertainties import umath as um
import scipy.stats as ss
import src.utilities.my_wrappers as mwr
import src.plotting.my_plotting_style as mps
import src.utilities.sobol_functions as sob
import config as cfg
mps.mps_defaults(quality='high')


def _lin(x, a, b):
    """ fit line to data using curve_fit"""
    return (a * x) + b


def _lin_0(x, a):
    """ fit line through zero data using curve_fit"""
    return a*x


@mwr.timeit
def test_params():
    training_set = xr.open_dataset(cfg.data_loc + cfg.merged_2005_file_name)
    test_set = xr.open_dataset(cfg.data_loc + cfg.merged_2004_file_name)
    training_set = test_set
    sowindsp_values = training_set.sowindsp.values
    tauvo_values = training_set.tauvo.values
    tauuo_values = training_set.tauuo.values
    wind_stress_mag = np.zeros(np.shape(tauvo_values))
    wind_stress_mag[:, :] = np.sqrt(np.square(tauuo_values[:, :]) + np.square(tauvo_values))
    assert(np.shape(sowindsp_values) == np.shape(wind_stress_mag))
    sqrt_stress = np.zeros(np.shape(tauvo_values))
    sqrt_stress[:, :] = np.sqrt(wind_stress_mag[:, :])
    # plt.plot(sowindsp_values.ravel()[0:10000], wind_stress_mag.ravel()[0:10000], 'x')
    # cor_mat = np.corrcoef(sowindsp_values.ravel(), sqrt_stress.ravel())
    # print(cor_mat)
    pearr = ss.pearsonr(sowindsp_values.ravel(), sqrt_stress.ravel())[0]
    print(pearr)

    def line_fit():
        popt, pcov = curve_fit(_lin, sowindsp_values.ravel(), sqrt_stress.ravel())
        perr = np.sqrt(np.diag(pcov))

        for i in range(len(popt)):
            print(r'%5.6f pm %5.6f' % tuple([popt[i], perr[i]]))

        popt, pcov = curve_fit(_lin_0, sowindsp_values.ravel(), sqrt_stress.ravel())
        perr = np.sqrt(np.diag(pcov))

        for i in range(len(popt)):
            print(r'%5.6f pm %5.6f' % tuple([popt[i], perr[i]]))

        sqrt_cd = ufloat(0.0383, 0.0001)
        cd = sqrt_cd**2
        print(cd)
        """
        Training Set
        0.9775744594156924
        0.038447 pm 0.000003
        -0.000509 pm 0.000019
        0.038371 pm 0.000002
        
        Test Set
        0.9772930436322036
        0.038268 pm 0.000003
        0.000285 pm 0.000019
        0.038310 pm 0.000002
        
        $ r_p = 0.9774 \pm 0.001,\;\; p<10^{-300}$
        $ m = 0.0383 \pm 0.0001$ 
        $ c_d = 0.001467 \pm 0.000008$
        % error estimated from differences in output between test/training year,
        % measured regression error in either year much smaller than this.
        % The error is propogated using the \texttt{python3.uncertanties} package
    
        """

    num = 1000

    plt.plot(sob.auto_npa_subsampler(sowindsp_values.ravel(), num),
             sob.auto_npa_subsampler(sqrt_stress.ravel(), num), 'x', color='cyan', alpha=0.5)
    plt.ylabel(r'$\sqrt{|\tau|}$ (kg$^{0.5}$ m$^{-0.5}$ s$^{-1}$)')
    plt.xlabel(r'$|U|$ (m s$^{-1})$')
    fig = plt.gcf()
    fig = mps.defined_size(fig, 'sc')
    mps.save_fig_twice(cfg.plots_loc + 'cd_finder')

