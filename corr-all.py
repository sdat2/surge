import numpy as np
import config as cfg
import src.utilities.my_wrappers as mwr
import src.utilities.my_io_functions as mif
import src.utilities.my_miscellaneous_functions as mmf


@mwr.timeit
def correlate_all(input_name=cfg.data_loc+ 'sketreme_tactic_2.json',
                  output_name=cfg.plots_loc + 'skextreme_second_tactic',
                  coastline='america'):
    import scipy.stats as ss

    save_prefix = mif.save_prefix(coastline=coastline)


    com_results_big = np.asarray(mif.read_json_object(cfg.data_loc
                                                      + save_prefix
                                                      +'lin_reg_res.json'),
                                                      dtype='float32')
    print('np.shape(com_results_big)',
          np.shape(com_results_big))
    # 1st index: point
    # 2nd index: 2004, 2005
    # 3rd index: MLR, Huber
    # 4nd index: intercept, coeff1, coeff2,  score_train, score_test
    no_points = np.shape(com_results_big)[0]
    magnitude_array = np.zeros([no_points, 2, 2], dtype='float32')
    adjusted_mag_array = np.zeros([no_points, 2, 2], dtype='float32')
    point_list = range(no_points)
    for i in range(2):
        for j in range(2):
            magnitude_array[:, i, j] = np.sqrt(np.square(com_results_big[:, i, j, 1])
                                               + np.square(com_results_big[:, i, j, 2]))
            adjusted_mag_array[:, i, j] = magnitude_array[:, i, j] * (com_results_big[:, i, j, 4]
                                                                      + com_results_big[:, i, j, 3]) / 2


    json_saved_obj = mif.read_json_object(input_name)

    # fig, axs = plt.subplots(3, sharex=True)
    points = range(len(json_saved_obj["model_location_list"]))

    location_npa = np.asarray(json_saved_obj["model_location_list"])
    scale_npa = np.asarray(json_saved_obj["model_scale_list"])
    shape_npa = np.asarray(json_saved_obj["model_shape_list"])

    loc_scal_rp =  ss.pearsonr(location_npa[:, 0], scale_npa[:, 0]) # r_{p, \mu \sigma}
    scal_shap_rp = ss.pearsonr(scale_npa[:, 0], shape_npa[:, 0]) # r_{p, \sigma \xi}
    shap_loc_rp = ss.pearsonr(shape_npa[:, 0], location_npa[:, 0]) # r_{p, \xi \mu}

    loc_scal_str = r"$r_{p;\; \mu \sigma}=$ "+ r"{0:.2f}".format(loc_scal_rp[0])
    scal_shap_str =r"$r_{p;\; \sigma \xi}=$" + r"{0:.2f}".format(scal_shap_rp[0])
    shap_loc_str = r"$r_{p;\; \xi \mu}=$" + r"{0:.2f}".format(shap_loc_rp[0])

    print(np.shape(location_npa), np.shape(adjusted_mag_array[:, 0, 0]))

    correlation =  ss.pearsonr(location_npa[:, 0], adjusted_mag_array[:, 1, 0])
    print(correlation)


correlate_all()
