import numpy as np
import numpy.ma as ma
import scipy.stats as ss
import matplotlib.pyplot as plt
import cmocean.cm as cmo
import matplotlib.colors as colors
from sklearn.linear_model import LinearRegression, HuberRegressor, RANSACRegressor, TheilSenRegressor
from sklearn import linear_model
import src.plotting.my_plotting_style as mps
import src.utilities.my_wrappers as mwr
import config as cfg
from scipy.optimize import curve_fit
from uncertainties import unumpy as unp
import src.plotting.custom_ticker as ctk
import src.utilities.my_io_functions as mif
import src.f_fourier_transform as fft
# import src.utilities.gaussian_regression_functions as grf
mps.mps_defaults()


def _sinusoid(x, alpha, omega, phi):   # sinusoid with zero mean
    return alpha*np.sin(omega*(x + phi))


def _lin(x, a, b):
    """ fit line to data using curve_fit"""
    return (a * x) + b


def _lin_0(x, a):
    """ fit line through zero data using curve_fit"""
    return a*x


@mwr.timeit
def get_xarray_grid(name_list, coastline='america'):
    """
    :param name_list: a list of xarray locations to load up
    :return:
    """
    save_prefix = mif.save_prefix(coastline=coastline)

    zos = mif.return_field_array(name_list, 'zos', coastline=coastline)
    sowindsp = mif.return_field_array(name_list, 'sowindsp', coastline=coastline)
    tauuo = mif.return_field_array(name_list, 'tauuo', coastline=coastline)
    tauvo = mif.return_field_array(name_list, 'tauvo', coastline=coastline)
    sowindsp_square = np.zeros(np.shape(zos))
    sowindsp_square[:, :] = np.square(sowindsp[:, :])
    no_points = np.shape(zos)[0]
    zos_mean = np.zeros(no_points, dtype='float32')
    print('no_points', no_points)

    year_len = 8760  # hours
    print(np.shape(zos))
    for j in range(no_points):
        zos_mean[j] = np.mean(zos[j, :])
    zos_demeaned = np.zeros(np.shape(zos), dtype='float32')
    for i in range(no_points):
        zos_demeaned[i, :] = zos[i, :] - zos_mean[i]

    def test():
        zos_mean_test = np.zeros(no_points, dtype='float32')
        for j in range(no_points):
            zos_mean_test[j] = np.mean(zos_demeaned[j, :])
            assert(zos_mean_test[j] < 10**(-6))

    test()

    num_hours = np.shape(zos_demeaned)[1]
    hours = range(num_hours)
    zos_demeaned_mean = np.zeros(num_hours, dtype='float32')

    for j in range(num_hours):
        zos_demeaned_mean[j] = np.mean(zos_demeaned[:, j])

    def run_sine_fits():
        popt_lol = [[], [], []]
        perr_lol = [[], [], []]
        for j in range(no_points):
            popt, pcov = curve_fit(_sinusoid, hours, zos_demeaned[j, :], p0=[0.3, 2*np.pi/8300.0,
                                                                             5000])
            perr = np.sqrt(np.diag(pcov))
            for i in range(len(popt)):
                print(r'%5.6f pm %5.6f' % tuple([popt[i], perr[i]]))
                popt_lol[i].append(popt[i])
                perr_lol[i].append(perr[i])

        popt, pcov = curve_fit(_sinusoid, hours, zos_demeaned_mean, p0=[0.3, 2 * np.pi / 8300.0,
                                                                        5000])

        perr = np.sqrt(np.diag(pcov))

        for j in range(len(popt)):
            print(r'%5.6f pm %5.6f' % tuple([popt[j], perr[j]]))

        alpha_unp = unp.uarray(popt_lol[0], perr_lol[0])
        omega_unp = unp.uarray(popt_lol[1], perr_lol[1])
        phi_unp = unp.uarray(popt_lol[2], perr_lol[2])

        fig, axs = plt.subplots(1, 3)
        axs = axs.ravel()
        x = alpha_unp
        y = omega_unp
        axs[0].errorbar(unp.nominal_values(x), unp.nominal_values(y),
                        yerr=unp.std_devs(y), xerr=unp.std_devs(x), fmt='x',
                        color='red',
                        ecolor='grey', markersize=0.5)
        axs[0].set_xlabel('$a$ (m)')
        axs[0].set_ylabel('$\omega$ (rad hr$^{-1}$)')
        x = omega_unp
        y = phi_unp
        axs[1].errorbar(unp.nominal_values(x), unp.nominal_values(y),
                        yerr=unp.std_devs(y), xerr=unp.std_devs(x), fmt='x',
                        color='red',
                        ecolor='grey', markersize=0.5)
        axs[1].set_xlabel('$\omega$ (rad hr$^{-1}$)')
        axs[1].set_ylabel('$\phi$ (hr)')
        x = phi_unp
        y = alpha_unp
        axs[2].errorbar(unp.nominal_values(x), unp.nominal_values(y),
                        yerr=unp.std_devs(y), xerr=unp.std_devs(x), fmt='x',
                        color='red',
                        ecolor='grey', markersize=0.5)
        axs[2].set_xlabel('$\phi$ (hr)')
        axs[2].set_ylabel('$a$ (m)')
        mps.defined_size(fig, 'dcsh')
        mps.save_fig_twice(cfg.plots_loc + 'fits/coefficients_of_sine')
    # run_sine_fits()

    def simple_test_plot():
        up_to = 30
        plt.plot(sowindsp_square[:, :up_to], zos_demeaned[:, :up_to], 'x')
        plt.ylabel('SSH Minus Yearly Mean SSH,  $\eta_i - \overline{\eta}_i$ (m)')
        plt.xlabel('Squared Wind Speed, $|U|^{2}$ (m$^{2}$ s$^{-2}$)')
        plt.show()

    def threshold_test():
        xmin = 'ok'
        thresholds = np.linspace(-2, 2, num=100, dtype='float32')
        pom_corr_list = []
        nom_corr_list = []
        for threshold in thresholds:
            print(threshold)
            zos_pom = ma.masked_less_equal(zos_demeaned, threshold)
            zos_nom = ma.masked_greater_equal(zos_demeaned, threshold)
            # pearr = ss.pearsonr(sowindsp_square.ravel(), zos.ravel())
            # print(pearr)
            corcoeff = ma.corrcoef(sowindsp_square.ravel(), zos_pom.ravel())
            pom_corr_list.append(corcoeff[1][0])
            corcoeff = ma.corrcoef(sowindsp_square.ravel(), zos_nom.ravel())
            nom_corr_list.append(corcoeff[1][0])

        plt.plot(thresholds, pom_corr_list, color='red', label=r'Positive $\eta$ Threshold')
        plt.plot(thresholds, nom_corr_list, color='blue', label=r'Negative $\eta$ Threshold')
        plt.xlim(-2, 2)
        plt.legend()
        plt.xlabel(r'$\Delta \eta$ Threshold (m)')
        plt.ylabel(r'Correlation Coefficient between $|U|^{2}$ and $\eta$')
        fig = plt.gcf()
        mps.defined_size(fig, size='dcsq')
        mps.save_fig_twice(cfg.plots_loc + 'threshold_plot_new')

    # threshold_test()

    print(np.shape(zos_demeaned), np.shape(sowindsp_square))

    def mlr_test(x_train, y_train, x_test, y_test):
        """
        Example output: (Even linear regression is too much)
        training score 0.9584952078123362
        test score 0.12919844477570047
        :param x_train: The training x data (e.g sowindsp_square).
        :param y_train:
        :param x_test:
        :param y_test:
        :return:
        """
        reg = LinearRegression()
        reg.fit(x_train, y_train)
        train_score = reg.score(x_train, y_train)
        test_score = reg.score(x_test, y_test)
        print('training score', train_score)
        print('test score', test_score)

    def run_mlr():
        zos_pom = ma.masked_less_equal(zos_demeaned, 0.01)
        mlr_test(sowindsp_square.T[:year_len, :], zos_pom.T[:year_len, :],
                 sowindsp_square.T[year_len:, :], zos_pom.T[year_len:, :])

    def line_fit(x_train, y_train):

        popt1, pcov1 = curve_fit(_lin, x_train, y_train)
        perr1 = np.sqrt(np.diag(pcov1))

        for i in range(len(popt1)):
            print(r'%5.6f pm %5.6f' % tuple([popt1[i], perr1[i]]))

        popt, pcov = curve_fit(_lin_0, x_train, y_train)
        perr = np.sqrt(np.diag(pcov))

        for i in range(len(popt)):
            print(r'%5.6f pm %5.6f' % tuple([popt[i], perr[i]]))

        return popt[0], perr[0], popt1[0], perr1[0]

    def linear_response(x_train, y_train, x_test, y_test):
        train_mask = y_train.mask
        # print(train_mask)
        test_mask = y_test.mask
        x_train = ma.array(x_train, mask=train_mask)[~train_mask]
        x_test = ma.array(x_test, mask=test_mask)[~test_mask]
        # x_train = x_train.compressed()
        y_train = y_train[~train_mask]
        # print('new length', len(y_train))
        # y_train = y_train.compressed()
        assert(len(x_train) == len(y_train))
        assert(len(x_train) > 0)
        x_test = x_test.compressed()
        y_test = y_test.compressed()
        pearrA = ss.pearsonr(x_train, y_train)[0]
        pearrB = ss.pearsonr(x_test, y_test)[0]
        popt0, perr0, popt1, perr1 = line_fit(x_train, y_train)
        x_test = x_test.reshape(-1, 1)
        y_test = y_test.reshape(-1, 1)
        x_train = x_train.reshape(-1, 1)
        y_train = y_train.reshape(-1, 1)
        print(pearrA, pearrB)
        reg = LinearRegression()
        reg.fit(x_train, y_train)
        train_score = reg.score(x_train, y_train)
        test_score = reg.score(x_test, y_test)
        print(train_score)
        print(test_score)
        return (train_score, test_score, reg.coef_[0], reg.intercept_,
                pearrA, popt0, perr0, popt1, perr1)

    def run_linear_response(threshold=0.01):
        zos_pom = ma.masked_less_equal(zos_demeaned, threshold)  # masking doesn't work at all with sklearn
        results_npa = np.zeros([9, no_points], dtype='float32')
        for i in range(no_points):
            results_npa[:, i] = linear_response(sowindsp_square[i, year_len:], zos_pom[i, year_len:],
                                                sowindsp_square[i, :year_len], zos_pom[i, :year_len])
        point_list = range(np.shape(results_npa)[1])
        fig, axs = plt.subplots(2, sharex=True)
        axs[0].plot(point_list, results_npa[2, :], color='orange',
                    label=r'\texttt{sklearn} LR')
        axs[0].plot(point_list, results_npa[5, :], color='purple',
                    label=r'\texttt{curvefit} LR no intercept')
        axs[0].fill_between(point_list, results_npa[5, :] - results_npa[6, :],
                            results_npa[5, :] + results_npa[6, :],
                            alpha=0.3, color='purple', label='%s $\sigma$ envelope' % str(1))
        axs[0].plot(point_list, results_npa[7, :], color='green',
                    label=r'\texttt{curvefit} LR w. intercept')
        axs[0].fill_between(point_list, results_npa[7, :] - results_npa[8, :],
                            results_npa[7, :] + results_npa[8, :],
                            alpha=0.3, color='green', label='%s $\sigma$ envelope' % str(1))
        axs[0].set_ylabel('LR m value')
        axs[0].legend()
        axs[1].plot(point_list, results_npa[0, :],
                    color='red', label='Train 2005')
        axs[1].plot(point_list, results_npa[1, :],
                    color='blue', label='Test 2004')
        axs[1].set_xlabel('Point Along the Coast')
        plt.xlim(0, len(point_list))
        ctk.add_ticks(coastline=coastline)
        axs[1].set_ylabel('LR Score')
        plt.legend()
        mps.defined_size(fig, 'dcm')
        mps.save_fig_twice(cfg.plots_loc + save_prefix + 'score_plot')  # + str(threshold)+'m')
        return results_npa

    def run_run_linear_response():
        response_analysis_list = []
        for j in [0]:  # [0.00, 0.01, 0.02, 0.03, 0.04, 0.05, 0.1]:
            response_analysis_list.append(run_linear_response(threshold=j).tolist())
        mif.write_json_object(response_analysis_list, cfg.data_loc
                                                      +  save_prefix
                                                      + 'response_analysis.json')

    # run_run_linear_response()

    def example_linear_response(point=172, name='no', title_place='New Orleans'):
        if coastline == 'america':
            save_dir = cfg.plots_loc + 'resp/'
        elif coastline == 'vin-china':
            save_dir = cfg.plots_loc + 'resp_vu/'

        zos_pom = ma.masked_less_equal(zos_demeaned, 0)
        plt.plot(sowindsp_square[point, year_len:],
                 zos_pom[point, year_len:],
                 'x', color='blue', alpha=0.5, label='2004', markersize=0.7)
        plt.plot(sowindsp_square[point, :year_len],
                 zos_pom[point, :year_len],
                 'x', color='red', alpha=0.5, label='2005', markersize=0.7)
        plt.legend()
        plt.xlabel('$|U|^{2}$ (m$^{2}$ s$^{-2}$)')
        plt.ylabel('$\Delta \eta$ (m)')
        plt.title(title_place + ' Responsiveness')
        mps.save_fig_twice(save_dir + save_prefix +'linear_' + name)
        #  plt.show()
        plt.clf()
        plt.plot(np.log(sowindsp[point, year_len:] + 1),
                 np.log(zos_pom[point, year_len:] + 1),
                 'x', color='blue', alpha=0.5,
                 label='2004', markersize=0.7)
        plt.plot(np.log(sowindsp[point, :year_len] + 1),
                 np.log(zos_pom[point, :year_len] + 1),
                 'x', color='red', alpha=0.5,
                 label='2005', markersize=0.7)
        plt.legend()
        plt.title(title_place + ' Responsiveness')
        plt.xlabel('$\log{(|U| + 1)}$')
        plt.ylabel('$\log{(\Delta \eta + 1)}$')
        mps.save_fig_twice(save_dir + save_prefix + 'alt_log_' + name)
        #  plt.show()
        plt.clf()
        plt.plot(np.log(sowindsp[point, year_len:]),
                 np.log(zos_pom[point, year_len:]),
                 'x', color='blue', alpha=0.5,
                 label='2004', markersize=0.7)
        plt.plot(np.log(sowindsp[point, :year_len]),
                 np.log(zos_pom[point, :year_len]),
                 'x', color='red', alpha=0.5,
                 label='2005', markersize=0.7)
        plt.legend()
        plt.title(title_place + ' Responsiveness')
        plt.xlabel('$\log{(|U|)}$')
        plt.ylabel('$\log{(\Delta \eta)}$')
        mps.save_fig_twice(save_dir + save_prefix + 'log_' + name)
        #  plt.show()
        plt.clf()

    def run_points_all_years():
        example_linear_response(point=172, name='no', title_place='New Orleans')
        example_linear_response(point=604, name='ny', title_place='New York')
        example_linear_response(point=357, name='mm', title_place='Miami')
        example_linear_response(point=692, name='bo', title_place='Boston')
        example_linear_response(point=756, name='ep', title_place='Eastport')
        example_linear_response(point=460, name='ch', title_place='Charleston')
        example_linear_response(point=53, name='rp', title_place='Rockport')
        example_linear_response(point=217, name='pe', title_place='Pensecola')
        ctk.add_ticks(coastline=coastline)

    def kurtosis_tests(x_values, y_values):
        excess_kurtosis_0 = ss.kurtosis(x_values)
        kurtosis_test = ss.kurtosistest(x_values)
        print(excess_kurtosis_0, kurtosis_test)
        excess_kurtosis_1 = ss.kurtosis(y_values)
        kurtosis_test = ss.kurtosistest(y_values)
        print(excess_kurtosis_1, kurtosis_test)
        return excess_kurtosis_0, excess_kurtosis_1

    def tau_tau_plot(point=172, name='norlean', zos=zos_demeaned):
        """
        import scipy.stats as ss
        moments_list[i][j, 3] = ss.kurtosis(field_npa_list[i][j, :])
        print(ss.kurtosistest(field_npa[i, :]))
        :param point:
        :param name:
        :return:
        """
        zos_hp = fft.high_pass_grid(zos,
                                    threshold_freq=2)  # I relabel zos_demeaned in a terrible way!!!
        if coastline == 'america':
            save_dir = cfg.plots_loc + 'tau-tau-hp/'
        elif coastline == 'vin-china':
            save_dir = cfg.plots_loc + 'tau-tau-hp-vc/'

        def _3d_lin_func(intercept, coef_1, coef_2, start_z, end_z):
            number_points = 100
            # format x, y, z
            fit_line = np.zeros([3, number_points], dtype='float32')
            fit_line[2, :] = np.linspace(start_z, end_z, number_points)[:]
            fit_line[0, :] = (fit_line[2, :] - intercept)/coef_1
            fit_line[1, :] = (fit_line[2, :] - intercept)/coef_2
            return fit_line

        def run_rmlr(train_year=2004):
            """
            https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.HuberRegressor.html
            :return:
            """
            # Results Array Format
            # 1st index: MLR, Huber, RANSAC
            # 2nd index: intercept, coeff1, coeff2,  score_train, score_test
            results = np.zeros([4, 5], dtype='float32')

            print('place = ', name)
            print('train_year =', train_year)
            print(year_len)
            x_train = np.zeros([year_len, 2])
            y_train = np.zeros(year_len)
            x_test = np.zeros([year_len, 2])
            y_test = np.zeros(year_len)
            if train_year == 2004:
                x_train[:, 0] = tauuo[point, year_len:]
                x_train[:, 1] = tauvo[point, year_len:]
                y_train[:] = zos_hp[point, year_len:]
                x_test[:, 0] = tauuo[point, :year_len]
                x_test[:, 1] = tauvo[point, :year_len]
                y_test[:] = zos_hp[point, :year_len]
            elif train_year == 2005:
                x_test[:, 0] = tauuo[point, year_len:]
                x_test[:, 1] = tauvo[point, year_len:]
                y_test[:] = zos_hp[point, year_len:]
                x_train[:, 0] = tauuo[point, :year_len]
                x_train[:, 1] = tauvo[point, :year_len]
                y_train[:] = zos_hp[point, :year_len]

            huber = HuberRegressor().fit(x_train, y_train)
            linear = LinearRegression().fit(x_train, y_train)
            ransac = RANSACRegressor().fit(x_train, y_train)
            theil = TheilSenRegressor().fit(x_train, y_train)
            l_score = linear.score(x_train, y_train)
            l_gen_score = linear.score(x_test, y_test)
            h_score = huber.score(x_train, y_train)
            h_gen_score = huber.score(x_test, y_test)
            r_score = ransac.score(x_train, y_train)
            r_gen_score = ransac.score(x_test, y_test)
            t_score = theil.score(x_train, y_train)
            t_gen_score = theil.score(x_test, y_test)
            results[1, 4] = h_gen_score
            results[1, 3] = h_score
            results[1, 0] = huber.intercept_
            results[1, 1] = huber.coef_[0]
            results[1, 2] = huber.coef_[1]
            results[0, 4] = l_gen_score
            results[0, 3] = l_score
            results[0, 0] = linear.intercept_
            results[0, 1] = linear.coef_[0]
            results[0, 2] = linear.coef_[1]
            results[2, 4] = r_gen_score
            results[2, 3] = r_score
            results[2, 0] = ransac.estimator_.intercept_
            results[2, 1] = ransac.estimator_.coef_[0]
            results[2, 2] = ransac.estimator_.coef_[1]
            results[3, 4] = t_gen_score
            results[3, 3] = t_score
            results[3, 0] = theil.intercept_
            results[3, 1] = theil.coef_[0]
            results[3, 2] = theil.coef_[1]
            # print('h_score', h_score)
            # print('h_gen_score', h_gen_score)
            # print('l_score', l_score)
            # print('l_gen_score', l_gen_score)
            # print("Huber coefficients:", huber.coef_)
            # print("Linear Regression coefficients:", linear.coef_)
            # print(np.shape(tauuo[point, year_len:]))
            print(results)
            return results

        # plot3d()
        com_results = np.zeros([2, 4, 5], dtype='float32')
        com_results[0, :, :] = run_rmlr()[:, :]
        com_results[1, :, :] = run_rmlr(train_year=2005)[:, :]

        def plot3d():
            from mpl_toolkits.mplot3d import Axes3D
            import matplotlib.pyplot as plt
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.scatter(tauuo[point, year_len:], tauvo[point, year_len:],
                       zos_hp[point, year_len:], marker='x', color='blue', s=0.3)
            ax.scatter(tauuo[point, :year_len], tauvo[point, :year_len],
                       zos_hp[point, :year_len], marker='x', color='red', s=0.3)
            min_z = np.min(zos_hp[point, :])
            max_z = np.max(zos_hp[point, :])
            lin_2004 = _3d_lin_func(com_results[0, 0, 0], com_results[0, 0, 1],
                                    com_results[0, 0, 2], min_z, max_z)
            huber_2004 = _3d_lin_func(com_results[0, 1, 0], com_results[0, 1, 1],
                                      com_results[0, 1, 2], min_z, max_z)
            ransac_2004 = _3d_lin_func(com_results[0, 2, 0], com_results[0, 2, 1],
                                      com_results[0, 2, 2], min_z, max_z)
            theil_2004 = _3d_lin_func(com_results[0, 3, 0], com_results[0, 3, 1],
                                      com_results[0, 3, 2], min_z, max_z)
            lin_2005 = _3d_lin_func(com_results[1, 0, 0], com_results[1, 0, 1],
                                    com_results[1, 0, 2], min_z, max_z)
            huber_2005 = _3d_lin_func(com_results[1, 1, 0], com_results[1, 1, 1],
                                      com_results[1, 1, 2], min_z, max_z)
            ransac_2005 = _3d_lin_func(com_results[1, 2, 0], com_results[1, 2, 1],
                                       com_results[1, 2, 2], min_z, max_z)
            theil_2005 = _3d_lin_func(com_results[1, 3, 0], com_results[1, 3, 1],
                                      com_results[1, 3, 2], min_z, max_z)
            plt.plot(lin_2004[0, :], lin_2004[1, :],
                     lin_2004[2, :], label='MLR 2004 r$^2$={0:.3f}, r$_g^2$= {1:.3f} '.format(com_results[0, 0, 3], com_results[0, 0, 4]))
            plt.plot(huber_2004[0, :], huber_2004[1, :],
                     huber_2004[2, :], label='Huber 2004 r$^2$={0:.3f}, r$_g^2$= {1:.3f} '.format(com_results[0, 1, 3], com_results[0, 1, 4]))
            plt.plot(ransac_2004[0, :], ransac_2004[1, :],
                     ransac_2004[2, :], label='RANSAC 2004 r$^2$={0:.3f}, r$_g^2$= {1:.3f} '.format(com_results[0, 2, 3], com_results[0, 2, 4]))
            plt.plot(theil_2004[0, :], theil_2004[1, :],
                     theil_2004[2, :], label='Theil-Sen 2004 r$^2$={0:.3f}, r$_g^2$= {1:.3f} '.format(com_results[0, 3, 3], com_results[0, 3, 4]))
            plt.plot(lin_2005[0, :], lin_2005[1, :],
                     lin_2005[2, :], label='MLR 2005, r$^2$={0:.3f}, r$_g^2$= {1:.3f} '.format(com_results[1, 0, 3], com_results[1, 0, 4]))
            plt.plot(huber_2005[0, :], huber_2005[1, :],
                     huber_2005[2, :], label='Huber 2005, r$^2$={0:.3f}, r$_g^2$= {1:.3f} '.format(com_results[1, 1, 3], com_results[1, 1, 4]))
            plt.plot(ransac_2005[0, :], ransac_2005[1, :],
                     ransac_2005[2, :], label='RANSAC 2005, r$^2$={0:.3f}, r$_g^2$= {1:.3f} '.format(com_results[1, 2, 3], com_results[1, 2, 4]))
            plt.plot(theil_2005[0, :], theil_2005[1, :],
                     theil_2005[2, :], label='Theil-Sen 2005, r$^2$={0:.3f}, r$_g^2$= {1:.3f} '.format(com_results[1, 3, 3], com_results[1, 3, 4]))
            ax.set_xlabel(r'Tauuo, $\tau_u$ (Pa)')
            ax.set_ylabel(r'Tauvo, $\tau_v$ (Pa)')
            ax.set_zlabel('$\Delta\eta_{\;\; \mathrm{hp}}$ (m)')
            ax.legend(bbox_to_anchor=(0., 1.02, 0.98, .102), loc='lower left',
                      ncol=1, mode="expand", borderaxespad=0.)
            ax.view_init(azim=50, elev=0)
            # plt.show()
            fig = plt.gcf()
            mps.defined_size(fig, size='scm')
            mps.save_fig_twice(cfg.plots_loc + '3d_plots/3d_plot' + save_prefix + name)
            plt.clf()

        plot3d()

        # plot3d()

        def normal_plot():
            # zos_demeaned = ma.masked_less_equal(zos_demeaned, -2)  # fairly much all of the data.
            plt.plot(tauuo[point, year_len:],
                     zos_hp[point, year_len:],
                     'x', color='blue', alpha=0.5,
                     label='2004', markersize=0.7)
            kurtosis_tests(tauuo[point, year_len:],
                           zos_hp[point, year_len:])
            plt.plot(tauuo[point, :year_len],
                     zos_hp[point, :year_len],
                     'x', color='red', alpha=0.5,
                     label='2005', markersize=0.7)
            kurtosis_tests(tauuo[point, :year_len],
                           zos_hp[point, :year_len])
            plt.xlabel(r'Tauuo, $\tau_u$ (Pa)')
            plt.ylabel('$\Delta\eta$ (m)')
            mps.save_fig_twice(save_dir+ save_prefix + name + '-u')
            plt.clf()
            # plt.show()
            plt.plot(tauvo[point, year_len:],
                     zos_hp[point, year_len:],
                     'x', color='blue', alpha=0.5,
                     label='2004', markersize=0.7)
            kurtosis_tests(tauvo[point, year_len:],
                           zos_hp[point, year_len:])
            plt.plot(tauvo[point, :year_len],
                     zos_hp[point, :year_len],
                     'x', color='red', alpha=0.5,
                     label='2005', markersize=0.7)
            kurtosis_tests(tauvo[point, :year_len],
                           zos_hp[point, :year_len])
            plt.xlabel(r'Tauvo, $\tau_v$ (Pa)')
            plt.ylabel('$\Delta\eta$ (m)')
            plt.legend()
            mps.save_fig_twice(save_dir + save_prefix + name + '-v')
            plt.clf()
        return com_results

    tau_tau_plot(point=172, name='norlean')


    def run_tau_tau_plot():
        tau_tau_plot(point=172, name='norlean')
        tau_tau_plot(point=357, name='miami')
        tau_tau_plot(point=217, name='pensecola')
        tau_tau_plot(point=604, name='new-york')
        tau_tau_plot(point=692, name='eastport')
        tau_tau_plot(point=53, name='rockport')
        tau_tau_plot(point=460, name='charleston')
        tau_tau_plot(point=692, name='boston')

    # run_tau_tau_plot()
    # tau_tau_plot(point=357, name='miami')
    # tau_tau_plot()

    def collect_tau_tau_plot():
        com_results_big = np.zeros([no_points, 2, 4, 5], dtype='float32')
        for i in range(no_points):
            print('point', i, 'out of', no_points)
            com_results_big[i, :, :, :] = tau_tau_plot(point=i, name='unknown')[:, :, :]
        mif.write_json_object(com_results_big.tolist(),
                              cfg.data_loc + save_prefix + 'lin_reg_res.json')

    def plot_tau_tau_plot():
        com_results_big = np.asarray(mif.read_json_object(cfg.data_loc
                                                          + save_prefix
                                                          + 'lin_reg_res.json'),
                                     dtype='float32')
        point_list = range(no_points)
        fig, axs = plt.subplots(4, sharex=True)
        # 1st index: point
        # 2nd index: 2004, 2005
        # 3rd index: MLR, Huber
        # 4nd index: intercept, coeff1, coeff2,  score_train, score_test
        axs[0].plot(point_list, com_results_big[:, 0, 0, 1], color='orange', label=r'\texttt{MLR} 2004 C0')
        axs[0].plot(point_list, com_results_big[:, 1, 0, 1], color='red', label=r'\texttt{MLR} 2005 C0')
        axs[0].plot(point_list, com_results_big[:, 0, 1, 1], color='green', label=r'\texttt{Huber} 2004 C0')
        axs[0].plot(point_list, com_results_big[:, 1, 1, 1], color='blue', label=r'\texttt{Huber} 2005 C0')
        axs[0].set_ylabel('LR m0 value (m/Pa)')
        axs[1].plot(point_list, com_results_big[:, 0, 0, 2], color='orange', label=r'\texttt{MLR} 2004 C1')
        axs[1].plot(point_list, com_results_big[:, 1, 0, 2], color='red', label=r'\texttt{MLR} 2005 C1')
        axs[1].plot(point_list, com_results_big[:, 0, 1, 2], color='green', label=r'\texttt{Huber} 2004 C1')
        axs[1].plot(point_list, com_results_big[:, 1, 1, 2], color='blue', label=r'\texttt{Huber} 2005 C1')
        axs[1].set_ylabel('LR m1 value (m/Pa)')
        axs[2].plot(point_list, com_results_big[:, 0, 0, 3], color='orange', label=r'\texttt{MLR} 2004 $r^2$')
        axs[2].plot(point_list, com_results_big[:, 1, 0, 3], color='red', label=r'\texttt{MLR} 2005 $r^2$')
        axs[2].plot(point_list, com_results_big[:, 0, 1, 3], color='green', label=r'\texttt{Huber} 2004 $r^2$')
        axs[2].plot(point_list, com_results_big[:, 1, 1, 3], color='blue', label=r'\texttt{Huber} 2005 $r^2$')
        axs[2].set_ylabel('$r^2$ Fit')
        axs[3].plot(point_list, com_results_big[:, 0, 0, 4], color='orange', label=r'\texttt{MLR} gen 2004 $r^2$')
        axs[3].plot(point_list, com_results_big[:, 1, 0, 4], color='red', label=r'\texttt{MLR} gen 2005 $r^2$')
        axs[3].plot(point_list, com_results_big[:, 0, 1, 4], color='green', label=r'\texttt{Huber} gen 2004 $r^2$')
        axs[3].plot(point_list, com_results_big[:, 1, 1, 4], color='blue', label=r'\texttt{Huber} gen 2005 $r^2$')
        axs[3].set_ylabel('$r^2$ Gen')
        axs[3].set_xlabel('Point Along the Coast')
        plt.xlim(0, no_points)
        mps.label_subplots(axs)
        ctk.add_ticks(coastline=coastline)
        plt.legend()
        mps.defined_size(fig, 'dcsq')
        mps.save_fig_twice(cfg.plots_loc + save_prefix +'rmlr')  # + str(threshold)+'m')

    # collect_tau_tau_plot()
    # plot_tau_tau_plot()

    def tanh_tanh_plot(point=357, name='miami'):
        def test_threshold():
            zos_lp = fft.low_pass_grid(input_grid=zos_demeaned,
                                       threshold_freq=2)
            fft.plot_low_pass_grid(zos_lp, save=False)
            zos_hp = fft.high_pass_grid(input_grid=zos,
                                        threshold_freq=2)
            fft.plot_high_pass_grid(zos_hp, save=False)

        zos_hp = fft.high_pass_grid(input_grid=zos,
                                    threshold_freq=2)

        def tanhinfy(np_array):
            std_dev = np.std(np_array)
            normed = np_array/(2*std_dev)
            return np.tanh(normed)

        zos_hp_tanh = tanhinfy(zos_hp[point, :])
        tauvo_tanh = tanhinfy(tauvo[point, :])
        tauuo_tanh = tanhinfy(tauuo[point, :])
        kurtosis_tests(zos_hp[point, :], zos_hp_tanh)
        kurtosis_tests(tauvo[point, :], tauvo_tanh)
        kurtosis_tests(tauuo[point, :], tauuo_tanh)

    # tanh_tanh_plot(point=172, name='norlean')

    """
    EP 756
    BO 692
    NY 604
    CH 460
    MM 357
    PE 217
    NO 172
    RP 53
    """

    # from scipy import stats
    # import numpy as np
    # https://stackoverflow.com/questions/38421624/python-scipy-linear-regression-with-nan-values
    # X = np.array([0, 1, 2, 3, 4, 5])
    # Y = np.array([np.NaN, 4, 5, 10, 2, 5])
    # Y = np.array([np.NaN, 4, 5, 10, 2, 5])
    # ss.linregress(X, Y)



"""

'gaussian_process_plot'  343.02433 s
'gaussian_process_regression'  108158.11952 s
'plot_mean_of_points_by_time'  108171.08219 s
'get_xarray_grid'  108175.66506 s
Traceback (most recent call last):
  File "run.py", line 209, in <module>
    get_responsiveness()
  File "run.py", line 206, in get_responsiveness
    cfg.data_loc + cfg.merged_2005_file_name])
  File "/Users/simon/surge/src/utilities/my_wrappers.py", line 36, in timed
    result = method(*args, **kw)
  File "/Users/simon/surge/src/e_efficiency_plot.py", line 76, in get_xarray_grid
    popt, pcov = curve_fit(_sinusoid, hours, zos_demeaned[i, :])
  File "/Users/simon/anaconda3/lib/python3.6/site-packages/scipy/optimize/minpack.py", line 768, in curve_fit
    raise RuntimeError("Optimal parameters not found: " + errmsg)
RuntimeError: Optimal parameters not found: Number of calls to function has reached maxfev = 600.

40.96952361870257 KurtosistestResult(statistic=52.50867416236113, pvalue=0.0)
16.368211540907456 KurtosistestResult(statistic=44.33547771487745, pvalue=0.0)
13.99216019057664 KurtosistestResult(statistic=42.73959797140647, pvalue=0.0)
8.363873578632981 KurtosistestResult(statistic=37.13317913095438, pvalue=8.192144767005189e-302)
4.986580607909987 KurtosistestResult(statistic=31.056517975894277, pvalue=9.316165700007566e-212)
8.363873578632981 KurtosistestResult(statistic=37.13317913095438, pvalue=8.192144767005189e-302)
10.786288168866621 KurtosistestResult(statistic=39.97143114205525, pvalue=0.0)
16.368211540907456 KurtosistestResult(statistic=44.33547771487745, pvalue=0.0)

"""
