import pandas as pd
from copy import deepcopy
import json
from urllib.request import urlopen
from urllib.parse import quote
import geopy.distance
import random
import datetime
from fitting import _make_data_plot
import numpy as np


def load_stations(station_id='', station_query='', limit=10000, river_name='', town_name=''):
    limit = f'&_limit={limit}'
    if station_query:
        query = f'catchmentName={quote(station_query)}'
    elif river_name:
        query = f'riverName={quote(river_name)}'
    elif town_name:
        query = f'town={quote(town_name)}'
    else:
        query = ''

    url = f"http://environment.data.gov.uk/flood-monitoring/id/stations?{query}{limit}"
    print(f"Retrieving: {url}")
    result = json.load(urlopen(url))
    print(f"{len(result['items'])} results loaded.")
    return result


def load_station(station_id):
    url = f"http://environment.data.gov.uk/flood-monitoring/id/stations/{station_id}"
    print(f"Retrieving: {url}")
    result = json.load(urlopen(url))
    return result


def load_readings(station_id='', since='2018-01-01', limit=10000):
    if station_id:
        print('Loading data...')
        return json.load(urlopen(
            f"http://environment.data.gov.uk/flood-monitoring/id/stations/{station_id}/readings?since={since}&_limit={limit}&_sorted"))


class Map(object):
    def __init__(self):
        self._points = []

    def add_point(self, coordinates, label='', tooltip=''):
        self._points.append((coordinates, label, tooltip))

    def __str__(self):
        centerLat = sum((x[0][0] for x in self._points)) / len(self._points) if self._points else 0
        centerLon = sum((x[0][1] for x in self._points)) / len(self._points) if self._points else 0
        centerLat, centerLon = (52.2053, 0.1218)
        markersCode = "\n".join(
            [f"""new google.maps.Marker({{
                position: new google.maps.LatLng({x[0]}, {x[1]}),
                map: map,
                title: "{tooltip}",
                label: "{label}",
                }});""" for x, label, tooltip in self._points
             ])
        return """
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAcI7vGjE113RP1d4KhzI5vjfvb10JLgzI&v=3.exp&sensor=false"></script>
            <div id="map-canvas" style="height: 100%; width: 100%"></div>
            <script type="text/javascript">
                var map;
                function show_map() {{
                    map = new google.maps.Map(document.getElementById("map-canvas"), {{
                        zoom: 12,
                        scaleControl: true,
                        center: new google.maps.LatLng({centerLat}, {centerLon})
                    }});
                    {markersCode}
                }}
                google.maps.event.addDomListener(window, 'load', show_map);
            </script>
        """.format(centerLat=centerLat, centerLon=centerLon,
                   markersCode=markersCode)


def get_clean_stations(result):
    clean = []
    for station in result['items']:
        if 'label' not in station.keys():
            continue
        if 'lat' not in station.keys():
            continue

        if type(station['label']) == list or type(station['lat']) == list:

            skip = False
            for key in ('label', 'lat', 'long', 'notation'):
                if type(station[key]) == list:
                    if len(station[key]) == 0:
                        skip = True
            if skip:
                continue

            newstation = deepcopy(station)
            for key in ('label', 'lat', 'long', 'notation'):
                if type(station[key]) == list:
                    station[key] = station[key][0]
                    newstation[key] = newstation[key][1:]
            
            result['items'].append(newstation)
        
        clean.append(station)
    
    result['items'] = clean
    return result


camStationLabels = [
    'E21026',
    'E24102',
    'E61301',
    'E60101', # Baites Bite
    'E23983',
    'E60501', # Jesus Lock
    'E60502',
    'E24028',
    'E21732', # Trumpington
]


cotswoldsStationLabels = [
    '1029TH',
    "1090_w2TH",
    "1210TH",
    "0130TH",
    "0190TH",
    "1099TH",
    "0470TH",
    "0701TH",
    "0913TH",
    "0260TH",
    "1289TH",
    "0630TH",
    "0660TH",
    "1102TH",
    "1020TH",
    "0490TH",
    "1082TH",
    "0903TH",
    "0890TH",
    "0680TH",
    "0278TH",
    "0911TH",
    "1208TH",
    "1290_w1TH",
    "1238TH",
    "0569TH",
    "0530TH",
    "0290TH",
    "1090_w1TH",
    "0898TH",
    "1100TH",
    "0155TH",
    # "0880TH",
    "0144TH",
    "1290_w2TH",
    "1080TH",
]


if __name__ == '__main__':

    print("Loading data... ", end='')
    data = pd.read_pickle('data/data.pkl')
    data.dropna(inplace=True)
    data['days'] = data['seconds'] * 1 / (24*60*60)  # Convert to days
    print('Done.')
    dayCutOff = 700

    # Make a map of stations around Cambridge
    map = Map()
    for stat in cotswoldsStationLabels:
        info = load_station(station_id=stat)
        station = info['items']
        lat = station['lat']
        lon = station['long']
        map.add_point((lat + (random.random()-0.5) * 1e-4,
                    lon + (random.random()-0.5) * 1e-4),
                    station['notation'], station['label'])

        station_data = data[data['measure'].str.contains(stat)]
        if len(station_data) == 0:
            raise ValueError(f'No data for station with id {station_id}')

        # Threshold the data at a certain date
        station_data = station_data[station_data['days'] > dayCutOff]

        # nth = len(station_data) // maxpoints * 2
        # station_data = station_data[::nth]

        X = station_data['days'].values[None, :].T
        y = station_data['value'].values[None, :].T

        _make_data_plot(y, X, info, f'COT-{stat}.pdf')
    with open("Cotswolds.html", "w") as out:
                print(map, file=out)

    # # Scrape all the historic data for the locks above for the last ten years:
    # today = datetime.date.today()
    # today = datetime.date(day=8, month=4, year=2018)

    # for daysAgo in range(1, 200):
    #     date = today - datetime.timedelta(days=daysAgo)
    #     url = f"http://environment.data.gov.uk/flood-monitoring/archive/readings-{date.isoformat()}.csv"

    #     print(f"Downloading url: {url}")    
    #     data = pd.read_csv(url, dtype={"dateTime": str, "measure": str, "value": str})

    #     data = data[data['measure'].str.contains('|'.join(camStationLabels + cotswoldsStationLabels))]
    #     data['measure'] = data['measure'].str.replace(
    #         'http://environment.data.gov.uk/flood-monitoring/id/measures/', '')

    #     data.to_pickle(f'data/CAM+COTSWOLDS-{date.isoformat()}.pkl')
