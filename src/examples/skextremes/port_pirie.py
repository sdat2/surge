import numpy as np
import skextremes as ske
import matplotlib.pyplot as plt
import src.plotting.my_plotting_style as mps
mps.mps_defaults()

# https://kikocorreoso.github.io/scikit-extremes/
# https://kikocorreoso.github.io/scikit-extremes/User%20guide.html

# Some datasets are included in the package. For example, we will use sea level data from Port Pirie, in Australia.
data = ske.datasets.portpirie()
print(data.description)
# As can be seen from the description of the dataset we have two fields,
#  sea_level, with the annual maximum sea level records, and year, indicating the year of the record.
# To get the dataset we can use the .asarray method to obtain all the fields as a numpy.array
#  or just select a field to get a 1D numpy.array with the records for the field.
data_array = data.asarray()
sea_levels = data.fields.sea_level
print(sea_levels)
print(np.shape(sea_levels))
# We have several options,
# located in the skextremes.models subpackage, to calculate extreme values.
#  Depending the chosen model some methods and attributes will be available.
#
# Models are divided in several packages:
# wind: Some very basic functions to calculate wind extreme values obtained from the Wind Energy industry.
# These are very basic approximations based on parameters of wind data and should only be used \
# to calculate the expected wind speed value for a 50 years return period.
# engineering: Basic models found in the literature based, mainly,
# on the Gumbel distribution and the Block Maxima approach.
# classic: A more classical approach from the theory to obtain extreme values
# using fitting accepted distributions generally used in the extreme value theory.
model = ske.models.engineering.Lieblein(sea_levels)
model.plot_summary()
plt.show()
print(model.c, model.loc, model.scale)

model = ske.models.classic.GEV(sea_levels,
                               fit_method='mle',
                               ci=0.05,
                               ci_method='delta')
print(model.params)
model.plot_summary()
plt.show()
