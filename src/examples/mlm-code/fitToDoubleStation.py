from sklearn.gaussian_process.kernels import *
from sklearn.gaussian_process import GaussianProcessRegressor
import sklearn.gaussian_process as gps
from matplotlib import pyplot as plt
from scipy.spatial.distance import cdist
import numpy as np
import pandas as pd
from urllib.request import urlopen
import json
import shared
from copy import deepcopy
import utils
import datetime
import matplotlib
from DoubleRBFKernel import DoubleRBFKernel


print("Loading data...")
data = pd.read_pickle('data/data.pkl')
data.dropna(inplace=True)
data['days'] = data['seconds'] * 1 / (24*60*60) # Convert to days


ν = 0.01
σ = 0.01
λ = 0.00001
λ2 = 0.00001
p = 1


kernel = ν**2 * RBF(length_scale=λ, length_scale_bounds = (1e-5,1e5)) + \
         WhiteKernel(noise_level=σ, noise_level_bounds = (1e-5, 1e5))


kernel = ν**2 * ExpSineSquared(length_scale=λ, periodicity=p) \
    * RBF(length_scale=λ2) + \
        WhiteKernel(noise_level=σ, noise_level_bounds=(1e-5, 1e5))


kernel = DoubleRBFKernel(
    length_scale=0.0001, sigma=0.0001, nu=0.0001, rho=0.0001)
    
# ----------------------------------------------------------------------
