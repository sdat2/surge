"""

https://rabernat.github.io/research_computing_2018/
maps-with-cartopy.html

"""
import numpy as np
import xarray as xr
import cartopy.crs as ccrs
import cartopy
import matplotlib
import matplotlib.pyplot as plt
import cartopy.feature as cfeat
import src.plotting.my_plotting_style as mps
import src.utilities.my_io_functions as mif
import config as cfg
if not cfg.local:
    matplotlib.use('agg')
mps.mps_defaults()


def create_nice_map(extent=[-100, -62, 23, 45]):
    fig, ax = plt.subplots(subplot_kw={'projection': ccrs.PlateCarree()})
    # extent = [-100, -40, 10, 40]
    ax.set_extent(extent)
    ax.gridlines(linewidth=0.5,
                 draw_labels=True,
                 #collection_kwargs={'xlabels_top': None, 'ylabels_right': None}
                 )
    ax.coastlines(resolution='50m', linewidth=0.5)  # 50m and 10m  also valid options
    # ax.add_feature(cfeat.LAKES, facecolor='none', edgecolor='black', linewidth=0.5)

    fig = plt.gcf()
    fig = mps.defined_size(fig, size='double_column_square')

    lakes = cfeat.NaturalEarthFeature(category='physical',
                                      name='lakes',
                                      scale='50m',
                                      facecolor='none',
                                      edgecolor='black',
                                      )
    ax.add_feature(lakes, linewidth=0.5)

    ax.add_feature(cfeat.BORDERS, linestyle=':',  # scale='50m',
                   edgecolor='grey', facecolor='none')

    # Create a feature for States/Admin 1 regions at 1:50m from Natural Earth
    states_provinces = cfeat.NaturalEarthFeature(category='cultural',
                                                 name='admin_1_states_provinces_lines',
                                                 scale='50m',
                                                 facecolor='none')
    ax.add_feature(states_provinces, edgecolor='gray', linewidth=0.2)

    plt.tight_layout()


def add_places():
    import src.plotting.places as plc
    places_d = plc.place_d
    for key in places_d:
        # plt.plot(places_d[k['loc_pair'][0], places_d['loc_pair'][1], label=key)
        plt.scatter(places_d[key]['loc_pair'][0], places_d[key]['loc_pair'][1],
                    label=places_d[key]['acron'] + ': ' + key,
                    s=100, alpha=1, linewidth=1.5, marker='+')
        plt.text(places_d[key]['loc_pair'][0] - 1.5,
                 places_d[key]['loc_pair'][1] + 0.2,
                 places_d[key]['acron'],
                 fontsize=6)


def add_example_points():
    ny_lon, ny_lat = -75, 41
    delhi_lon, delhi_lat = -96, 24.61
    plt.plot([ny_lon, delhi_lon], [ny_lat, delhi_lat],
             color='blue', linewidth=2, marker='o',
             transform=ccrs.Geodetic(),
             )
    # plt.show()


# create_nice_map()
# add_example_points()


def add_plot_yin_stations(legend=True):
    csv_lol = mif.read_csv_file(cfg.yin_stations_full_path,
                                to_string_list=[2, 3])
    if not legend:
        x_list = []
        y_list = []
        for i in range(len(csv_lol)):
            x_list.append(csv_lol[i][0])
            y_list.append(csv_lol[i][1])
        plt.scatter(x_list, y_list, marker='+', s=100)
    else:
        colour_list = mps.replacement_color_list(len(csv_lol))
        for i in range(len(csv_lol)):
            plt.scatter(csv_lol[i][0], csv_lol[i][1], marker='+',
                        label=csv_lol[i][2]+', '+csv_lol[i][3],
                        c=colour_list[i], s=100, alpha=1, linewidth=1.5)
            plt.legend()
    plt.savefig(cfg.yin_plot_path_sans_suffix + '.png', bbox_inches='tight')
    plt.savefig(cfg.yin_plot_path_sans_suffix + '.pdf', bbox_inches='tight')


def add_plot_possible_locations():
    csv_lol = mif.read_csv_file(cfg.init_points_full_path)
    x_list = []
    y_list = []
    for i in range(len(csv_lol)):
        x_list.append(csv_lol[i][0])
        y_list.append(csv_lol[i][1])
    plt.scatter(x_list, y_list, marker='+',
                s=100, linewidth=1, cmap='viridis')
    plt.show()


def add_coast_points_mark1():
    csv_lol = mif.read_csv_file(cfg.h_auto_loc_list_loc)
    x_list = []
    y_list = []
    for i in range(len(csv_lol)):
        x_list.append(csv_lol[i][0])
        y_list.append(csv_lol[i][1])
    plt.scatter(x_list, y_list, marker='+', s=10,
                linewidth=1, label='Coastal T-Grid Cells')
    plt.legend()
    plt.show()
    #plt.savefig(cfg.coast_cell_mark1_sans_suffix + '.png', bbox_inches='tight')
    #plt.savefig(cfg.coast_cell_mark1_sans_suffix + '.pdf', bbox_inches='tight')


def add_coast_points_mark_two():
    csv_lol = mif.read_csv_file(cfg.h_auto_us_loc_list_loc)
    x_list = []
    y_list = []
    for i in range(len(csv_lol)):
        x_list.append(csv_lol[i][0])
        y_list.append(csv_lol[i][1])
    plt.scatter(x_list, y_list, marker='+', s=10, linewidth=1, label='Coastal T-Grid Cells')
    plt.legend()
    plt.show()
    # plt.savefig(cfg.us_coast_mark_two_sans_suffix + '.png', bbox_inches='tight')
    # plt.savefig(cfg.us_coast_mark_two_sans_suffix + '.pdf', bbox_inches='tight')


def add_coast_points_mark_three():
    csv_lol = mif.read_csv_file(cfg.h_slimmed_coast_loc_list_loc)
    x_list = []
    y_list = []
    for i in range(len(csv_lol)):
        x_list.append(csv_lol[i][0])
        y_list.append(csv_lol[i][1])
    plt.scatter(x_list, y_list, marker='+', s=10, linewidth=1, label='Coastal T-Grid Cells')
    plt.legend()
    plt.savefig(cfg.us_slimmed_coast_mark_three_sans_suffix + '.png', bbox_inches='tight')
    plt.savefig(cfg.us_slimmed_coast_mark_three_sans_suffix + '.pdf', bbox_inches='tight')
    plt.show()


def plot_nc_points():
    # h_nc_points_sans_suffix
    point_set = xr.load_dataset(cfg.toy_loc + cfg.toy_point_nc)
    lat_npa = point_set.coords['nav_lat'].values
    lon_npa = point_set.coords['nav_lon'].values
    plt.plot(lon_npa, lat_npa, 'g+-', markersize=10, label='Coastal T-Grid Cells')
    plt.legend()
    plt.savefig(cfg.h_nc_points_sans_suffix + '.png', bbox_inches='tight')
    plt.savefig(cfg.h_nc_points_sans_suffix + '.pdf', bbox_inches='tight')
    plt.show()


create_nice_map()
# add_plot_yin_stations()
# add_plot_possible_locations()
# add_coast_points_mark1()
# add_coast_points_mark_two()
# add_coast_points_mark_three()
# plot_nc_points()
