import config as cfg

def run_bam():
    import src.extreme.block_maxima as bam
    # bam.find_yearly_max(input_name=cfg.data_loc+cfg.vc_merged_1950_name,
    #                     output_name=cfg.data_loc+'vc_block_max.json')
    # bam.skextreme_tactic_1()
    # bam.tactic_1_plot()
    # bam.skextreme_tactic_2(input_name=cfg.data_loc + 'vc_block_max.json',
    #                        output_name=cfg.data_loc + 'vc_sketreme_tactic_2.json')
    if True:
        bam.tactic_2_plot()

        bam.tactic_2_plot(input_name=cfg.data_loc + 'vc_sketreme_tactic_2.json',
                          output_name=cfg.plots_loc + 'vc_skextreme_second_tactic',
                          coastline='vin-china')

    bam.plot_individual_both()

run_bam()
