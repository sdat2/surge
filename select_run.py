import src.utilities.my_wrappers as mwr
import config as cfg


@mwr.timeit
def run_in_seq():  # takes roughly 100 seconds
    import src.get_points.h_look_for_coast_points as hlc
    hlc.run_look()
    import src.get_points.j_nearest_neighbour_sort as jnn
    jnn.run_sort()
    import src.get_points.f_select_points as fsp
    fsp.test_for_single_netcdf(cfg.new_toy_loc
                               + 'nemo_ba130o_1d_20050801-20050901_grid-T.nc',
                               cfg.new_toy_loc
                               + 'selected_daily_points_august_05.nc')


def east_asia_single_test():
    import src.get_points.f_select_points as fsp

    fsp.test_for_single_netcdf(cfg.toy_loc
                               + cfg.toy_ea_T_file_name,
                               cfg.toy_loc
                               + 'test-vin-china.nc',
                               coastline='vin-china')

    fsp.test_for_single_netcdf("/Users/simon/surge/data/ea_toy/"
                               + "nemo_ba130o_1d_20050801-20050901_grid-T.nc",
                               "/Users/simon/surge/data/ea_toy/"
                               + 'test_new-vin-china.nc',
                               coastline='vin-china')

# east_asia_single_test()


def incompatible():
    import src.get_points.f_select_points as fsp
    fsp.select_month_xr([cfg.toy_loc + cfg.big_T_file_name,
                         cfg.toy_loc + cfg.big_U_file_name,
                         cfg.toy_loc + cfg.big_V_file_name],
                        cfg.toy_loc + cfg.toy_point_nc)
    import src.get_points.g_plot_datapoints as gpd
    gpd.create_nice_map()
    gpd.plot_nc_points()


@mwr.timeit
def extract_points(months_to_process, year=2004):
    import src.get_points.f_select_points as fsp
    if year == 2004:
        year_list = cfg.prefix_list_2004
    elif year == 2005:
        year_list = cfg.prefix_list_2005
    for value, prefix in enumerate(year_list):
        if value in months_to_process:
            pref_path = cfg.jasmin_day_small + prefix
            fsp.select_month_xr([pref_path+'T.nc',
                                 pref_path+'U.nc',
                                 pref_path+'V.nc'],
                                cfg.jasmin_day_point
                                + prefix
                                + cfg.combined_suffix_nc)


@mwr.timeit
def extract_vin_chin_points(months_to_process, year=2004):
    import src.get_points.f_select_points as fsp
    if year == 2004:
        year_list = cfg.prefix_list_2004
    elif year == 2005:
        year_list = cfg.prefix_list_2005
    for value, prefix in enumerate(year_list):
        if value in months_to_process:
            pref_path = cfg.jasmin_ea_small + prefix
            fsp.select_month_xr([pref_path+'T.nc',
                                 pref_path+'U.nc',
                                 pref_path+'V.nc'],
                                cfg.jasmin_vc_point
                                + prefix
                                + cfg.combined_suffix_nc,
                                coastline='vin-china')
    if year == 2004:
        year_list = cfg.day_prefixes_2004
    elif year == 2005:
        year_list = cfg.day_prefixes_2005
    for value, prefix in enumerate(year_list):
        if value in months_to_process:
            pref_path = cfg.jasmin_ea_day_small + prefix
            fsp.select_month_xr([pref_path+'T.nc',
                                 pref_path+'U.nc',
                                 pref_path+'V.nc'],
                                cfg.jasmin_vc_day_point
                                + prefix
                                + cfg.combined_suffix_nc,
                                coastline='vin-china')

# extract_vin_chin_points(range(0, 12), year=2004)
# extract_vin_chin_points(range(0, 12), year=2005)


@mwr.timeit
def slim_days():
    """
    'slim_file'  51.42366 s

    'slim_file'  17.36974 s

    'slim_file'  16.55540 s

    :return:
    """
    import src.get_points.a_slim_only as aso
    aso.run_through_folder(cfg.jasmin_day_big, cfg.jasmin_day_small)


# slim_days()
@mwr.timeit
def slim_1950():
    import src.get_points.a_slim_only as aso
    aso.run_through_folder(cfg.control_1950_source, cfg.small_1950, relabel=True)


# slim_1950()

@mwr.timeit
def pointwise_1950(min_inclusive, max_exclusive):
    import src.get_points.f_select_points as fsp
    fsp.run_through_1950_folder(cfg.small_1950, cfg.point_1950,
                                min_inclusive, max_exclusive)

# pointwise_1950(0, 10)
# pointwise_1950(10, 30)

def merge_1950():
    # might make rather a large merged file at the end.
    import src.get_points.m_merge_year_points as myp
    myp.run_through_1950_folder(cfg.point_1950, cfg.data_loc, cfg.merged_1950_name)


# merge_1950()


@mwr.timeit
def extract_points_day(months_to_process, year=2004):
    import src.get_points.f_select_points as fsp
    if year == 2004:
        year_list = cfg.day_prefixes_2004
    elif year == 2005:
        year_list = cfg.day_prefixes_2005
    for value, prefix in enumerate(year_list):
        if value in months_to_process:
            pref_path = cfg.jasmin_day_small + prefix
            fsp.select_month_xr([pref_path+'T.nc',
                                 pref_path+'U.nc',
                                 pref_path+'V.nc'],
                                cfg.jasmin_day_point
                                + prefix
                                + cfg.combined_suffix_nc)


@mwr.timeit
def extract_2004():
    extract_points_day([0], year=2004)  # 1
    extract_points_day([1, 2, 3], year=2004)  # 2
    extract_points_day([4, 5, 6], year=2004)  # 3
    extract_points_day([7, 8, 9], year=2004)  # 4
    extract_points_day([10, 11], year=2004)  # 5


# extract_2004()


@mwr.timeit
def extract_2005():
    extract_points_day([0], year=2005)  # 1
    extract_points_day([1, 2, 3], year=2005)  # 2
    extract_points_day([4, 5, 6], year=2005)  # 3
    extract_points_day([7, 8, 9], year=2005)  # 4
    extract_points_day([10, 11], year=2005)  # 5


# extract_2005()


@mwr.timeit
def run_day_merge(year=2004):
    import src.get_points.m_merge_year_points as myp
    if year == 2004:
        pref_list = cfg.day_prefixes_2004
        name = cfg.day_2004_file_name
    elif year == 2005:
        pref_list = cfg.day_prefixes_2005
        name = cfg.day_2005_file_name
    myp.run_merge([cfg.jasmin_day_point
                   + x + cfg.combined_suffix_nc for x in pref_list],
                  cfg.data_loc + name)


@mwr.timeit
def run_vc_merge(year=2004):
    import src.get_points.m_merge_year_points as myp
    if year == 2004:
        pref_list = cfg.day_prefixes_2004
        name = cfg.day_vc_2004_file_name
    elif year == 2005:
        pref_list = cfg.day_prefixes_2005
        name = cfg.day_vc_2005_file_name
    myp.run_merge([cfg.jasmin_vc_day_point
                   + x + cfg.combined_suffix_nc for x in pref_list],
                  cfg.data_loc + name)
    if year == 2004:
        pref_list = cfg.prefix_list_2004
        name = cfg.merged_vc_2004_file_name
    elif year == 2005:
        pref_list = cfg.prefix_list_2005
        name = cfg.merged_vc_2005_file_name
    myp.run_merge([cfg.jasmin_vc_point
                   + x + cfg.combined_suffix_nc for x in pref_list],
                  cfg.data_loc + name)
    if year == 2005:
        myp.run_through_1950_folder(cfg.vc_point_1950, cfg.data_loc, cfg.vc_merged_1950_name)


# run_vc_merge(year=2004)
# run_vc_merge(year=2005)
# run_in_seq()   ##  tmux new -s points


@mwr.timeit
def run_merge(year=2004):
    import src.get_points.m_merge_year_points as myp
    if year == 2004:
        pref_list = cfg.prefix_list_2004
        name = cfg.merged_2004_file_name
    elif year == 2005:
        pref_list = cfg.prefix_list_2005
        name = cfg.merged_2005_file_name
    myp.run_merge([cfg.sep_point_dir
                   + x + cfg.combined_suffix_nc for x in pref_list],
                  # cfg.combined_point_dir +
                  name)

# run_merge(year=2004)
# run_merge(year=2005)
# run_merge()


def slim_east_asia():
    """ This Function should do all the slimming neccesary to extend the
        project to a comparitive project with east asia """
    import src.get_points.a_slim_only as aso
    # aso.slim_file(output_dir=cfg.ea_toy_loc, coastline='east-asia')
    aso.run_through_folder(cfg.jasmin_init_subdir,
                           cfg.jasmin_ea_small,
                           coastline='east-asia')
    aso.run_through_folder(cfg.jasmin_day_big,
                           cfg.jasmin_ea_day_small,
                           coastline='east-asia')
    aso.run_through_folder(cfg.control_1950_source,
                           cfg.ea_small_1950,
                           coastline='east-asia',
                           relabel=True)

# slim_east_asia()
def slim_vas():
    import src.get_points.a_slim_only as aso
    aso.run_through_folder(cfg.control_1950_vas,
                           cfg.vas_small_1950,
                           relabel=True)
    aso.run_through_folder(cfg.control_1950_vas,
                           cfg.ea_vas_small_1950,
                           coastline='east-asia',
                           relabel=True)

slim_vas()


def slim_uas():
    import src.get_points.a_slim_only as aso
    aso.run_through_folder(cfg.control_1950_uas,
                           cfg.uas_small_1950,
                           relabel=True)
    aso.run_through_folder(cfg.control_1950_uas,
                           cfg.ea_uas_small_1950,
                           coastline='east-asia',
                           relabel=True)


# slim_uas()


def reslim_jasmin_files():
    import src.get_points.a_slim_only as aso
    # aso.run_through_folder(cfg.new_toy_loc, cfg.test_loc)
    aso.run_through_folder(cfg.jasmin_init_subdir,
                           cfg.jasmin_small_subdir)

# reslim_jasmin_files()


def create_new_point_list():
    import src.get_points.h_look_for_coast_points as hlp
    hlp.run_look()



def slim_bathy():
    import src.get_points.a_slim_only as aso
    aso.slim_bath(coastline='east-asia',
                  output_path=cfg.slim_ea_bath_path)
    aso.slim_bath()


# slim_bathy()
