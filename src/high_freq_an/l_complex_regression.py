import numpy as np
# from numba import jit
import matplotlib.pyplot as plt
import matplotlib
import cmocean.cm as cmo
import matplotlib.colors as colors
import src.plotting.my_plotting_style as mps
import src.utilities.my_wrappers as mwr
from sklearn.linear_model import LinearRegression, HuberRegressor, RANSACRegressor, TheilSenRegressor
import config as cfg
import src.plotting.custom_ticker as ctk
import src.utilities.my_io_functions as mif
import src.f_fourier_transform as fft
# import src.utilities.gaussian_regression_functions as grf
mps.mps_defaults()


@mwr.timeit
# @jit
def regress_complex(name_list):
    zos = mif.return_field_array(name_list, 'zos')
    tauuo = mif.return_field_array(name_list, 'tauuo')
    tauvo = mif.return_field_array(name_list, 'tauvo')
    no_points = np.shape(zos)[0]
    zos_mean = np.zeros(no_points, dtype='float32')
    print(np.shape(zos))
    for j in range(no_points):
        zos_mean[j] = np.mean(zos[j, :])
    zos_demeaned = np.zeros(np.shape(zos), dtype='float32')
    for i in range(no_points):
        zos_demeaned[i, :] = zos[i, :] - zos_mean[i]

    zos_hp = fft.high_pass_grid(zos, threshold_freq=2)

    num_hours = np.shape(zos_demeaned)[1]
    year_len = num_hours // 2

    # Regression Variables:
    # Inputs:
    # tauuo
    # :and: tauvo
    # :and: zos previous timestep
    # :and: zos climateological
    # Target:
    # Zos Change
    # :or: Actual Zos (will probably just learn persistence model).

    zos_prev = np.zeros([np.shape(zos)[0], np.shape(zos)[1]-1])
    zos_diff_targ = np.zeros([np.shape(zos)[0], np.shape(zos)[1]-1])
    tauvo_short = np.zeros([np.shape(zos)[0], np.shape(zos)[1]-1])
    tauuo_short = np.zeros([np.shape(zos)[0], np.shape(zos)[1]-1])

    tauuo_short[:, :] = tauuo[:, :np.shape(zos)[1]-1]
    tauvo_short[:, :] = tauvo[:, :np.shape(zos)[1]-1]
    zos_prev[:, :] = zos_hp[:, :np.shape(zos)[1]-1]

    for i in range(np.shape(zos)[1]-1):
        zos_diff_targ[:, i] = zos[:, i+1] - zos[:, i]

    @mwr.timeit
    def run_rmlr(point=100, train_year=2004):
        """
        https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.HuberRegressor.html
        :return:
        """
        # Results Array Format
        # 1st index: MLR, Huber, RANSAC
        # 2nd index: intercept, coeff1, coeff2,  score_train, score_test
        results = np.zeros([4, 6], dtype='float32')

        print('train_year =', train_year)
        print(year_len)
        if train_year == 2004:
            x_train = np.zeros([year_len, 3])
            # print('x_train', np.shape(x_train))
            y_train = np.zeros(year_len)
            x_test = np.zeros([np.shape(zos)[1] - year_len - 1, 3])
            y_test = np.zeros(np.shape(zos)[1] - year_len - 1)
            # print('x_test', np.shape(x_test))
            #######
            x_test[:, 0] = tauuo_short[point, year_len:]
            x_test[:, 1] = tauvo_short[point, year_len:]
            x_test[:, 2] = zos_prev[point, year_len:]
            y_test[:] = zos_diff_targ[point, year_len:]
            ######
            x_train[:, 0] = tauuo[point, :year_len]
            x_train[:, 1] = tauvo[point, :year_len]
            x_train[:, 2] = zos_prev[point, :year_len]
            y_train[:] = zos_diff_targ[point, :year_len]
        elif train_year == 2005:
            x_test = np.zeros([year_len, 3])
            y_test = np.zeros(year_len)
            x_train = np.zeros([np.shape(zos)[1] - year_len -1, 3])
            y_train = np.zeros(np.shape(zos)[1] - year_len- 1)
            #######
            x_train[:, 0] = tauuo_short[point, year_len:]
            x_train[:, 1] = tauvo_short[point, year_len:]
            x_train[:, 2] = zos_prev[point, year_len:]
            y_train[:] = zos_diff_targ[point, year_len:]
            #######
            x_test[:, 0] = tauuo_short[point, :year_len]
            x_test[:, 1] = tauvo_short[point, :year_len]
            x_test[:, 2] = zos_prev[point, :year_len]
            y_test[:] = zos_diff_targ[point, :year_len]

        huber = HuberRegressor().fit(x_train, y_train)
        linear = LinearRegression().fit(x_train, y_train)
        ransac = RANSACRegressor().fit(x_train, y_train)
        theil = TheilSenRegressor().fit(x_train, y_train)
        l_score = linear.score(x_train, y_train)
        l_gen_score = linear.score(x_test, y_test)
        h_score = huber.score(x_train, y_train)
        h_gen_score = huber.score(x_test, y_test)
        r_score = ransac.score(x_train, y_train)
        r_gen_score = ransac.score(x_test, y_test)
        t_score = theil.score(x_train, y_train)
        t_gen_score = theil.score(x_test, y_test)
        results[1, 5] = h_gen_score
        results[1, 4] = h_score
        results[1, 0] = huber.intercept_
        results[1, 1] = huber.coef_[0]
        results[1, 2] = huber.coef_[1]
        results[1, 3] = huber.coef_[2]
        results[0, 5] = l_gen_score
        results[0, 4] = l_score
        results[0, 0] = linear.intercept_
        results[0, 1] = linear.coef_[0]
        results[0, 2] = linear.coef_[1]
        results[0, 3] = linear.coef_[2]
        results[2, 5] = r_gen_score
        results[2, 4] = r_score
        results[2, 0] = ransac.estimator_.intercept_
        results[2, 1] = ransac.estimator_.coef_[0]
        results[2, 2] = ransac.estimator_.coef_[1]
        results[2, 3] = ransac.estimator_.coef_[2]
        results[3, 5] = t_gen_score
        results[3, 4] = t_score
        results[3, 0] = theil.intercept_
        results[3, 1] = theil.coef_[0]
        results[3, 2] = theil.coef_[1]
        results[3, 3] = linear.coef_[2]

        print(results)

    for point in range(200):
        run_rmlr(point=point)
        run_rmlr(point=point, train_year=2005)


