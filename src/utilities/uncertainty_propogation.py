"""
from uncertainties import ufloat
from uncertainties import umath as um

"""
from uncertainties import unumpy, ufloat
import uncertainties.umath as um


def take_log_y_values(data_d):
    """

    :param data_d: The standard dict format used for data plotting throughout this repository
    :return:
    """
    for key in data_d["x_values_d"]:
        y_values_unpa = unumpy.uarray(data_d['y_values_d'][key], data_d['y_errors_d'][key])
        for i in range(len(y_values_unpa)):
            if unumpy.nominal_values(y_values_unpa)[i] > 0:
                y_values_unpa[i] = um.log10(y_values_unpa[i])
            else:
                print(unumpy.nominal_values(y_values_unpa)[i], ' is less than 0')
                y_values_unpa[i] = ufloat(0, 1)
        data_d["y_values_d"][key] = unumpy.nominal_values(y_values_unpa)
        data_d["y_errors_d"][key] = unumpy.std_devs(y_values_unpa)

    return data_d


def take_difference_y_values(data_d):
    """

    :param data_d: The standard dict format used for data plotting throughout this repository
    :return:
    """
    for key in data_d["x_values_d"]:
        y_values_unpa = unumpy.uarray(data_d['y_values_d'][key], data_d['y_errors_d'][key])
        y_values_unpa_new = unumpy.uarray(data_d['y_values_d'][key][0: len(y_values_unpa) - 1],
                                          data_d['y_errors_d'][key][0: len(y_values_unpa) - 1])
        for i in range(len(y_values_unpa_new)):
            y_values_unpa_new[i] = y_values_unpa[i] - y_values_unpa[i+1]
        data_d["y_values_d"][key] = unumpy.nominal_values(y_values_unpa_new)
        data_d["y_errors_d"][key] = unumpy.std_devs(y_values_unpa_new)
        data_d["x_values_d"][key] = data_d["x_values_d"][key][0:len(y_values_unpa_new)]

    return data_d


def test_unp_plot():
    import matplotlib.pyplot as plt
    from uncertainties import unumpy as unp
    # input syntax: nominal_list, std_dev_list
    x = unp.uarray([1, 2],
                   [0.1, 0.05])
    y = unp.uarray([1, 1.5],
                   [0.01, 0.04])
    plt.errorbar(unp.nominal_values(x), unp.nominal_values(y),
                 yerr=unp.std_devs(y), xerr=unp.std_devs(x),
                 fmt='x', ecolor='grey')
    plt.xlabel('x value (dimensionless)')
    plt.ylabel('y value (dimensionless)')
    plt.show()
