"""
fftw_tests.py
============

pyfftw tests
- apparently much faster than the alternative scipy routines.

Test examples taken from
https://hgomersall.github.io/pyFFTW/sphinx/tutorial.html#quick-and-easy-the-pyfftw-interfaces-module
"""
import pyfftw
import numpy


def init_test():
    a = pyfftw.empty_aligned(128, dtype='complex128', n=16)
    a[:] = numpy.random.randn(128) + 1j*numpy.random.randn(128)
    b = pyfftw.interfaces.numpy_fft.fft(a)
    c = numpy.fft.fft(a)
    numpy.allclose(b, c)


def one_dimensional_transforms():
    a = pyfftw.empty_aligned(128, dtype='complex128')
    b = pyfftw.empty_aligned(128, dtype='complex128')

    fft_object = pyfftw.FFTW(a, b)
    c = pyfftw.empty_aligned(128, dtype='complex128')
    ifft_object = pyfftw.FFTW(b, c, direction='FFTW_BACKWARD')
    # Generate some data
    ar, ai = numpy.random.randn(2, 128)
    a[:] = ar + 1j * ai

    fft_a = fft_object()
    assert fft_a is b

    fft_a = fft_object()
    ifft_b = ifft_object()
    assert ifft_b is c
    assert numpy.allclose(a, c)
    assert a is not c

    d = pyfftw.empty_aligned(4, dtype='complex128')
    e = pyfftw.empty_aligned(4, dtype='complex128')
    f = pyfftw.empty_aligned(4, dtype='complex128')
    fft_object = pyfftw.FFTW(d, e)
    assert fft_object.input_array is d  # get the input array from the object
    f[:] = [1, 2, 3, 4]  # Add some data to f
    fft_object(f)
    assert fft_object.input_array is not d  # No longer true!
    assert fft_object.input_array is f  # It has been updated with f :)


def multi_dimensional_transforms():
    a = pyfftw.empty_aligned((128, 64), dtype='complex128')
    b = pyfftw.empty_aligned((128, 64), dtype='complex128')

    # Plan an fft over the last axis
    fft_object_a = pyfftw.FFTW(a, b)

    # Over the first axis
    fft_object_b = pyfftw.FFTW(a, b, axes=(0,))

    # Over the both axes
    fft_object_c = pyfftw.FFTW(a, b, axes=(0, 1))


def fftw_builders():
    a = pyfftw.empty_aligned((128, 64), dtype='complex128')

    # Generate some data
    ar, ai = numpy.random.randn(2, 128, 64)
    a[:] = ar + 1j * ai

    fft_object = pyfftw.builders.fft(a)

    b = fft_object()

    a = pyfftw.empty_aligned((128, 64), dtype='complex128')

    fft_wrapper_object = pyfftw.builders.fftn(a, s=(32, 256))

    b = fft_wrapper_object()

