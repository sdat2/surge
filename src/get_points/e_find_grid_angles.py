"""
e_find_grid_angles.py
=====================
this produces a grid which shows how much the ORCA12
grid is deformed w.r.t. lat-lon.
"""
import iris
import iris.analysis.cartography as iac
import src.utilities.my_wrappers as mwr
import config as cfg


@mwr.timeit
def find_grid_angles_from_nc(full_file_path):
    """
    This looks at a grid, and uses IRIS to work out the deformation
    of the grid with respect to a simple lat-lon grid.

    :param full_file_path: The file path of the .nc file
    :return: void - file saved.
    """
    cubes = iris.load(full_file_path)
    first = True
    for cube in cubes:
        if first:
            a_obj = iac.gridcell_angles(cube,
                                        y=None,
                                        cell_angle_boundpoints='mid-lhs, mid-rhs')
            print(a_obj.coords())
            first = False
        else:
            break

    iris.save(a_obj, 'grid.nc')


find_grid_angles_from_nc(cfg.laptop_home + cfg.toy_loc + cfg.big_T_file_name)
