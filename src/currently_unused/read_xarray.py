#!/usr/bin/env python3.7

import numpy as np
import pandas as pd
import xarray as xr
import dask as ds
import matplotlib.pyplot as plt
import cartopy.crs as ccrs

home = '/home/users/sithom/'
file_name = home + 'nemo_ba130o_1h_20050801-20050831_grid-T.nc'

xr_object = xr.open_dataset(file_name)

print(xr_object.__repr__())

time = '500.0'

small_slice = xr_object.sel(time_counter='time', nav_lat=slice(10, 40),
                            nav_lon=slice(-100, -40), method='nearest')


zos = small_slice.zos
sowindsp = small_slice.sowindsp


carree = ccrs.PlateCarree()
map_proj = carree
fig = plt.figure(figsize=[10, 5])

ax1 = plt.subplot(1, 2, 1, projection=map_proj)
ax2 = plt.subplot(1, 2, 2, projection=map_proj, sharex=ax1, sharey=ax1)

fig.subplots_adjust(bottom=0.05,
                    top=0.95,
                    left=0.04,
                    right=0.95,
                    wspace=0.02)
shrink = 0.5
ax1.coastlines()
ax2.coastlines()
ax1.gridlines(xlocs=range(0, 361, 20), ylocs=range(-20, -60, 20))
ax2.gridlines(xlocs=range(0, 361, 45), ylocs=range(-80, -19, 20))

sowindsp.plot(transform=carree,  # the data's projection
              ax=ax1,
              subplot_kws={"projection": map_proj},  # the plot's projection
              cbar_kwargs={"shrink": shrink}
              )
ax1.set_title("Zonal Component of Velocity, U, " + time)

zos.plot(transform=carree,  # the data's projection
         ax=ax2,
         subplot_kws={"projection": map_proj},  # the plot's projection
         cbar_kwargs={"shrink": shrink},
         )
ax2.set_title("Meridional Component of Velocity, V, " + time)

plt.tight_layout()
plt.savefig('test.pdf', rasterized=True)

"""

prefix=moose:/crum/mi-ba130/ond.nc.file/
name=nemo_ba130o_1d_20050801-20050831_grid-
moo get $prefix${name}T.nc ${name}T.nc


"""
