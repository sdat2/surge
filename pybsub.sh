#!/bin/bash
# Script Written by Dr. J. Scott Hosking (BAS) (jask@bas.ac.uk)
# Reused under permission.
#
# Makes submitting a python script to 
#   the JASMIN LOTUS cluster a 'one-liner'
#
# Default queue is short-serial, although changed to long-serial if
#   a wallclock time of more than 24 hours is requested.
#
# Default Wallclock time is 1 hour
#
# Usage:
#
#   Submit script (default wallclock time and queue)
#     $ pybsub.sh script.py
#
#   Submit script with a wallclock time of 6 hours (to default queue)
#     $ pybsub.sh -w 6 script.py
#
#   Submit script to high-mem queue for 48 hours (queue wallclock limit)
#     $ pybsub.sh -q high-mem -w 48 script.py
#
# -- Scott Hosking (23rd March 2017)


###To do:
### Add a way to increase memory limit, e.g., to 24GB
###   #BSUB -R "rusage[mem=24000]" -M 24000000


### Default values
WALLCLOCK_HOURS=1
QUEUE="short-serial" 

###############################
### Apply command line options
###############################

while test $# -gt 0; do
        case "$1" in
                -w)
                        shift
                        if test $# -gt 0; then

                            export WALLCLOCK_HOURS=$1

                            re='^[0-9]+$'
                            if ! [[ $WALLCLOCK_HOURS =~ $re ]] ; then
                               	echo "error: -w needs to be followed by a number" >&2; exit 1
                            fi

                        else
                            echo "WALLCLOCK_TIME (-w) not specified"
                            exit 1
                        fi
                        shift
                        ;;

                -q)
                        shift
                        if test $# -gt 0; then

                            export QUEUE=$1

                        else
                            echo "QUEUE (-q) not specified"
                            exit 1
                        fi
                        shift
                        ;;

                *)
                        break
                        ;;
        esac
done

### Set serial queue (short-serial max of 24 hours)
if [ $QUEUE == "short-serial" ]; then
    if [ "$WALLCLOCK_HOURS" -gt 24 ]; then
        echo ""
        echo ">> Requested >24hr on short-serial queue, changing to long-serial queue <<"
        QUEUE="long-serial" 
    fi
fi

### Set high-mem queue (max 48 hours)
if [ $QUEUE == "high-mem" ]; then
    if [ "$WALLCLOCK_HOURS" -gt 48 ]; then
        echo ""
        echo ">> Reducing wallclock to 48 hours (high-mem limit) <<"
        WALLCLOCK_HOURS=48 
    fi
fi

WALLCLOCK_TIME=`printf "%02d\n" $WALLCLOCK_HOURS`:00

FILE=$1
COMMAND="$@"

###############################
### Check file exists
###############################

if [ ! -f $FILE ]; then
    echo "The file "$FILE" not found!"
    exit 1
fi

#################################
### Setup LOTUS folder + clean up
#################################

LOTUS_OUT_DIR=$HOME/LOTUS/
ARCHIVE=$LOTUS_OUT_DIR/OLDER_THAN_5_DAYS

### Create folder if it doesn't exist
mkdir -p $LOTUS_OUT_DIR
mkdir -p $ARCHIVE

### Archive files older than 5 days
find $LOTUS_OUT_DIR -maxdepth 1 -mtime +5 -type f -exec mv "{}" $ARCHIVE \;

### Delete LOTUS output files (.err and .out) older than 30 days old
find $ARCHIVE -mtime +30 -name "*.err" -type f -delete
find $ARCHIVE -mtime +30 -name "*.out" -type f -delete

#################################
### Create bsub script
#################################

### http://help.ceda.ac.uk/article/113-submit-jobs

FILE=`basename $FILE .py`
fname=$FILE'_'$QUEUE'.bsub'

cat > $fname << EOF
#!/bin/bash 
#BSUB -q $QUEUE 
#BSUB -o ${HOME}/LOTUS/${FILE}_%J.out 
#BSUB -e ${HOME}/LOTUS/${FILE}_%J.err 
#BSUB -W $WALLCLOCK_TIME

### Run python script specifying the 'Agg' Matplotlib  
### backend for plotting without an X server
MPLBACKEND=Agg python $COMMAND

EOF

##################################
### Submit script to desired queue
##################################

echo " "
echo "######################################################"
echo "Running python script on LOTUS cluster: "$FILE
echo 'WALLCLOCK_TIME=' $WALLCLOCK_TIME
echo "$ bsub < "$fname 
bsub < $fname
echo "Standard output/error files from LSF will be saved to "$HOME"/LOTUS/"
echo "   and stored for 30 days before being deleted."
echo "To view your running jobs type 'bjobs' "
echo "######################################################"
echo " "

exit 0
