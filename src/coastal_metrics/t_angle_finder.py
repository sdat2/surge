"""
t_angle_finder.py
================
https://docs.scipy.org/doc/numpy/reference/generated/numpy.arctan2.html

"""
import numpy as np
import scipy.ndimage.filters as filt
# scipy.ndimage.filters.gaussian_gradient_magnitude(input, sigma, output=None, mode='reflect', cval=0.0, **kwargs)
import xarray as xr
import matplotlib.pyplot as plt
import cmocean.cm as cmo
import src.utilities.my_io_functions as mif
import src.plotting.my_plotting_style as mps
import src.utilities.my_wrappers as mwr
import config as cfg
import src.plotting.custom_ticker as ctk
mps.mps_defaults()


def angle_averager(angle_list):
    """
    https://rosettacode.org/wiki/Averages/Mean_angle
    :param angle_list:
    :return: float angle answer ( in radians )
    \begin{equation}
    \bar{\alpha}=\operatorname{atan} 2\left(\frac{1}{n} \cdot
    \sum_{j=1}^{n} \sin \alpha_{j}, \frac{1}{n} \cdot \sum_{j=1}^{n}
    \cos \alpha_{j}\right)
    \end{equation}
    """
    total_1 = 0.0
    total_2 = 0.0
    for item in angle_list:
        total_1 += np.sin(item)
        total_2 += np.cos(item)
    length = len(angle_list)
    print(np.std(angle_list))
    return np.mod(np.arctan2(total_1/length,
                             total_2/length),
                  2*np.pi)


def angle_smoother(full_angle_list, sigma):
    sin_npa = np.sin(full_angle_list)
    cos_npa = np.cos(full_angle_list)
    filt_sin = filt.gaussian_filter1d(sin_npa, sigma, axis=-1,
                                      order=0, output=None,
                                      mode='reflect',
                                      # cval=0.0,
                                      truncate=4.0)
    filt_cos = filt.gaussian_filter1d(cos_npa, sigma, axis=-1,
                                      order=0, output=None,
                                      mode='reflect',
                                      # cval=0.0,
                                      truncate=4.0)
    return np.mod(np.arctan2(filt_sin,
                             filt_cos),
                  2*np.pi)


#@jit(cache=True)
def single_derivative(element_two, element_one):
    temp_norm = element_two - element_one
    temp_pos = element_two  - element_one + 2*np.pi
    temp_neg = element_two  - element_one - 2*np.pi
    temp_list = [temp_norm, temp_pos, temp_neg]
    index = np.argmin(np.abs(temp_list))
    return temp_list[index]


def take_angular_derivative(angle_list):
    """

    :param angle_list:
    :return:
    """
    deriv_list = []

    for i in range(len(angle_list)):
        if i == 0:
            temp_deriv = single_derivative(angle_list[i+1], angle_list[i])
            # temp_deriv = angle_list[i+1] - angle_list[i]
        elif i == len(angle_list) - 1:
            temp_deriv = single_derivative(angle_list[i], angle_list[i-1])
            # temp_deriv = (angle_list[i] - angle_list[i-1])
        else:
            temp_deriv = single_derivative(angle_list[i+1], angle_list[i-1])/2
            # temp_deriv = (angle_list[i+1] - angle_list[i-1])/2
        deriv_list.append(temp_deriv)
    return deriv_list


@mwr.timeit
def get_angles(input_file='data/lon_lat_point.csv', coastline='america'):
    """
    https://rosettacode.org/wiki/Averages/Mean_angle
    :param input_file: a file which has the latitudes and longitudes of the points that were selected.
    Life occurs in the pre-asymtotics.
    :return:
    """
    save_prefix = mif.save_prefix(coastline=coastline)


    if coastline == 'america':
       xr_obj = xr.load_dataset('data/new_toy/' + cfg.toy_T_file_name)
       pair_array = np.asarray(mif.read_csv_file(cfg.h_slimmed_ordered_point_loc),
                               dtype='int16')
       location_list = mif.read_csv_file(input_file, to_int_list=[2])
    elif coastline == 'vin-china':
       xr_obj = xr.load_dataset('data/ea_toy/' + cfg.toy_T_file_name)
       pair_array = np.asarray(mif.read_csv_file(cfg.h_vin_chin_list),
                               dtype='int16')
       location_list = mif.read_csv_file(input_file, to_int_list=[2])

    # xr_slice = xr_obj.isel(time_counter=0).zos
    xr_slice = xr_obj.isel(time_counter=0).sowindsp
    # print(xr_slice.__repr__)

    array = xr.ufuncs.isnan(xr_slice).values

    angle_list = []

    rotated_angle_point_lol = []
    target_point_lol = []
    point_to_index_lol = []

    # Real Algorithm Begins

    for i in range(len(location_list)):
        if i == 0:
            x_diff = location_list[i + 1][0] - location_list[i][0]
            y_diff = location_list[i + 1][1] - location_list[i][1]
        elif i == len(location_list) - 1:
            x_diff = location_list[i][0] - location_list[i - 1][0]
            y_diff = location_list[i][1] - location_list[i - 1][1]
        else:
            x_diff = location_list[i + 1][0] - location_list[i - 1][0]
            y_diff = location_list[i + 1][1] - location_list[i - 1][1]
        angle = np.arctan2(x_diff, y_diff) + np.pi/2

        def points_to_getter(dummy_angle):
            return [int(np.round(pair_array[i, 1] + (np.sin(dummy_angle)), decimals=0)),
                    int(np.round(pair_array[i, 0] + (np.cos(dummy_angle)), decimals=0))]

        points_to_index = points_to_getter(angle)
        target_point_lol.append(points_to_index)
        if array[points_to_index[1], points_to_index[0]]:
            rotated_angle_point_lol.append(points_to_index)
            temp_angle = angle + np.pi
            new_points_to_index = points_to_getter(temp_angle)
            if not array[new_points_to_index[1], new_points_to_index[0]]:
                angle = temp_angle
            else:
                temp_angle = temp_angle + np.pi/2
                new_points_to_index = points_to_getter(temp_angle)
                if not array[new_points_to_index[1], new_points_to_index[0]]:
                    angle = temp_angle
                else:
                    temp_angle = temp_angle - np.pi
                    new_points_to_index = points_to_getter(temp_angle)
                    if not array[new_points_to_index[1], new_points_to_index[0]]:
                        angle = temp_angle

            # Real Algorithm Ends

            print('rotating angle')

        print(location_list[i][2], angle)
        point_to_index_lol.append(points_to_index)
        angle_list.append(np.mod(angle, 2*np.pi))

    angle_npa = np.asarray(angle_list,
                           dtype='float32')
    target_npa = np.asarray(target_point_lol,
                            dtype='int32')
    rot_target_npa = np.asarray(rotated_angle_point_lol,
                                dtype='int32')

    mif.write_json_object(point_to_index_lol,
                          cfg.data_loc  + save_prefix + 'point_to_adjacent.json')

    v_npa = np.cos(angle_npa)
    u_npa = np.sin(angle_npa)
    location_list.pop()
    input_npa = np.asarray(location_list)
    angle_average_list = []
    av_length = 25

    for i in range(0, len(angle_list)//av_length):
        angle_average_list.append([(i + 1/2)*av_length,
                                  np.mod(angle_averager(angle_npa[(i*av_length):((i+1)*av_length)].tolist()),
                                         2*np.pi)])

    def plot_quick_quiver():
        plt.quiver(input_npa[:, 0], input_npa[:, 1], u_npa, v_npa)
        # plt.show()
        plt.clf()

    average_angle_array = np.asarray(angle_average_list)
    v_average = np.cos(average_angle_array[:, 1])*5
    u_average = np.sin(average_angle_array[:, 1])*5

    @mwr.timeit
    def plot_better_quiver():
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.imshow(array, aspect='auto',
                  cmap=plt.cm.Blues_r)
        ax.plot(pair_array[:, 1], pair_array[:, 0])
        ax.scatter(target_npa[:, 0],
                   target_npa[:, 1], s=1, color='red',
                   label='initial target point (ocean)')
        ax.scatter(rot_target_npa[:, 0],
                   rot_target_npa[:, 1], s=1, color='orange',
                   label='initial target point (land)')
        plt.gca().invert_yaxis()
        ax.quiver(pair_array[:, 1], pair_array[:, 0],
                  u_npa, v_npa, color='green')
        for i in range(np.shape(average_angle_array)[0]):
            ax.quiver(pair_array[int(average_angle_array[i, 0]), 1],
                      pair_array[int(average_angle_array[i, 0]), 0],
                      u_average[i], v_average[i], color='orange')
        plt.xlabel('x index')
        plt.ylabel('y index')
        mps.defined_size(fig, size='dcsq')

        mps.save_fig_twice(cfg.plots_loc + save_prefix + 'quiver_plot')

        # plt.show()
        del fig
        plt.clf()

    plot_quick_quiver()

    angle_lol = [[x] for x in angle_list]
    angle_average_lol = []
    for i in range(len(average_angle_array)):
        angle_average_lol.append([average_angle_array[i, 0],
                                  average_angle_array[i, 1]])

    mif.write_csv_file(angle_lol, 'data/' + save_prefix + 'angle.csv')
    mif.write_csv_file(angle_average_lol, 'data/' + save_prefix + 'average_angle.csv')



    def running_average(run_length):
        running_average_list = []
        for i in range(np.shape(angle_npa)[0]):
            running_average_list.append([angle_averager(angle_npa[np.max([i-run_length, 0]):
                                                                  np.min([i+run_length,
                                                                          np.shape(angle_npa)[0]])])])
        return running_average_list

    running_average_list = running_average(10)

    mif.write_csv_file(running_average_list,
                       'data/' + save_prefix + 'running_angle.csv')

    # plot_better_quiver()
    # make_angle_plot()

    def angle_derivative_plot():
        deriv_list = take_angular_derivative(np.asarray(running_average_list).ravel())
        deriv_list20 = take_angular_derivative(np.asarray(running_average(20)).ravel())
        deriv_list50 = take_angular_derivative(np.asarray(running_average(50)).ravel())
        deriv_list100 = take_angular_derivative(np.asarray(running_average(100)).ravel())
        deriv_list200 = take_angular_derivative(np.asarray(running_average(200)).ravel())

        fig, axs = plt.subplots(2, sharex=True)
        axs[0].plot(range(len(deriv_list)), deriv_list, label='10 point')
        axs[0].plot(range(len(deriv_list)), deriv_list20, label='20 point')
        axs[0].plot(range(len(deriv_list)), deriv_list50, label='50 point')
        axs[0].plot(range(len(deriv_list)), np.zeros(len(deriv_list)), color='black')
        axs[0].legend()
        axs[0].set_ylabel("Angle Derivative (radians)")
        axs[0].set_xticks([])
        ctk.add_ticks(coastline=coastline)

        axs[1].plot(range(len(deriv_list)), deriv_list100, label='100 point')
        axs[1].plot(range(len(deriv_list)), deriv_list200, label='200 point')
        axs[1].plot(range(len(deriv_list)), deriv_list50, label='50 point')
        axs[1].plot(range(len(deriv_list)), np.zeros(len(deriv_list)), color='black')
        plt.legend()
        plt.xlabel("Point along the Coast")
        plt.ylabel("Angle Derivative (radians)")
        fig = plt.gcf()
        ctk.add_ticks(coastline=coastline)
        plt.xlim(0, len(deriv_list))
        fig = mps.defined_size(fig, 'dcm')
        mps.save_fig_twice('plots/' + save_prefix + 'angle_deriv')
        del fig
        plt.clf()

    angle_derivative_plot()

    # plt.show()
    sigma_list = range(1, 100)
    smoothed_angle_grid = np.zeros([len(angle_list), len(sigma_list)], dtype='float32')

    for i in range(len(sigma_list)):
        smoothed_angle_grid[:, i] = angle_smoother(angle_list, sigma_list[i])[:]

    def plot_angle_heatmap():
        plt.imshow(smoothed_angle_grid.T, cmap=cmo.phase, aspect='auto')
        plt.gca().invert_yaxis()
        cbar = plt.colorbar(orientation='vertical',
                            shrink=0.8, aspect=20,
                            fraction=0.2, pad=0.02)
        cbar.set_label(r'$B$ (rad)')
        ctk.add_ticks(coastline=coastline)
        plt.ylabel(r'$\sigma$ (pt)')
        plt.xlabel(r'')
        fig = plt.gcf()
        mps.defined_size(fig, size='scsh')

        mps.save_fig_twice(cfg.plots_loc + save_prefix + 'angle_heatmap')

        # plt.show()
        plt.clf()
        del fig

    plot_angle_heatmap()

    # plot_angle_heatmap()
    angle_derivative_grid = np.zeros(np.shape(smoothed_angle_grid), dtype='float32')
    for i in range(len(sigma_list)):
        angle_derivative_grid[:, i] = np.asarray(take_angular_derivative(smoothed_angle_grid[:, i].tolist()))

    def plot_derivative_angle_heatmap(start, stop, ax, fig):

        tmp_array = angle_derivative_grid.T[start: stop, :]
        vmin, vmax = mps.return_balanced_vmin_vmax(tmp_array)
        length_sigma = np.shape(tmp_array)[0]
        if (stop-start) < 30:
            tick_locs = list(range(0, length_sigma, 5))
        elif (stop-start) > 30:
            tick_locs = list(range(0, length_sigma, 10))

        tick_values = [x + start + 1 for x in tick_locs]

        im = ax.imshow(tmp_array,
                       cmap=cmo.balance,
                       vmin=vmin,
                       vmax=vmax,
                       aspect='auto')

        ax.invert_yaxis()
        cbar = fig.colorbar(im, ax=ax, orientation='vertical',
                            shrink=0.8, aspect=20,
                            fraction=0.2, pad=0.02)
        cbar.set_label(r'$\frac{dB}{d\mathrm{pt}}$ (rad pt$^{-1}$)')
        ax.set_yticks(tick_locs)
        ax.set_yticklabels(tick_values)
        ax.set_ylabel(r'$\sigma$ (pt)')

        # plt.show()

    def plot_different_derivative_ranges():
        plt.clf()
        fig, axs = plt.subplots(3, sharex=True)
        plot_derivative_angle_heatmap(1, 11, axs[2], fig)
        plot_derivative_angle_heatmap(10, 30, axs[1], fig)
        plot_derivative_angle_heatmap(30, 100, axs[0], fig)
        plt.xlabel(r'Point Along Coast')
        ctk.add_ticks(coastline=coastline)
        fig = plt.gcf()
        mps.defined_size(fig, size='scm')
        mps.label_subplots(axs)
        mps.save_fig_twice(cfg.plots_loc + save_prefix + 'derivative_heatmap')
        # ../surge/plots/derivative_heatmap.pdf

    plot_different_derivative_ranges()

    mif.write_json_object(angle_derivative_grid.tolist(),
                          cfg.data_loc + save_prefix + 'angle_derivatives.json')
    mif.write_json_object(smoothed_angle_grid.tolist(),
                          cfg.data_loc + save_prefix + 'smoothed_angle.json')


def make_angle_plot(coastline='america'):
    """

    :return:
    """
    save_prefix = mif.save_prefix(coastline=coastline)

    angle_lol = mif.read_csv_file('data/' + save_prefix + 'angle.csv')
    average_angle_array = np.asarray(mif.read_csv_file('data/' + save_prefix + 'average_angle.csv'))
    walking_average_angle_array = np.asarray(mif.read_csv_file('data/' + save_prefix + 'running_angle.csv')) / np.pi * 180

    av_angle_list = []
    for i in range(len(angle_lol)):
        next_index = np.abs(np.square(average_angle_array[:, 0] - i)).argmin()
        av_angle_list.append(average_angle_array[next_index, 1] / np.pi*180)
    angle_list = [np.mod(x[0], 2*np.pi) / np.pi * 180 for x in angle_lol]
    # print(angle_lol)

    fig, axs = plt.subplots(1)
    mps.defined_size(fig, 'sc')
    plt.plot(range(len(angle_list)), angle_list,
             color='#a3c1ad', label='Angle at Point')
    plt.plot(range(len(angle_list)), av_angle_list,
             color='#002147', label='Average Angle over 50 Points')
    plt.plot(range(len(angle_list)), walking_average_angle_array,
             color='#CB4154', label='10pt Symetric Running Average')
    plt.xlim(0, len(angle_list))
    plt.yticks([0, 90, 180, 270, 360])
    plt.ylabel('Normal Coast Bearing (degrees)')
    plt.legend()
    ctk.add_ticks(coastline=coastline)

    mps.save_fig_twice('plots/' + save_prefix + 'angles_plots')

    del fig

    fig, axs = plt.subplots(1)
    plt.hist(angle_list, bins=100, color='#a3c1ad', density=True)
    plt.xticks([0, 90, 180, 270, 360])
    plt.xlabel('Normal Coast Bearing (degrees)')
    plt.ylabel('Density')
    mps.defined_size(fig, size='sc')

    mps.save_fig_twice('plots/' + save_prefix + 'angles_hist')

    del fig
