"""
phase_vs_lag_test.py
====================

To try and see how two signals might be correlated to one another with some
lag, there are two obvious options:

   --> Look at the phases from the Fourier transform of the two signals.
   --> Do a lag correlation graph. (This sounds less fun)

But it isn't obvious which is better or more elegant.

"""
import numpy as np
import math
from scipy.signal import find_peaks
import matplotlib.pyplot as plt
import cmocean.cm as cmo
import config as cfg
import src.f_fourier_transform as fft
import src.utilities.my_io_functions as mif
import src.utilities.my_wrappers as mwr
import src.low_freq.comp_low_thermal as clt
import src.plotting.my_plotting_style as mps
import src.plotting.custom_ticker as ctk
mps.mps_defaults()


@mwr.timeit
def lag_correlator(array_one, array_two):
    full_length = np.shape(array_one)[1]
    length = full_length // 2
    correlate_npa = np.zeros([np.shape(array_one)[0], length], dtype='float32')
    for i in range(length):
        correlate_npa[:, i] = clt.two_array_correlator(array_one[:, i:], array_two[:, :full_length -i])
    return correlate_npa


@mwr.timeit
def plot_correlate_grid(correlate_npa, coastline='america'):
    vmin, vmax = mps.return_balanced_vmin_vmax(correlate_npa)
    plt.imshow(correlate_npa.T, aspect='auto',
               cmap=cmo.balance, vmin=vmin, vmax=vmax)
    plt.gca().invert_yaxis()
    plt.colorbar()
    ctk.add_ticks(coastline=coastline)
    plt.show()


def get_first_peaks(grid, peak_no=0):
    length = np.shape(grid)[0]
    peaks = np.zeros(length)
    widths = np.zeros(length)
    prominences = np.zeros(length)

    for j in range(length):
        print(j)
        start = 0
        tmp_peaks, tmp_properties = find_peaks(grid[j, start:],
                                               prominence=0.001,
                                               width=10,
                                               threshold=0)
        print(tmp_peaks)

        #plt.plot(grid[j, start:])
        #plt.show()

        if len(tmp_peaks) > peak_no:
            peaks[j] = int(tmp_peaks[peak_no]) + start
            widths[j] = tmp_properties['widths'][peak_no]
            prominences[j] = tmp_properties['prominences'][peak_no]
        elif peak_no == 1 and len(tmp_peaks) > 0:
            peaks[j] = int(tmp_peaks[0]) + start
            widths[j] = tmp_properties['widths'][0]
            prominences[j] = tmp_properties['prominences'][0]
        else:
            peaks[j] = np.nan
            widths[j] = np.nan
            prominences[j] = np.nan

    return peaks, widths, prominences


@mwr.timeit
def auto_plot_correlate_grid(array_one, array_two,
                             save_name, coastline='america'):
    plt.clf()
    correlate_npa_one = lag_correlator(array_one, array_two)
    correlate_npa_two = lag_correlator(array_two, array_one)
    vmin, vmax = mps.return_balanced_vmin_vmax(correlate_npa_one)
    fig, axs = plt.subplots(2, sharex=True)
    im0 = axs[0].imshow(correlate_npa_one.T, aspect='auto',
                        cmap=cmo.balance, vmin=vmin, vmax=vmax)
    fig.colorbar(im0, ax=axs[0], label=r'Correlation, $r_p$')
    #, orientation="horizontal")#, location='bottom' )

    peaks, widths, prominences = get_first_peaks(correlate_npa_one, peak_no=0)
    axs[0].plot(range(len(peaks)), peaks, color='black')
    axs[0].plot(range(len(peaks)), peaks + (widths / 2),
                '--', color='black')
    axs[0].plot(range(len(peaks)), peaks - (widths / 2),
                '--', color='black')

    axs[0].invert_yaxis()
    axs[0].set_ylabel('Lag (days)')
    im1 = axs[1].imshow(correlate_npa_two.T, aspect='auto',
                        cmap=cmo.balance, vmin=vmin, vmax=vmax)
    fig.colorbar(im1, ax=axs[1], label=r'Correlation, $r_p$') #, orientation="horizontal")#, location='bottom' )
    ctk.add_ticks(coastline=coastline)
    axs[1].set_ylabel('Reverse Lag (days)')
    mps.defined_size(fig, size='dcm')
    plt.tight_layout()
    mps.save_fig_twice(save_name)
    # plt.show()


@mwr.timeit
def _y_values(y_array, array):
    
    new_array = np.zeros(np.shape(array))

    for i in range(len(array)):
        if math.isnan(array[i]):
           new_array[i] = np.nan
        else:
            value = y_array[int(array[i]), i]
            if value > - 250 and value < 250:
                new_array[i] = value
            else:
                new_array[i] = np.nan

    # print(new_array)
    return new_array


@mwr.timeit
def auto_plot_comb_correlate_grid(array_one, array_two,
                                  save_name, coastline='america'):
    plt.clf()
    correlate_npa_one = lag_correlator(array_one, array_two)
    correlate_npa_two = lag_correlator(array_two, array_one)
    shape = np.shape(correlate_npa_one)
    comb_correlate_array = np.zeros([shape[0], shape[1]*2], dtype='float32')
    comb_correlate_array[:, :shape[1]] = np.flip(correlate_npa_one[:, :], axis=1)
    comb_correlate_array[:, shape[1]:] = correlate_npa_two[:, :]
    peaks, widths, prominences = get_first_peaks(comb_correlate_array, peak_no=0)
    print(peaks)
    print(widths)
    print(prominences)

    vmin, vmax = -1, 1
    lag_axis = np.flip(range(-shape[1], shape[1], 1))
    point_axis = range(shape[0])
    point_mesh, lag_mesh = np.meshgrid(point_axis, lag_axis)
    fig, axs = plt.subplots(1)
    pcm = plt.pcolormesh(point_mesh, lag_mesh,
                         comb_correlate_array.T, cmap=cmo.balance)
    fig = plt.gcf()
    fig.colorbar(pcm, ax=axs, label=r'Correlation, $r_p$')
    #, orientation="horizontal")#, location='bottom' )

    axs.plot(range(len(peaks)),
             _y_values(lag_mesh, peaks),
             color='black')
    axs.plot(range(len(peaks)),
             _y_values(lag_mesh, peaks + (widths // 2)),
             '--', color='black')
    axs.plot(range(len(peaks)),
             _y_values(lag_mesh, peaks - (widths // 2)),
             '--', color='black')

    plt.ylabel('Lag (days)')
    ctk.add_ticks(coastline=coastline)
    mps.save_fig_twice(save_name)
    # plt.show()


@mwr.timeit
def test(coastline='america'):

    zos_npa = clt.get_np_array(coastline=coastline)
    ssh_day_npa = clt.array_coarsener(zos_npa)
    tos_npa = clt.get_np_array(field='tos', time='d', coastline=coastline)

    demeaned_temp = clt.array_demean(tos_npa)
    demeaned_zos = clt.array_demean(ssh_day_npa)

    low_pass_zos = fft.low_pass_grid(demeaned_zos, threshold_freq=2, time='d')
    low_pass_tos = fft.low_pass_grid(demeaned_temp, threshold_freq=2, time='d')

    # correlation_npa = clt.two_array_correlator(demeaned_temp, demeaned_zos)
    # correlation_low_zos = clt.two_array_correlator(demeaned_temp, low_pass_zos)
    # correlation_low_zos_low_tos = clt.two_array_correlator(low_pass_tos, low_pass_zos)

    save_prefix = mif.save_prefix(coastline=coastline)
    save_dir = cfg.plots_loc + 'lag/' + save_prefix  # + 'zm_'

    print(save_dir)

    #correlate_npa = lag_correlator(low_pass_tos, low_pass_zos)
    #plot_correlate_grid(correlate_npa, coastline=coastline)

    auto_plot_comb_correlate_grid(demeaned_zos, demeaned_temp,
                                  save_dir + 'correlate_dmdm',
                                  coastline=coastline)

    # auto_plot_comb_correlate_grid(low_pass_zos, demeaned_temp, save_dir + 'correlate', coastline=coastline)

    def plot_power_phase():
        fft.auto_plot_power_phase(demeaned_zos, save_dir+'fft_pp_zos',
                                  coastline=coastline)
        fft.auto_plot_power_phase(demeaned_temp, save_dir+'fft_pp_tos',
                                  coastline=coastline)

    # fft.return_power_phase(demeaned_temp)
