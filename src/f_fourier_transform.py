"""
f_fourier_transform.py
======================
Adapted from example at:
https://github.com/ipython-books/cookbook-2nd-code/blob/master/
chapter10_signal/01_fourier.ipynb
https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.find_peaks.html
"""
import scipy.signal as sig  # sig.find_peaks()
from numba import jit
import datetime
import numpy as np
import scipy as sp
import scipy.fftpack
import pandas as pd
import matplotlib.pyplot as plt
import xarray as xr
import cmocean.cm as cmo
import matplotlib.dates as mdates
import config as cfg
import src.plotting.my_plotting_style as mps
import src.utilities.my_wrappers as mwr
import src.utilities.my_io_functions as mif
import src.plotting.custom_ticker as ctk
mps.mps_defaults()


@mwr.timeit
def get_xarray_grid(name_list, coastline='america'):
    """
    :param name_list: a list of xarray locations to load up
    :return:
    """
    zos = mif.return_field_array(name_list, 'zos', coastline=coastline)
    # sowindsp = mif.return_field_array(name_list, 'sowindsp', coastline=coastline)
    # tauuo = mif.return_field_array(name_list, 'tauuo', coastline=coastline)
    # tauvo = mif.return_field_array(name_list, 'tauvo', coastline=coastline)
    no_points = np.shape(zos)[0]
    zos_mean = np.zeros(no_points, dtype='float32')
    # print(np.shape(zos))
    for j in range(no_points):
        zos_mean[j] = np.mean(zos[j, :])
    zos_demeaned = np.zeros(np.shape(zos), dtype='float32')
    for i in range(no_points):
        zos_demeaned[i, :] = zos[i, :] - zos_mean[i]
    #   test_different_ffts(zos_demeaned)
    #   amp_grid, frequncy_scale = return_fft_grid(zos_demeaned)
    #   plot_fft_grid(amp_grid, frequncy_scale)
    #   low_pass_filter(amp_grid[150, :], frequncy_scale, zos_demeaned[150, :])
    # ab_init_low_pass_filter(zos_demeaned[172, :])
    # SH 477

    if coastline == 'vin-china':
        ab_init_low_pass_filter(zos_demeaned[477, :], name='Shanghai', file_prefix='sh')
        ab_init_low_pass_filter(zos_demeaned[735, :], name='Tianjin', file_prefix='tj')
        ab_init_low_pass_filter(zos_demeaned[62, :], name='Hai Phong', file_prefix='hp')

    # ab_init_low_pass_filter(zos_demeaned[357, :], name='Miami', file_prefix='mm')
    # ab_init_low_pass_filter(zos_demeaned[604, :], name='New York', file_prefix='ny')

    def plot_pass_grids():
        low_pass_npa = low_pass_grid(zos_demeaned)
        plot_low_pass_grid(low_pass_npa, coastline=coastline)
        high_pass_npa = high_pass_grid(zos_demeaned)
        plot_high_pass_grid(high_pass_npa, coastline=coastline)

    plot_pass_grids()


@mwr.timeit
def high_pass_grid(input_grid, threshold_freq=2):
    high_pass_npa = np.zeros(np.shape(input_grid))
    number_coast_points = np.shape(input_grid)[0]
    for i in range(number_coast_points):
        temp_fft = sp.fftpack.fft(input_grid[i, :])
        fftfreq = sp.fftpack.fftfreq(len(temp_fft), 1. / (365 * 24))
        temp_fft[np.abs(fftfreq) < threshold_freq] = 0
        high_pass_npa[i, :] = np.real(sp.fftpack.ifft(temp_fft))
    return high_pass_npa


@mwr.timeit
def plot_high_pass_grid(high_pass_npa, save=True, coastline='america'):
    plt.clf()
    maxim = np.max(high_pass_npa)
    minim = np.min(high_pass_npa)
    vmin = np.min([minim, -maxim])
    vmax = np.max([-minim, maxim])
    plt.imshow(high_pass_npa.T, vmin=vmin, vmax=vmax,
               cmap=cmo.balance, aspect='auto')
    plt.gca().invert_yaxis()
    cbar = plt.colorbar()
    cbar.set_label('Change from Mean $\Delta \eta_{\mathrm{\;\;hp}}$ (m)')
    ctk.add_ticks(coastline=coastline)
    fig = plt.gcf()
    mps.defined_size(fig, size='dcsq')
    plt.ylabel('Hours Since Start of 2004')

    if save:
        save_prefix = mif.save_prefix(coastline=coastline)
        mps.save_fig_twice(cfg.plots_loc + save_prefix + 'high_pass_grid')

    plt.show()


@mwr.timeit
def low_pass_grid(input_grid, threshold_freq=2, time='h'):
    if time == 'h':
        freq_mult = 1. / (365 * 24)
    elif time == 'd':
        freq_mult = 1. / 365

    low_pass_npa = np.zeros(np.shape(input_grid))
    number_coast_points = np.shape(input_grid)[0]

    for i in range(number_coast_points):
        temp_fft = sp.fftpack.fft(input_grid[i, :])
        fftfreq = sp.fftpack.fftfreq(len(temp_fft), freq_mult)
        temp_fft[np.abs(fftfreq) >= threshold_freq] = 0
        low_pass_npa[i, :] = np.real(sp.fftpack.ifft(temp_fft))
    return low_pass_npa


@mwr.timeit
def plot_low_pass_grid(low_pass_npa, save=True, coastline='america'):
    plt.clf()
    maxim = np.max(low_pass_npa)
    minim = np.min(low_pass_npa)
    vmin = np.min([minim, -maxim])
    vmax = np.max([-minim, maxim])
    plt.imshow(low_pass_npa.T, vmin=vmin, vmax=vmax,
               cmap=cmo.balance, aspect='auto')
    plt.gca().invert_yaxis()
    cbar = plt.colorbar()
    cbar.set_label('Change from Mean $\Delta \eta_{\mathrm{\;\;lp}}$ (m)')
    ctk.add_ticks(coastline=coastline)
    fig = plt.gcf()
    mps.defined_size(fig, size='dcsq')
    plt.ylabel('Hours Since Start of 2004')

    if save:
        save_prefix = mif.save_prefix(coastline=coastline)
        mps.save_fig_twice(cfg.plots_loc + save_prefix + 'low_pass_grid')

    plt.show()


@mwr.timeit
def ab_init_low_pass_filter(zos_comp, threshold_freq=2,
                            name='New Orleans',
                            file_prefix='norlean'):
    temp_fft = sp.fftpack.fft(zos_comp)
    fftfreq = sp.fftpack.fftfreq(len(temp_fft), 1. / (365*24))
    temp_fft[np.abs(fftfreq) >= threshold_freq] = 0
    temp_slow = np.real(sp.fftpack.ifft(temp_fft))
    temp_fft2 = sp.fftpack.fft(zos_comp)
    temp_fft2[np.abs(fftfreq) < threshold_freq] = 0
    temp_fast = np.real(sp.fftpack.ifft(temp_fft2))
    assert(np.allclose(zos_comp[:], temp_fast[:]+temp_slow[:], rtol=1e-03, atol=1e-03))
    fig, ax = plt.subplots(1, 1, figsize=(6, 3))
    plt.plot(range(len(temp_slow)), temp_fast, color='red',
             alpha=0.7, label='High Pass Filter for ' + name)
    plt.plot(range(len(temp_slow)), temp_slow, color='blue',
             alpha=1, label='Low Pass Filter for ' + name)
    plt.plot(range(len(temp_slow)), zos_comp, color='purple',
             alpha=0.7, label='$\Delta\eta$ (m) for ' + name)
    plt.xlabel('Hours Since the Start of 2004')
    plt.ylabel(r'$\Delta\eta$ (m)')
    plt.legend()
    plt.xlim([0, len(temp_slow)])
    fig = plt.gcf()
    mps.defined_size(fig, size='dcsh')
    mps.save_fig_twice(cfg.plots_loc + file_prefix + '-low-pass')
    # plt.show()
    # temp.plot(ax=ax, lw=.5)
    # ax.plot_date(date, temp_slow, '-')


@mwr.timeit
def low_pass_filter(temp_fft, fftfreq, zos_comp):
    x = np.arange(5)
    np.allclose(sp.ifft(sp.fft(x)), x, atol=1e-15)
    temp_fft_bis = temp_fft.copy()
    temp_fft_bis[np.abs(fftfreq) > 0.01] = 0
    temp_slow = np.real(sp.fftpack.ifft(temp_fft_bis))
    plt.plot([2*x for x in range(np.shape(temp_slow)[0])], temp_slow)
    plt.plot(range(np.shape(temp_slow)[0]*2), zos_comp)
    plt.show()


"""
ifft
y(j) = (x * exp(2*pi*sqrt(-1)*j*np.arange(n)/n)).mean().
fft
y(j) = (x * exp(-2*pi*sqrt(-1)*j*np.arange(n)/n)).sum().
"""


@mwr.timeit
def return_fft_complex_grid(value_grid):
    from scipy.fftpack import fft, fftshift

    time_interval = 1  # hour
    number_samples = np.shape(value_grid)[1]
    number_points = np.shape(value_grid)[0]
    # amp_grid = np.zeros([number_points, number_samples // 2])
    frequency_scale = np.linspace(0, 1.0 / (2.0 * time_interval),
                                  number_samples // 2)
    fft_grid = np.zeros([number_points,
                         np.shape(np.fft.fft(value_grid[0, :]))[0]],
                         dtype='complex128'
                         )

    for j in range(number_points):
        fft_grid[j, :] = fftshift(np.fft.fft(value_grid[j, :]))

    fftfreq = sp.fftpack.fftfreq(len(value_grid[0, :]), 1/365) # years
    fftfreq = fftshift(fftfreq)

    return fft_grid, fftfreq


@mwr.timeit
def return_power_phase(value_grid):

    # return amp_grid, frequency_scale
    fft_grid, fftfreq = return_fft_complex_grid(value_grid)
    fft_power = np.zeros(np.shape(fft_grid))
    fft_phase = np.zeros(np.shape(fft_grid))
    fft_power[:, :] = np.abs(fft_grid[:, :]) ** 2
    fft_phase[:, :] = np.angle(fft_grid[:, :])

    return fft_power, fft_phase, fftfreq


@mwr.timeit
def plot_fft_power_phase(fft_power, fft_phase,
                         fftfreq, save_name,
                         coastline='america'):
    plt.clf()
    # out_to = 16

    out_to = 151
    start = len(fftfreq) // 2 - out_to
    stop = len(fftfreq) // 2 + out_to

    fft_power = fft_power[:, start:stop]
    fft_phase = fft_phase[:, start:stop]
    fftfreq = fftfreq[start:stop]

    fig, axs = plt.subplots(2, sharex=True, sharey=True)
    im0 = axs[0].pcolormesh(fft_power.T, cmap=cmo.amp, vmin=0)
    fig.colorbar(im0, ax=axs[0],
                 label='Power')
    im1 = axs[1].pcolormesh(fft_phase.T, cmap=cmo.phase)
    fig.colorbar(im1, ax=axs[1],
                 label='Phase (radians)')
    ctk.add_ticks(coastline=coastline) # coastline=coastline)

    step = (out_to - 1) // 3
    tick_locs = []
    tick_labels = []
    tick_locs = [len(fftfreq) // 2 + step*x for x in range(-3, 4, 1) ]

    for i in range(len(tick_locs)):
        #tick_locs.append(i*step)
        tick_labels.append("{:1.0f}".format(fftfreq[tick_locs[i]]))  # 1.2e

    axs[1].set_yticks(tick_locs)
    axs[1].set_yticklabels(tick_labels)
    axs[0].set_ylabel('Frequency (yr$^{-1}$)')
    axs[1].set_ylabel('Frequency (yr$^{-1}$)')
    mps.defined_size(fig, size='dcm')
    plt.tight_layout()
    mps.save_fig_twice(save_name)
    # plt.show()


def auto_plot_power_phase(value_grid, save_name, coastline='america'):

    fft_power, fft_phase, fftfreq = return_power_phase(value_grid)
    plot_fft_power_phase(fft_power, fft_phase, fftfreq, save_name, coastline=coastline)


@mwr.timeit
def return_fft_grid(value_grid):
    time_interval = 1  # hour
    number_samples = np.shape(value_grid)[1]
    number_points = np.shape(value_grid)[0]
    amp_grid = np.zeros([number_points, number_samples // 2])
    frequency_scale = np.linspace(0, 1.0 / (2.0 * time_interval),
                                  number_samples // 2)
    plt.show()

    for j in range(number_points):
        amp_grid[j, :] = (2.0 / number_samples
                          * np.abs(np.fft.fft(value_grid[j, :])[
                          0:number_samples // 2]))

    return amp_grid, frequency_scale


@mwr.timeit
def plot_fft_grid(amp_grid,
                  frequency_scale,
                  coastline='america'):
    save_prefix = mif.save_prefix(coastline=coastline)
    assert np.shape(amp_grid)[1] == np.shape(frequency_scale)[0]
    up_to_limit = 500

    plt.imshow(amp_grid[:, :up_to_limit].T,
               cmap=cmo.amp, aspect='auto')
    cbar = plt.colorbar(orientation='vertical',
                        shrink=0.625, aspect=20,
                        fraction=0.2, pad=0.02)

    ctk.add_ticks()
    plt.gca().invert_yaxis()
    labels = ['0', ]
    locs = [0, ]

    for i in range(1, 5):
        index = i * up_to_limit // 5
        place = frequency_scale[index] * 24
        labels.append("{:.3f}".format(place))
        locs.append(index)

    plt.yticks(np.asarray(locs),
               np.asarray(labels))
    plt.ylabel('Frequency (Day$^{-1}$)')
    fig = plt.gcf()
    mps.defined_size(fig, size='dcsh')
    cbar.set_label('Amplitude')
    cbar.ax.invert_yaxis()
    mps.save_fig_twice(cfg.plots_loc
                       + save_prefix
                       + 'fourier_transform_grid')


def test_different_ffts(zos_demeaned):
    s_values = np.zeros(np.shape(zos_demeaned)[1])
    s_values = zos_demeaned[150, :]

    # print(np.shape(s_values))
    time_interval = 1  # sampling interval
    number_points = s_values.size

    # run different fourier transform options
    fft = np.fft.fft(s_values)
    yf = scipy.fftpack.fft(s_values)
    xf = np.linspace(0.0, 1.0 / (2.0 * time_interval), number_points / 2)
    # 1/T = frequency
    f = np.linspace(0,
                    1 / time_interval,
                    number_points)

    def example_a():
        plt.ylabel("Amplitude")
        plt.xlabel("Frequency [Hr$^{-1}$]")
        plt.bar(f[:number_points // 2],
                np.abs(fft)[:number_points // 2] * 1 / number_points,
                width=1.5)  # 1 / N is a normalization factor
        plt.plot(f[:number_points // 2],
                 np.abs(fft)[:number_points // 2] * 1 / number_points)
        plt.xlim()
        plt.show()

    def example_b():
        plt.subplot(2, 1, 1)
        plt.plot(xf,
                 2.0 / number_points * np.abs(yf[0:number_points // 2]))
        plt.ylabel("Amplitude")
        plt.xlabel("Frequency [Hr$^{-1}$]")
        plt.xlim([0, 0.1])
        plt.subplot(2, 1, 2)
        plt.plot(xf[1:], 2.0 / number_points * np.abs(yf[0:number_points // 2])[1:])
        # plt.xlim(0, 365 * 24 / 2)
        plt.xlim([0, 0.1])
        plt.ylabel("Amplitude")
        plt.xlabel("Frequency [Hr$^{-1}$]")
        plt.tight_layout()
        plt.show()

    # example_b()

    def example_c():
        from scipy import fftpack
        f_s = 1
        fft_c = fftpack.fft(s_values)
        freqs = fftpack.fftfreq(len(s_values)) * f_s
        fig, ax = plt.subplots()
        ax.stem(freqs, np.abs(fft_c))
        ax.set_xlabel('Frequency in Hertz [Hr$^{-1}$]')
        ax.set_ylabel('Frequency Domain (Spectrum) Magnitude')
        # ax.set_xlim(-f_s / 2, f_s / 2)
        # ax.set_ylim(-5, 110)
        plt.show()

    # example_c()
    # plt.plot(range(len(fft)), fft)
    # plt.show()


def load_example_dataframe():
    """
    This bit of the script doesn't really seem to work, possibly because of
    dodgy internet connection. I will try and directly replace it below with
    a version using my own xarrays.
    :return:
    """
    print('start download')
    df0 = pd.read_csv('https://github.com/ipython-books/'
                      'cookbook-2nd-data/blob/master/'
                      'weather.csv?raw=true',
                      na_values=(-9999),
                      parse_dates=['DATE'])
    print('end download')

    df = df0[df0['DATE'] >= '19940101'] # trim what is available
    print(df.head())
    df_avg = df.dropna().groupby('DATE').mean()
    print(df_avg.head())
    date = df_avg.index.to_datetime()
    temp = (df_avg['TMAX'] + df_avg['TMIN']) / 20.
    return date, temp


def load_my_example(netcdf_name,
                    var_name):
    """

    :param netcdf_name: a path to the netcdf that I want to use.
    :return:
    """
    init_var_list = ["nav_lon", "nav_lat", "sowindsp",
                     "tauuo", "tauvo", "uos", "vos", "zos"]
    drop_var_list = []
    for member in init_var_list:
        if member != var_name:
            drop_var_list.append(member)
    xr_obj_y = xr.load_dataset(netcdf_name).drop_vars(drop_var_list)
    new_date_time_index = xr_obj_y.indexes['time_centered'].to_datetimeindex()
    df = xr_obj_y.to_dataframe().dropna(axis='columns')
    print(df.head())
    df_avg = df.dropna().groupby('time_centered').mean()
    # df_avg['time_centered'] = new_date_time_index
    # df_avg['time_centered'] = pd.to_datetime(df_avg['time_centered'],
    #                                          format=' % Y - % m - % d % H: % M: % S')
    # df_avg['datetime'] = new_date_time_index
    # df_avg = df_avg.reindex(new_date_time_index)
    # df_avg = df_avg.drop(column='time_centered')
    # .to_pydatetime()  #  xr.to_datetimeindex()
    print('df_avg', df_avg.head())
    return new_date_time_index, df_avg


def plot_mean(ax0, temp,
              var_name):
    temp.plot(ax=ax0, lw=.5)
    # ax.set_ylim(-10, 40)
    ax0.set_xlabel('')
    ax0.margins(x=0)
    # myFmt = mdates.DateFormatter('%m')
    # ax0.xaxis.set_major_formatter(myFmt)
    ax0.set_xlabel('Date (January 2005 - December 2005)')
    ax0.set_ylabel('Mean ' + var_name)
    ax0.set_xticks([], minor=False)
    # ax0.set_xlim(np.min(temp.index.values), np.max(temp.index.values))


def plot_fourier_transform(ax1, ax2, temp, coastline='america'):
    # temp = temp.reset_index(drop=True)
    # temp.reset_index(drop=True, inplace=True)
    # temp = temp.drop(['time_centered'], axis=1)
    print('temp head', temp.head())
    temp_fft = sp.fftpack.fft(temp)
    temp_psd = np.abs(temp_fft) ** 2
    fftfreq = sp.fftpack.fftfreq(len(temp_psd), 1. / (365*24))
    i = fftfreq > 0
    ax1.plot(fftfreq[i],
             10 * np.log10(temp_psd[i]))
    ax1.set_xlim(0, 365*24/2)
    # this is the Nyquist frequency
    # ax1.set_xlabel('Frequency (1/year)')
    ax1.set_ylabel('PSD (dB)')
    peaks, _ = sig.find_peaks(temp_psd.flatten(),
                              prominence=1, distance=20)
    ax2.plot(fftfreq[i], temp_psd[i])
    print('shape fft', np.shape(fftfreq))
    print('peaks', peaks)
    for j in range(len(peaks)):
        if peaks[j] < 8760:
            x_coord = fftfreq.flatten()[peaks[j]]
            y_coord = temp_psd.flatten()[peaks[j]]
            if x_coord > 0 and x_coord != 8760:
                ax2.plot(x_coord,
                         y_coord,
                         marker='o')
                ax2.text(x_coord+30, y_coord,
                         str(int(x_coord)) + ', ' + "{0:.2f}".format(y_coord),
                         fontsize=6)
    ax2.set_xlim(0, 365*24/2)
    #  this should be the aliasing limit? (i.e this is the Nyquist frequency)
    ax2.set_xlabel('Frequency (1/year)')
    ax2.set_ylabel('PSD')
    print('temp describe')
    print(temp.describe())
    #  temp['datetime'] = pd.to_datetime(temp.index)
    #  print('temp describe with time_centered')
    #  print(temp.describe)
    return temp_fft, fftfreq


def plot_low_pass_filter(temp_fft, fftfreq,
                         temp, date, var_name):
    temp_fft_bis = temp_fft.copy()
    temp_fft_bis[np.abs(fftfreq) > 0.5] = 0
    temp_slow = np.real(sp.fftpack.ifft(temp_fft_bis))
    fig, ax = plt.subplots(1, 1, figsize=(6, 3))
    temp.plot(ax=ax, lw=.5)
    ax.plot_date(date, temp_slow, '-')
    #  temp_slow_df = temp.assign(slow=temp_slow).drop(columns=var_name)
    #  temp_slow_df.plot(ax=ax, lw=.5
    #  temp_slow.plot(ax=ax)  # ax.plot(date, temp_slow)
    ax.set_xlim(datetime.date(2005, 1, 1),
                datetime.date(2006, 1, 1))
    #  ax.set_ylim(-10, 40)
    ax.set_xlabel('Date')
    ax.set_ylabel('Mean standardised ' + var_name)


def run_all(netcdf_name,
            var_name, coastline='america'):
    # date, temp = load_example_dataframe()
    save_prefix = mif.save_prefix(coastline=coastline)

    fig, axs = plt.subplots(3)
    fig = mps.defined_size(fig, size='dcsq')
    date, temp = load_my_example(netcdf_name, var_name)
    plot_mean(axs[0], temp, var_name)
    temp_fft, fftfreq = plot_fourier_transform(axs[1], axs[2], temp)
    mps.save_fig_twice(cfg.f_fourier_trans_test_loc
                       + save_prefix + 'fourier_transform_plots_' + var_name)
    plot_low_pass_filter(temp_fft, fftfreq,
                         temp, date, var_name)
    mps.save_fig_twice(cfg.f_fourier_trans_test_loc
                       + save_prefix +'fourier_low_pass_' + var_name)
