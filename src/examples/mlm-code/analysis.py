from scipy import stats
import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
import utils
import matplotlib.patches as mpatches


files = [
    'Sheet 1-Cut300 shift.csv',
    'Sheet 1-Cut420and400 shift+RBF.csv',
    'Sheet 1-Cut700 shift + RBF.csv',
    'Sheet 1-Cut730 - shift + RBF.csv',
    'Sheet 1-Cut738 - shift+RBF.csv',
    'Sheet 1-Cut738 - just shifted.csv',
]


dfs = []
plt.figure(figsize=(8,4))
for file in files:
    dfs.append(pd.DataFrame.from_csv('data/tables/'+file))


x = []
y = []
names = [
    '440 Days',
    '340 Days',
    '40 Days', 
    '10 Days',
    '3 Days',
    r'3 Days, no $\Delta\equiv0$ term'
]

for df in dfs:

    ys1 = df[['DISTANCE', '1099TH-Delay', '1099TH-LML']].dropna()  # 6360
    ys2 = df[['DISTANCE', '0903TH-Delay', '0903TH-LML']].dropna()  # 21930
    ys3 = df[['DISTANCE', '1102TH-Delay', '1102TH-LML']].dropna()  # 0
    xs1 = ys1['DISTANCE'] - 6360
    lml1 = ys1['1099TH-LML'].values.astype(float)
    ys1 = ys1['1099TH-Delay']
    xs2 = ys2['DISTANCE'] - 21930
    lml2 = ys2['0903TH-LML'].values.astype(float)
    ys2 = ys2['0903TH-Delay']
    xs3 = ys3['DISTANCE'] - 0
    lml3 = ys3['1102TH-LML'].values.astype(float)
    ys3 = ys3['1102TH-Delay']

    ax = plt.gca()
    # ax.set_yscale('log')

    plt.scatter(np.concatenate((xs1, xs2, xs3))/1000, np.concatenate((ys1, ys2, ys3))*24, marker='x', label=names.pop(-1), s = np.sqrt(np.concatenate((lml1, lml2, lml3))))

    x = np.concatenate((x, xs1, xs2, xs3))
    y = np.concatenate((y, ys1, ys2, ys3))

plt.xlim((0, None))

y = y[np.where(x > 0)]
x = x[np.where(x > 0)]

xs = np.linspace(0, np.max(x)+5000, 1000)
ys = xs / np.sqrt(9.81) / 60 / 60 / 24
# plt.plot(xs/1000, ys*24+0.5, label=r'Theoretical $\Delta = \frac{x}{\sqrt{dg}} + v_0$, $d=1.0$m, $v_0=0.5$ms$^{-1}$', linestyle='--')
plt.fill_between(xs/1000, ys * 0.3162*24+0, ys * 2.236*24+2, alpha=0.5,
                 label=r"Theoretical $\Delta = \frac{x}{\sqrt{dg}} + v_0$," "\n"
                 r"$0.1$m $< d <$ $5.0$m," "\n" r"$0$ms$^{-1}<v_0<2$ms$^{-1}$")

# first_legend = plt.legend(handles=[line1], loc=1)

# # Add the legend manually to the current Axes.
# ax = plt.gca().add_artist(first_legend)

handles, labels = ax.get_legend_handles_labels()
handles = [handles[-1]] + handles[0:-1]
labels = [labels[-1]] + labels[0:-1]
ax.legend(handles, labels, loc=2, ncol=1)  # 8 for log

slope, intercept, r_valu, p_value, std_err = stats.linregress(x, y)
# plt.plot(xs, sloe*xs + intercept, ls='--', lw=0.5)
print(slope, intercept, p_value)
plt.tight_layout()
plt.xlabel(r'Distance between stations, $x$ / km')
plt.ylabel(r'Signal delay, $\Delta$ / hours')
plt.savefig('Delay.pdf', format='pdf', bbox_inches='tight')
plt.show()

