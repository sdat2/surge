import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import cmocean.cm as cmo
import src.utilities.my_wrappers as mwr
import config as cfg
import src.low_freq.comp_low_thermal as clt


@mwr.timeit
def test_long_load(coastline='america'):
    
    if coastline == 'america':
        ds = xr.open_dataset(cfg.data_loc + cfg.merged_1950_name)
        zos = ds.zos.values
        print(ds.coords['time'])
    elif coastline == 'vin-china':
        ds = xr.open_dataset(cfg.data_loc + cfg.vc_merged_1950_name)
        zos = ds.zos.values.T

    print(coastline)
    print(np.mean(zos))
    print(np.shape(zos))
    demeaned_zos = clt.array_demean(zos)
    print(np.mean(demeaned_zos))
    yearly_zos = clt.array_coarsener(demeaned_zos, coarsening_length=360)

    def trend_test_plot():
       plt.clf()
       plt.imshow(yearly_zos.T, cmap='seismic', aspect='auto')
       plt.title(r'Experiment=\texttt{control-1950}, Model=\texttt{HadGEM3}')
       plt.gca().invert_yaxis()
       plt.colorbar(label='Yearly Mean SST diff from 99 year mean')
       plt.show()
