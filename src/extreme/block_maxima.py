"""
block_maxima.py
================

raise TypeError("can only decode Dataset or DataStore objects")
TypeError: can only decode Dataset or DataStore objects

"""
import numpy as np
import xarray as xr
import skextremes as ske
import matplotlib.pyplot as plt
import src.utilities.my_wrappers as mwr
import src.utilities.my_io_functions as mif
import src.plotting.my_plotting_style as mps
import src.plotting.custom_ticker as ctk
import config as cfg
mps.mps_defaults()


@mwr.timeit
def find_yearly_max(input_name=cfg.data_loc+ cfg.merged_1950_name,
                    output_name=cfg.data_loc + 'block_max.json'):

    zos_xr = xr.decode_cf(xr.open_dataset(input_name)).zos
    climatology_mean = zos_xr.mean("time")
    zos_xr = xr.apply_ufunc(lambda x, m: (x - m),
                            zos_xr,
                            climatology_mean)
    # zos_xr =
    print(zos_xr)
    print(zos_xr.coords['time'].values)
    points = zos_xr.coords['point'].values
    mean_npa = np.zeros([len(points), 101], dtype='float32')
    max_npa = np.zeros([len(points), 101], dtype='float32')


    for point in points:
        point_xr = zos_xr.isel(point=point)
        #print(point)
        if point < 910:
            point_df = point_xr.to_dataframe().drop(["x", "y",
                                                     "latitude",
                                                     "longitude"],
                                                     axis=1)
            # print(point_df)
            # point_max_df = point_df.groupby(point_df['time'].dt.year).max()
            # point_df  = point_xr.set_index('time')

            point_max_df = point_df.groupby(lambda x: x.year)['zos'].agg(['mean', 'max'])

            # point_max_df.plot()
            # plt.show()

            # point_max_df = point_df.resample('AS', on='time')['a'].agg(['sum', 'mean', 'max'])
            point_max_np = point_max_df.to_numpy()
            mean_npa[point, :] = point_max_np[:, 0]
            max_npa[point, :] = point_max_np[:, 1]

            # print(point_max_np)
            # print(np.shape(point_max_np))

    save_object = {"mean": mean_npa.tolist(),
                   "max": max_npa.tolist()}

    mif.write_json_object(save_object, output_name)
    # print(point_xr)
    # point_max_df = point_df.groupby('year').max()

    # print(zos_xr)


def skextreme_tactic_1():

    json_saved_obj = mif.read_json_object(cfg.data_loc + 'block_max.json')
    max_npa = np.asarray(json_saved_obj["max"], dtype='float32')

    point_no = np.shape(max_npa)[0]

    model_c_list = []
    model_loc_list = []
    model_scale_list = []

    for point in range(point_no):
        if point < 800:
            model = ske.models.engineering.Lieblein(max_npa[point, :])
            # model.plot_summary()
            # plt.show()
            print(model.c, model.loc, model.scale)

            model_c_list.append(model.c)
            model_loc_list.append(model.loc)
            model_scale_list.append(model.scale)

    save_object = {"model_c_list": model_c_list,
                   "model_loc_list": model_loc_list,
                   "model_scale_list": model_scale_list}

    mif.write_json_object(save_object, cfg.data_loc + 'sketreme_tactic_1.json')


@mwr.timeit
def tactic_1_plot():

    json_saved_obj = mif.read_json_object(cfg.data_loc
                                          + 'sketreme_tactic_1.json')

    fig, axs = plt.subplots(2, sharex=True)
    points = range(len(json_saved_obj["model_c_list"]))

    axs[0].plot(points, json_saved_obj["model_loc_list"])
    axs[0].set_ylabel(r"Location, $\mu$")

    axs[1].plot(points, json_saved_obj["model_scale_list"])
    axs[1].set_ylabel(r"Scale, $\sigma$")

    # axs[2].plot(points, json_saved_obj["model_c_list"])
    #axs[2].set_ylabel("c")

    plt.xlim([0, len(points)])

    ctk.add_ticks()
    plt.tight_layout()
    axs = fig.axes
    mps.label_subplots(axs)
    mps.defined_size(fig, size='dcm')

    mps.save_fig_twice(cfg.plots_loc + 'skextreme_first_tactic')
    # plt.show()


@mwr.timeit
def skextreme_tactic_2(input_name=cfg.data_loc + 'block_max.json',
                       output_name=cfg.data_loc + 'sketreme_tactic_2.json'):

    json_saved_obj = mif.read_json_object(input_name)
    max_npa = np.asarray(json_saved_obj["max"], dtype='float32')

    point_no = np.shape(max_npa)[0]

    model_shape_list = []
    model_location_list = []
    model_scale_list = []

    for point in range(point_no):

        if point < 910:
            try:
                print('\n', point, 'out of', point_no, '\n')
                model = ske.models.classic.GEV(max_npa[point, :],
                                               fit_method='mle',
                                               ci=0.05,
                                               ci_method='delta')


                model_shape_list.append([model.params['shape'],
                                         model.params_ci['shape'][1]
                                         -model.params['shape']])

                model_location_list.append([model.params['location'],
                                            model.params_ci['location'][1]
                                            -model.params['location']])

                model_scale_list.append([model.params['scale'],
                                         model.params_ci['scale'][1]
                                         -model.params['scale'] ])
            except:
                model_shape_list.append([np.nan, np.nan])
                model_location_list.append([np.nan, np.nan])
                model_scale_list.append([np.nan, np.nan])


    save_object = {"model_shape_list": model_shape_list,
                   "model_location_list": model_location_list,
                   "model_scale_list": model_scale_list}

    mif.write_json_object(save_object,
                          output_name)


@mwr.timeit
def tactic_2_plot(input_name=cfg.data_loc+ 'sketreme_tactic_2.json',
                  output_name=cfg.plots_loc + 'skextreme_second_tactic',
                  coastline='america'):
    import scipy.stats as ss

    json_saved_obj = mif.read_json_object(input_name)

    fig, axs = plt.subplots(3, sharex=True)
    points = range(len(json_saved_obj["model_location_list"]))

    location_npa = np.asarray(json_saved_obj["model_location_list"])
    scale_npa = np.asarray(json_saved_obj["model_scale_list"])
    shape_npa = np.asarray(json_saved_obj["model_shape_list"])

    loc_scal_rp =  ss.pearsonr(location_npa[:, 0], scale_npa[:, 0]) # r_{p, \mu \sigma}
    scal_shap_rp = ss.pearsonr(scale_npa[:, 0], shape_npa[:, 0]) # r_{p, \sigma \xi}
    shap_loc_rp = ss.pearsonr(shape_npa[:, 0], location_npa[:, 0]) # r_{p, \xi \mu}

    loc_scal_str = r"$r_{p;\; \mu \sigma}=$ "+ r"{0:.2f}".format(loc_scal_rp[0])
    scal_shap_str =r"$r_{p;\; \sigma \xi}=$" + r"{0:.2f}".format(scal_shap_rp[0])
    shap_loc_str = r"$r_{p;\; \xi \mu}=$" + r"{0:.2f}".format(shap_loc_rp[0])

    print(loc_scal_rp, scal_shap_rp, shap_loc_rp)

    location_up = location_npa[:, 0] + location_npa[:, 1]
    location_down = location_npa[:, 0] - location_npa[:, 1]

    location_up_68 = location_npa[:, 0] + location_npa[:, 1]/2
    location_down_68 = location_npa[:, 0] - location_npa[:, 1]/2

    shape_up = shape_npa[:, 0] + shape_npa[:, 1]
    shape_down = shape_npa[:, 0] - shape_npa[:, 1]

    shape_up_68  = shape_npa[:, 0] + shape_npa[:, 1]/2
    shape_down_68  = shape_npa[:, 0] - shape_npa[:, 1]/2

    scale_up = scale_npa[:, 0] + scale_npa[:, 1]
    scale_down = scale_npa[:, 0] - scale_npa[:, 1]

    scale_up_68 = scale_npa[:, 0] + scale_npa[:, 1]/2
    scale_down_68 = scale_npa[:, 0] - scale_npa[:, 1]/2

    axs[0].plot(points, location_npa[:, 0],
                color='#CB4154')
    axs[0].fill_between(points, location_down, location_up,
                        color='#a3c1ad', alpha=0.2)
    axs[0].fill_between(points, location_down_68, location_up_68,
                        color='#a3c1ad', alpha=0.5)
    axs[0].set_ylabel(r"Location, $\mu$, (m)")

    axs[1].plot(points, scale_npa[:, 0],
                color='#CB4154')
    axs[1].fill_between(points, scale_down, scale_up,
                        color='#a3c1ad', alpha=0.2)
    axs[1].fill_between(points, scale_down_68, scale_up_68,
                        color='#a3c1ad', alpha=0.5)
    axs[1].set_ylabel(r"Scale, $\sigma$, (m)")

    axs[2].plot(points, shape_npa[:, 0,],
                label='Estimate',
                color='#CB4154')
    axs[2].fill_between(points, shape_down_68, shape_up_68,
                        color='#a3c1ad', alpha=0.5,
                        label='68\% Confidence Interval')
    axs[2].fill_between(points, shape_down, shape_up,
                        color='#a3c1ad', alpha=0.2,
                        label='95\% Confidence Interval')
    axs[2].set_ylabel(r"Shape, $\xi$")

    plt.legend(loc="lower right")

    fig.suptitle(loc_scal_str +',~~~~'+ scal_shap_str +',~~~~'+ shap_loc_str,
                     y=1.01, fontsize=10)

    plt.xlim([0, len(points)])

    ctk.add_ticks(coastline=coastline)
    plt.tight_layout()
    axs = fig.axes
    mps.label_subplots(axs)
    mps.defined_size(fig, size='dcm')

    mps.save_fig_twice(output_name)
    # plt.show()

# "MM 357, NO 172"

def plot_individual_both():
    """Plot Individual Plots"""

    json_saved_obj = mif.read_json_object(cfg.data_loc + 'block_max.json')
    max_npa = np.asarray(json_saved_obj["max"], dtype='float32')

    initials = ["NO"]
    points = [172]

    # initials = ["MM", "NO", "PNO"]
    # points = [357, 172, 172-12]

    for i in range(len(points)):
        print(initials[i])

        model = ske.models.classic.GEV(max_npa[points[i], :],
                                       ev_unit=' $\;$(m)',
                                       block_unit='(Yrs)',
                                       fit_method='mle',
                                       ci=0.05,
                                       ci_method='delta',
                                       # ci_method='bootstrap'
                                       )

        print(model.params)
        print(model.params_ci)

        if True:
            model.plot_summary()
            fig = plt.gcf()

            try:
                mu_string = r"{0:.2f}$\pm${1:.2f}".format(model.params['location'],
                                                          np.abs((model.params['location']
                                                                 - model.params_ci['location'][1])/2))
                sigma_string = r"{0:.2f}$\pm${1:.2f}".format(model.params['scale'],
                                                             np.abs((model.params['scale']
                                                                    - model.params_ci['scale'][1])/2))
                xi_string = r"{0:.2f}$\pm${1:.2f}".format(model.params['shape'],
                                                          np.abs((model.params['shape']
                                                                 - model.params_ci['shape'][1])/2))

            except:
                print('Confidence itervals not available')
                mu_string = r"{0:.2f}".format(model.params['location'])
                sigma_string = r"{0:.2f}".format(model.params['scale'])
                xi_string = r"{0:.2f}".format(model.params['shape'])

            fig.suptitle(r'$\quad$ $\mu=$' + mu_string
                         + r'm, $\sigma=$' + sigma_string
                         + r'm, $\xi=$' + xi_string,
                         y=1.03, fontsize=10)

            # plt.text(0.5, 1.05, r'$\mu=$, $\sigma=$, $\xi=$',
            #          transform=fig.transFigure,
            #          horizontalalignment='center')

            mps.defined_size(fig, size='dcm')
            plt.tight_layout()
            axs = fig.axes
            mps.label_subplots(axs)

            mps.save_fig_twice(cfg.plots_loc + 'GEV_model' + initials[i])


            # plt.show()

            plt.clf()

        if False:

            model.plot_pi_gp(0, 1.9, 12)
            fig = plt.gcf()
            mps.defined_size(fig, size='sc')
            # plt.show()
            mps.save_fig_twice(cfg.plots_loc + 'GEV_pi_plateau_' + initials[i])
            plt.clf()

def lieblien():
        model = ske.models.engineering.Lieblein(max_npa[points[i], :],
                                                ev_unit=' (m)',
                                                block_unit='(Yrs)')
        model.plot_summary()

        fig = plt.gcf()
        mps.defined_size(fig, size='dcsh')
        plt.tight_layout()
        axs = fig.axes
        mps.label_subplots(axs)
        plt.title('')
        mps.save_fig_twice(cfg.plots_loc + 'Lieblein_model' + initials[i])

        # plt.show()

        plt.clf()
