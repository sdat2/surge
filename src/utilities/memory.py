"""
memory.py
=========
https://stackoverflow.com/questions/938733/total-memory-used-by-python-process
http://code.activestate.com/recipes/286222/
https://www.pluralsight.com/blog/tutorials/how-to-profile-memory-usage-in-python
https://pypi.org/project/memory-profiler/
memory-profiler 0.57.0


"""
import os
import resource as res
import config as cfg


def get_mem():
    # if in linux, the answer is in kilobytes.
    # if on mac, the answer is in bytes
    return res.getrusage(res.RUSAGE_SELF).ru_maxrss


_proc_status = '/proc/%d/status' % os.getpid()

_scale = {'kB': 1024.0, 'mB': 1024.0*1024.0,
          'KB': 1024.0, 'MB': 1024.0*1024.0}


def _VmB(VmKey):
    '''Private.
    '''
    global _proc_status, _scale
     # get pseudo file  /proc/<pid>/status
    try:
        t = open(_proc_status)
        v = t.read()
        t.close()
    except:
        return 0.0  # non-Linux?
     # get VmKey line e.g. 'VmRSS:  9999  kB\n ...'
    i = v.index(VmKey)
    v = v[i:].split(None, 3)  # whitespace
    if len(v) < 3:
        return 0.0  # invalid format?
     # convert Vm value to bytes
    return float(v[1]) * _scale[v[2]]


def linux_memory(since=0.0):
    '''Return memory usage in bytes.
    '''
    return _VmB('VmSize:') - since


def linux_resident(since=0.0):
    '''Return resident memory usage in bytes.
    '''
    return _VmB('VmRSS:') - since



def linux_stacksize(since=0.0):
    '''Return stack size in bytes.
    '''
    return _VmB('VmStk:') - since
