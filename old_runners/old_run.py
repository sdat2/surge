import src.utilities.my_wrappers as mwr
import config as cfg

@mwr.timeit
def run_fftw_test():
    import src.old_tests.fftw_tests as ftw
    ftw.init_test()
    ftw.one_dimensional_transforms()
    ftw.multi_dimensional_transforms()
    ftw.fftw_builders()


def run_fft_test():
    import src.old_tests.fourier_transforms as fft
    fft.tests()

# run_fft_test()


def run_reg():
    import src.currently_unused.r_rotate_and_regress as rrr
    rrr.rotate_and_regress()

# run_reg()
