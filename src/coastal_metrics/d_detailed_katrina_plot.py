import numpy as np
import numpy.ma as ma
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import xarray as xr
import cmocean.cm as cmo
import src.plotting.custom_ticker as ctk
import src.plotting.my_plotting_style as mps
import src.utilities.my_io_functions as mif
import src.utilities.my_wrappers as mwr
import src.get_points.g_plot_datapoints as gpd
import config as cfg
import src.plotting.places as plc
mps.mps_defaults(quality='high')


@mwr.timeit
def load_katrina():
    """

    # http://xarray.pydata.org/en/stable/time-series.html
    :return:
    """
    training_set = xr.decode_cf(xr.open_dataset(cfg.data_loc + cfg.merged_2005_file_name))
    bathymetry = xr.open_dataset(cfg.slim_bath_path)

    print(bathymetry)
    tick_list = ctk.return_place_tick_list()
    print(tick_list)
    poi_tick = 0

    for i in range(len(tick_list)):
        if tick_list[i][0] == 'NO':
            poi_tick = tick_list[i][1]

    points_either_side = 40
    orig_angle_npa = np.asarray(mif.read_csv_file('data/angle.csv')).ravel()[int(poi_tick)-points_either_side:
                                                                             int(poi_tick)+points_either_side]
    smoothed_angle_grid = np.asarray(mif.read_json_object(cfg.data_loc + 'smoothed_angle.json'), dtype='float32')
    angle_npa = smoothed_angle_grid[:, 10].ravel()[int(poi_tick)-points_either_side:
                                                   int(poi_tick)+points_either_side]

    select_list = mif.read_csv_file(cfg.h_slimmed_ordered_point_loc)
    no_point = select_list[int(poi_tick)]
    box_len = 30
    bathymetry = bathymetry.isel(x=slice(int(no_point[1])-box_len, int(no_point[1])+box_len),
                                 y=slice(int(no_point[0])-box_len*2, int(no_point[0])+box_len))
    bath_nav_lats = bathymetry.coords['nav_lat'].values
    bath_nav_lons = bathymetry.coords['nav_lon'].values
    bath_values = bathymetry.variables['Bathymetry'].values
    bath_values = ma.masked_where(bath_values == 0, bath_values)

    # assert(len(select_list) == 757)
    # print(training_set)
    # Katrina is at point 250
    training_set = training_set.sel(time_centered=slice('2005-08-24', '2005-09-04'))
    print(training_set)
    place_of_interest_coast_point = [training_set.isel(point=poi_tick).coords['nav_lon'].values,
                                     training_set.isel(point=poi_tick).coords['nav_lat'].values]

    point_near_no = [training_set.isel(point=poi_tick-12).coords['nav_lon'].values,
                     training_set.isel(point=poi_tick-12).coords['nav_lat'].values]
    place_of_interest_xr = xr.decode_cf(training_set.isel(point=poi_tick))
    point_near_no_xr = xr.decode_cf(training_set.isel(point=poi_tick-12))
    high_water_place_xr = xr.decode_cf(training_set.isel(point=poi_tick+17))
    high_water_place_point = [high_water_place_xr.coords['nav_lon'].values,
                              high_water_place_xr.coords['nav_lat'].values]
    training_set = training_set.isel(point=slice(poi_tick-points_either_side, poi_tick+points_either_side))
    print(training_set)
    nav_lats = training_set.coords['nav_lat'].values
    nav_lons = training_set.coords['nav_lon'].values

    def nice_plot():
        print(np.shape(orig_angle_npa))
        print(np.shape(angle_npa))
        assert(np.shape(orig_angle_npa) == np.shape(angle_npa))
        gpd.create_nice_map(extent=[-90, -87.6, 27.9, 30.5])
        plt.plot(nav_lons, nav_lats,
                 color='green', label='ORCA12 coast')
        vmin = 0
        vmax = 200
        plt.scatter(bath_nav_lons.ravel(),
                    bath_nav_lats.ravel(),
                    c=bath_values.ravel(),
                    s=3,
                    marker='o',
                    cmap=cmo.deep,
                    alpha=0.5,
                    norm=colors.Normalize(vmin=vmin,
                                          vmax=vmax),
                    label='Bathymetry')
        cbar = plt.colorbar(# fraction=2,
                            # pad=0.2,
                            norm=colors.Normalize(vmin=vmin,
                                                  vmax=vmax))
        poi_map_point = plc.place_d["New Orleans, LA"]["loc_pair"]
        plt.plot(poi_map_point[0], poi_map_point[1], '+',
                 color='pink', label='NO')
        plt.plot(place_of_interest_coast_point[0],
                 place_of_interest_coast_point[1], 'x',
                 color='orange', label="NO's closest coast",
                 markersize=10)
        plt.plot(high_water_place_point[0],
                 high_water_place_point[1],
                 'x',
                 color='black',
                 label='Nearby bay', markersize=10)
        plt.plot(point_near_no[0],
                 point_near_no[1],
                 '+',
                 color='purple',
                 label='Nearby headland', markersize=10)
        cbar.set_label('Depth (m)')
        cbar.ax.invert_yaxis()
        plt.quiver(nav_lons, nav_lats,
                   np.sin(orig_angle_npa),
                   np.cos(orig_angle_npa),
                   color='green',
                   label='Original angles',
                   alpha=0.5)
        plt.quiver(nav_lons, nav_lats,
                   np.sin(angle_npa),
                   np.cos(angle_npa),
                   color='red',
                   label='$\sigma$=10pt smooth',
                   alpha=0.5)

        ax = plt.gca()
        ax.text(0.02, 0.95, 'E',
                color='black',
                transform=ax.transAxes,
                fontsize=8, fontweight='bold', va='top')

        plt.legend(fontsize=6, ncol=2, loc=8)
        fig = plt.gcf()
        mps.defined_size(fig, size='sc')
        mps.save_fig_twice(cfg.plots_loc + 'new_orleans_map')

    nice_plot()

    def plot_katrina():
        fig, axs = plt.subplots(4, sharex=True)
        times = place_of_interest_xr.coords['time_centered'].values
        axs[0].plot(times, place_of_interest_xr.zos.values, color='orange')
        axs[0].plot(times, point_near_no_xr.zos.values, color='purple')
        axs[0].plot(times, high_water_place_xr.zos.values, color='black')
        axs[0].set_ylabel(r'$\eta$ (m)')
        # plt.legend(fontsize=6)

        axs[1].plot(times, place_of_interest_xr.tauvo.values, color='orange')
        axs[1].plot(times, point_near_no_xr.tauvo.values, color='purple')
        axs[1].plot(times, high_water_place_xr.tauvo.values, color='black')
        axs[1].set_ylabel(r'$\tau_v$ (Pa)')
        axs[2].plot(times, place_of_interest_xr.tauuo.values, color='orange')
        axs[2].plot(times, point_near_no_xr.tauuo.values, color='purple')
        axs[2].plot(times, high_water_place_xr.tauuo.values, color='black')
        axs[2].set_ylabel(r'$\tau_u$ (Pa)')
        axs[3].plot(times, place_of_interest_xr.sowindsp.values, color='orange', label="New Orleans' Closest Coast")
        axs[3].plot(times, point_near_no_xr.sowindsp.values, color='purple', label='Nearby Headland')
        axs[3].plot(times, high_water_place_xr.sowindsp.values, color='black', label='Nearby Bay')
        axs[3].set_ylabel(r'$|v_{w}|$ (m s$^{-1}$)')

        plt.xlim(np.min(times), np.max(times))
        # plt.xlabel('Time')
        mps.defined_size(fig, size='sc')
        mps.label_subplots(axs=axs, fontsize=8)
        import matplotlib.dates as mdates

        formatter = mdates.DateFormatter("%m-%d")
        # format as dates
        axs[3].xaxis.set_major_formatter(formatter)
        locator = mdates.DayLocator()
        axs[3].xaxis.set_major_locator(locator)
        plt.xticks(rotation=90)

        # plt.plot(x_values, y_values)

        # place_of_interest_xr.zos.plot(ax=axs[0])
        # place_of_interest_xr.tauuo.plot(ax=axs[1])
        # place_of_interest_xr.tauvo.plot(ax=axs[2])
        # place_of_interest_xr.sowindsp.plot(ax=axs[3])
        mps.save_fig_twice(cfg.plots_loc + 'katrina_graph')

    plot_katrina()

    # load_miami()
    # load_boston()
    # print(place_of_interest_xr)


@mwr.timeit
def load_miami():
    """

    # http://xarray.pydata.org/en/stable/time-series.html
    :return:
    """
    training_set = xr.decode_cf(xr.open_dataset(cfg.data_loc + cfg.merged_2005_file_name))
    bathymetry = xr.open_dataset(cfg.slim_bath_path)
    print(bathymetry)
    tick_list = ctk.return_place_tick_list()
    print(tick_list)
    poi_tick = 0
    for i in range(len(tick_list)):
        if tick_list[i][0] == 'MM':
            poi_tick = tick_list[i][1]
    points_either_side = 50
    orig_angle_npa = np.asarray(mif.read_csv_file('data/angle.csv')).ravel()[int(poi_tick)-points_either_side:
                                                                             int(poi_tick)+points_either_side]
    smoothed_angle_grid = np.asarray(mif.read_json_object(cfg.data_loc + 'smoothed_angle.json'), dtype='float32')
    angle_npa = smoothed_angle_grid[:, 10].ravel()[int(poi_tick)-points_either_side:
                                                   int(poi_tick)+points_either_side]

    select_list = mif.read_csv_file(cfg.h_slimmed_ordered_point_loc)
    poi_point = select_list[int(poi_tick)]
    box_len = 30
    bathymetry = bathymetry.isel(x=slice(int(poi_point[1])-box_len, int(poi_point[1])+box_len),
                                 y=slice(int(poi_point[0])-box_len*2, int(poi_point[0])+box_len))
    bath_nav_lats = bathymetry.coords['nav_lat'].values
    bath_nav_lons = bathymetry.coords['nav_lon'].values
    bath_values = bathymetry.variables['Bathymetry'].values
    bath_values = ma.masked_where(bath_values == 0, bath_values)

    training_set = training_set.sel(time_centered=slice('2005-08-25', '2005-09-05'))
    print(training_set)
    place_of_interest_coast_point = [training_set.isel(point=poi_tick).coords['nav_lon'].values,
                                     training_set.isel(point=poi_tick).coords['nav_lat'].values]
    training_set = training_set.isel(point=slice(poi_tick-points_either_side, poi_tick+points_either_side))
    print(training_set)
    nav_lats = training_set.coords['nav_lat'].values
    nav_lons = training_set.coords['nav_lon'].values

    def nice_plot():
        poi_map_point = plc.place_d["Miami, FL"]["loc_pair"]
        gpd.create_nice_map(extent=[poi_map_point[0]-2, poi_map_point[0]+2,
                                    poi_map_point[1]-2, poi_map_point[1]+2])
        plt.plot(nav_lons, nav_lats, color='green', label='ORCA12 coast')
        vmin = 0
        vmax = 200
        plt.scatter(bath_nav_lons.ravel(),
                    bath_nav_lats.ravel(),
                    c=bath_values.ravel(),
                    cmap=cmo.deep,
                    alpha=0.5,
                    norm=colors.Normalize(vmin=vmin,
                                          vmax=vmax),
                    label='bathymetry')
        cbar = plt.colorbar(norm=colors.Normalize(vmin=vmin,
                                                  vmax=vmax))
        plt.plot(poi_map_point[0], poi_map_point[1], 'x', color='pink', label="Miami, FL")
        plt.plot(place_of_interest_coast_point[0], place_of_interest_coast_point[1], 'x',
                 color='orange', label='Miami Closest Coast')
        cbar.set_label('Depth (m)')
        cbar.ax.invert_yaxis()
        plt.quiver(nav_lons, nav_lats,
                   np.sin(orig_angle_npa),
                   np.cos(orig_angle_npa),
                   color='green',
                   label='original angles',
                   alpha=0.5)
        plt.quiver(nav_lons, nav_lats,
                   np.sin(angle_npa),
                   np.cos(angle_npa),
                   color='red',
                   label='10 pt $\sigma$ smoothed',
                   alpha=0.5)
        plt.legend()
        fig = plt.gcf()
        mps.defined_size(fig, size='dcsq')
        mps.save_fig_twice(cfg.plots_loc + 'miami_map')

    nice_plot()


@mwr.timeit
def load_boston():
    """

    # http://xarray.pydata.org/en/stable/time-series.html
    :return:
    """
    training_set = xr.decode_cf(xr.open_dataset(cfg.data_loc + cfg.merged_2005_file_name))
    bathymetry = xr.open_dataset(cfg.slim_bath_path)
    print(bathymetry)
    tick_list = ctk.return_place_tick_list()
    print(tick_list)
    poi_tick = 0
    for i in range(len(tick_list)):
        if tick_list[i][0] == 'BO':
            poi_tick = tick_list[i][1]
    points_either_side = 70
    orig_angle_npa = np.asarray(mif.read_csv_file('data/angle.csv')).ravel()[int(poi_tick)-points_either_side:
                                                                             int(poi_tick)+points_either_side]
    smoothed_angle_grid = np.asarray(mif.read_json_object(cfg.data_loc + 'smoothed_angle.json'), dtype='float32')
    angle_npa = smoothed_angle_grid[:, 15].ravel()[int(poi_tick)-points_either_side:
                                                   int(poi_tick)+points_either_side]
    select_list = mif.read_csv_file(cfg.h_slimmed_ordered_point_loc)
    poi_point = select_list[int(poi_tick)]
    box_len = 30
    bathymetry = bathymetry.isel(x=slice(int(poi_point[1])-box_len, int(poi_point[1])+box_len),
                                 y=slice(int(poi_point[0])-box_len*2, int(poi_point[0])+box_len))
    bath_nav_lats = bathymetry.coords['nav_lat'].values
    bath_nav_lons = bathymetry.coords['nav_lon'].values
    bath_values = bathymetry.variables['Bathymetry'].values
    bath_values = ma.masked_where(bath_values == 0, bath_values)

    training_set = training_set.sel(time_centered=slice('2005-08-25', '2005-09-05'))

    print(training_set)
    place_of_interest_coast_point = [training_set.isel(point=poi_tick).coords['nav_lon'].values,
                                     training_set.isel(point=poi_tick).coords['nav_lat'].values]
    training_set = training_set.isel(point=slice(poi_tick-points_either_side,
                                                 poi_tick+points_either_side))
    print(training_set)
    nav_lats = training_set.coords['nav_lat'].values
    nav_lons = training_set.coords['nav_lon'].values

    def nice_plot():
        poi_map_point = plc.place_d["Boston, MA"]["loc_pair"]
        gpd.create_nice_map(extent=[poi_map_point[0]-1.2, poi_map_point[0]+2,
                                    poi_map_point[1]-2, poi_map_point[1]+2])
        plt.plot(nav_lons, nav_lats, color='green', label='ORCA12 coast')
        vmin = 0
        vmax = 200
        plt.scatter(bath_nav_lons.ravel(),
                    bath_nav_lats.ravel(),
                    c=bath_values.ravel(),
                    cmap=cmo.deep,
                    alpha=0.5,
                    norm=colors.Normalize(vmin=vmin,
                                          vmax=vmax),
                    label='bathymetry')
        cbar = plt.colorbar(norm=colors.Normalize(vmin=vmin,
                                                  vmax=vmax))
        plt.plot(poi_map_point[0], poi_map_point[1], 'x', color='pink', label="Boston, MA")
        plt.plot(place_of_interest_coast_point[0], place_of_interest_coast_point[1], 'x',
                 color='orange', label='Boston Closest Coast')
        cbar.set_label('Depth (m)')
        cbar.ax.invert_yaxis()
        plt.quiver(nav_lons, nav_lats,
                   np.sin(orig_angle_npa),
                   np.cos(orig_angle_npa),
                   color='green',
                   label='original angles',
                   alpha=0.5)
        plt.quiver(nav_lons, nav_lats,
                   np.sin(angle_npa),
                   np.cos(angle_npa),
                   color='red',
                   label='10 pt $\sigma$ smoothed',
                   alpha=0.5)
        plt.legend()
        fig = plt.gcf()
        mps.defined_size(fig, size='dcsq')
        mps.save_fig_twice(cfg.plots_loc + 'boston_map')
    nice_plot()


def heatmap_example():
    from matplotlib.colors import LogNorm
    import matplotlib.pyplot as plt
    import numpy as np
    np.random.seed(0)

    # normal distribution center at x=0 and y=5
    x = np.random.randn(100000)
    y = np.random.randn(100000) + 5

    plt.hist2d(x, y, bins=40, norm=LogNorm())
    plt.xlabel('x')
    plt.ylabel('y')
    cbar = plt.colorbar()
    cbar.ax.set_ylabel('Counts')
    plt.show()
