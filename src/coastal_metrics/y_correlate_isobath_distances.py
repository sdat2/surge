"""
y_correlate_isobath_distances.py
================================

Make a small plot correlating the different isobath distances lines.
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import cmocean.cm as cmo
import src.utilities.my_io_functions as mif
import config as cfg
import src.plotting.my_plotting_style as mps
mps.mps_defaults()


def run_correlate_isobaths(coastline='america'):
    save_prefix = mif.save_prefix(coastline=coastline)
    saved_d = mif.read_json_object(cfg.data_loc + save_prefix+'isobath_dist.json')
    depths_list = saved_d["depths_list"]
    dist_lol = saved_d["dist_lol"]
    for i in range(len(depths_list)):
        print(depths_list[i])
    dist_npa = np.asarray(dist_lol)
    print('shape', np.shape(dist_npa))
    corrcoef = np.corrcoef(dist_npa.T[2:, :])
    print('corrcoef', corrcoef)
    print('shape', np.shape(corrcoef))
    maxim = np.max(corrcoef)
    minim = np.min(corrcoef)
    vmin = np.min([minim, -maxim])
    vmax = np.max([-minim, maxim])
    plt.imshow(corrcoef, cmap=cmo.balance,
               norm=colors.Normalize(vmin=vmin,
                                     vmax=vmax))
    cbar = plt.colorbar(pad=0.02, shrink=0.6)
    cbar.set_label('Correlation Coefficient')
    plt.gca().invert_yaxis()
    depths_list.pop(0)
    depths_list.pop(1)
    plt.xticks(range(8),
               depths_list)
    fig = plt.gcf()
    mps.defined_size(fig, size='scsq')
    plt.yticks(range(8),
               depths_list)
    plt.xlabel(r'Distance to $x$ m Isobath')
    plt.ylabel(r'Distance to $x$ m Isobath')
    mps.save_fig_twice(cfg.plots_loc
                       + save_prefix +'isobath_correlate')
    plt.show()

# dict_to_save = {"depths_list": depths_list,
#                 "dist_lol": dist_lol}
