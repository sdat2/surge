
    These files are not my own, although I have changed their syntax, as I find using the
* opperator to import all functions from a seperate file to make the repository unreadable.
They were provided to me by personal communication with Matthew Le Maitre,
a recent graduate of the Department of Computer Science at the University of Cambridge.
I keep them as a useful guide to using sci-kit-learn to run and optimise a bespoke kernel.
His project involved using these to try and extract the rate of flood propogation from
the upper Thames. Although it's results were negative, the work he completed was nonetheless
impressive, and acted as an inspiration to my Master's project, despite the fact that all
of the specifics were different.
    Specifically I think that reading his project put me along the path to reading works such
as the PhD Thesis of Duavenaud (Cambridge, 2014) where the act of choosing a Kernel is
viewed as a highly important step in Gaussian Process Regression. And that the field expert should put
a large amount of thought into, as it encodes the complete prior, and will drastically change the
results that kriging will produce.
    Both works are cited in my Master's project report, and so I can hardly be accused of plagiarism
on this count. Indeed, having this example of using the sklearn syntax for bespoke gaussian process
regression is of the same help as a high quality stack-exchange answer, which is something on which
all programmers rely.
