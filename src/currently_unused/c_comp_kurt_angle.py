import numpy as np
import scipy.stats as ss
import src.utilities.my_io_functions as mif
import matplotlib.pyplot as plt
import config as cfg
import src.plotting.my_plotting_style as mps
import src.plotting.custom_ticker as ctk
from sklearn.linear_model import LinearRegression
mps.mps_defaults()


def comp():
    """
    A function for comparing the point stats with the convexity results.
    The results aren't that interesting :(
    :return:
    """
    angle_derivative_grid = np.asarray(mif.read_json_object(cfg.data_loc
                                        + 'angle_derivatives.json'), dtype='float32')
    smoothed_angle_grid = np.asarray(mif.read_json_object(cfg.data_loc
                                        + 'smoothed_angle.json'), dtype='float32')
    moments_list = mif.read_json_object(cfg.data_loc + 'moments_list.json')
    response_list = mif.read_json_object(cfg.data_loc + 'response_analysis.json')
    bath_gradient = mif.read_json_object(cfg.data_loc+'bath_grad.json')

    choose_first = np.asarray(response_list[0], dtype='float32')
    print(np.shape(choose_first))

    for i in range(5):
        pearr = ss.pearsonr(choose_first[i, :], angle_derivative_grid[:, 15])
        print(pearr)

    for i in range(len(moments_list)):
        moments_list[i] = np.asarray(moments_list[i], dtype='float32')

    print('moments list shape', np.shape(moments_list[0]))
    print('shape angle derivative grid', np.shape(angle_derivative_grid))
    print('smoothed angle shape', np.shape(smoothed_angle_grid))

                # print('i = '+str(i)+', j = '+str(j)+', k = '+str(k), pearr)

    def plot_correlations():
        pearr_grid = np.zeros([len(moments_list), 4, 99])
        for i in range(len(moments_list)):
            for j in range(4):
                for k in range(99):
                    pearr = ss.pearsonr(moments_list[i][:, j], angle_derivative_grid[:, k])
                    pearr_grid[i, j, k] = pearr[0]
        fig, axs = plt.subplots(1, 4, sharey=True)
        axs[0].plot(range(99), pearr_grid[0, 0, :],  color='blue', label='2004')
        axs[0].plot(range(99), pearr_grid[1, 0, :],  color='red', label='2005')
        axs[0].set_xlabel(r'$\sigma$')
        axs[0].set_ylabel(r'$r_p$')
        axs[1].plot(range(99), pearr_grid[0, 1, :],  color='blue', label='2004')
        axs[1].plot(range(99), pearr_grid[1, 1, :],  color='red', label='2005')
        axs[1].set_xlabel(r'$\sigma$')
        axs[2].plot(range(99), pearr_grid[0, 2, :],  color='blue', label='2004')
        axs[2].plot(range(99), pearr_grid[1, 2, :],  color='red', label='2005')
        axs[2].set_xlabel(r'$\sigma$')
        axs[3].plot(range(99), pearr_grid[0, 3, :],  color='blue', label='2004')
        axs[3].plot(range(99), pearr_grid[1, 3, :],  color='red', label='2005')
        axs[3].set_xlabel(r'$\sigma$')
        plt.legend()
        for i, label in enumerate(('A', 'B', 'C', 'D')):
            font_color = 'black'
            axs.ravel()[i].text(0.05, 0.95, label,
                                color=font_color,
                                transform=axs.ravel()[i].transAxes,
                                fontsize=13, fontweight='bold', va='top')
        mps.defined_size(fig, size='dcsh')
        mps.save_fig_twice(cfg.plots_loc + 'check_similarity')

    def plot_correlations_2():
        pearr_grid = np.zeros([len(moments_list), 4, 99])
        for i in range(len(moments_list)):
            for j in range(4):
                for k in range(99):
                    pearr = ss.pearsonr(choose_first[j, :], angle_derivative_grid[:, k])
                    pearr_grid[i, j, k] = pearr[0]
        fig, axs = plt.subplots(1, 4, sharey=True)
        axs[0].plot(range(99), pearr_grid[0, 0, :],  color='orange', label='2004')
        # axs[0].plot(range(99), pearr_grid[1, 0, :],  color='red', label='2005')
        axs[0].set_xlabel(r'$\sigma$')
        axs[0].set_ylabel(r'$r_p$')
        axs[1].plot(range(99), pearr_grid[0, 1, :],  color='orange', label='2004')
        # axs[1].plot(range(99), pearr_grid[1, 1, :],  color='red', label='2005')
        axs[1].set_xlabel(r'$\sigma$')
        axs[2].plot(range(99), pearr_grid[0, 2, :],  color='orange', label='2004')
        # axs[2].plot(range(99), pearr_grid[1, 2, :],  color='red', label='2005')
        axs[2].set_xlabel(r'$\sigma$')
        axs[3].plot(range(99), pearr_grid[0, 3, :],  color='orange', label='2004')
        # axs[3].plot(range(99), pearr_grid[1, 3, :],  color='red', label='2005')
        axs[3].set_xlabel(r'$\sigma$')
        # plt.legend()
        for i, label in enumerate(('A', 'B', 'C', 'D')):
            font_color = 'black'
            axs.ravel()[i].text(0.05, 0.95, label,
                                color=font_color,
                                transform=axs.ravel()[i].transAxes,
                                fontsize=13, fontweight='bold', va='top')
        mps.defined_size(fig, size='dcsh')
        mps.save_fig_twice(cfg.plots_loc + 'check_new')

    # plot_correlations()
    # plot_correlations_2()

    def plot_corcoeff_derivative_bath():
        fig, axs = plt.subplots(3, sharex=True)
        axs[0].plot(range(len(choose_first[2, :].ravel().tolist())), choose_first[2, :])
        axs[0].set_ylabel('Responsiveness')
        axs[1].plot(range(len(choose_first[2, :].ravel().tolist())), angle_derivative_grid[:, 15])
        axs[1].set_ylabel('Angle Derivative ($\sigma=15$)')
        axs[2].plot(range(len(choose_first[2, :].ravel().tolist())), bath_gradient)
        axs[2].set_ylabel('$\Delta$Bath (m)')
        plt.xlim(0, len(choose_first[2, :].ravel().tolist()))
        ctk.add_ticks()
        mps.save_fig_twice(cfg.plots_loc + 'reg_ready')
        plt.show()

    # plot_corcoeff_derivative_bath()

    for i in range(len(bath_gradient)):
        bath_gradient[i] = np.log(np.abs(bath_gradient[i])+1)

    def plot_corcoeff_derivative_bath2():
        fig, axs = plt.subplots(3, sharex=True)
        axs[0].plot(range(len(choose_first[2, :].ravel().tolist())), choose_first[2, :])
        axs[0].set_ylabel('Responsiveness')
        axs[1].plot(range(len(choose_first[2, :].ravel().tolist())), angle_derivative_grid[:, 15])
        axs[1].set_ylabel('Angle Derivative ($\sigma=15$)')
        axs[2].plot(range(len(choose_first[2, :].ravel().tolist())), bath_gradient)
        axs[2].set_ylabel('Warped Bath')
        plt.xlim(0, len(choose_first[2, :].ravel().tolist()))
        ctk.add_ticks()
        mps.save_fig_twice(cfg.plots_loc + 'reg_ready2')
        plt.show()

    # plot_corcoeff_derivative_bath2()

    def mlr_test(x_train, y_train, x_test, y_test):
        """
        Example output: (Even linear regression is too much)
        training score 0.9584952078123362
        test score 0.12919844477570047
        :param x_train: The training x data (e.g sowindsp_square).
        :param y_train:
        :param x_test:
        :param y_test:
        :return:
        """
        reg = LinearRegression()
        reg.fit(x_train, y_train)
        train_score = reg.score(x_train, y_train)
        test_score = reg.score(x_test, y_test)
        print('training score', train_score)
        print('test score', test_score)

    year_len = 8760  # hours

    def plot_plot_against_each_other():
        fig, axs = plt.subplots(1, 3)
        mps.defined_size(fig, size='dcsh')
        axs = axs.ravel()
        axs[0].plot(angle_derivative_grid[:, 15], choose_first[2, :], 'x', markersize=2)
        axs[0].set_xlabel('Angle Derivative ($\sigma=15$)')
        axs[0].set_ylabel('Responsiveness')
        y_var = angle_derivative_grid[:, 15]
        x_var = choose_first[2, :]
        pearr = ss.pearsonr(x_var, y_var)
        print(pearr)
        # axs[1].set_ylabel('Responsiveness')
        axs[1].set_xlabel('Warped Bath')
        axs[1].set_ylabel('Responsiveness')
        axs[1].plot(bath_gradient, choose_first[2, :], 'x', markersize=2)
        # axs[1].set_ylabel('Responsiveness')
        y_var = bath_gradient
        x_var = choose_first[2, :]
        pearr = ss.pearsonr(x_var, y_var)
        print(pearr)
        axs[2].plot(bath_gradient, angle_derivative_grid[:, 15], 'x', markersize=2)
        axs[2].set_xlabel('Angle Derivative ($\sigma=15$)')
        axs[2].set_ylabel('Warped Bath')
        plt.tight_layout()
        mps.save_fig_twice('reg_look')
        plt.show()

        y_var = angle_derivative_grid[:, 15]
        x_var = bath_gradient
        pearr = ss.pearsonr(x_var, y_var)
        print(pearr)
        x_array = np.zeros([len(bath_gradient), 2], dtype='float32')
        x_array[:, 0] = np.asarray(bath_gradient)[:]
        x_array[:, 1] = angle_derivative_grid[:, 15]
        y_array = np.zeros([len(bath_gradient), 1], dtype='float32')
        y_array[:, 0] = choose_first[2, :]

        mlr_test(x_array, y_array,
                 x_array, y_array)
        """
        (0.29759626545299317, 6.028295738050312e-17)
        (-0.487836035427191, 1.6374781898114467e-46)
        (-0.06936474304070968, 0.056440296516197475)
        """
    plot_plot_against_each_other()


# i = 1, j = 3, k =9 (0.23206837, 1.0269710091766365e-10)
# i = 1, j = 3, k =10 (0.22901781, 1.8221977327992553e-10)
# i = 0, j = 3, k =9 (0.1072979, 0.0031184545547800003)
# i = 0, j = 1, k =14 (0.3831381, 7.061371146632411e-28)
# i = 0, j = 1, k =14 (0.3831381, 7.061371146632411e-28)
# i = 0, j = 1, k =14 (0.3831381, 7.061371146632411e-28)
