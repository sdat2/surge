import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import matplotlib
import cmocean.cm as cmo
import matplotlib.colors as colors
from sklearn.linear_model import LinearRegression
from sklearn import linear_model
import src.plotting.my_plotting_style as mps
import src.utilities.my_wrappers as mwr
import config as cfg
import src.plotting.custom_ticker as ctk
import src.utilities.my_io_functions as mif
import src.f_fourier_transform as fft
# import src.utilities.gaussian_regression_functions as grf
mps.mps_defaults()


@mwr.timeit
def get_xarray_grid(name_list,
                    regress=True,
                    plot=True):
    """
    :param name_list: a list of xarray locations to load up
    :return:
    """
    tfreq_max = 50
    if regress:
        regress_fft(name_list, tfreq_max=tfreq_max)
    if plot:
        _plot_result(tfreq_max=tfreq_max)


@mwr.timeit
def regress_fft(name_list, tfreq_max=1000):
    zos = mif.return_field_array(name_list, 'zos')
    tauuo = mif.return_field_array(name_list, 'tauuo')
    tauvo = mif.return_field_array(name_list, 'tauvo')
    no_points = np.shape(zos)[0]
    zos_mean = np.zeros(no_points, dtype='float32')
    print(np.shape(zos))
    for j in range(no_points):
        zos_mean[j] = np.mean(zos[j, :])
    zos_demeaned = np.zeros(np.shape(zos), dtype='float32')
    for i in range(no_points):
        zos_demeaned[i, :] = zos[i, :] - zos_mean[i]

    num_hours = np.shape(zos_demeaned)[1]
    year_len = num_hours // 2

    @mwr.timeit
    def rmlr_for_tfreq(tfreq=2):
        zos_hp = fft.high_pass_grid(zos,
                                    threshold_freq=tfreq)  # I relabel zos_demeaned in a terrible way!!!
        print("Threshold Frequency = "
              + str(tfreq_max))

        def rmlr_both_years(point=172):
            def run_rmlr(train_year=2004):
                """
                https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.HuberRegressor.html
                :return:
                """
                # Results Array Format
                # 1st index: MLR, Huber, RANSAC
                # 2nd index: intercept, coeff1, coeff2,  score_train, score_test
                results = np.zeros([4, 5], dtype='float32')

                # print('train_year =', train_year)
                # print(year_len)
                from sklearn.linear_model import LinearRegression, HuberRegressor, RANSACRegressor, TheilSenRegressor
                x_train = np.zeros([year_len, 2])
                y_train = np.zeros(year_len)
                x_test = np.zeros([year_len, 2])
                y_test = np.zeros(year_len)
                if train_year == 2004:
                    x_train[:, 0] = tauuo[point, year_len:]
                    x_train[:, 1] = tauvo[point, year_len:]
                    y_train[:] = zos_hp[point, year_len:]
                    x_test[:, 0] = tauuo[point, :year_len]
                    x_test[:, 1] = tauvo[point, :year_len]
                    y_test[:] = zos_hp[point, :year_len]
                elif train_year == 2005:
                    x_test[:, 0] = tauuo[point, year_len:]
                    x_test[:, 1] = tauvo[point, year_len:]
                    y_test[:] = zos_hp[point, year_len:]
                    x_train[:, 0] = tauuo[point, :year_len]
                    x_train[:, 1] = tauvo[point, :year_len]
                    y_train[:] = zos_hp[point, :year_len]

                huber = HuberRegressor().fit(x_train, y_train)
                linear = LinearRegression().fit(x_train, y_train)
                l_score = linear.score(x_train, y_train)
                l_gen_score = linear.score(x_test, y_test)
                h_score = huber.score(x_train, y_train)
                h_gen_score = huber.score(x_test, y_test)
                score_av = (l_score + l_gen_score + h_score + h_gen_score)/4
                return score_av

            score_2004 = run_rmlr()
            score_2005 = run_rmlr(train_year=2005)
            score_av = score_2004/2 + score_2005/2
            return score_av

        score_av_list = []

        for i in range(np.shape(tauvo)[0]):
            score_av_list.append(rmlr_both_years(point=i))

        return np.asarray(score_av_list)

    num_freq = 100
    tfreq_array = np.linspace(0, tfreq_max, num_freq)
    score_av_array = np.zeros([np.shape(tauvo)[0], num_freq], dtype='float32')

    for i in range(len(tfreq_array)):
        score_av_array[:, i] = rmlr_for_tfreq(tfreq=tfreq_array[i])[:]

    mif.write_json_object(score_av_array.tolist(),
                          cfg.data_loc + 'up_to_'+str(tfreq_max)+'_tfreq_test.json')


@mwr.timeit
def _plot_result(tfreq_max=1000):
    save_loc = cfg.plots_loc + 'reg_fft/up_to_' + str(tfreq_max) + '_'
    score_av_array = np.asarray(mif.read_json_object(cfg.data_loc
                                                     + 'up_to_'+str(tfreq_max)+'_tfreq_test.json'))
    plt.imshow(score_av_array.T, cmap=cmo.amp,
               vmin=0, vmax=1, aspect='auto')
    plt.gca().invert_yaxis()
    true_num_freq = 100
    num_freq = 11
    ratio = true_num_freq/num_freq
    tfreq_array = np.linspace(0, tfreq_max, num_freq)
    plt.yticks([x*ratio for x in range(num_freq)], tfreq_array)
    plt.ylabel('Threshold Frequency (yr$^{-1}$)')
    ctk.add_ticks()
    cbar = plt.colorbar()
    cbar.set_label(r'$\bar{r^2}$')
    fig = plt.gcf()
    mps.defined_size(fig, size='dcsq')
    plt.tight_layout()
    mps.save_fig_twice(save_loc + 'full_coast')
    plt.show()
    plt.clf()

    score_av_npa = np.zeros(np.shape(score_av_array)[1],
                            dtype='float32')

    for i in range(len(score_av_npa)):
        score_av_npa[i] = np.mean(score_av_array[:, i])

    plt.plot(range(len(score_av_npa)), score_av_npa, '#CB4154')
    plt.xlim(0, len(score_av_npa))
    true_num_freq = 100
    num_freq = 6
    ratio = true_num_freq/num_freq
    tfreq_array = np.linspace(0, tfreq_max, num_freq)
    plt.xticks([x*ratio for x in range(num_freq)], tfreq_array)
    plt.ylabel(r'$\langle\bar{r^2}\rangle$')
    plt.xlabel('Threshold Frequency (yr$^{-1}$)')
    fig = plt.gcf()
    mps.defined_size(fig, size='sc')
    plt.tight_layout()
    mps.save_fig_twice(save_loc + 'coastal_average')
    plt.show()
    plt.clf()
