import src.utilities.my_wrappers as mwr
import config as cfg

# run_in_seq()

def run_stats():
    import src.a_mean_skew_kurt as msk
    for field in ['zos', 'tauuo', 'tauvo']:
        msk.get_xarray_grid([cfg.data_loc + cfg.merged_2004_file_name,
                             cfg.data_loc + cfg.merged_2005_file_name],
                             field=field)


run_stats()


def run_vc_stats():
    import src.a_mean_skew_kurt as msk
    for field in ['zos', 'tauuo', 'tauvo']:
        msk.get_xarray_grid([cfg.data_loc + cfg.merged_vc_2004_file_name,
                             cfg.data_loc + cfg.merged_vc_2005_file_name],
                            coastline='vin-china', field=field)


run_vc_stats()


def run_demean():
    import src.currently_unused.b_de_mean_fields as bdf
    bdf.de_mean_fields(cfg.data_loc + cfg.merged_2004_file_name,
                       cfg.data_loc + cfg.standardised_2004_file_name)


def run_nreg():
    import src.currently_unused.ml_crude_reg as mcr
    mcr.run_regression(cfg.combined_point_dir
                       + cfg.standardised_2005_file_name)


@mwr.timeit
def run_demean_test():
    import src.old_tests.b_standardise_test as bst
    bst.run_basic_tests(cfg.combined_point_dir
                        + cfg.standardised_2005_file_name)
