
import smtplib
import ssl

port = 465  # For SSL
smtp_server = "smtp.gmail.com"
sender_email = "sdat2g@gmail.com"  # Enter your address
receiver_email = "sdat2@cam.ac.uk"  # Enter receiver address
password = "IChangedThisToSendEmailsfromPython"


def send_email(message):
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, message)


def completion_message(script, object, optional_status="None"):
    subject_line = "Subject: " + script + " has finished processing " + object
    body_of_text = "The script also reported: " + optional_status
    message = subject_line + '\n\n' + body_of_text + '\n'
    send_email(message)

# send_email("""\
# Subject: Hi there

# This message is sent from Python.

# This can now send automatically. """
