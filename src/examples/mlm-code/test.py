import numpy as np                      
import matplotlib.pyplot as plt         
import matplotlib                       
import sklearn.gaussian_process as gp   
import pandas                           
from shared import *
from utils import *

from DoubleRBFKernel import DoubleRBFKernel
from DoubleShiftedRBFKernel import DoubleShiftedRBFKernel
from DoubleShiftedSineKernel import DoubleShiftedSineKernel
from DoubleShiftedRBFSineKernel import DoubleShiftedRBFSineKernel


t = np.linspace(-3, 3, 200)
k = np.ones((200,))
k[::2] = 2
y = np.sin(2*t) + np.random.rand(len(t)) * 0.5 + 0.2*t
y[::2] += 2
t[::2] += 5
# y += 0.4*np.cos(0.77*t)


### View the data
plt.figure(figsize=(10, 4))
plt.scatter(t, y)
plt.title(r"""$y$ against $t$""")
plt.tight_layout()
plt.figure(figsize=(10, 4))
plt.scatter(t, y)
plt.title(r"""$y$ against $k$""")
plt.tight_layout()
# plt.show()
plt.savefig('plots/f1-name.pdf', format='pdf',bbox_inches='tight', pad_inches=0)

name = 'shiftedRBF_Sine'

### Train a GP
# ν = 1
# σ = 1
# λ=np.exp(-1)
X = np.concatenate((t[np.newaxis].T, k[np.newaxis].T), axis=1)

kernel = DoubleShiftedRBFKernel(length_scale=0.1, shift=0, shift_scale_bounds='fixed')
kernel = DoubleShiftedRBFKernel(length_scale=1, sigma=0.1, nu=10, rho=0.001, shift=5, shift_scale_bounds=(1e-7, 10))
# kernel = DoubleShiftedSineKernel(length_scale=0.5, sigma=0.1, nu=10, rho=0.01, shift=5, period=3        , period_scale_bounds=(1, 20), shift_scale_bounds=(4,8))

# kernel = DoubleShiftedRBFSineKernel(sine_length_scale=0.5, sigma=0.1, nu=10, rho=0.01,
        #                             period=3, period_scale_bounds=(1, 20), shift_scale_bounds=(4, 8), shift=5,
        #                             rbf_length_scale=5, rbf_length_scale_bounds=(0.01,1000)
        #                          ) + \
        # DoubleRBFKernel(length_scale=0.1)

# kernel = DoubleRBFKernel(length_scale=0.1)

reg = gp.GaussianProcessRegressor(kernel=kernel, alpha=0.0, n_restarts_optimizer=1000).fit(X, y)

t_ = np.linspace(-10, 15, 2000)
k_ = np.ones((2000,))
X_ = np.concatenate((t_[:, np.newaxis], k_[:, np.newaxis]), axis=1)

t_1s = []
y_1s = []
t_2s = []
y_2s = []
for i in range(len(t)):
    if k[i] == 1.0:
        t_1s.append(t[i])
        y_1s.append(y[i])
    else:
        t_2s.append(t[i])
        y_2s.append(y[i])

y_mean, y_cov = reg.predict(X_, return_cov=True)
plt.figure(figsize=(15, 5))
plt.plot(t_, y_mean, 'k', lw=2, zorder=9)
plt.scatter(t_1s, y_1s, c='red')
plt.fill_between(t_, np.transpose(y_mean) - 2*np.sqrt(np.diag(y_cov)), np.transpose(y_mean) + 2*np.sqrt(np.diag(y_cov)), alpha=0.5, color='k')
plt.title(r"""\begin{verbatim}Initial: %s\end{verbatim}
\begin{verbatim}Optimum: %s\end{verbatim}
 Log-Marginal-Likelihood: %.4f """ % (
    kernel,
    reg.kernel_,
    reg.log_marginal_likelihood(reg.kernel_.theta)))
plt.tight_layout()
plt.savefig(f'plots/test-{name}-1.pdf', format='pdf', bbox_inches='tight')


t_ = np.linspace(-10, 15, 2000)
k_ = np.ones((2000,))*2
X_ = np.concatenate((t_[:, np.newaxis], k_[:, np.newaxis]), axis=1)

y_mean, y_cov = reg.predict(X_, return_cov=True)
plt.figure(figsize=(15, 5))
plt.plot(t_, y_mean, 'k', lw=2, zorder=9)
plt.scatter(t_2s, y_2s, c='green')
plt.fill_between(t_, np.transpose(y_mean) - 2*np.sqrt(np.diag(y_cov)),
                 np.transpose(y_mean) + 2*np.sqrt(np.diag(y_cov)), alpha=0.5, color='k')
plt.title(r"""\begin{verbatim}Initial: %s\end{verbatim}
\begin{verbatim}Optimum: %s\end{verbatim}
 Log-Marginal-Likelihood: %.4f """ % (
    kernel,
    reg.kernel_,
    reg.log_marginal_likelihood(reg.kernel_.theta)))
plt.tight_layout()
plt.savefig(f'plots/test-{name}-2.pdf', format='pdf', bbox_inches='tight')
plt.show()
