"""
Extracting relevant data using iris
=================================

This is remarkably hard to do, and I wonder whether I need
to interpolate the netcdf files at all or can cope until
the end.

This is all rather slow so currently unused.
"""

from __future__ import unicode_literals
import numpy as np
import cartopy.crs as ccrs
import iris
import iris.analysis.cartography
import config as cfg
from iris.util import promote_aux_coord_to_dim_coord
import xarray as xr
from src.utilities import my_wrappers as mwr
minmax = lambda x: (np.min(x), np.max(x))


def wrap_lon360(lon):
    """
    Function required for bbox_extract_2Dcoords
    :param lon: The longitude.
    :return:
    """
    lon = np.atleast_1d(lon).copy()
    positive = lon > 0
    lon = lon % 360
    lon[np.logical_and(lon == 0, positive)] = 360
    return lon


def wrap_lon180(lon):
    """
    Function required for bbox_extract_2Dcoords.
    Needs wrap_lon360
    :param lon: The longitude.
    :return:
    """
    lon = np.atleast_1d(lon).copy()
    angles = np.logical_or((lon < -180), (180 < lon))
    lon[angles] = wrap_lon360(lon[angles] + 180) - 180
    return lon


@mwr.timeit
def bbox_extract_2Dcoords(cube, bbox):
    """
    Extract a sub-set of a cube inside a lon, lat bounding box
    bbox=[lon_min lon_max lat_min lat_max].
    NOTE: This is a work around to subset an iris cube that has
    2D lon, lat coords.
    Needs wrap_lon180 and wrap_lon360

    """
    lons = cube.coord('longitude').points
    lats = cube.coord('latitude').points
    lons = wrap_lon180(lons)

    inregion = np.logical_and(np.logical_and(lons > bbox[0],
                                             lons < bbox[2]),
                              np.logical_and(lats > bbox[1],
                                             lats < bbox[3]))
    region_inds = np.where(inregion)
    imin, imax = minmax(region_inds[0])
    jmin, jmax = minmax(region_inds[1])
    return cube[..., imin:imax + 1, jmin:jmax + 1]


def run_bbox_extract_2D_coords(cube, bbox):
    """
    This was taken from somewhere, mainly runs the function above.
    :param cube: The iris cube which is to be trimmed.
    :param bbox: The bbox of the trimmed cube. [lon_min lon_max lat_min lat_max]
    :return:
    """
    sub_cube = bbox_extract_2Dcoords(cube, bbox)
    print('Original {} and new {} horizontal shapes'.format(cube.shape[1:],
                                                            sub_cube.shape[1:]))
    return sub_cube


"""
def plot_cube(cube):
    # Load data
    # Choose plot projections

    projections = {}
    projections['Mollweide'] = ccrs.Mollweide()
    projections['PlateCarree'] = ccrs.PlateCarree()
    projections['NorthPolarStereo'] = ccrs.NorthPolarStereo()
    projections['Orthographic'] = ccrs.Orthographic(central_longitude=-90,
                                                    central_latitude=45)

    pcarree = projections['PlateCarree']
    # Transform cube to target projection
    new_cube, extent = iris.analysis.cartography.project(cube, pcarree,
                                                         nx=400, ny=200)

    # Plot data in each projection
    for name in sorted(projections):
        fig = plt.figure()
        fig.suptitle('ORCA12 Data Projected to {}'.format(name))
        # Set up axes and title
        ax = plt.subplot(projection=projections[name])
        # Set limits
        ax.set_global()
        # plot with Iris quickplot pcolormesh
        qplt.pcolormesh(new_cube)
        # Draw coastlines
        ax.coastlines()

        iplt.show()
"""


@mwr.timeit
def return_rectilinear_cube(cube, nxny=[4000, 4000]):
    """

    :param cube: the iris cube to be interpolated.
    :return:
    """
    # let's not coarse grain the data yet
    new_cube1, extent = iris.analysis.cartography.project(cube, ccrs.PlateCarree(),
                                                          nx=nxny[0], ny=nxny[1])# nx=418, ny=721)
    print('extent= ', extent)

    xr_obj1 = xr.DataArray.from_iris(new_cube1)
    print(xr_obj1.__repr__())
    # xr_obj1.to_netcdf(path=cube.name() + '-big' + '.nc')

    # plot_cube(new_cube1)

    return new_cube1


def return_subset(cube):
    """
    Removes the box of coordinates encompassing all the region I think could be of interest.
    ## DID NOT WORK with 2d axis ###
    TODO delete, this really doesn't work with it
    :param cube:
    :return:
    """
    # subsetA = cube.intersection(projection_x_coordinate=(-100, -40))
    subsetA = cube.intersection(longitude=(-100, -40))
    # subset = subsetA.intersection(projection_y_coordinate=(10, 40))
    subset = subsetA.intersection(latitude=(10, 40))
    return subset


def test_coords(cube):
    """
    This just prints the coordinates inside the iris cube

    :param cube:
    :return:
    """
    # print(cube.coords.CoordExtent())
    print(cube.coords())


@mwr.timeit
def broken_run_slim_down_and_split():
    """
    This version doesn't work at all.
    :return:
    """
    cubes = iris.load(cfg.toy_loc + cfg.toy_U_file_name)
    for cube in cubes:
        subset = run_bbox_extract_2D_coords(cube,  [-100, 10, -40, 40])
        # test_coords(cube)
        print(subset.name())
        # subset = return_subset(cube)
        new_cube = return_rectilinear_cube(subset, nxny=[721, 418])
        subset = run_bbox_extract_2D_coords(new_cube,  [-100, 10, -40, 40])
        # let's not coarse grain the data yet
        iris.save(subset, cube.name()+'-iris.nc')


@mwr.timeit
def run_slim_down_and_split(input_file_dir, input_file_name, output_file_dir):
    """
    This version works, but is slow.
    It also is only valid for low latitudes.
    TODO merge cubes so that all are in the same place. Partially done.
    TODO divorce from the old file.
    :return:
    """
    cubes = iris.load(input_file_dir + input_file_name)

    def slim_and_split(f_cube):
        differing_attrs = ['file_name', 'name', 'timeStamp', 'TimeStamp']
        for attribute in differing_attrs:
            f_cube.attributes[attribute] = ''
        promote_aux_coord_to_dim_coord(f_cube, 'time')
        subset = run_bbox_extract_2D_coords(cube,  [-100, 10, -40, 40])
        # new_cube = return_rectilinear_cube(subset, nxny=[721, 418])
        new_cube = return_rectilinear_cube(f_cube, nxny=[4320, 2160])
        subset = run_bbox_extract_2D_coords(new_cube, [-100, 10, -40, 40])
        # test_coords(cube)
        print(subset.name())
        # subset = return_subset(cube)
        return subset

    new_cubes = []
    for cube in cubes:
        new_cubes.append(slim_and_split(cube))

    del cubes
    cubes = iris.cube.CubeList(new_cubes)
    cube = cubes.concatenate()

    iris.save(cube, output_file_dir + 'try-' + input_file_name)


# broken_run_slim_down_and_split()
run_slim_down_and_split(cfg.toy_loc, cfg.toy_V_file_name, cfg.toy_loc)

# run_slim_down_and_split(cfg.toy_loc, cfg.toy_U_file_name, cfg.toy_loc)
# run_slim_down_and_split(cfg.toy_loc, cfg.toy_T_file_name, cfg.toy_loc)


def nearest_index(input_array_A, input_array_B, target_value_A, target_value_B):
    """
     an index that references the right value in the flattened arrays.
    :param input_array_A:
    :param input_array_B:
    :param target_value_A:
    :param target_value_B:
    :return:
    """
    return np.abs(np.square(input_array_A-target_value_A)
                  + np.square(input_array_B-target_value_B)).argmin()


def find_index_pair(nav_lat, nav_lon, shape, lon_in, lat_in, test=False):
    """
    TODO: I've managed to break this!!! :(
    :param nav_lat:
    :param nav_lon:
    :param shape:
    :param lon_in:
    :param lat_in:
    :param test:
    :return:
    """
    index = nearest_index(nav_lon.flatten(),
                          nav_lat.flatten(),
                          lon_in, lat_in)
    x, y = index // shape[1], np.mod(index, shape[1])  # Broken?
    if test:
        print('index', index)
        print('lon, lat', lon_in, lat_in)
        chosen_lat = nav_lat[x, y]
        chosen_lon = nav_lon[x, y]
        print('chosen lon, chosen lat',
              chosen_lon, chosen_lat)
        print('x, y', x, y)
    return x, y


@mwr.timeit
def run_index_extraction(csv_object, xr_ds):
    nav_lat = xr_ds.coords['nav_lat'].values
    nav_lon = xr_ds.coords['nav_lon'].values
    shape = np.shape(nav_lon)
    pair_array = np.zeros(np.shape(csv_object), dtype='int16')
    # run_pair_test(csv_object, nav_lat, nav_lon, shape)
    for i in range(np.shape(csv_object)[0]):
        pair_array[i][0], pair_array[i][1] = \
            find_index_pair(nav_lat, nav_lon, shape,
                            csv_object[i][0], csv_object[i][1],
                            test=True)
    return pair_array


def run_pair_test(csv_object, nav_lat, nav_lon, shape):
    lon_in = csv_object[2][0]
    lat_in = csv_object[2][1]
    find_index_pair(nav_lat, nav_lon, shape,
                    lon_in, lat_in, test=True)
    lon_in = csv_object[6][0]
    lat_in = csv_object[6][1]
    find_index_pair(nav_lat, nav_lon, shape,
                    lon_in, lat_in, test=True)
    lon_in = csv_object[5][0]
    lat_in = csv_object[5][1]
    find_index_pair(nav_lat, nav_lon, shape,
                    lon_in, lat_in, test=True)
