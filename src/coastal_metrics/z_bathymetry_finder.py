import numpy as np
from numba import jit  # script now takes 30 seconds rather than 300 due to numba optimisation.
import xarray as xr
import matplotlib.pyplot as plt
import src.utilities.my_io_functions as mif
import src.plotting.custom_ticker as ctk
import src.plotting.my_plotting_style as mps
import src.utilities.my_wrappers as mwr
import config as cfg
mps.mps_defaults()


@mwr.timeit
def produce_bathymetry(coastline='america'):
    if coastline == 'america':
        smoothed_angle_grid = np.asarray(mif.read_json_object(cfg.data_loc + 'smoothed_angle.json'), dtype='float32')
        angles_15 = smoothed_angle_grid[:, 15]
        bathymetry = xr.open_dataset(cfg.slim_bath_path).Bathymetry.values
        select_list = np.asarray(mif.read_csv_file(cfg.h_slimmed_ordered_point_loc), dtype='int16').tolist()
        #  print(np.shape(smoothed_angle_grid))
        #  print(angles_15)
        point_to_list = mif.read_json_object(cfg.data_loc + 'point_to_adjacent.json')
        bath_gradient = []
        assert(len(select_list) == len(point_to_list))

        def make_place_plot():
            plt.imshow(bathymetry)
            plt.gca().invert_yaxis()
            plt.plot(np.asarray(select_list)[:, 1],
                     np.asarray(select_list)[:, 0],
                     color='orange', label='Coastline Points')
            plt.plot(np.asarray(point_to_list)[:, 0],
                     np.asarray(point_to_list)[:, 1],
                     color='red', label='Immediately Offshore Points')
            plt.legend()
            plt.xlabel('x grid axis')
            plt.ylabel('y grid axis')
            mps.save_fig_twice(cfg.plots_loc + 'simple_bath_extract')
            plt.show()
            plt.clf()

        for i in range(len(select_list)):
            bath_gradient.append(- bathymetry.T[select_list[i][1], select_list[i][0]].tolist()
                                 + bathymetry.T[point_to_list[i][0], point_to_list[i][1]].tolist())

        #  print(bath_gradient)
        mif.write_json_object(bath_gradient, cfg.data_loc+'bath_grad.json')

        def make_grad_plot():
            plt.plot(range(len(bath_gradient)), bath_gradient)
            ctk.add_ticks()
            plt.ylabel('Change in Bathymetry to Offshore Box')
            plt.xlim(0, len(bath_gradient))
            mps.save_fig_twice(cfg.plots_loc + 'direct_bath_gradient')
            plt.show()
            plt.clf()

        pair_array = np.asarray(mif.read_csv_file(cfg.h_slimmed_ordered_point_loc),
                                dtype='int16')

    elif coastline == 'vin-china':
        bathymetry = xr.open_dataset(cfg.slim_ea_bath_path).Bathymetry.values
        pair_array = np.asarray(mif.read_csv_file(cfg.h_vin_chin_list),
                                dtype='int16')


    @mwr.timeit
    @jit(cache=True)  # significant performance enhancement.
    def find_isobath(tmp_bathymetry, crit_depth=100):
        isobath_index_list = []
        shape_bathymetry = np.shape(tmp_bathymetry)
        for i in range(0, shape_bathymetry[0]-1):
            for j in range(0, shape_bathymetry[1]-1):
                if tmp_bathymetry[i, j] >= crit_depth:
                    if tmp_bathymetry[i-1, j] < crit_depth:
                        isobath_index_list.append([i, j])
                    if tmp_bathymetry[i, j-1] < crit_depth:
                        isobath_index_list.append([i, j])
                    if tmp_bathymetry[i+1, j] < crit_depth:
                        isobath_index_list.append([i, j])
                    if tmp_bathymetry[i, j+1] < crit_depth:
                        isobath_index_list.append([i, j])
        return isobath_index_list

    depths_list = [1, 10, 12, 20, 30, 50, 100, 200, 500, 1000]
    color_list = mps.replacement_color_list(len(depths_list))
    isobath_lol = []
    for depth in depths_list:
        isobath_lol.append(find_isobath(bathymetry, crit_depth=depth))
    print(len(isobath_lol))

    def plot_isobath_lol(isobath_lol):
        import cmocean.cm as cmo
        # import matplotlib.colors as colors
        plt.imshow(bathymetry,
                   cmap=cmo.deep)
        plt.gca().invert_yaxis()
        cbar = plt.colorbar(pad=0.02, shrink=0.5)
        cbar.set_label('Depth (m)')
        cbar.ax.invert_yaxis()

        for i, depth in enumerate(depths_list):
            isobath_npa = np.asarray(isobath_lol[i])
            plt.plot(isobath_npa[:, 1], isobath_npa[:, 0],
                     'x', markersize=0.05,
                     color=color_list[i])
            plt.plot([0], [0],
                     'x', markersize=3,
                     color=color_list[i],
                     label='' + str(depth) + 'm')

        plt.plot(pair_array[:, 1], pair_array[:, 0],
                 'x', markersize=0.05,
                 color='black')
        plt.plot([0], [0],
                 'x', markersize=3,
                 color='black',
                 label='Points')

        # plt.legend(ncol=4, loc='lower right', fontsize=6)
        plt.legend(bbox_to_anchor=(-0.2, 1.02, 1.35, .102), loc='lower left',
                   ncol=4, mode="expand", borderaxespad=0.)

        plt.xlabel('x grid axis')
        plt.ylabel('y grid axis')
        fig = plt.gcf()
        mps.defined_size(fig, size='sc')
        if coastline == 'america':
            mps.save_fig_twice(cfg.plots_loc + 'bath_list')
        elif coastline == 'vin-china':
            mps.save_fig_twice(cfg.plots_loc + 'vc_bath_list')
        # plt.show()
        plt.clf()

    plot_isobath_lol(isobath_lol)

    # @jit
    def min_value_point_array(value, array):
        return np.amin(np.sqrt(np.square(value[0] - array[:, 0])
                               + np.square(value[1] - array[:, 1])))

    def distance_to_min(value, array):
        # new_array = array.__copy__()
        # index = index_point_array(value, array)
        min_value = min_value_point_array(value, array)
        # print(index)
        # print(np.shape(array))
        # print(index.__str__)
        # print(array[int(index), :])
        # return distance_points(value,
        #                        new_array[index, :])
        return min_value

    @mwr.timeit
    # @jit() # numba.jit just complains about not being told types.
    def return_distances_lol():
        dist_lol = []
        for i in range(np.shape(pair_array)[0]):
            temp_dist_list = []
            for j in range(len(depths_list)):
                temp_dist_list.append(distance_to_min(pair_array[i],
                                                      np.asarray(isobath_lol[j])))
            dist_lol.append(temp_dist_list)
        return dist_lol

    @mwr.timeit
    def plot_distances_lol(distances_lol):

        distances_npa = np.asarray(distances_lol)
        for i, depth in enumerate(depths_list):
            isobath_npa = np.asarray(isobath_lol[i])
            plt.plot(range(len(distances_npa[:, i])),
                     distances_npa[:, i],
                     markersize=0.1,
                     color=color_list[i],
                     label= str(depth) + 'm')

        plt.ylabel('Distance (grid boxes)')
        # plt.xlabel('Point along the Coast')
        plt.xlim([0, len(distances_npa[:, 0])])
        plt.legend(bbox_to_anchor=(-0.1, 1.02, 1.1, .102),
                   ncol=4, mode="expand", borderaxespad=0.)
        ctk.add_ticks(coastline=coastline)
        fig = plt.gcf()
        mps.defined_size(fig, size='sc')
        if coastline == 'america':
            mps.save_fig_twice(cfg.plots_loc + 'distance_isobath')
        elif coastline == 'vin-china':
            mps.save_fig_twice(cfg.plots_loc + 'vc_distance_isobath')


    dist_lol = return_distances_lol()
    plot_distances_lol(dist_lol)
    print('shape distance lol ', np.shape(np.asarray(dist_lol)))
    dict_to_save = {"depths_list": depths_list,
                    "dist_lol": dist_lol}
    if coastline == 'america':
        mif.write_json_object(dict_to_save,
                              cfg.data_loc + 'isobath_dist.json')
    if coastline == 'vin-china':
        mif.write_json_object(dict_to_save,
                              cfg.data_loc + 'vc_isobath_dist.json')
    plt.clf()
