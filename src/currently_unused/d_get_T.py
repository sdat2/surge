import b_squeeze as bsq
import config as cfg

bsq.run_slim_down_and_split(cfg.jasmin_subdir, cfg.big_T_file_name,
                            cfg.jasmin_repo + cfg.toy_loc)
