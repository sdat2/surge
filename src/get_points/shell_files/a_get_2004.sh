#!/bin/bash

final_loc=/gws/nopw/j04/primavera1/open/sithom-bas/new_versions/
prefix=moose:/crum/mi-ba130/onh.nc.file/
name=nemo_ba130o_1h_20040101-20040131_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1h_20040201-20040228_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1h_20040301-20040331_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1h_20040401-20040430_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1h_20040501-20040531_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
exit
