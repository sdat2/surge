from sklearn.gaussian_process.kernels import *
from sklearn.gaussian_process import GaussianProcessRegressor
import sklearn.gaussian_process as gp
from matplotlib import pyplot as plt
from scipy.spatial.distance import cdist
import numpy as np
import pandas as pd
from urllib.request import urlopen
import json
from copy import deepcopy
import utils
import datetime
import matplotlib

# ----------------------------------------------------------------------


def getStationInfo(station_id):
    index = None
    if type(station_id) == tuple:
        index = station_id[1]
        station_id = station_id[0]
    url = f"http://environment.data.gov.uk/flood-monitoring/id/stations/{station_id}"
    print(f'Loading {url}...')
    info = json.load(urlopen(
        url))
    print(f'Fitting Data to {station_id}, index {repr(index)}')
    print(f"Name:     {info['items']['label']}")
    print('Measures:')
    if index is not None: info['items']['measures'] = info['items']['measures'][index]
    print(f'    Name:     {info["items"]["measures"]["label"]}')
    print(f'    Measures: {info["items"]["measures"]["parameterName"]}')
    print(f'    Units:    {info["items"]["measures"]["unitName"]}')
    print(f'    Period:   {info["items"]["measures"]["period"]}')
    return info


def _make_plot(y_pred, y, x, X, sigma, gp, info, kernel, name='', **kwargs):

    y_pred= y_pred.flatten()
    sigma=sigma.flatten()
    X = X.flatten()
    x = x.flatten()
    y = y.flatten()

    # Plot the function, the prediction and the 95% confidence interval based on the MSE
    plt.figure(figsize=(12, 4))
    dates = matplotlib.dates.date2num(
        list(map(datetime.datetime.fromtimestamp, 24*60*60*x+1483228800)))  # add 1/1/17
    Dates = matplotlib.dates.date2num(
        list(map(datetime.datetime.fromtimestamp, 24*60*60*X+1483228800)))
    ax = plt.gca()
    ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('Mon %-d %b '))
    plt.plot_date(dates, y_pred, 'b-', label=u'Prediction')
    plt.errorbar(Dates, y, 0.0005, fmt='r.',
                 markersize=3, label=u'Observations')
    plt.fill(np.concatenate([dates, dates[::-1]]),
             np.concatenate([y_pred - 1.9600 * sigma,
                             (y_pred + 1.9600 * sigma)[::-1]]),
             alpha=.5, fc='b', ec='None', label=r'95\% confidence interval')
    plt.title(r"""\begin{verbatim}Initial: %s\end{verbatim}
    \begin{verbatim}Optimum: %s\end{verbatim}
    Log-Marginal-Likelihood: %.4f """ % (
        kernel,
        gp.kernel_,
        gp.log_marginal_likelihood(gp.kernel_.theta)))
    plt.xlabel('$T$')

    months = matplotlib.dates.MonthLocator()  # every month
    weeks = matplotlib.dates.WeekdayLocator(byweekday=matplotlib.dates.MO)  # every month
    days = matplotlib.dates.DayLocator()

    ax.grid(True)
    ax.xaxis.set_major_locator(weeks)
    ax.xaxis.set_minor_locator(days)
    plt.xticks(rotation=45)
    plt.ylim((np.min(y)-0.05, np.max(y)+0.05))
    if info is None:
        plt.ylabel("Water Level / m")
    else:
        plt.ylabel(info['items']["measures"]["parameterName"] +
               ' /' + info['items']["measures"]["unitName"])
    plt.legend(loc='upper left')
    # plt.tight_layout()
    if not name:
        if not info:
            name = f'{np.random.randint(0,9999)}.pdf'
        else:
            name = f"{info['items']['label']}.pdf"
    plt.savefig(f'plots/{name}', format='pdf', bbox_inches='tight')
    plt.close('all')



def _make_data_plot(y, X, info, name=''):

    X = X.flatten()
    y = y.flatten()

    # Plot the function, the prediction and the 95% confidence interval based on the MSE
    plt.figure(figsize=(6,3))
    Dates = matplotlib.dates.date2num(
        list(map(datetime.datetime.fromtimestamp, 24*60*60*X+1483228800)))
    ax = plt.gca()
    ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter('Mon %-d %b '))
    plt.errorbar(Dates, y, 0.0005, fmt='r.',
                 markersize=3, label=u'Observations')
    # plt.xlabel('$T$')

    months = matplotlib.dates.MonthLocator()  # every month
    weeks = matplotlib.dates.WeekdayLocator(
        byweekday=matplotlib.dates.MO)  # every month
    days = matplotlib.dates.DayLocator()

    ax.grid(True)
    ax.xaxis.set_major_locator(weeks)
    ax.xaxis.set_minor_locator(days)
    plt.xticks(rotation=45)
    if info is None:
        plt.ylabel("Water Level / m")
    else:
        if type(info['items']['measures']) is list:
            info['items']['measures'] = info['items']['measures'][0]
        plt.ylabel(info['items']["measures"]["parameterName"] +
               ' /' + info['items']["measures"]["unitName"])
    plt.legend(loc='upper left')
    plt.tight_layout()
    if not name:
        name = f"{info['items']['label']}.pdf"
    plt.savefig(f'plots/{name}', format='pdf', bbox_inches='tight')



def fitToStation(data, station_ids, kernel, plot=True, bagging=None, maxpoints=-1, name='', dayCutOff=700, _bagCall=False, *args, **kwargs):
    """Fits data from one or more stations to a kernel
    
    Arguments:
        data {np.ndarray} -- Station data, indexed by [station, data], where
                             stations are in the same order as they appear in station_ids
        station_ids {str or list[str]} -- (list of) strings giving the unique station IDs  strings giving the unique station IDs  strings giving the unique station IDs  strings giving the unique station IDs  strings giving the unique station IDs
        kernel {sklearn Kernel} -- The kernel to use to fit a GP to the data.
    
    Keyword Arguments:
        plot {bool} -- If True, plot the fitted model (default: {False})
        bagging {tuple(int, int)} -- Tuple indicating the number of bags (first item) and the number of samples to be taken per bag (second item).
        # TODO Implement Bagging
    """

    if type(station_ids) == str:
        return _fitToSingleStation(data, station_ids, kernel, plot=plot, bagging=bagging, maxpoints=maxpoints, dayCutOff=dayCutOff, name=name, _bagCall=_bagCall, *args, **kwargs)
    X = np.zeros((0,2))
    y = np.zeros((0,1))
    label = 1.0
    for station_id in station_ids:
        index = None
        if _bagCall:
            info = None
        else:
            info = getStationInfo(station_id)
        if type(station_id) == tuple:
            index = station_id[1]
            station_id = station_id[0]
            
        if index is not None:
            station_data = data[data['measure'].str.contains(
            info['items']['measures']['notation'])]
        else:
            station_data = data[data['measure'].str.contains(station_id)]

        if len(station_data) == 0:
            print(f"No data for station with id {station_id}")
            return
            # raise ValueError(f'No data for station with id {station_id}')

        # Threshold the data at a certain date
        station_data = station_data[station_data['days'] > dayCutOff]

        if bagging:
            station_data = station_data.sample(n=bagging//2, replace=False)
        elif maxpoints > 0 and len(station_data) > maxpoints//2:
            nth = len(station_data) // maxpoints * 2
            station_data = station_data[::nth]

        days = station_data['days'].values[None,:].T
        blank_labels = np.ones(days.shape[0])[None,:].T

        station_x = np.concatenate((days, label * blank_labels), axis=1)
        station_y = station_data['value'].values[None,:].T
        X = np.concatenate((X, station_x), axis=0)
        y = np.concatenate((y, station_y), axis=0)
        label = 2

    # Instantiate a Gaussian Process model
    if len(X) == 0:
        print("X is ZERO")
        return
    gp = GaussianProcessRegressor(kernel=kernel, alpha=0, n_restarts_optimizer=20)

    # Fit to data using Maximum Likelihood Estimation of the parameters
    print('Fitting model...')
    gp.fit(X, y)

    # Make the prediction on the meshed x-axis (ask for MSE as well)
    x = np.linspace(np.min(X[:, 0]), np.max(X[:, 0]), 1000)
    x = np.append(x,x)
    x = x[None, :].T
    x = np.append(x, np.append(np.ones(len(x)//2),
                               2*np.ones(len(x)//2))[None, :].T, axis=1)

    y_pred, sigma = gp.predict(x, return_std=True)

    split = np.sum(X[:, 1] == 1)
   
    if plot:
        _make_plot(y_pred[len(y_pred)//2:],
                y[split:],
                x[len(x)//2:, 0],
                X[split:, 0],
                sigma[len(sigma)//2:],
                gp,
                info, kernel, name.replace('pdf', '-1.pdf'))
                
        _make_plot(y_pred[:len(y_pred)//2],
                y[:split],
                x[:len(x)//2, 0],
                X[:split, 0],
                sigma[:len(sigma)//2],
                gp,
            info, kernel, name.replace('pdf', '-2.pdf'))
        # plt.show()
    
    if _bagCall:
        return gp, x, y_pred, sigma, X, y, info, kernel

    return gp



def _fitToSingleStation(data, station_id, kernel, plot=True, bagging=None, maxpoints=-1, name='', dayCutOff=700, _bagCall = False, *args, **kwargs):
    """Fits data from one station to a kernel
    """
    if _bagCall:
        info = None
    else:
        info = getStationInfo(station_id)

    data = data[data['measure'].str.contains(station_id)]

    if len(data) == 0:
        # raise ValueError(f'No data for station with id {station_id}')
        print(f"No data for station with id {station_id}")
        return

    # Threshold the data at a certain date
    data = data[data['days'] > dayCutOff]

    if bagging:
        data = data.sample(n=bagging, replace=False)
    elif maxpoints > 0 and len(data) > maxpoints:
        nth = len(data) // maxpoints
        data = data[::nth]

    y = data.value.values.reshape(1, -1).T.ravel()
    X = data.days.values.reshape(1, -1).T

    if not kernel:
        _make_data_plot(y, X, info, name)
        return

    # Instantiate a Gaussian Process model
    gp = GaussianProcessRegressor(kernel=kernel, alpha=0, n_restarts_optimizer=20)

    # Fit to data using Maximum Likelihood Estimation of the parameters
    print('Fitting model...')
    gp.fit(X, y)

    # Make the prediction on the meshed x-axis (ask for MSE as well)
    x = np.atleast_2d(np.linspace(np.min(X), np.max(X), 2000)).T
    y_pred, sigma = gp.predict(x, return_std=True)

    # Plot the function, the prediction and the 95% confidence interval based on the MSE
    if plot: 
        _make_plot(y_pred, y, x, X, sigma, gp, info, kernel, name)
        # plt.show()

    if _bagCall:
        return gp, x, y_pred, sigma, X, y, info, kernel

    return gp



def runBagging(bagsize, nbags, *args, **kwargs):
    xs = np.zeros((nbags, 2000))
    xlabels = np.zeros((nbags, 2000))
    y_preds = np.zeros((nbags, 2000))
    sigmas = np.zeros((nbags, 2000))
    gps = []

    allX = []
    ally = []
    LMLs = []
    info = None
    kernel = None
    for i in range(nbags):
        gp, x, y_pred, sigma, X, y, info, kernel = fitToStation(
            *args, **kwargs, bagging=bagsize, _bagCall=True)
        
        gps.append(gp)
        xs[i] = np.squeeze(x[:,0])
        if np.squeeze(x).ndim > 1:
            xlabels[i] = np.squeeze(x[:, 1])
        else:
            xlabels[i] = np.squeeze(x)
        y_preds[i] = np.squeeze(y_pred)
        sigmas[i] = np.squeeze(sigma)
        allX.append(np.squeeze(X))
        ally.append(np.squeeze(y))
        LMLs.append(gp.log_marginal_likelihood(gp.kernel_.theta))
    
    x = np.average(xs, axis=0, weights=LMLs)
    y_pred = np.mean(y_preds, axis=0)
    sigma = np.mean(sigmas, axis=0)

    X=np.concatenate(allX, axis=0)
    y=np.concatenate(ally, axis=0)

    if X.ndim == 2:
        _make_plot(y_pred[np.where(xlabels[0].flatten()==1)],
                   y[np.where(X[:,1].flatten() == 1)],
                   x[np.where(xlabels[0].flatten() == 1)],
                   X[np.where(X[:,1] == 1)][:,0].flatten(),
                   sigma[np.where(xlabels[0].flatten() == 1)],
                   gps[-2],
                   info, kernel, name=kwargs['name'].replace('.pdf', '-1.pdf'))

        _make_plot(y_pred[np.where(xlabels[0].flatten() == 2)],
                   y[np.where(X[:,1].flatten() == 2)],
                   x[np.where(xlabels[0].flatten() == 2)],
                   X[np.where(X[:,1] == 2)][:,0].flatten(),
                   sigma[np.where(xlabels[0].flatten() == 1)],
                   gps[-1],
                   info, kernel, name=kwargs['name'].replace('.pdf', '-2.pdf'))
        # plt.show()
    else:
        _make_plot(y_pred, y, x, X, sigma,
                   gps[-1], info, kernel, name=kwargs['name'])
        

    
