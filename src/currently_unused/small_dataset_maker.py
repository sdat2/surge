#!/usr/bin/env python3.7

import numpy as np
import pandas as pd
import xarray as xr
import dask as ds

home = '/home/users/sithom/'
file_name = home + 'nemo_ba130o_1h_20050801-20050831_grid-T.nc'

xr_object = xr.open_dataset(file_name)

print(xr_object.__repr__())

time = '500.0'

small_slice = xr_object.isel(time_counter=time,
                             bounds_lat=slice(10, 40),
                             bounds_lon=slice(-100, -40))

small_slice.to_netcdf(path=home+'small.nc')


from pyproj import Proj
nxv,  nyv = np.meshgrid(nx, ny)

unausp = Proj('+proj=lcc +lat_1=49 +lat_2=46 +lat_0=47.5 '
              '  +lon_0=13.33333333333333 +x_0=400000 +y_0=400000 +ellps=bessel'
              '    +towgs84=577.326,90.129,463.919,5.137,1.474,5.297,2.4232 +units=m +no_defs ')
nlons, nlats = unausp(nxv, nyv, inverse=True)
upLon,  upLat = np.meshgrid(nlons, nlats)


# upnew_lon = np.linspace(w.X[0], w.X[-1], w.dims['X'] // 5)
# upnew_lat = np.linspace(w.Y[0], w.Y[-1], w.dims['Y'] //5)
# uppds = w.interp(Y=upnew_lat, X=upnew_lon)

