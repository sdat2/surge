import src.utilities.my_wrappers as mwr
import config as cfg

@mwr.timeit
def pointwise_1950(min_inclusive, max_exclusive):
    import src.get_points.f_select_points as fsp
    fsp.run_through_1950_folder(cfg.small_1950, cfg.point_1950,
                                min_inclusive, max_exclusive)

@mwr.timeit
def pointwise_vc_1950(min_inclusive, max_exclusive):
    import src.get_points.f_select_points as fsp
    fsp.run_through_1950_folder(cfg.ea_small_1950, cfg.vc_point_1950,
                                min_inclusive, max_exclusive,
                                coastline='vin-china')

# pointwise_1950(0, 10)
# pointwise_1950(10, 110)
# pointwise_1950(110, 210)
# pointwise_1950(210, 310)
# pointwise_1950(310, 410)
# pointwise_1950(410, 510)
# pointwise_1950(510, 610)
# pointwise_1950(610, 710)
# pointwise_1950(710, 810)
# pointwise_1950(810, 910)
# pointwise_1950(910, 1010)
# pointwise_1950(1010, 1110)
# pointwise_1950(1110, 1212)

pointwise_vc_1950(0, 1212)

"""
there are  1212  files to go through
conda activate jasmin-surge
vim point_runner.py
cat point_runner.py
python3 point_runner.py

# CTRL+b + c

10, 110 # done
110, 210 # done
210, 310 # done
310, 410 # done
410, 510 # done
510, 610 # done
610, 710 # done
710, 810 # done
810, 910 # done
910, 1010 # done
1010, 1110 # done
1110, 1212 #  done

# CTRL+b + d

"""
