import sklearn.gaussian_process as gps
from scipy.spatial.distance import cdist
import numpy as np


class DoubleRBFKernel(gps.kernels.Kernel):
    def __init__(self, length_scale=1.0, length_scale_bounds=(1e-5, 1e5),
                 rho=1.0, rho_scale_bounds=(1e-5, 1e5),
                 sigma=1.0, sigma_scale_bounds=(1e-5, 1e5),
                 nu=1.0, nu_scale_bounds=(1e-5, 1e5)):

        self.length_scale = length_scale
        self.length_scale_bounds = length_scale_bounds
        self.rho = rho
        self.rho_scale_bounds = rho_scale_bounds
        self.sigma = sigma
        self.sigma_scale_bounds = sigma_scale_bounds
        self.nu = nu
        self.nu_scale_bounds = nu_scale_bounds

    @property
    def hyperparameter_length_scale(self):
        # return Hyperparameter(name, type, bounds, num.dimensions).
        # The Hyperparameter class also has a property .fixed.
        return gps.kernels.Hyperparameter("length_scale", 'numeric', self.length_scale_bounds, 1)

    @property
    def hyperparameter_rho(self):
        return gps.kernels.Hyperparameter("rho", 'numeric', self.rho_scale_bounds, 1)

    @property
    def hyperparameter_sigma(self):
        return gps.kernels.Hyperparameter("sigma", 'numeric', self.sigma_scale_bounds, 1)

    @property
    def hyperparameter_nu(self):
        return gps.kernels.Hyperparameter("nu", 'numeric', self.nu_scale_bounds, 1)

    def is_stationary(self):
        return True

    def diag(self, X):
        return np.diagonal(self(X, X))

    def __call__(self, X, Y=None, eval_gradient=False):
        X = np.atleast_2d(X)
        Y = np.atleast_2d(Y) if Y is not None else X

        t1 = X[:, 0:1]
        k1 = X[:, 1:2]
        t2 = Y[:, 0:1]
        k2 = Y[:, 1:2]

        if X.ndim != 2 or Y.ndim != 2 or X.shape[1] != 2 or Y.shape[1] != 2:
            raise ValueError("Features must be have shape[1] == 2")
        length_scale = np.squeeze(self.length_scale).astype(float)
        rho = np.squeeze(self.rho).astype(float)
        sigma = np.squeeze(self.sigma).astype(float)
        nu = np.squeeze(self.nu).astype(float)
        if length_scale.ndim != 0:
            raise ValueError("Length scale must be a number")
        if rho.ndim != 0:
            raise ValueError("Rho must be a number")
        if sigma.ndim != 0:
            raise ValueError("Sigma must be a number")
        if nu.ndim != 0:
            raise ValueError("Nu must be a number")

        # Matrix_ij of 1 if k1_i = k2_j else 0
        n1 = len(k1)
        n2 = len(k2)

        delta_k1k2 = (k1[:, None] == k2).all(axis=-1)
        delta_t1t2 = (t1[:, None] == t2).all(axis=-1)

        K = nu**2 * np.exp(-0.5 * cdist(t1, t2, metric='sqeuclidean') / length_scale**2) \
            + rho**2 * delta_k1k2 + sigma**2 * delta_k1k2 * delta_t1t2

        if not eval_gradient:
            return K

        # The gradient is with respect to log-transformed parameters.
        # I.e. if length_scale=exp(x), then eval_gradient should return d/dx.
        # If there are multiple hyperparameters, return an array with one
        # column for each non-fixed hyperparameter, sorted alphabetically
        # by hyperparameter name.
        if self.hyperparameter_length_scale.fixed:
            ddlength = np.empty((len(X), len(Y), 0))
        else:
            ddlength = 0.1*nu**2 * np.exp(-0.5 * cdist(t1, t2, metric='sqeuclidean') / length_scale**2) \
                * cdist(t1, t2, metric='sqeuclidean') / length_scale**3
        if self.hyperparameter_rho.fixed:
            ddrho = np.empty((len(X), len(Y), 0))
        else:
            ddrho = 2*rho*delta_k1k2

        if self.hyperparameter_nu.fixed:
            ddnu = np.empty((len(X), len(Y), 0))
        else:
            ddnu = np.exp(-0.5 * cdist(t1, t2,
                                       metric='sqeuclidean') / length_scale**2) * 2 * nu

        if self.hyperparameter_sigma.fixed:
            ddsigma = np.empty((len(X), len(Y), 0))
        else:
            ddsigma = 2*sigma*delta_k1k2*delta_t1t2

        return K, np.concatenate((ddlength[:, :, np.newaxis],
                                  ddnu[:, :, np.newaxis],
                                  ddrho[:, :, np.newaxis],
                                  ddsigma[:, :, np.newaxis]), axis=2)

    def __repr__(self):
        return f"NEWRBF(length_scale={self.length_scale:.3g},rho={self.rho:.3g},sigma={self.sigma:.3g},nu={self.nu:.3g})"


