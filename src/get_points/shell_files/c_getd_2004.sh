#!/bin/bash

final_loc=/gws/nopw/j04/primavera1/open/sithom-bas/day_versions/
prefix=moose:/crum/mi-ba130/ond.nc.file/
name=nemo_ba130o_1d_20040101-20040201_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1d_20040201-20040301_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1d_20040301-20040401_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1d_20040401-20040501_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
name=nemo_ba130o_1d_20040501-20040601_grid-
moo get $prefix${name}T.nc $final_loc${name}T.nc
moo get $prefix${name}U.nc $final_loc${name}U.nc
moo get $prefix${name}V.nc $final_loc${name}V.nc
exit
