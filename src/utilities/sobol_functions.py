import sobol_seq as sob
import numpy as np
import src.utilities.my_wrappers as mwr


def sobol_generator(seed=12345, number_of_samples=1000):
    """

    :param seed:     # is is basically index in the sobol sequence
    :param number_of_samples: how many
    :return:
    """
    ndim = 1
    srn_list = []
    for i in range(0, number_of_samples):
        [srn, seed] = sob.i4_sobol(ndim, seed)
        srn_list.append(srn.tolist()[0])
    return srn_list


def sobol_index_generator(seed=12345, number_of_samples=1000,
                          initial_list_size=32140):
    """

    :param seed: is is basically index in the sobol sequence
    :param number_of_samples: dimension of the problem
    :param initial_list_size: how many are you initially told to take
    :return:
    """
    unscaled_float_list = sobol_generator(seed=seed,
                                          number_of_samples=number_of_samples)
    scaled_int_list = [int(x*initial_list_size) for x in unscaled_float_list]
    return scaled_int_list


def list_subsampler(list, chosen_index_list):
    """

    :param list:
    :param chosen_index_list:
    :return:
    """
    new_list = []
    for j in range(len(list)):
        if j in chosen_index_list:
            new_list.append(list[j])

    return new_list


def npa_subsampler(npa, chosen_index_list):
    """

    :param npa:
    :param chosen_index_list:
    :return:
    """
    return np.array(list_subsampler(npa.tolist(), chosen_index_list))


@mwr.timeit
def auto_npa_subsampler(npa, number_of_samples):
    """
    :param npa: a 1d numpy array
    :param number_of_samples: an integer number of samples that you would like back
    """
    chosen_index_list = sobol_index_generator(seed=12345, number_of_samples=number_of_samples,
                                              initial_list_size=np.shape(npa)[0])
    return npa_subsampler(npa, chosen_index_list)


@mwr.timeit
def npa_subsampler_gpr_format(weird_npa, chosen_index_list):
    """

    :param weird_npa: gaussian process regression requires the numpy array to be in a weird format
    :param chosen_index_list: the list for subsampling previously used.
    :return:
    """
    subsample_npa = npa_subsampler(weird_npa[:, 0], chosen_index_list)
    rng = np.random.RandomState(0)  # passing seed in?
    x_temp = np.array(subsample_npa)
    x_npa_5pu = rng.uniform(0, 5, len(subsample_npa))[:, np.newaxis]
    x_npa_5pu[:, 0] = x_temp[:]
    x_npa_5pu.reshape(-1, 1)
    print('transformed shapes', np.shape(x_npa_5pu))
    return x_npa_5pu
