import glob
import pandas as pd
import matplotlib.pyplot as plt
import utils
import sklearn.gaussian_process as gp   # Gaussian process modeling



data = pd.DataFrame()
## Load and concatenate all files
for file in utils.getProgressBar("Loading historic data")(glob.glob('data/*.pkl')):
    thisData = pd.read_pickle(file)
    data = pd.concat((thisData, data))

print("Formatting Data...")
data['dateTime'] = pd.to_datetime(
    data['dateTime'], format=r'%Y-%m-%dT%H:%M:%SZ')
data.to_pickle('alldata.pkl')

