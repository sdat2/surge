"""
https://stackoverflow.com/questions/37099026/matplotlib-add-custom-tick-label-and-remove-custom-tick-on-y-axis
https://stackoverflow.com/questions/22245949/adding-a-custom-tick-and-label


import matplotlib.pyplot as plt
import numpy as np

fig,ax=plt.subplots()
x = np.linspace(0,10,1000)
ax.plot(x,np.exp(-(x-np.pi)**2))
plt.draw() # this is required, or the ticklabels may not exist (yet) at the next step
# ax.set_xticklabels()
labels = [w.get_text() for w in ax.get_xticklabels()]
locs = list(ax.get_xticks())
labels += [r'$\pi$']
locs += [np.pi]
ax.set_xticklabels(labels)
ax.set_xticks(locs)
ax.grid()
plt.draw()
plt.show()

"""
import numpy as np
import matplotlib.pyplot as plt
import src.utilities.my_wrappers as mwr
import src.utilities.my_io_functions as mif
import src.plotting.places as plc
import config as cfg


@mwr.timeit
def return_place_tick_list(coastline='america'):
    """
    TODO make this more general in some way.

    :return: a list formatted like:
    EP 756
    BO 692
    NY 604
    DP 511
    CH 460
    MM 357
    PE 217
    NO 172
    GA 80
    RP 53
    =======
    DG 903
    TJ 735
    YT 656
    SH 477
    QZ 290
    HK 206
    HP 62
    HU 1
    """
    if coastline == 'america':
        input_file = 'data/lon_lat_point.csv'
        place_d = plc.place_d

    if coastline == 'vin-china':
        place_d = plc.c_place_d
        input_file = cfg.data_loc + 'vc_lon_lat.csv'


    lon_lat_list = mif.read_csv_file(input_file)
    lon_lat_npa = np.asarray(lon_lat_list)
    # print(lon_lat_npa)
    new_tick_list = []
    # discontinuity_ticks = []


    for key in place_d:
        loc_pair = place_d[key]["loc_pair"]
        next_index = np.abs(np.square(lon_lat_npa[:, 1] - loc_pair[1])
                            + np.square(lon_lat_npa[:, 0] - loc_pair[0])).argmin()
        print(place_d[key]["acron"], next_index)
        new_tick_list.append([place_d[key]["acron"], next_index])

    for i in range(np.shape(lon_lat_npa)[0]-1):
        distance = np.sqrt(np.square(lon_lat_npa[i, 0] - lon_lat_npa[i+1, 0])
                           + np.square(lon_lat_npa[i, 1] - lon_lat_npa[i+1, 1]))
        if distance > 0.3:
            # This was arbitrarily chosen, but seemed to pick up the
            # most important discontinuities [created by me skipping bays etc.].
            # print(i+0.5, distance)
            new_tick_list.append(['X', i+0.5])

    return new_tick_list

def _get_ticks(coastline='america'):
    tick_list = return_place_tick_list(coastline=coastline)
    labels = []
    locs = []
    for i in range(len(tick_list)):
        labels.append(tick_list[i][0])
        # print(tick_list[i][0])
        locs.append(tick_list[i][1])
        # print(tick_list[i][1])
    return locs, labels


def add_ticks(yticks=False, coastline='america', fontsize=5):
    locs, labels = _get_ticks(coastline=coastline)
    plt.xticks(np.asarray(locs),
               np.asarray(labels),
               fontsize=fontsize)
    if yticks:
        plt.yticks(np.asarray(locs),
                   np.asarray(labels),
                   fontsize=fontsize)

def add_ticks_to_ax(ax, yticks=False, coastline='america', fontsize=5):
    locs, labels = _get_ticks(coastline=coastline)
    ax.set_xticks(np.asarray(locs))
    ax.set_xticklabels(np.asarray(labels),
                       fontsize=fontsize)
    if yticks:
        ax.set_yticks(np.asarray(locs))
        ax.set_yticklabels(np.asarray(labels),
                           fontsize=fontsize)
