"""
config.py was becoming too crowded :(

I needed somewhere to put the important places to plot that could be used
by multiple scripts
"""

place_d = {"Eastport, ME": {"loc_pair": [-66.98, 44.90], "acron": "EP"},
           "Boston, MA": {"loc_pair": [-71.056, 42.360], "acron": "BO"},
           # "Woods Hole, MA": {"loc_pair": [-70.671, 41.524], "acron": "WH"}, This seems to niche oceany
           "New York, NY": {"loc_pair": [-74.0060, 40.7128], "acron": "NY"},  # 40.7128° N, 74.0060° W
           # "Duck Peir, NC": {"loc_pair": [-75.74, 36.18], "acron": "DP"},
           "Charleston, SC": {"loc_pair": [-79.93, 32.78], "acron": "CH"},
           "Miami, FL": {"loc_pair": [-80.1918, 25.7617], "acron": "MM"},  # 25.7617° N, 80.1918° W
           "Pensacola, FL": {"loc_pair": [-87.21, 30.40], "acron": "PE"},
           "New Orleans, LA": {"loc_pair": [-90.0715, 29.9511], "acron": "NO"},  # 29.9511° N, 90.0715° W
           "Galveston, TX": {"loc_pair": [-94.79, 29.29], "acron": "GA"},
           "Rockport, TX": {"loc_pair": [-97.05, 28.02], "acron": "RP"},
           }

c_place_d = {"Donggang": {"loc_pair": [124.1527,  39.8630], "acron": "DG"}, # 39.8630° N, 124.1527° E
             "Tianjin": {"loc_pair": [117.3616,  39.3434], "acron": "TJ"},
             "Yantai": {"loc_pair": [121.4479,  37.4645], "acron": "YT"}, # 37.4645° N, 121.4479° E
             "Shanghai": {"loc_pair": [121.4737, 31.2304], "acron": "SH"},
             "Quanzhou": {"loc_pair": [118.6757, 24.8741], "acron": "QZ"},
             "Hong Kong": {"loc_pair": [114.1694, 22.3193], "acron": "HK"},  # 40.7128° N, 74.0060° W
             "Hai Phong": {"loc_pair": [106.6881, 20.8449], "acron": "HP"}, # 20.8449° N, 106.6881° E
             "Hue": {"loc_pair": [107.59546, 16.4619], "acron": "HU"} # 20.8449° N, 106.6881° E
             }

j_places_d = {"Sendai JP": {"loc_pair": [140.869, 38.2682], "acron": "SE"},
              "Tokyo, JP": {"loc_pair": [139.6503, 35.6762], "acron": "TO"},
              "Osaka, JP": {"loc_pair": [135.5023, 34.6937], "acron": "OS"},}

# 400 to 880 in x
# 2150 to 2800 in y
