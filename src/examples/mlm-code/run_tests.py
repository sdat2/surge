from sklearn.gaussian_process.kernels import RBF, ExpSineSquared, WhiteKernel, RationalQuadratic, Matern
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import shared
from copy import deepcopy
from fitting import *
from DoubleRBFKernel import DoubleRBFKernel
from DoubleShiftedRBFKernel import DoubleShiftedRBFKernel
from DoubleShiftedSineKernel import DoubleShiftedSineKernel
from DoubleShiftedRBFSineKernel import DoubleShiftedRBFSineKernel


print("Loading data... ", end='')
data = pd.read_pickle('data/data.pkl')
data.dropna(inplace=True)
data['days'] = data['seconds'] * 1 / (24*60*60)  # Convert to days
print('Done.')


print("Creating Kernels... ", end='')
# RBF
ν = 0.2
σ = 0.00002
λ = 0.3
rbf = ν**2 * RBF(length_scale=λ, length_scale_bounds=(1e-5, 1e5)) + \
    WhiteKernel(noise_level=σ, noise_level_bounds=(1e-8, 1e5))

# RationalQuadratic
ν = 0.2
σ = 0.00002
λ = 0.3
α = 1.0
rq = ν**2 * RationalQuadratic(length_scale=λ, alpha=α, length_scale_bounds=(1e-5, 1e5)) \
     + WhiteKernel(noise_level=σ, noise_level_bounds=(1e-8, 1e5))

# Matern
ν = 0.2
σ = 0.00002
λ = 0.3
ν = 2.0
mat = ν**2 * Matern(length_scale=λ, nu=ν, length_scale_bounds=(
    1e-5, 1e5)) + WhiteKernel(noise_level=σ, noise_level_bounds=(1e-8, 1e5))

# SineSquared * RBF
ν  = 0.2
σ  = 0.00002
λ  = 1.0
λ2 = 1.0
p  = 1.0
ss = ν**2 * ExpSineSquared(length_scale=λ, periodicity=p) \
    * RBF(length_scale=λ2) + \
    WhiteKernel(noise_level=σ, noise_level_bounds=(1e-5, 1e5))

# Double RBF
doubleRBF = DoubleRBFKernel(length_scale=0.2, sigma=0.1, nu=0.005, rho=0.005)

# Double Shifted RBF
aperiodic = DoubleShiftedRBFKernel(
    length_scale=0.05, sigma=0.1, 
    nu=0.1, rho=0.01, shift=1e-4, 
    shift_scale_bounds=(1e-7, 1)) #+ doubleRBF

# Double Shifted Sine
doubleSine=DoubleShiftedSineKernel(
                                 length_scale=0.5, sigma=0.01, nu=0.1, rho=0.01,
                                 shift=5, period=3, period_scale_bounds=(1, 20), shift_scale_bounds=(0.001, 8))

# Double Shifted RBF Sine
periodic = DoubleShiftedRBFSineKernel(sine_length_scale=0.1, sigma=0.1, 
                                      nu=0.1, rho=0.01,
                                      period=1, period_scale_bounds=(0.1, 3), shift_scale_bounds=(1e-5, 8),
                                      shift=1e-4,
                                      rbf_length_scale=5, rbf_length_scale_bounds=(0.1, 1000) + doubleRBF
                                      )

print('Done.')


cutOff = 738
cam = ['E60101', 'E60502']
cotsmall = ['1082TH', '1090_w2TH']
cotlong = [('0530TH', 0), ('1099TH', 0)]

starts = [
    # '0155TH'      #  ?
    # '0144TH'      #  ?
    # '0130TH'      #  ? *
    '0278TH',        #  ?                                               63720m Old George Pub!
    ('0290TH', 0),    #  ? similar one goes neg                          60230 (5558mchange)
    ('0190TH', 0),    #  v similar                                       56380m
    '0569TH',        #                                                  43970m
    ('0701TH', 1),    # similar, < 1 is snoother                         36710m
    ('0898TH', 0),    # >1, there is a gap                               34350m
    ('0903TH', 0),    # >1                                               21930m
    ('1099TH', 0),    # >1                                               6360m
    ('1100TH', 0),    # >5                                               879m
    ('1102TH', 0),    # slightly diff, one >1 one not. Unsure which best 0m
]

stops = [
    ('1099TH', 0), 
    ('1102TH', 0),       # > 1.5
    ('0903TH', 0)

]

# for start in starts:
#     for stop in stops:
#         fitToStation(deepcopy(data), [start, stop], aperiodic, plot=True,
#                     maxpoints=00, name=f'cut738/BetterRBF/{start} to {stop}.pdf', dayCutOff=cutOff
#         )


# # Double Sinusoid


# # # Test the single kernels
# # for station_id in ['E60101', 'E60502']:
# #     i = 0
# #     for kernel in (rbf, ss, mat, rq):
# #         for cutoff in (700, 720, 0):
# #             fitToStation(deepcopy(data), station_id, kernel, dayCutOff=cutoff,
# #                          maxpoints=1000, name=f'{station_id}-cut{cutoff}-kern-{i}.pdf')
# #             runBagging(250, 10, deepcopy(data), station_id, kernel,
# #                        dayCutOff=cutoff, name=f'{station_id}-cut{cutoff}-kern-{i}-bagging.pdf')
# #         i += 1


# # Test the paired kernels
# for cutoff in (700, 720, 0):
#     # runBagging(250, 10, deepcopy(data), ['E60101', 'E60502'], doubleRBF,
#             #    dayCutOff=cutoff, name=f'bagging/JL-BB-Bagging-cut{cutoff}.pdf')
fitToStation(deepcopy(data), ['E60101', 'E60502'], periodic, plot=True,
             maxpoints=500, name=f'JesusLockDouble-cut{733}-SHIFT.pdf', dayCutOff=733)
