import numpy as np

# --1) calculate angle theta between grids:

del_lat = np.roll(lat, -1, 1) - np.roll(lat, 1, 1)
theta_arr = np.arctan2(del_lat, del_lon)

# [this is crude but assuming distortion is small,
#  this should be a good approximation of the angle between grids]

# ---2) rotate velocity components into desired coordinate system,
#       by converting essentially into polar coordinates (h, alpha) and then the rotation is simple.


def rotate_uv_v2(u, v, theta):
    """
    Convert u,v (velocity components source coordinate system) to xvel,yvel
    (velocity components target coordinate system)
    ---theta gives angle between grids *?sign convention etc?*
    ---Inputs; u,v arrays shape [nt,gridsize]
    --- and theta (radians) array shape [gridsize]
    --- calculate alpha (a) NEW CODE
    ----NB; whether angles [-pi,pi] or [0,2pi] doesn't matter;
        give same result, and theta and alpha can differ; same results in np.sin, np.cos
    ----BUT must both be defined same direction and from same axis.
    """
    a = np.arctan2(v, u)
    h = np.sqrt(u**2 + v**2)
    yvel, xvel = h * np.sin(a + theta), h * np.cos(a + theta)
    return xvel, yvel


# ---3) regrid your new xvel, yvel using your favoured interpolation routine.
