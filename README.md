# surge

*A Storm Surge Project for Part III Physics*

- Physical Oceanography conducted whilst a visiting scientist at
  the British Antarctic Survey, although its subject is neither
  British nor Antarctic nor a Survey.

This project is produced by the candidate 8205R, and where
not otherwise stated the code is all my own original work.

*Environment Setup*

It requires the packages specified in the initial.yml file,
that should be installed using anaconda3 with the command
on bash terminal:

>> conda env create --file initial.yml

>> conda activate jaspy

This specification exactly matches the virtual environment
available on the JASMIN HPC called jaspy3.7-m3-4.5.11-r20181219,
so that local and remote development were identical. You should
now be in this repository.

There were however, some problems with this setup, and so
a user will still need to update xarray, install cmocean etc.
This repository also requires that a LaTeX distribution installed
to plot, but this can be disabled by changing the rc.params in the
src.plotting.my_plotting_style.py file. [Change TeX option to False]

>> conda update xarray

>> conda install -c conda-forge cmocean

>> conda install -c conda-forge uncertainties

>> conda install -c conda-forge eccodes

>> conda install -c conda-forge pynio

I was very impressed by the *numba* package for Python optimisation.
It speeds up basic python with numpy considerably.
[https://numba.pydata.org/numba-doc/dev/user/5minguide.html]

>> conda install numba

If you are using an intel CPU machine, then the numba processes
will go slightly faster with the intel SVML installed. (maybe 10% or more
based on very limited testing).

>> conda install -c numba icc_rt

To get the *skextreme* examples to work you will need to locally install
the repository in your environment after cloning it from github.

>> cd ..

>>  pip install git+https://github.com/OpenHydrology/lmoments3.git

>> git clone https://github.com/sdat2/scikit-extremes.git

>> cd scikit-extremes

>> pip install -e .

>> cd ..

>> cd surge

*Data Included*

I include some 'toy' data files in the folder /toy. These
are useful small files, used for development and testing,
as they allow any bugs to be teased out quickly. I chose
the month of August 2005 (i.e including Katrina) because
they were the most interesting results to view.

I also include the key data sets used, as netcdfs containing
the points across the coast and their variables.

The Control-1950 Experiment:
https://view.es-doc.org/?renderMethod=name&project=cmip6&type=cim.2.designing.NumericalExperiment&client=esdoc-url-rewrite&name=control-1950

*Running Project*

To cope with the sprawling number of files needed to do data analysis,
and try out new ideas, I put the source code in a number of folders, and
run everything through the run.py file. This ensures that references
are always with respect to the base of the project folder.

>> python3 run.py


*Structure of the Repository*

The source code python files are contained within src/, and the majority
of the files have been further subdivided by their purpose (e.g. 'get_points'
for those scripts that were used to create the netcdf point data files from
the original 2d fields).

pybsub.sh is a script that will allow batch processing on JASMIN, and
trial-llc.ipynb will only work on the oceans.pangeo server (so far as I know).

*Acknowledgements*

I collaborated with Dr.s Dan Jones, Laure Zanna, Pierre Mathiot, and Rory Bingham.

The ORCA12 Data comes from the Met Office, and was made by Pierre Mathiot for this
project based on some reanalysis products (ERA4 and CORE2). The control-1950
experiment was made available by PRIMAVERA.


*Epilogue*

" the true logic for this world is the calculus of Probabilities,
which takes account of the magnitude of the probability which is,
or ought to be, in a reasonable man’s mind."
  — James Clerk Maxwell [1850] [1]


[1] Williams, C. K. & Rasmussen, C. E. Gaussian processes for
    machine learning 3 (MIT press Cambridge, MA, 2006).

*Appendix*

The way that I accessed Jasmin was by calling some combination of:

>> ssh-add -K ~/.ssh/id_rsa_jasmin

>> ssh -A -X sithom@jasmin-login1.ceda.ac.uk

>> ssh -A -X sithom@cems-sci2.cems.rl.ac.uk

>> ssh -A -X sithom@jasmin-sci3.ceda.ac.uk

>> ssh -A -X sithom@mass-cli1.ceda.ac.ukg

*Running the command*

>> cloc $(git ls-files) > environment/cloc.txt


Suggests that the repository is v. large and under-commented.

As I was writing up my environment self-annihilated:

ImportError:

IMPORTANT: PLEASE READ THIS FOR ADVICE ON HOW TO SOLVE THIS ISSUE!

Importing the numpy c-extensions failed.
- Try uninstalling and reinstalling numpy.
- If you have already done that, then:
  1. Check that you expected to use Python3.7 from "/Users/simon/anaconda3/envs/jaspy1/bin/python3",
     and that you have no directories in your PATH or PYTHONPATH that can
     interfere with the Python and numpy version "1.18.1" you're trying to use.
  2. If (1) looks fine, you can open a new issue at
     https://github.com/numpy/numpy/issues.  Please include details on:
     - how you installed Python
     - how you installed numpy
     - your operating system
     - whether or not you have multiple versions of Python installed
     - if you built from source, your compiler versions and ideally a build log

- If you're working with a numpy git repository, try `git clean -xdf`
  (removes all files not under version control) and rebuild numpy.

Note: this error has many possible causes, so please don't comment on
an existing issue about this - open a new one instead.

Original error was: dlopen(/Users/simon/anaconda3/envs/jaspy1/lib/python3.7/site-packages/numpy/core/_multiarray_umath.cpython-37m-darwin.so, 2): Library not loaded: @rpath/libopenblas.dylib
  Referenced from: /Users/simon/anaconda3/envs/jaspy1/lib/python3.7/site-packages/numpy/core/_multiarray_umath.cpython-37m-darwin.so
  Reason: image not found
