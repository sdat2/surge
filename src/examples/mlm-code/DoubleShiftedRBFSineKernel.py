import sklearn.gaussian_process as gps
from scipy.spatial.distance import cdist
import numpy as np
from copy import deepcopy


class DoubleShiftedRBFSineKernel(gps.kernels.Kernel):
    def __init__(
                self, 
                sine_length_scale=1.0, sine_length_scale_bounds=(1e-5, 1e5),
                rbf_length_scale=1.0, rbf_length_scale_bounds=(1e-5, 1e5),
                rho=1.0, rho_scale_bounds=(1e-5, 1e5),
                sigma=1.0, sigma_scale_bounds=(1e-5, 1e5),
                shift=1e-5, shift_scale_bounds=(1e-8, 1e5),
                nu=1.0, nu_scale_bounds=(1e-5, 1e5),
                period=1.0, period_scale_bounds=(1e-5, 1e5)
               ):


        self.sine_length_scale = sine_length_scale
        self.sine_length_scale_bounds = sine_length_scale_bounds
        self.rbf_length_scale = rbf_length_scale
        self.rbf_length_scale_bounds = rbf_length_scale_bounds
        self.rho = rho
        self.rho_scale_bounds = rho_scale_bounds
        self.sigma = sigma
        self.sigma_scale_bounds = sigma_scale_bounds
        self.nu = nu
        self.nu_scale_bounds = nu_scale_bounds
        self.shift = shift
        self.shift_scale_bounds = shift_scale_bounds
        self.period = period
        self.period_scale_bounds = period_scale_bounds

    @property
    def hyperparameter_sine_length_scale(self):
        # return Hyperparameter(name, type, bounds, num.dimensions).
        # The Hyperparameter class also has a property .fixed.
        return gps.kernels.Hyperparameter("sine_length_scale", 'numeric', self.sine_length_scale_bounds, 1)

    @property
    def hyperparameter_rbf_length_scale(self):
        return gps.kernels.Hyperparameter("rbf_length_scale", 'numeric', self.rbf_length_scale_bounds, 1)

    @property
    def hyperparameter_rho(self):
        return gps.kernels.Hyperparameter("rho", 'numeric', self.rho_scale_bounds, 1)

    @property
    def hyperparameter_sigma(self):
        return gps.kernels.Hyperparameter("sigma", 'numeric', self.sigma_scale_bounds, 1)

    @property
    def hyperparameter_nu(self):
        return gps.kernels.Hyperparameter("nu", 'numeric', self.nu_scale_bounds, 1)
    
    @property
    def hyperparameter_shift(self):
        return gps.kernels.Hyperparameter("shift", 'numeric', self.shift_scale_bounds, 1)

    @property
    def hyperparameter_period(self):
        return gps.kernels.Hyperparameter("period", 'numeric', self.period_scale_bounds, 1)

    def is_stationary(self):
        return True

    def diag(self, X):
        return np.diagonal(self(X, X))

    def _K(self, t1, t2, k1, k2, dr=0, ds=0, dn=0, dls=0, dp=0, dlr=0):

        rho = np.squeeze(self.rho).astype(float) + dr
        sigma = np.squeeze(self.sigma).astype(float) + ds
        nu = np.squeeze(self.nu).astype(float) + dn
        sine_length_scale = np.squeeze(self.sine_length_scale).astype(float) + dls
        rbf_length_scale = np.squeeze(self.rbf_length_scale).astype(float) + dlr
        period = np.squeeze(self.period) + dp

        delta_k1k2 = (k1[:, None] == k2).all(axis=-1)
        delta_t1t2 = (np.abs(t1[:, None] - t2) == 0).all(axis=-1)

        d = cdist(t1, t2, metric='euclidean')
        K = nu**2 * np.exp(-2 * np.sin( np.pi / period * d )**2 / sine_length_scale**2) \
            * np.exp(-d**2/2/rbf_length_scale**2) \
            + rho**2 * delta_k1k2 + sigma**2 * delta_k1k2 * delta_t1t2

        return K

    def __call__(self, X, Y=None, eval_gradient=False):
        X = np.atleast_2d(X)
        Y = np.atleast_2d(Y) if Y is not None else X

        t1 = X[:, 0:1]
        k1 = X[:, 1:2]
        t2 = Y[:, 0:1]
        k2 = Y[:, 1:2]

        if X.ndim != 2 or Y.ndim != 2 or X.shape[1] != 2 or Y.shape[1] != 2:
            raise ValueError("Features must be have shape[1] == 2")
        
        sine_length_scale = np.squeeze(self.sine_length_scale).astype(float)
        rbf_length_scale = np.squeeze(self.rbf_length_scale).astype(float)
        rho = np.squeeze(self.rho).astype(float)
        sigma = np.squeeze(self.sigma).astype(float)
        nu = np.squeeze(self.nu).astype(float)
        shift = np.squeeze(self.shift).astype(float)
        
        if sine_length_scale.ndim != 0:
            raise ValueError("Sine length scale must be a number")
        if rbf_length_scale.ndim != 0:
            raise ValueError("RBF length scale must be a number")
        if rho.ndim != 0:
            raise ValueError("Rho must be a number")
        if sigma.ndim != 0:
            raise ValueError("Sigma must be a number")
        if nu.ndim != 0:
            raise ValueError("Nu must be a number")
        if shift.ndim != 0:
            raise ValueError("Scale must be a number")

        # Matrix_ij of 1 if k1_i = k2_j else 0
        n1 = len(k1)
        n2 = len(k2)

        # Adjust for time delay where k==2
        t1 = deepcopy(t1)
        t2 = deepcopy(t2)

        t1[np.where(k1 == 2)] -= shift
        t2[np.where(k2 == 2)] -= shift


        delta_k1k2 = (k1[:, None] == k2).all(axis=-1)
        delta_t1t2 = ( np.abs(t1[:, None] - t2) == 0 ).all(axis=-1)

        K = self._K(t1, t2, k1, k2)

        if not eval_gradient:
            return K

        # The gradient is with respect to log-transformed parameters.
        # I.e. if length_scale=exp(x), then eval_gradient should return d/dx.
        # If there are multiple hyperparameters, return an array with one
        # column for each non-fixed hyperparameter, sorted alphabetically
        # by hyperparameter name.
        if self.hyperparameter_sine_length_scale.fixed:
            ddsinelength = np.empty((len(X), len(Y), 0))
        else:
            ddsinelength = ( self._K(t1,t2,k1,k2,dls=1e-8) - self._K(t1,t2,k1,k2) ) / 1e-8
            ddsinelength = ddsinelength[:,:,None]

        if self.hyperparameter_rbf_length_scale.fixed:
            ddrbflength = np.empty((len(X), len(Y), 0))
        else:
            ddrbflength = (self._K(t1, t2, k1, k2, dlr=1e-8) -
                            self._K(t1, t2, k1, k2)) / 1e-8
            ddrbflength = ddrbflength[:, :, None]
        
        if self.hyperparameter_rho.fixed:
            ddrho = np.empty((len(X), len(Y), 0))
        else:
            ddrho = 2*rho*delta_k1k2
            ddrho = ddrho[:,:,None]

        if self.hyperparameter_nu.fixed:
            ddnu = np.empty((len(X), len(Y), 0))
        else:
            ddnu = ( self._K(t1,t2,k1,k2,dn=1e-8) - self._K(t1,t2,k1,k2) ) / 1e-8
            ddnu = ddnu[:,:,None]

        if self.hyperparameter_sigma.fixed:
            ddsigma = np.empty((len(X), len(Y), 0))
        else:
            ddsigma = 2*sigma*delta_k1k2*delta_t1t2
            ddsigma = ddsigma[:,:,None]

        if self.hyperparameter_period.fixed:
            ddperiod = np.empty((len(X), len(Y), 0))
        else:
            ddperiod = ( self._K(t1,t2,k1,k2,dp=1e-8) - self._K(t1,t2,k1,k2) ) / 1e-8
            ddperiod = ddperiod[:, :, None]

        if self.hyperparameter_shift.fixed:
            ddshift = np.empty((len(X), len(Y), 0))
        else:
            # Let's approximate
            deltashift = 1e-8
            t1_plusdt1 = deepcopy(t1)
            t2_plusdt2 = deepcopy(t2)
            t1_plusdt1[np.where(k1 == 2)] -= deltashift
            t2_plusdt2[np.where(k2 == 2)] -= deltashift

            # dcdist = cdist(t1_plusdt1, t2_plusdt2, metric='sqeuclidean') - \
            # cdist(t1, t2, metric='sqeuclidean')
            # dcdist /= deltashift

            # ddshift = -nu**2/length_scale**2/2 \
            #  * dcdist \
            #  * np.exp(-0.5 * cdist(t1, t2, metric='sqeuclidean') / length_scale**2)
            
            # ddshift = np.ones((len(X), len(Y), 0))

            ddshift = self._K(t1_plusdt1, t2_plusdt2, k1, k2) - self._K(t1, t2, k1, k2)
            ddshift /= deltashift

            ddshift = ddshift[:,:,None]


        return K, np.concatenate((
                                  ddnu,
                                  ddperiod,
                                  ddrbflength,
                                  ddrho,
                                  ddshift,
                                  ddsigma,
                                  ddsinelength
                                  ), axis=2)

    def __repr__(self):
        return f"SHIFTRBF(sine_length_scale={self.sine_length_scale:.3g},rho={self.rho:.3g},sigma={self.sigma:.3g},nu={self.nu:.3g},shift={self.shift:.8g},period={self.period:.2g},rbf_length_scale={self.rbf_length_scale})"


