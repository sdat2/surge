import numpy as np
import xarray as xr
#import iris
import matplotlib.pyplot as plt
import iris.analysis.cartography as iac
import src.utilities.my_wrappers as mwr
import config as cfg
import src.utilities.my_io_functions as mif
import src.plotting.my_plotting_style as mps
mps.mps_defaults()


@mwr.timeit
def rotate_and_regress():
    training_set = xr.open_dataset(cfg.data_loc + cfg.merged_2004_file_name)
    test_set = xr.open_dataset(cfg.data_loc + cfg.merged_2004_file_name)
    angle_list = mif.read_csv_file('data/running_angle.csv')
    angle_npa = np.asarray(angle_list).ravel()
    # print(angle_npa)
    print(training_set)
    print(test_set)
    y_name = 'zos'

    zos = training_set.variables['zos'].values
    tauuo = training_set.variables['tauuo'].values
    tauvo = training_set.variables['tauvo'].values

    print(np.shape(training_set.variables['zos'].values))  # (757, 8760)
    print(np.shape(training_set.variables['tauuo'].values))  # (757, 8760)
    print(np.shape(training_set.variables['tauvo'].values))  # (757, 8760)

    def plot_cross_plot():
        fig, axs = plt.subplots(3)
        axs[0].plot(tauuo, tauvo, 'x')
        axs[0].set_ylabel(r'$\tau_v$')
        axs[0].set_xlabel(r'$\tau_u$')
        axs[1].plot(tauuo, zos, 'x')
        axs[1].set_ylabel(r'$\eta$')
        axs[1].set_xlabel(r'$\tau_u$')
        axs[2].plot(tauvo, zos, 'x')
        axs[2].set_ylabel(r'$\eta$')
        axs[2].set_xlabel(r'$\tau_v$')
        fig = mps.defined_size(fig, size='scl')
        plt.show()

    cross = np.zeros(np.shape(tauvo))
    along = np.zeros(np.shape(tauvo))

    for i in range(np.shape(zos)[0]):
        # print('angle', angle_npa[i])  # ok, need to check this.
        cross[i, :] = np.cos(angle_npa[i])*tauvo[i, :] + np.sin(angle_npa[i])*tauuo[i, :]
        along[i, :] = -np.cos(angle_npa[i])*tauuo[i, :] + np.sin(angle_npa[i])*tauvo[i, :]

    def plot_cz():
        plt.plot(cross, zos, 'x')
        plt.show()

    print(np.corrcoef(cross.ravel(), zos.ravel()))
    print(np.corrcoef((along.ravel(), zos.ravel())))

    delt_zos = np.zeros([np.shape(zos)[0], np.shape(zos)[1]-1])

    for i in range(np.shape(zos)[1]-1):
        delt_zos[:, i] = zos[:, i+1] - zos[:, i]

    print(np.corrcoef(cross[:, :-1].ravel(), delt_zos.ravel()))
    print(np.corrcoef((along[:, :-1].ravel(), delt_zos.ravel())))

    def plot_cross_plot_new():
        fig, axs = plt.subplots(3)
        axs[0].plot(along, cross, 'x')
        axs[0].set_ylabel(r'$\tau_{\perp}$')
        axs[0].set_xlabel(r'$\tau_{\parallel}$')
        axs[1].plot(cross[:, :-1], delt_zos, 'x')
        axs[1].set_ylabel(r'$\Delta\eta$')
        axs[1].set_xlabel(r'$\tau_{\perp}$')
        axs[2].plot(along[:, :-1], delt_zos, 'x')
        axs[2].set_ylabel(r'$\Delta\eta$')
        axs[2].set_xlabel(r'$\tau_{\parallel}$')
        fig = mps.defined_size(fig, size='scl')
        mps.save_fig_twice(cfg.plots_loc + 'rotation_test')
        plt.tight_layout()
    plot_cross_plot_new()


def rotate_using_iris(u_cubes, v_cubes):
    for i in [0, 1]:
        for j in range(np.shape(u_cubes[i])[0]):
            u_cubes[i][j, :, :], v_cubes[i][j, :, :] = \
               iac.rotate_grid_vectors(u_cubes[i][j, :, :], v_cubes[i][j, :, :])

# iris.analysis.cartography.rotate_grid_vectors()
