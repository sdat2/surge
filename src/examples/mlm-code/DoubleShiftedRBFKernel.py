import sklearn.gaussian_process as gps
from scipy.spatial.distance import cdist
import numpy as np
from copy import deepcopy


class DoubleShiftedRBFKernel(gps.kernels.Kernel):
    def __init__(self, length_scale=1.0, length_scale_bounds=(1e-5, 1e5),
                 rho=1.0, rho_scale_bounds=(1e-5, 1e5),
                 sigma=1.0, sigma_scale_bounds=(1e-5, 1e5),
                 shift=1e-5, shift_scale_bounds=(1e-8, 1e5),
                 nu=1.0, nu_scale_bounds=(1e-5, 1e5)):
                #  scale=1.0, scale_scale_bounds=(1e-3, 1e3)

        self.length_scale = length_scale
        self.length_scale_bounds = length_scale_bounds
        self.rho = rho
        self.rho_scale_bounds = rho_scale_bounds
        self.sigma = sigma
        self.sigma_scale_bounds = sigma_scale_bounds
        self.nu = nu
        self.nu_scale_bounds = nu_scale_bounds
        self.shift = shift
        self.shift_scale_bounds = shift_scale_bounds
        # self.scale = scale
        # self.scale_scale_bounds = scale_scale_bounds

    @property
    def hyperparameter_length_scale(self):
        # return Hyperparameter(name, type, bounds, num.dimensions).
        # The Hyperparameter class also has a property .fixed.
        return gps.kernels.Hyperparameter("length_scale", 'numeric', self.length_scale_bounds, 1)

    @property
    def hyperparameter_rho(self):
        return gps.kernels.Hyperparameter("rho", 'numeric', self.rho_scale_bounds, 1)

    @property
    def hyperparameter_sigma(self):
        return gps.kernels.Hyperparameter("sigma", 'numeric', self.sigma_scale_bounds, 1)

    @property
    def hyperparameter_nu(self):
        return gps.kernels.Hyperparameter("nu", 'numeric', self.nu_scale_bounds, 1)
    
    @property
    def hyperparameter_shift(self):
        return gps.kernels.Hyperparameter("shift", 'numeric', self.shift_scale_bounds, 1)

    # @property
    # def hyperparameter_scale(self):
    #     return gps.kernels.Hyperparameter("scale", 'numeric', self.scale_scale_bounds, 1)

    def is_stationary(self):
        return True

    def diag(self, X):
        return np.diagonal(self(X, X))

    def _K(self, t1, t2, k1, k2):

        rho = np.squeeze(self.rho).astype(float)
        sigma = np.squeeze(self.sigma).astype(float)
        nu = np.squeeze(self.nu).astype(float)
        length_scale = np.squeeze(self.length_scale).astype(float)
        # scale = np.squeeze(self.scale).astype(float)

        delta_k1k2 = (k1[:, None] == k2).all(axis=-1)
        delta_t1t2 = (np.abs(t1[:, None] - t2) == 0).all(axis=-1)

        K = nu**2 * np.exp(-0.5 * cdist(t1, t2, metric='sqeuclidean') / length_scale**2) \
            + rho**2 * delta_k1k2 + sigma**2 * delta_k1k2 * delta_t1t2

        # scaler = np.ones(delta_k1k2.shape)
        # scaler[:, np.where(k1 == 2)] *= scale
        # scaler[np.where(k1 == 2), :] *= scale

        return K #* scaler

    def __call__(self, X, Y=None, eval_gradient=False):
        X = np.atleast_2d(X)
        Y = np.atleast_2d(Y) if Y is not None else X

        t1 = X[:, 0:1]
        k1 = X[:, 1:2]
        t2 = Y[:, 0:1]
        k2 = Y[:, 1:2]

        if X.ndim != 2 or Y.ndim != 2 or X.shape[1] != 2 or Y.shape[1] != 2:
            raise ValueError("Features must be have shape[1] == 2")
        
        length_scale = np.squeeze(self.length_scale).astype(float)
        rho = np.squeeze(self.rho).astype(float)
        sigma = np.squeeze(self.sigma).astype(float)
        nu = np.squeeze(self.nu).astype(float)
        shift = np.squeeze(self.shift).astype(float)
        # scale = np.squeeze(self.scale).astype(float)
        
        
        if length_scale.ndim != 0:
            raise ValueError("Length scale must be a number")
        if rho.ndim != 0:
            raise ValueError("Rho must be a number")
        if sigma.ndim != 0:
            raise ValueError("Sigma must be a number")
        if nu.ndim != 0:
            raise ValueError("Nu must be a number")
        if shift.ndim != 0:
            raise ValueError("Scale must be a number")

        # Matrix_ij of 1 if k1_i = k2_j else 0
        n1 = len(k1)
        n2 = len(k2)

        # Adjust for time delay where k==2
        t1 = deepcopy(t1)
        t2 = deepcopy(t2)

        t1[np.where(k1 == 2)] -= shift
        t2[np.where(k2 == 2)] -= shift


        delta_k1k2 = (k1[:, None] == k2).all(axis=-1)
        delta_t1t2 = (np.abs(t1[:, None] - t2) == 0 ).all(axis=-1)

        K = nu**2 * np.exp(-0.5 * cdist(t1, t2, metric='sqeuclidean') / length_scale**2) \
            + rho**2 * delta_k1k2 + sigma**2 * delta_k1k2 * delta_t1t2

        # scaler = np.ones(delta_k1k2.shape)
        # scaler[:, np.where(k2 == 2)] *= scale
        # scaler[np.where(k1 == 2), :] *= scale
        # K *= scale

        if not eval_gradient:
            return K

        # The gradient is with respect to log-transformed parameters.
        # I.e. if length_scale=exp(x), then eval_gradient should return d/dx.
        # If there are multiple hyperparameters, return an array with one
        # column for each non-fixed hyperparameter, sorted alphabetically
        # by hyperparameter name.
        if self.hyperparameter_length_scale.fixed:
            ddlength = np.empty((len(X), len(Y), 0))
        else:
            ddlength = 0.1*nu**2 * np.exp(-0.5 * cdist(t1, t2, metric='sqeuclidean') / length_scale**2) \
                * cdist(t1, t2, metric='sqeuclidean') / length_scale**3
            ddlength = ddlength[:, :, None]
        
        if self.hyperparameter_rho.fixed:
            ddrho = np.empty((len(X), len(Y), 0))
        else:
            ddrho = 2*rho*delta_k1k2
            ddrho = ddrho[:, :, None]

        if self.hyperparameter_nu.fixed:
            ddnu = np.empty((len(X), len(Y), 0))
        else:
            ddnu = np.exp(-0.5 * cdist(t1, t2,
                                       metric='sqeuclidean') / length_scale**2) * nu
            ddnu = ddnu[:, :, None]

        if self.hyperparameter_sigma.fixed:
            ddsigma = np.empty((len(X), len(Y), 0))
        else:
            ddsigma = 2*sigma*delta_k1k2*delta_t1t2
            ddsigma = ddsigma[:, :, None]

        if self.hyperparameter_shift.fixed:
            ddshift = np.empty((len(X), len(Y), 0))
        else:
            # Let's approximate
            deltashift = 1e-8
            t1_plusdt1 = deepcopy(t1)
            t2_plusdt2 = deepcopy(t2)
            t1_plusdt1[np.where(k1 == 2)] -= deltashift
            t2_plusdt2[np.where(k2 == 2)] -= deltashift

            # dcdist = cdist(t1_plusdt1, t2_plusdt2, metric='sqeuclidean') - \
            # cdist(t1, t2, metric='sqeuclidean')
            # dcdist /= deltashift

            # ddshift = -nu**2/length_scale**2/2 \
            #  * dcdist \
            #  * np.exp(-0.5 * cdist(t1, t2, metric='sqeuclidean') / length_scale**2)
            
            # ddshift = np.ones((len(X), len(Y), 0))

            ddshift = self._K(t1_plusdt1, t2_plusdt2, k1, k2) - self._K(t1, t2, k1, k2)
            ddshift /= deltashift

            ddshift = ddshift[:, :, None]

        # if self.hyperparameter_scale.fixed:
        #     ddscale = np.empty((len(X), len(Y), 0))
        # else:
        #     # Let's approximate
        #     self.scale += 1e-8
        #     K_dK = self._K(t1, t2, k1, k2)
        #     self.scale -= 1e-8

        #     ddscale = (K_dK - K) / 1e-8
        #     ddscale = ddscale[:, :, None]

        return K, np.concatenate((ddlength,
                                  ddnu,
                                  ddrho,
                                #   ddscale,
                                  ddshift,
                                  ddsigma,
                                  ), axis=2)

    def __repr__(self):
        return f"SHIFTRBF(length_scale={self.length_scale:.3g}," \
               f"rho={self.rho:.3g},sigma={self.sigma:.3g}," \
               f"nu={self.nu:.3g},shift={self.shift:.8g})"


