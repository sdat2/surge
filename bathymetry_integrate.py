
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from uncertainties import unumpy, ufloat
import uncertainties.umath as um
import cmocean.cm as cmo
import src.utilities.my_io_functions as mif
import src.plotting.custom_ticker as ctk
import config as cfg
import src.plotting.my_plotting_style as mps
mps.mps_defaults()



def run_isobath_integrate(coastline='america'):
    save_prefix = mif.save_prefix(coastline=coastline)
    saved_d = mif.read_json_object(cfg.data_loc + save_prefix+'isobath_dist.json')
    depths_list = saved_d["depths_list"]
    dist_lol = saved_d["dist_lol"]
    for i in range(len(depths_list)):
        print(depths_list[i])
    dist_npa = np.asarray(dist_lol)[:, 2:]
    depths_npa = np.asarray(depths_list)[2:]
    print(np.shape(dist_npa))
    print(np.shape(depths_npa))

    grav_const = ufloat(9.81, 0.005) #
    grid_cell = ufloat(9.25, 1)*(10**(3)) # pm 1
    density_water = ufloat(1024.5, 4.5)  # 1020 to 1029

    # d eta / d x approx tau / (rho g H)
    # eta/tau = \int 1/(rho g H)

    def single_plot(nominals, sigmas):
        plt.plot(range(np.shape(dist_npa)[0]), nominals )
        plt.fill_between(range(np.shape(dist_npa)[0]), nominals+sigmas, nominals-sigmas, alpha=0.4)
        ctk.add_ticks(coastline=coastline)
        plt.ylabel('Theoretical Responsiveness (m Pa$^{-1}$)')
        plt.xlim([0, np.shape(dist_npa)[0]])
        plt.show()

    def plot_both(nominals_low, sigmas_low, nominals_high, sigmas_high, show=False, save=False):
        plt.plot(range(np.shape(dist_npa)[0]), nominals_low, color='brown', label=r'Lower bound', alpha=0.5)
        plt.fill_between(range(np.shape(dist_npa)[0]), nominals_low+sigmas_low,
                         nominals_low-sigmas_low, alpha=0.2, color='brown', label=r'1$\sigma$ CI')
        plt.plot(range(np.shape(dist_npa)[0]), nominals_high, color='purple', label=r'Upper bound', alpha=0.5)
        plt.fill_between(range(np.shape(dist_npa)[0]), nominals_high+sigmas_high,
                         nominals_high-sigmas_high, alpha=0.2, color='purple', label=r'1$\sigma$ CI')
        ctk.add_ticks(coastline=coastline)
        plt.xlim([0, np.shape(dist_npa)[0]])
        plt.legend()
        fig = plt.gcf()
        mps.defined_size(fig, size='dcsh')
        if save:
            plt.ylabel('Theoretical Responsiveness (m Pa$^{-1}$)')
            mps.save_fig_twice('plots/theory')

        if show:
            plt.show()

    def integrate_section(isobath_no=0, bound='lower'):
        assert bound in ['lower', 'upper']
        if bound == 'lower':
            denominator = (density_water * grav_const * depths_npa[isobath_no] )
        elif bound == 'upper':
            if isobath_no == 0:
                denominator = (density_water
                               * grav_const
                               * 10 )
            else:
                denominator = (density_water
                               * grav_const
                               * depths_npa[isobath_no-1] )

        print(denominator)

        test_list = []

        for i in range(np.shape(dist_npa)[0]):
            if isobath_no == 0:
                test_list.append(dist_npa[i, isobath_no] * grid_cell / denominator)
            else:
                test_list.append((dist_npa[i, isobath_no] - dist_npa[i, isobath_no-1]) * grid_cell / denominator)


        test_npa = np.array(test_list)

        nominals = unumpy.nominal_values(test_npa)
        sigmas = unumpy.std_devs(test_npa)

        return unumpy.uarray(nominals, sigmas)

    def integrate_total(bound='lower'):
        integrate_lunpa = []


        for isobath_no in range(len(depths_npa)):
            integrate_lunpa.append(integrate_section(isobath_no=isobath_no, bound=bound))

        resp = integrate_lunpa[0]

        print(type(resp))
        print(np.shape(resp))

        for i in range(1, len(integrate_lunpa)):
            resp += integrate_lunpa[i]

        # single_plot(unumpy.nominal_values(resp), unumpy.std_devs(resp))
        return unumpy.nominal_values(resp), unumpy.std_devs(resp)

    nominals_low, sigmas_low = integrate_total(bound='lower')
    nominals_high, sigmas_high = integrate_total(bound='upper')

    plot_both(nominals_low, sigmas_low, nominals_high, sigmas_high, save=True, show=True)



    # print('shape', np.shape(dist_npa))
    # corrcoef = np.corrcoef(dist_npsa.T[2:, :])
    # print('corrcoef', corrcoef)
    # print('shape', np.shape(corrcoef))


run_isobath_integrate()
