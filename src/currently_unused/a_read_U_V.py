#!/usr/bin/env python3.7

import numpy as np
import pandas as pd
import xarray as xr
import dask as ds
import config as cfg


u_xr = xr.open_dataset(cfg.toy_U_file_name)
v_xr = xr.open_dataset(cfg.toy_V_file_name)

print(u_xr.__repr__())
print(v_xr.__repr__())


def print_all_coords():
    """
    This function is a bad idea
    :return:
    """
    for i in range(np.shape(u_xr.coords["nav_lat"])[0]):
        for j in range(np.shape(u_xr.coords["nav_lat"])[1]):
            print(i, j, u_xr.coords["nav_lat"][i, j].values, u_xr.coords["nav_lon"][i, j].values)


def take_subset(ds, lat_lim=[10, 40], lon_lim=[-100, -40]):
    """
    Code originally taken from http://xarray.pydata.org/en/stable/indexing.html
    TODO this does not work with the NEMO
    :param ds:
    :param lat_lim:
    :param lon_lim:
    :return:
    """
    lc = ds.coords["nav_lon"]
    la = ds.coords["nav_lat"]
    ds.loc[dict(nav_lon=lc[(lc > lon_lim[0]) & (lc < lon_lim[1])],
                nav_lat=la[(la > lat_lim[0]) & (la < lat_lim[1])])] = 100


take_subset(u_xr)
take_subset(v_xr)


@mwr.timeit
def change_attributes(cube):
    """
    WARNING: NOT CURRENTLY USED, Xarray implementation used in preference
    To make iris cubes mergeable then certain attributes need to be changed.
    This replaces the time index with the time_centered so that successive .nc files
    can be merged together.
    :param cube: An iris cube.
    :return:
    """
    differing_attrs = ['file_name', 'name', 'timeStamp', 'TimeStamp']
    for attribute in differing_attrs:
        cube.attributes[attribute] = ''
    promote_aux_coord_to_dim_coord(cube, 'time')
    return cube


@mwr.timeit
def return_rectilinear_cube(cube, nxny=[4320, 2160]):
    """
    WARNING: NOT CURRENTLY USED AS TOO SLOW
    This should be able to take a netcdf file on a curvilinear grid, and transform that
    so that it is on a 1/12 degree lat-lon grid instead. It is unfortunately very slow,
    and uses a ?linear? interpolation scheme rather than the Akima interpolation scheme used in
    the sosie FORTRAN repository.

    :param cube: the iris cube to be interpolated.
    :return:
    """
    # let's not coarse grain the data yet
    new_cube1, extent = iris.analysis.cartography.project(cube, ccrs.PlateCarree(),
                                                          nx=nxny[0], ny=nxny[1])# nx=418, ny=721)
    print('extent= ', extent)

    xr_obj1 = xr.DataArray.from_iris(new_cube1)
    print(xr_obj1.__repr__())
    # xr_obj1.to_netcdf(path=cube.name() + '-big' + '.nc')

    # plot_cube(new_cube1)

    #return new_cube1


@mwr.timeit
def load_and_save_cube(full_file_path):
    """
    This function would run a couple of IRIS

    :param full_file_path:
    :return:
    """
    cubes = iris.load(full_file_path)
    for cube in cubes:
        cube = change_attributes(cube)
        return_rectilinear_cube(cube)

    # iris.save(cubes, full_file_path)
    # xr_obj = xr.DataArray.from_iris(cubes)
    # xr_obj.to_netcdf(full_file_path+'4')