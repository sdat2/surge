import src.utilities.my_wrappers as mwr
import config as cfg


def get_responsiveness():
    import src.high_freq_an.e_efficiency_plot as eep
    eep.get_xarray_grid([cfg.data_loc + cfg.merged_2004_file_name,
                         cfg.data_loc + cfg.merged_2005_file_name])


def vc_responsiveness():
    import src.high_freq_an.e_efficiency_plot as eep
    eep.get_xarray_grid([cfg.data_loc + cfg.merged_vc_2004_file_name,
                         cfg.data_loc + cfg.merged_vc_2005_file_name],
                        coastline='vin-china')

# vc_responsiveness()
# get_responsiveness()


def analyse_responsiveness():
    import src.high_freq_an.j_efficiency_analyse as jea
    # jea.find_lr_angle(coastline='vin-china')
    jea.find_responsiveness_magnitude(coastline='america')
    # jea.responsiveness_regress(coastline='vin-china')


analyse_responsiveness()


def fourier_regress():
    import src.high_freq_an.k_fourier_regress as kfr
    kfr.get_xarray_grid([cfg.data_loc + cfg.merged_2004_file_name,
                         cfg.data_loc + cfg.merged_2005_file_name])


# fourier_regress()
# import src.examples.skextremes.port_pirie


def complex_regression():
    """I tried to improve things in regression by making the problem slightly
       easier to solve. Now I try just predicting the change in the sea surface height
       given some knowledge of the different parameters. """
    import src.high_freq_an.l_complex_regression as lcr
    lcr.regress_complex([cfg.data_loc + cfg.merged_2004_file_name,
                         cfg.data_loc + cfg.merged_2005_file_name])


# import src.get_points.c_china_coast_plot as ccp
